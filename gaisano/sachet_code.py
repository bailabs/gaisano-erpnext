import frappe
from events import get_rounded_price
from apisync import update_netcost_and_margins
import csv


def change_to_piece(filename):
    #filename = '/home/frappe/frappe-v7/sites/site1.local/private/files/test.csv'
    not_found=[]

    with open(filename, 'r') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        for i,row in enumerate(csv_reader):
            if i>0:
                if find_item_from_barcode(row[0]) is not None:
                    item_doc = frappe.get_doc("Item", find_item_from_barcode(row[0]))
                    cost_with_freight = item_doc.item_cost_with_freight
                    markup = item_doc.retail_margin_rate

                    new_packing = int(row[1])
                    new_uom = row[2]
                    cost_per_piece = float(cost_with_freight/new_packing)
                    srp_piece = cost_per_piece * (1+(markup/100))
                    print cost_per_piece, srp_piece
                    srp_piece = round(get_rounded_price(srp_piece),2)

                    #print("OLD", item_doc.name, item_doc.packing, item_doc.item_price_retail_1, item_doc.item_price_retail_with_margin)
                    print("NEW", item_doc.name, new_packing, cost_per_piece, markup, srp_piece)

                    frappe.db.sql("""UPDATE `tabItem` set packing = %s, pos_uom = %s, item_price_retail_1=%s,
                                  item_price_retail_with_margin = %s, modified = NOW() where name = %s""",(new_packing, new_uom,cost_per_piece, srp_piece, item_doc.name))
                    frappe.db.commit()

                    item_doc = frappe.get_doc("Item", find_item_from_barcode(row[0]))
                    update_netcost_and_margins(item_doc)

                    if check_uom_if_exists(item_doc.name, new_uom):
                        pass
                        #print("UOM exists")
                    else:
                        insert_uom(item_doc.name, new_uom, new_packing)
                else:
                    not_found.append(row[0])

    for item in not_found:
        print("NOT FOUND:", item)




def find_item_from_barcode(barcode):
    item = frappe.db.sql("""SELECT name from `tabItem` where barcode_retial = %s limit 1""",barcode)
    if len(item)>0:
        return item[0][0]
    else:
        return None

def check_uom_if_exists(item, uom):
    count_uom = frappe.db.sql("""SELECT count(*) from `tabUOM Conversion Detail` where parent = %s and uom = %s""",(item, uom))
    if len(count_uom)>0:
        if count_uom[0][0]>0:
            return True
        else:
            return False
    else:
        return False

def insert_uom(item, uom, packing):
    uom_doc = {
        "parent":item,
        "uom": uom,
        "conversion_factor": packing,
        "parentfield":"uoms",
        "parenttype": "Item",
        "doctype":"UOM Conversion Detail"
    }

    frappe.get_doc(uom_doc).insert()

from __future__ import unicode_literals
import unicodedata, datetime
import frappe, decimal
import frappe.defaults
from frappe import _
from frappe.utils import cstr, cint, flt, comma_or, getdate, nowdate, formatdate, format_time
import poevents
from events import get_rounded_price
from poevents import get_supplier_discount, check_discount_in_branch


def set_transfer_qty(doc, method):

    branch = frappe.get_doc("Branch",doc.branch)
    branch_code = branch.branch_code if (branch.branch_code !="")and(branch.branch_code !=None) else branch
    doc.branch_code = branch_code

    ###Check BO Branch Discount###
    if doc.type == "Bad Order":
        is_branch_discount = check_discount_in_branch(doc.supplier_discount, doc.branch)
        if not is_branch_discount:
            msg= "Discount "+doc.supplier_discount_name+" is not available in "+doc.branch+ "."
            frappe.throw(msg)
        if not ('New Stock Entry' in str(doc.name)):
            set_bo_number(doc)


    amt = 0
    sell_amt = 0
    doc.items = update_items(doc)
    override_costs = doc.change_cost_for_wet_market

    for row in doc.get("items"):
        discount = (doc.supplier_discount)
        if not flt(row.qty):
            frappe.throw(_("Row {0}: Qty is mandatory").format(row.idx))
        if not flt(row.conversion_factor):
            frappe.throw(_("Row {0}: UOM Conversion Factor is mandatory").format(row.idx))


        item_det = frappe.db.sql("""Select barcode_retial, item_cost, item_price, item_price_retail_with_margin, item_price_retail_1, item_cost_with_freight from tabItem where name = %s""", (row.item_code))
        item_cost_with_freight = item_det[0][5]
        barcode = str(item_det[0][0])
        item_cost = str(item_det[0][1])
        item_wholesale = float(item_det[0][2])
        item_retail = round(float(item_det[0][3]),2)
        bo_temp = (float(item_det[0][1])/ row.conversion_factor) if ((item_det[0][1] > 0) and (item_det[0][1] != None)) else 0
        item_bo = decimal.Decimal(str(bo_temp)).quantize(decimal.Decimal('.01'), rounding=decimal.ROUND_HALF_UP)
        #print "ITEM BO",bo_temp, item_bo, "0-------------------------0000 BO"
        #print "WHOLESALE | RETAIL"
        #print item_wholesale, "|", item_retail
        row.barcode_retial = barcode
        conv_qty = row.qty_dummy/row.conversion_factor
        #print "CONV:", row.conversion_factor, "QTY_CONV:",conv_qty
        packing = frappe.db.sql("""Select packing from `tabItem`
                      where item_code = %s""", (row.item_code))
        row.transfer_qty = conv_qty
        row.qty = conv_qty

        for p in packing:
            pkg_list = int(p[0])
            row.packing = pkg_list
        new_item = poevents.get_item_details_SE(row.item_code, row.s_warehouse, doc.posting_date, doc.branch)
        if row.free_item:
            if doc.type != "Bundling":
                print "===============FREE================"
                row.price = 0
                row.basic_rate = 0
                row.amount = 0
                row.basic_amount = 0
                row.selling_amount = 0
                continue
            else:
                pass
        else:
            if doc.type == "Bundling":
                print "===============FREE================"
                row.price = 0
                row.basic_rate = 0
                row.amount = 0
                row.basic_amount = 0
                row.selling_amount = 0
                continue
            else:
                pass

        #TODO: finalize this one!
        if doc.type=="Stock Transfer":
            if override_costs == "CHANGE COST FOR WET MARKET/VEGETABLES":
                print "do nothing. Use Price"
            elif override_costs == "STOCK TRANSFER TO OTHER DEPARTMENTS (USE SRP)":
                #print "USE SRP"
                if (row.uom == new_item['uom']):
                    row.price = float(item_wholesale)
                else:
                    row.price = float(item_retail)
                    #row.basic_rate = float(item_retail)
            else:
                #use cost
                row.price = float(item_cost_with_freight)/row.conversion_factor
        elif (doc.type == 'Bad Order'):
            #TODO : if use_retail_cost, use item price. Else, check packing.
            if doc.use_retail_cost_in_bo == 1:
                if (row.uom == new_item['uom']):
                    row.price = float(item_wholesale)
                else:
                    row.price = float(item_retail)
            else:
                item_bo = decimal.Decimal(str(float(item_cost_with_freight)/row.conversion_factor)).quantize(decimal.Decimal('.01'), rounding=decimal.ROUND_HALF_UP)
                row.price = item_bo
        elif doc.type == 'Wholesale':
            if (row.uom == new_item['l_uom']):
                row.price = float(item_retail)
            else:
                row.price = float(item_wholesale)
        else:
            row.price = float(item_cost_with_freight)/row.conversion_factor

        print "PRICES:", row.price, row.basic_rate
        row.amount = float(row.price) * float(row.qty_dummy)
        row.basic_amount = row.amount
        amt += round(row.basic_amount, 2)
        row.selling_amount = float(row.price) * float(row.qty_dummy)
        sell_amt += round(row.selling_amount, 2)
        row.valuation_rate = row.price

    #set amount and price
    doc.total_amount = round(amt, 2)
    doc.total_selling_amount = round(sell_amt, 2)

    # calculate total less supplier discount
    disc1 = float(doc.discount_1) / 100.00 if doc.discount_1!= None else 0
    disc2 = float(doc.discount_2) / 100.00 if doc.discount_2!= None else 0
    disc3 = float(doc.discount_3) / 100.00 if doc.discount_3!= None else 0
    disc4 = float(doc.discount_4) / 100.00 if doc.discount_4!= None else 0

    #cost
    discounted_amount = ((((float(doc.total_amount)*(1-disc1))*(1-disc2))*(1-disc3))*(1-disc4))
    #discount_amount = doc.total_amount - discounted_amount
    doc.grand_total_less_discount = decimal.Decimal(str(float(discounted_amount))).quantize(decimal.Decimal('.01'),rounding=decimal.ROUND_HALF_UP)
    #TODO: Quantize; round up to 2 decimal places
    #discounted = float(doc.total_amount) - float(doc.total_amount * disc1)
    #second_discount = float(discounted) - float(discounted * disc2)
    #final_total = float(second_discount) - float(second_discount * disc3)
    #final_total = final_total - (final_total*disc4)
    #doc.grand_total_less_discount = float(final_total)

    #price
    discounted_1 = float(doc.total_selling_amount) - float(doc.total_selling_amount * disc1)
    discounted_2 = float(discounted_1) - float(discounted_1 * disc2)
    discounted_3 = float(discounted_2) - float(discounted_2 * disc3)
    discounted_4 = float(discounted_3) - float(discounted_3 * disc4)
    # doc.total_selling_amount_less_discount = float(discounted_4)
    discounted_selling = ((((float(doc.total_selling_amount) * (1 - disc1)) * (1 - disc2)) * (1 - disc3)) * (1 - disc4))
    doc.total_selling_amount_less_discount = float(discounted_selling)

    for i, item in enumerate(doc.items):
        if item.name != None:
            item_id = branch_code + '-' + item.name
            doc.items[i].item_id = item_id
            print item_id

def cost_override_calculation(doc):
    items = doc.items
    for row in items:
        print row.valuation_rate, row.qty, row.qty_dummy, row.transfer_qty, row.amount, row.price

@frappe.whitelist()
def get_supplier_item_grp_disc(name=None):
    count = frappe.db.sql("""SELECT count(*) FROM `tabSupplier Discounts` WHERE supplier_name = %s""",
                          (name))
    print count
    if not count[0][0]:
        return {"has_discount": ("0")}
    else:
        return {"has_discount": ("1")}


@frappe.whitelist()
def get_supplier_disc_detail(disc_grp):
    count = frappe.db.sql("""SELECT count(*) FROM `tabSupplier Discounts` WHERE name = %s""",
                          (disc_grp))
    sup = frappe.db.sql("""Select supplier_name from `tabSupplier Discounts`
                  where name = %s""", (disc_grp))
    if not count[0][0]:
        print "No Discount"
    else:
        discounts = frappe.db.sql(
            """SELECT bo_disc_1, bo_disc_2, bo_disc_3, bo_disc_4 FROM `tabSupplier Discounts` WHERE name = %s""",
            (disc_grp))

        discount_1 = int(discounts[0][0])
        discount_2 = int(discounts[0][1])
        discount_3 = int(discounts[0][2])
        discount_4 = int(discounts[0][3])
        return {"discount_1": discount_1, "discount_2": discount_2,
                "discount_3": discount_3, "discont_4":discount_4, "sup": sup[0][0]}

def get_uom_list(doctype, txt, searchfield, start, page_len, filters):

    return frappe.db.sql("""Select uom from `tabUOM Conversion Detail` where parent = %s""", (filters.get("item_code")))

@frappe.whitelist()
def get_uom_conversion(name, uom):
    count = frappe.db.sql("""SELECT conversion_factor from `tabUOM Conversion Detail` where parent = %s AND uom = %s""",
                          (name, uom))

    print "===================================================="
    print "Nisulod ko sa UOM"
    if count:
        factor = int(count[0][0])
        return {"factor": factor}
    else:
        return {"factor": 0}

@frappe.whitelist()
def get_uom(name):
    doc_max = frappe.db.sql("""select max(conversion_factor) max(uom) from `tabUOM Conversion Detail` WHERE parent = %s""",
                        (name))
    uom = str(doc_max[0][1])
    uom_def = frappe.get_doc("Item", name)
    return {"uom" : uom, "uom_default": uom_def.stock_uom}

@frappe.whitelist()
def get_stock_uom(name):
    doc = frappe.get_doc("Item", name)
    return {"uom" : doc.stock_uom}

def update_items(doc):
    items = doc.items
    print "================UPDATE SE ITEMS=================="
    for item in items:
        item_doc = frappe.get_doc("Item",item.item_code)
        print item.item_code, item.packing, item.barcode_retial
        item.item_description = item_doc.item_name_dummy
        new_item = poevents.get_item_details_SE(item.item_code, item.s_warehouse, doc.posting_date, doc.branch)
        print new_item
        item.inventory = get_item_details(item.item_code, item.s_warehouse, doc.posting_date, doc.posting_time, doc.type, discount = doc.supplier_discount)["inventory"]
        #item.uom = new_item['uom']
        if (doc.type == 'Bad Order'):
            if (item.uom == new_item['l_uom']):
                item.conversion_factor = new_item['con_fac']
        #def get_item_details_SE(item_code= None, warehouse=None, date=None, branch=None):
    return items

"""
FORCE SUBMIT STOCK ENTRY RECORDS AND CREATE SLE's
"""

@frappe.whitelist()
def force_submit(se):
    se_doc = frappe.get_doc("Stock Entry", se)

    frappe.db.sql("""UPDATE `tabStock Entry Detail` set docstatus = 1, modified = NOW() where parent = %s""", se)
    frappe.db.sql("""UPDATE `tabStock Entry` set docstatus = 1, modified = NOW() where name = %s""", se)
    try:
        update_stock_ledger(se_doc)
    except:
        frappe.throw("Error on SLE creation")
        #print "Error on SLE creation"
        #return "Error on SLE creation"
    else:
        delete_sle_not_in_branch(se)
        create_sle_log(se_doc)


def update_stock_ledger(self):
    sl_entries = []

    # make sl entries for source warehouse first, then do for target warehouse
    for d in self.get('items'):
        if cstr(d.s_warehouse):
            sl_entries.append(self.get_sl_entries(d, {
                "warehouse": cstr(d.s_warehouse),
                "actual_qty": -flt(d.transfer_qty),
                "incoming_rate": 0
            }))

    for d in self.get('items'):
        if cstr(d.t_warehouse):
            sl_entries.append(self.get_sl_entries(d, {
                "warehouse": cstr(d.t_warehouse),
                "actual_qty": flt(d.transfer_qty),
                "incoming_rate": flt(d.valuation_rate)
            }))

        # On cancellation, make stock ledger entry for
        # target warehouse first, to update serial no values properly

        # if cstr(d.s_warehouse) and self.docstatus == 2:
        # 	sl_entries.append(self.get_sl_entries(d, {
        # 		"warehouse": cstr(d.s_warehouse),
        # 		"actual_qty": -flt(d.transfer_qty),
        # 		"incoming_rate": 0
        # 	}))

    if self.docstatus == 2:
        sl_entries.reverse()

    self.make_sl_entries(sl_entries, self.amended_from and 'Yes' or 'No')

def delete_sle_not_in_branch(se_name):
    branch = frappe.db.get_value("Server Information", None, "branch")
    rows = frappe.db.sql("""select sle.name, sle.warehouse, sle.item_code, w.branch, sle.posting_date from `tabStock Ledger Entry` sle
                        inner join `tabWarehouse` w on sle.warehouse = w.name where w.branch != %s and
                        sle.voucher_no = %s""", (branch, se_name))
    frappe.db.sql("""DELETE sle from `tabStock Ledger Entry` sle inner join `tabWarehouse` w on sle.warehouse = w.name
                        where w.branch != %s and sle.voucher_no=%s""",
                  (branch, se_name))
    frappe.db.commit()

    print "NO OF SLE ROWS DELETED:", len(rows)

@frappe.whitelist()
def get_item_details(item_code, from_warehouse, date, posting_time, type, se_uom = None, stt_type = None, use_retail_cost = None, discount=None):
    item = frappe.get_doc("Item", item_code)
    #packing, wholesale_price, retail_price, uom = item.packing, round(item.item_price, 2), round(item.item_price_retail_with_margin,2), item.uom
    packing, wholesale_price, retail_price, uom = item.packing, get_rounded_price(item.item_price), get_rounded_price(item.item_price_retail_with_margin), item.uom
    inv = get_balance(item_code, from_warehouse, date, posting_time)
    inventory = "PKG: " + str(packing) + " | INV: " + str(inv) + " " + uom + "/s" + " | WHLSL: P" + str(wholesale_price) + " | RTL: P" + str(
        retail_price) + " | UOM: "+uom+" | RTL UOM: "+item.pos_uom
    print "==========================="
    print "SE UOM", se_uom
    item_uom = se_uom if se_uom!=None else uom
    if type == "Wholesale":
        if item_uom == uom:
            price = wholesale_price
        else:
            price = retail_price
    elif type == "Withdrawals":
        price = item.item_cost
    elif type == "Bad Order":
        item_discount = get_supplier_discount(item_code)
        discount = "" if discount is None else discount
        item_discount_text = "No Existing Supplier Discount" if item_discount == "Has NO Supplier Discount" else item_discount[0]
        print discount, item_discount, item_discount_text, (discount in item_discount)
        if (discount not in item_discount) and (discount is not None):
            frappe.throw("Item " + item.item_name_dummy + "(" + item_discount_text+ ") does not belong in this Supplier Discount.",
                         "Warning")
            price = 0
        if item_uom == uom:
            if use_retail_cost ==1:
                price = item.item_price
            else:
                price = item.item_cost_with_freight
        else:
            if use_retail_cost == 1:
                price = item.item_price_retail_with_margin
            else:
                price = item.item_cost_with_freight/item.packing
    elif type == "Stock Transfer":
        if stt_type == "CHANGE COST FOR WET MARKET/VEGETABLES":
            price = 0.0
        elif stt_type=="STOCK TRANSFER TO OTHER DEPARTMENTS (USE SRP)":
            price = wholesale_price if item_uom == uom else retail_price
        else:
            price = item.item_cost_with_freight
    else:
        price = item.item_cost_with_freight

    return {"inventory":inventory, "barcode_retial": item.barcode_retial, "item_cost":item.item_cost, "uom": uom, "packing": packing, "price":price, "pos_uom":item.pos_uom, "description":item.item_name_dummy, "inv":inv}


def get_balance(item_code,warehouse, posting_date, posting_time=None):
    from erpnext.stock.stock_ledger import get_previous_sle
    from frappe.utils import flt, cstr, nowdate, nowtime
    if not posting_date: posting_date = nowdate()
    if not posting_time: posting_time = nowtime()

    last_entry = get_previous_sle({
        "item_code": item_code,
        "warehouse": warehouse,
        "posting_date": posting_date,
        "posting_time": posting_time})
    try:
        return last_entry.qty_after_transaction
    except:
        return 0

@frappe.whitelist()
def create_item_id(from_date, to_date):
    se_name = frappe.db.sql("""SELECT name,branch_code from `tabStock Entry` where posting_date BETWEEN %s and %s""",(from_date, to_date))
    for se_names in se_name:
        naming = frappe.db.sql("""SELECT name from `tabStock Entry Detail` WHERE parent = %s AND item_id IS NULL""",(se_names[0]))
        print se_names[0]
        for namings in naming:
            print namings[0]
            se_doc = frappe.get_doc("Stock Entry", se_names[0])
            branch_code = frappe.get_doc('Branch',se_doc.branch).branch_code
            print branch_code, se_doc.branch
            item_id = branch_code + "-" + namings[0]
            frappe.db.sql("""UPDATE `tabStock Entry Detail` set item_id = %s where parent = %s""",(item_id, se_names[0]))
            frappe.db.commit()

def check_se_sle(doc, method):
    create_sle_log(doc)
    count_sle = frappe.db.sql("""SELECT COUNT(*) from `tabStock Ledger Entry` where voucher_no = %s""", doc.name)
    if (2*len(doc.items)) != int(count_sle[0][0]):
        if doc.type!="Bad Order":
            frappe.db.sql("""delete from `tabStock Ledger Entry` where voucher_no = %s""",doc.name)
            frappe.db.sql("""delete from `slelogging` where voucher_no = %s""",doc.name)
            frappe.db.commit()
            frappe.throw("Inventory generated is wrong. Please submit document again.")


def get_balance2(warehouse, item_code, posting_date, posting_time):
    to_date = str(posting_date)+ " " + str(posting_time)
    from_date = "2015-01-01 00:00:00"
    recon = 0


    sr = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty from `tabStock Reconciliation Item` itm
                            join `tabStock Reconciliation` sr on sr.name = itm.parent where itm.warehouse = %s and itm.item_code = %s
                            and TIMESTAMP(sr.posting_date, sr.posting_time)< %s and sr.docstatus = 1 order by posting_date desc, posting_time desc,
                            sr.name desc limit 1""", (warehouse, item_code, to_date))

    if len(sr)>0:
        if sr[0][0]:
            from_date = sr[0][0]
            recon = sr[0][1]

    print(str(from_date), str(to_date))

    rrs = frappe.db.sql("""select sum(itm.stock_qty) from `tabPurchase Receipt Item` itm join `tabPurchase Receipt` rr on
                        rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s and rr.docstatus = 1
                        and TIMESTAMP(rr.posting_date, rr.posting_time)>=%s and TIMESTAMP(rr.posting_date, rr.posting_time)< %s""",
                        (warehouse, item_code, from_date, to_date))

    ses_minus = frappe.db.sql("""select sum(-1*itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time)>= %s and TIMESTAMP(se.posting_date, se.posting_time)<%s
                              and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))

    ses_plus = frappe.db.sql("""select sum(itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time)>= %s and TIMESTAMP(se.posting_date, se.posting_time)<%s
                              and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))

    total_rr = rrs[0][0] if rrs[0][0] is not None else 0
    total_se_minus = ses_minus[0][0] if ses_minus[0][0] is not None else 0
    total_se_plus = ses_plus[0][0] if ses_plus[0][0] is not None else 0

    inv = total_rr + total_se_minus + total_se_plus + recon

    return inv

#SLE LOGGING FEATURE: insert and delete on submit/cancel

def create_sle_log(doc):
    from rrevents import find_sle
    from erpnext.stock.stock_ledger import get_previous_sle
    voucher_no = doc.name
    posting_date = doc.posting_date
    posting_time = doc.posting_time
    try:
        posting_time = datetime.datetime.strptime(str(doc.posting_time), "%H:%M:%S")
    except:
        posting_time = datetime.datetime.strptime(str(doc.posting_time), "%H:%M:%S.%f")
    is_match = 0

    print("#################CREATE SLE LOG##################")
    ##SOURCE WAREHOUSE: Negative item qty
    for item in doc.items:
        actual_qty = -item.transfer_qty
        last_entry = get_previous_sle({
            "item_code": item.item_code,
            "warehouse": item.s_warehouse,
            "posting_date": posting_date,
            "posting_time": (posting_time - datetime.timedelta(seconds=1)).strftime("%H:%M:%S")})
        print last_entry
        try:
            qty_after_transaction = float(last_entry.qty_after_transaction) + float(actual_qty)
        except:
            qty_after_transaction = actual_qty
        else:
            qty_after_transaction = float(last_entry.qty_after_transaction) + float(actual_qty)
        #frappe.throw("STOP.")
        # check_match
        match = find_sle(voucher_no, item.item_code, actual_qty, qty_after_transaction, item.s_warehouse, posting_date,
                         posting_time.strftime("%H:%M:%S"))
        if match > 0:
            is_match = 1
        print(voucher_no, item.item_code, actual_qty, qty_after_transaction, item.s_warehouse, posting_date, posting_time,
              is_match)
        frappe.db.sql("""insert into slelogging(voucher_no, item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match)
                            values(%s,%s,%s,%s,%s,%s,%s,%s)""", (
            voucher_no, item.item_code, actual_qty, qty_after_transaction, item.s_warehouse, posting_date, posting_time.strftime("%H:%M:%S"),
            is_match))
        frappe.db.commit()

    ##TRANSFER WAREHOUSE: Add
    for item in doc.items:
        actual_qty = item.transfer_qty
        last_entry = get_previous_sle({
            "item_code": item.item_code,
            "warehouse": item.t_warehouse,
            "posting_date": posting_date,
            "posting_time": (posting_time - datetime.timedelta(seconds=1)).strftime("%H:%M:%S")})
        try:
            qty_after_transaction = float(last_entry.qty_after_transaction) + float(actual_qty)
        except:
            qty_after_transaction = actual_qty
        else:
            qty_after_transaction = float(last_entry.qty_after_transaction) + float(actual_qty)

        # check_match
        match = find_sle(voucher_no, item.item_code, actual_qty, qty_after_transaction, item.t_warehouse, posting_date,
                         posting_time.strftime("%H:%M:%S"))
        if match > 0:
            is_match = 1
        print(voucher_no, item.item_code, actual_qty, qty_after_transaction, item.t_warehouse, posting_date, posting_time,
              is_match)
        frappe.db.sql("""insert into slelogging(voucher_no, item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match)
                            values(%s,%s,%s,%s,%s,%s,%s,%s)""", (
            voucher_no, item.item_code, actual_qty, qty_after_transaction, item.t_warehouse, posting_date, posting_time.strftime("%H:%M:%S"),
            is_match))
        frappe.db.commit()

def set_bo_number(doc, method=None):
     if doc.type =="Bad Order":
         temp_name = str(doc.name).split('-')[2]
         if ("TEMP" in str(doc.name)):
             bo_name = "TEMP/"+temp_name[0:6] + temp_name[8:11]
         else:
             if doc.amended_from is not None or doc.amended_from =="":
                 bo_name = temp_name[0:6] + temp_name[8:11] + "-"+str(doc.name).split('-')[3]
             else:
                 bo_name = temp_name[0:6]+temp_name[8:11]
         #print bo_name, temp_name, temp_name[0:6], temp_name[9:11],"$$$$$$$$$$$$$$$$$$$$$$$$"
         doc.bo_series = bo_name
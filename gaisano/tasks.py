import frappe
import os
import datetime
import dbf
import csv

@frappe.whitelist()
def get_branch():
    branch = frappe.db.get_value("Server Information", None, "Branch")
    return branch

def process_task_run():
    path = frappe.get_site_path().replace("./", "")
    if frappe.db.get_value("System Settings", None, "run_task") == 'permission':
        frappe.db.set_value("System Settings", None, "run_task", "")
        os.system("cd /home/frappe/frappe-v7 && /usr/local/bin/bench --site " + path + " reset-perms")

    if frappe.db.get_value("System Settings", None, "run_task") == 'backup':
        print "im here"
        frappe.db.set_value("System Settings", None, "run_task", "")
        os.system("cd /home/frappe/frappe-v7 && /usr/local/bin/bench --site " + path + " backup")

#pop up for erp copy old to new
@frappe.whitelist()
def check_last_erp():
    msg = ""
    dbf_path = frappe.db.get_value("Server Information", None, "grocery_dbf_dir")
    try:
        os.system("mv /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.DBF /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.dbf")
        # os.system("mv /home/gaisano1/gaisano_chaka/POS_LOG.DBF /home/gaisano1/gaisano_chaka/POS_LOG.dbf")
        table = dbf.Table('/media/GROCERY_SERVER/' + dbf_path + '/POS_LOG', codepage='cp437')
        # table = dbf.Table('/home/gaisano1/gaisano_chaka/POS_LOG', codepage='cp437')
        table.open()
        print table
        for record in table:
            timestamp = str(record[0]) + " : " + str(record[1])
            msg = "<div class='alert alert-info' role ='alert'><center> Last Run ERP <strong>" + str(timestamp) + "</strong>.</center></div>"
        os.system("mv /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.dbf /media/GROCERY_SERVER/" + dbf_path + "/POS_LOG.DBF")
        # os.system("mv /home/gaisano1/gaisano_chaka/POS_LOG.dbf /home/gaisano1/gaisano_chaka/POS_LOG.DBF")
    except:
        msg = "<div class='alert alert-warning' role ='alert'><center><strong> POS_LOG.DBF not found</strong>.</center></div>"
    return msg




#TODO: Don't know where this is used. -_-
def create_item_csv():
    item = frappe.db.sql("""Select a.item_code, "50" as dep_code, a.barcode_retial,(select uom from `tabUOM Conversion Detail` where parent = a.item_code Order By conversion_factor DESC limit 1),
          a.item_price_retail_1, a.item_cost, "FALSE" as special, modified,
           "" as item_short, "" as supp_code, 0 as points, "" as price_w, "" as minimum, "FALSE" as senior from `tabItem` as a""")

    # os.chdir('/media/backup/pos_data/item_dbf')
    #os.chdir('/home/gaisano/Documents/grocery/dbftemp')
    os.chdir('/home/littlehera/gaisano_chaka')
    # table = dbf.Table('ITEM', 'ITEM_NAME C(70); DEP_CODE C(10); BAR_CODE C(30); UNIT C(10); PRICE N(10,2) ; COST N(10,2); SPECIAL C(10); UPDATED D; ITEM_SHORT C(15); SUPP_CODE C(10); POINTS N(10,0); PRICE_W C(10); MINIMUM C(10); SENIOR C(10)')
    # table.open()
    table = dbf.Table('ITEM')
    table.open()
    print "im here++++++++++"
    for data in table:
        dbf.delete(data)
        table.pack()

    print "Finisssshhhhhhhhhhhhhhhhh"

#TODO: Don't know where this is used. -_-
def open_csv():
    os.chdir('/home/gaisano/Documents/grocery/dbftemp')

    # table = dbf.Table('ITEM', 'ITEM_NAME C(70); DEP_CODE C(10); BAR_CODE C(30); UNIT C(10); PRICE N(10,2) ; COST N(10,2); SPECIAL C(10); UPDATED D; ITEM_SHORT C(15); SUPP_CODE C(10); POINTS N(10,0); PRICE_W C(10); MINIMUM C(10); SENIOR C(10)')
    # table.open()
    table = dbf.Table('ITEM')
    # table.delete_record()
    table.open()
    for data in table:
        print data.field

    print "Finisssshhhhhhhhhhhhhhhhh"


#TODO: Don't know where this is used. -_-
def set_discount():
    item_disc = frappe.db.sql("""Select parent, items from `tabSupplier Discount Items`""")

    for i in item_disc:
        try:
            parent = str(i[0])
            item_code = str(i[1])

            disc = frappe.get_doc("Supplier Discounts", parent)
            total_disc = round(float((disc.disc_1 + disc.disc_2 + disc.disc_3 + disc.disc_4) / 100), 2)

            frappe.db.sql("""Update `tabItem` set item_discount = %s
                            where item_code = %s and item_discount = 0""", (total_disc, item_code))
            frappe.db.commit()

            print item_code
            print total_disc

        except:
            print "cant get item ++++"

#TODO: doublecheck (Delete all items inside dbf and replace with updated items)
def generate_item_dbf():
    branch = get_branch()

    os.system("mv /media/backup/pos_data/pos_dbf/ITEM.DBF /media/backup/pos_data/pos_dbf/ITEM.dbf")
    table = dbf.Table('/media/backup/pos_data/pos_dbf/ITEM', codepage='cp437')
    table.open()

    delete_records(table) #delete all items
    print "-------------New Items---------------"
    item_det = frappe.db.sql("""Select substring(item_name_dummy,1,25),dep_code, substring(barcode_retial,1,13),
                            substring(pos_uom,1,6), item_price_retail_with_margin,item_cost, special, substring(REPLACE(REPLACE(`modified`, '-', ''), ' ', ''),1,16), substring(item_short_name,1,16),
                            supp_code, points, item_price, minimum, senior from `tabItem` where type!='Disabled' and type!='Phased Out'""")

    for data in item_det:
        print data[0], data[2]

        table.append(data)

    barcode = frappe.db.sql("""select item_name_dummy,barcode_retial,barcode_pack from `tabItem` where type!='Disabled' and type!='Phased Out' and barcode_pack <> barcode_retial and length(barcode_pack) = 13""")
    print "----------------start of WHOLESALE"
    for data in barcode:
        barcode_data = frappe.db.sql("""select substring(item_name_dummy,1,25),dep_code, substring(barcode_pack,1,13),
                                        substring(uom,1,6), item_price,item_cost, special, substring(REPLACE(REPLACE(`modified`, '-', ''), ' ', ''),1,16), substring(item_short_name,1,16),
                                        supp_code, points, item_price, minimum, senior from `tabItem` where type!='Disabled' and type!='Phased Out' and SUBSTRING(barcode_pack,1,13) = %s""",data[2])
        print barcode_data[0][0], barcode_data[0][2], barcode_data[0][3]
        table.append(barcode_data[0])

    print "-------------number of records-------------"
    print len(table)


    os.system("mv /media/backup/pos_data/pos_dbf/ITEM.dbf /media/backup/pos_data/pos_dbf/ITEM.DBF")
    #os.system("mv /home/littlehera/gaisano_chaka/ITEM.dbf /home/littlehera/gaisano_chaka/ITEM.DBF")

def display_record():#TODO: REMOVE. not used
    os.chdir('/home/gaisano/Documents/grocery/dbftemp')
    table = dbf.Table('ITEM')
    table.open()
    for record in table:
        try:
            print record
        except:
            print "error"

def delete_records(table = None):
    print "------------DELETE ITEMS----------------"
    if table == None:
        branch = get_branch()

        table = dbf.Table('/media/backup/pos_data/pos_dbf/ITEM', codepage='cp437')
        #table = dbf.Table('/home/littlehera/gaisano_chaka/ITEM', codepage='cp437')
        table.open()
    for data in table:
        print "delete"
        dbf.delete(data)
    table.pack()

def display_records(table = None):
    if table == None:
        branch = get_branch()
        # table = dbf.Table('/home/gaisano1/gaisano_chaka/ITEM', codepage='cp437')
        table = dbf.Table('/media/backup/pos_data/pos_dbf/ITEM', codepage='cp437')
        table.open()
    for record in table:
        try:
            print record
        except:
            print "error"
    print "-------------number of records-------------"
    print len(table)

#TODO: Don't know where this is used. -_-
def change_short_name():
        item = frappe.db.sql("""Select substring(barcode_retial,1,13), name from `tabItem`""")
        branch = get_branch()

        os.chdir('/media/backup/pos_data/item_dbf')
        # os.chdir('/home/gaisano/Documents/grocery/dbftemp')
        table = dbf.Table('ITEM', codepage='cp437')

        with table:
            procod_idx = table.create_index(lambda rec: (rec.bar_code))
            print "Im Here"
            for i in item:
                barcode = str(i[0])
                name = str(i[1])
                match = procod_idx.search(match=(barcode,), partial=True)
                if match:
                    record = match[0]
                    with record:
                        print barcode
                        frappe.db.sql("""Update `tabItem` set item_short_name = %s
                              where barcode_retial = %s""", (record.item_short, barcode))
                        frappe.db.commit()
                        print record.item_short

#TODO: edit this one
def copy_dbf_price():
    item = frappe.db.sql("""Select substring(barcode_retial,1,13), name from `tabItem`""")

    branch = get_branch()

    os.chdir('/media/backup/pos_data/item_dbf')

    #os.chdir('/home/littlehera/Documents')
    table = dbf.Table('ITEM', codepage='cp437')
    senior = 0
    with table:
        procod_idx = table.create_index(lambda rec: (rec.bar_code))
        print "Im Here"
        for i in item:
            barcode = str(i[0])
            name = str(i[1])
            match = procod_idx.search(match=(barcode,), partial=True)
            if match:
                record = match[0]
                with record:
                    print barcode
                    try:
                        if(record.senior==True):
                            senior = 1
                            print "Senior is TRUE"
                        else:
                            senior = 0
                            print "Senior is FALSE"
                    except:
                        senior = 0

                    frappe.db.sql("""Update `tabItem` set item_price_retail_with_margin = %s,
                          dep_code = %s, supp_code = %s, points = %s, minimum = %s, senior = %s,
                          pos_uom = %s where barcode_retial = %s""",
                        (record.price,
                         record.dep_code,
                         record.supp_code,
                         record.points,
                         record.minimum,
                         senior,
                         record.unit,
                         barcode))
                    frappe.db.commit()
                    print "Updated"


def generate_depcode_dbf():
    branch = get_branch()

    os.system("mv /media/backup/pos_data/pos_dbf/DEP.DBF /media/backup/pos_data/pos_dbf/DEP.dbf")
    table = dbf.Table('/media/backup/pos_data/pos_dbf/DEP', codepage='cp437')
    #table = dbf.Table('/home/littlehera/gaisano_chaka/test DEP', codepage='cp437')
    #table = dbf.Table('/home/littlehera/gaisano_chaka/depcopy', codepage='cp437')
    table.open()
    delete_records(table) #delete all items
    print "-------------DEP CODES---------------"
    depcodes = frappe.db.sql("""SELECT name, SUBSTRING(department,1,10), markup, SUBSTRING(append, 1, 10), DATE(date_app), SUBSTRING(modify, 1, 10), DATE(date_mod), vat from `tabDepartment Code`""")
    for data in depcodes:
        print data
        table.append(data)
    print "-------------number of records-------------"
    print table.field_names
    for record in table:
        print record.dep_code, record.department, record.markup, record.append, record.date_app, record.modify, record.date_mod, record.vat
        print type(record.dep_code), type(record.department), type(record.markup), type(record.append), type(record.date_app), type(record.modify), type(record.date_mod), type(record.vat)
    print len(table)

    os.system("mv /media/backup/pos_data/pos_dbf/DEP.dbf /media/backup/pos_data/pos_dbf/DEP.DBF")

def set_supp_code():
    item_code = frappe.db.sql("""Select supp_code, item_code from `tabItem`""")

    for i in item_code:
        try:
            supp = str(i[0])
            code = str(i[1])

            sup = frappe.db.sql("""Select supplier from `tabItem Supplier` where parent = %s""", (code))

            for s in sup:
                s_name = str(s[0])
                frappe.db.sql("""Update `tabSupplier` set supp_code = %s where name = %s""", (supp, s_name))

                print "Finish"
        except:
            print "Cant"

#UPDATE DEP CODE for ALL ITEMS

def update_items_depcode():
    items = frappe.db.sql("""SELECT name from `tabItem`""")
    for item in items:
        item_doc = frappe.get_doc("Item", item[0])
        if (item_doc.item_category != "") and (item_doc.item_category != None):
            if (item_doc.dep_code == "") or (item_doc.dep_code == None):
                print item_doc.name, item_doc.item_category
                print "NO DEP_CODE. UPDATE"
                depcode = get_dep_code(item_doc.item_category)
                if depcode!=None:
                    frappe.db.sql("""UPDATE `tabItem` set dep_code = %s where name = %s""", (depcode, item_doc.name))
                    print depcode
            else:
                continue
    frappe.db.commit()
    pass

@frappe.whitelist()
def get_dep_code(department):
    dep_code = frappe.db.sql("""Select dep_code, department from `tabDepartment Code` where department = %s""",department)
    if len(dep_code)>0:
        return str(dep_code[0][0])
    else:
        return None
    pass

def update_dep_codes():
    records = frappe.db.sql("""SELECT name from `tabDepartment Code`""")
    for record in records:
        dep_doc = frappe.get_doc("Department Code", record[0])
        owner = frappe.get_doc("User",dep_doc.owner).first_name
        modified_by = frappe.get_doc("User", dep_doc.modified_by).first_name
        if len(dep_doc.department) > 10:
            print "len department", len(dep_doc.department), dep_doc.department
        frappe.db.sql("""UPDATE `tabDepartment Code` set append = %s, date_app = %s, modify = %s, date_mod = %s
                        where name = %s""", (owner, dep_doc.creation, modified_by, dep_doc.modified, dep_doc.name))
    frappe.db.commit()
    pass

#create a list from the ITEM.dbf file
def make_list(i= None, errors = None, d = None):
    data = d if d != None else []
    branch = get_branch()

    table = dbf.Table('/media/backup/pos_data/grocery_dbf/ITEM', codepage='cp437')
    #table = dbf.Table('/home/gaisano1/gaisano_chaka/grocery', codepage='cp437')
    table.open()
    idx = (i) if i != None else 0
    error_count = (errors) if errors!=None else 0
    #try:
    while idx < (len(table)):
        try:
            is_deleted = dbf.is_deleted(table[idx])
            if is_deleted is False:
                if table[idx].cost_w != None:
                    data.append({"barcode":table[idx].bar_code, "cost_w":table[idx].cost_w, "cost_r":table[idx].cost_r,
                                 "item_code":table[idx].item_name, "price_w":table[idx].price_w,
                                 "price_r":table[idx].price_r, "vat":table[idx].vat, "freight":table[idx].freight,
                                 "supplier":table[idx].supp_code, "packing":table[idx].packing})
            idx += 1
        except:
            #print "ERROR on ", idx
            idx += 1
            continue

    #except:
    #    error_count +=1
    #    data = make_list(idx + 1, error_count, data)
    return data

def get_list():
    data = make_list()
    print "------------end--------------"
    return data

def find_item_in_old(table, barcode):
    old = loop_search(table, barcode)
    return old[0]['cost_w']

def item_in_old(table, barcode):
    old = loop_search(table, barcode)
    return None if len(old)<1 else old[0]

#look for the fastest algorithm.
def find_item_test():
    import time
    list_ts = time.time()
    table = get_list()
    list_tf = time.time()
    list_tc = list_tf - list_ts

    bs_ts = time.time()
    print binary_search(0, len(table) - 1, "4806521930019", table)
    bs_tf = time.time()
    bs_tc = bs_tf - bs_ts

    loop_ts = time.time()
    print loop_search(table, "4806521930019")
    loop_tf = time.time()
    loop_tc = loop_tf - loop_ts

    print "list generation", list_tc
    print "binary search", bs_tc
    print "loop_search", loop_tc

#recursion lol.
def binary_search(start, end, barcode, table):

    idx = int(start)
    end = int(end)
    mid = int((end+idx)/2)
    if mid>idx:
        binary_search(start, mid, barcode, table)
    else:
        if table[start]["barcode"] == barcode:
            return table[start]
    if end>(mid+1):
        binary_search((mid+1), end, barcode, table)
    else:
        if table[mid+1]["barcode"] == barcode:
            return table[mid+1]

def loop_search(table, barcode):
    return [element for element in table if element["barcode"] == barcode]

@frappe.whitelist()
def disable_discounts():
    today = datetime.date.today()
    print today
    discounts = frappe.db.sql("""SELECT name from `tabSupplier Discounts` where valid_until = %s""",today)
    for discount in discounts:
        print discount[0]
        frappe.db.sql("""UPDATE `tabSupplier Discounts` set disable = 1 where name = %s""", discount[0])
    frappe.db.commit()

@frappe.whitelist()
def get_branch():
    branch = frappe.db.get_value("Server Information", None, "branch")
    return branch

@frappe.whitelist()
def get_branch_and_branch_code():
    branch = frappe.db.get_value("Server Information", None, "branch")
    branch_doc = frappe.get_doc("Branch", branch)
    return {"branch":branch, "branch_code":branch_doc.branch_code}
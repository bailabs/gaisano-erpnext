# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from gaisano.poevents import get_balance
from datetime import datetime, date, time, timedelta


class StockRequisition(Document):
	pass

@frappe.whitelist()
def fill_details(item_code,warehouse):
	item = frappe.db.sql("""SELECT name, description from `tabItem` where item_code = %s""",(item_code))
	item_code = item[0][0]
	description = item[0][1]
	now = datetime.now()
	item_details = get_balance(warehouse,item_code, now)
	if item_details <= 0:
		msg = description + " is out of stock"
	else:
		msg = ""
	inv = "INV: " + str(item_details)

	return {"description": description,'inv': inv, 'msg': msg}
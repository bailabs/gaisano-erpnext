// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt


frappe.ui.form.on('Advanced Settings', 'onload', function(frm){
	if (frappe.user.has_role("Support Team")){
		frm.set_df_property('api_hostname', 'hidden', 0);
		frm.set_df_property('api_username', 'hidden', 0);
		frm.set_df_property('api_password', 'hidden', 0);
		frm.set_df_property('test_connection', 'hidden', 0);
	}
})


frappe.ui.form.on("Advanced Settings", "test_connection", function(frm){
	var hostname = frm.doc.api_hostname;
	var username = frm.doc.api_username;
	frappe.call({
		method: "gaisano.apisync.connect_to_server",
		callback:function(r){
			console.log('testing')
		}
	});


})

frappe.ui.form.on('Advanced Settings', {
	onload: function(frm) {
	cur_frm.set_query("po", "pos", function(frm) {
        return {
            "filters": {
                "docstatus": 2   }
        };
        });
	cur_frm.set_query("rr", "rrs", function(frm) {
        return {
            "filters": {
                "docstatus": 2   }
        };
        });
	cur_frm.set_query("r_doctype", function(frm) {
        return {
            "filters": [
            	['DocType', 'name', 'in', 'Purchase Order, Purchase Receipt, Stock Entry']
			]
        };
        });
	cur_frm.set_query("po_wrong_date", function(frm) {
        return {
            "filters": [
            	['docstatus', '!=', 2]
			]
        };
        });
	cur_frm.set_query("transfer_doctype", function(frm) {
        return {
            "filters": [
            	['DocType', 'name', 'in', 'Purchase Order, Purchase Receipt, Stock Entry']
			]
        };
        });
	cur_frm.set_query("new_doc", function(frm) {
        return {
            "filters": {
                "docstatus": 0   }
        };
        });
	//cur_frm.set_query("original_doc", function(frm) {
    //    return {
    //        "filters": {
    //            "docstatus": 1  }
    //    };
    //    });

	}

});

frappe.ui.form.on("Advanced Settings", "apply_markup", function(frm){
	var progress_bar = '<span id="progress_bar_text">Updating Items Markup</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    frappe.call({
    	method:"gaisano.gaisano.doctype.advanced_settings.advanced_settings.markup_item_prices",
		args:{
    		"markup": frm.doc.markup
		},
		callback: function(r){
    		progress.hide();
    		frappe.msgprint(r.message);
		}
	});
});

frappe.ui.form.on("Advanced Settings", "get_pos", function(frm){
	//gaisano.events.update_all_discount_items
	frappe.call({
		method:"gaisano.gaisano.doctype.advanced_settings.advanced_settings.get_cancelled_pos",
		args:{
			"limit":frm.doc.no_pos
		},
		callback:function(r){
			if(r.message){
				frm.clear_table("pos");
				fill_po_table(frm, r.message.pos);
			}
		}
	});
});

frappe.ui.form.on("Advanced Settings", "get_rrs", function(frm){
	//gaisano.events.update_all_discount_items
	frappe.call({
		method:"gaisano.gaisano.doctype.advanced_settings.advanced_settings.get_cancelled_rrs",
		args:{
			"limit":frm.doc.no_rrs
		},
		callback:function(r){
			if(r.message){
				frm.clear_table("pos");
				fill_rr_table(frm, r.message.rrs);
			}
		}
	});
});

function fill_po_table(frm, pos){
	var counter = pos.length;
		for(i=0; i<counter; i++){
			console.log("Updating : "+i+ " "+pos[i][0]);
			var new_row = frm.add_child("pos");
			new_row.po = pos[i][0];
			new_row.supplier_discount = pos[i][1];
			new_row.po_date = pos[i][2];
			new_row.created_by = pos[i][3];
			frm.refresh_field("pos");
		}
}

function fill_rr_table(frm, rrs){
	var counter = rrs.length;
	if (rrs.length > 0)
		for(i=0; i<counter; i++){
			console.log("Updating : "+i+ " "+rrs[i][0]);
			var new_row = frm.add_child("rrs");
			new_row.rr = rrs[i][0];
			new_row.supplier_discount = rrs[i][1];
			new_row.rr_date = rrs[i][2];
			new_row.created_by = rrs[i][3];
			frm.refresh_field("rrs");
		}
}

frappe.ui.form.on("PO for Delete", "po", function(frm, cdt, cdn){
	var item = locals[cdt][cdn];
	frappe.call({
		method: "frappe.client.get",
		args:{
			"doctype": "Purchase Order",
			"name" : item.po
		},
		callback: function(r){
			if(r.message){
				console.log(r.message);
				frappe.model.set_value(cdt, cdn, "supplier_discount", r.message.supplier_discount);
				frappe.model.set_value(cdt, cdn, "po_date", r.message.modified);
				frappe.model.set_value(cdt, cdn, "created_by", r.message.owner);
				frm.refresh_field("pos");
			}
		}
	});
});

frappe.ui.form.on("RR for Delete", "rr", function(frm, cdt, cdn){
	var item = locals[cdt][cdn];
		frappe.call({
		method: "frappe.client.get",
		args:{
			"doctype": "Purchase Receipt",
			"name" : item.rr
		},
		callback: function(r){
			if(r.message){
				console.log(r.message);
				frappe.model.set_value(cdt, cdn, "supplier_discount", r.message.supplier_discount);
				frappe.model.set_value(cdt, cdn, "rr_date", r.message.modified);
				frappe.model.set_value(cdt, cdn, "created_by", r.message.owner);
				frm.refresh_field("rrs");
			}
		}
	});

});

cur_frm.cscript.delete_button = function(frm, cdt, cdn){
	console.log(locals[cdt][cdn]);
	var item = locals[cdt][cdn];
	var doc = cdt;
	var table = locals[cdt][cdn].parentfield
	cur_frm.get_field(locals[cdt][cdn].parentfield).grid.grid_rows[locals[cdt][cdn].idx-1].remove();

	cur_frm.refresh_field(table);
	//Pass po/rr # to python function which will delete the PO/RR and all the items.

	frappe.call({
		method:"gaisano.gaisano.doctype.advanced_settings.advanced_settings.delete_document",
		args:{
			"doctype":doc,
			"document":item
		},
		callback:function(r){
				if(r.message){
					msgprint ("Hmm. Something Went Wrong.");
				}
		}
	});
}


frappe.ui.form.on("Advanced Settings", "delete_po", function(frm){
	var items = frm.doc.pos;
	if(items.length > 0)
	for(var i = 0; i< items.length; i++){
		var item = items[i];
		var doc = "PO";
		var table = items[i].parentfield;
		console.log(item, doc, table, items[i].idx);
		cur_frm.get_field(table).grid.grid_rows[items[i].idx-1].remove();
		cur_frm.refresh_field(table);
		frappe.call({
			method:"gaisano.gaisano.doctype.advanced_settings.advanced_settings.delete_document",
			args:{
				"doctype":doc,
				"document":item
			},
			callback:function(r){

					if(r.message){
						msgprint ("Hmm. Something Went Wrong.");
					}
			}
		});
	}
});

frappe.ui.form.on("Advanced Settings", "delete_rr", function(frm){
	var items = frm.doc.rrs;
	if(items.length > 0)
	for(var i = 0; i< items.length; i++){
		var item = items[i];
		var doc = "RR";
		var table = items[i].parentfield;
		console.log(item, doc, table, items[i].idx);
		cur_frm.get_field(table).grid.grid_rows[items[i].idx-1].remove();
		cur_frm.refresh_field(table);
		frappe.call({
			method:"gaisano.gaisano.doctype.advanced_settings.advanced_settings.delete_document",
			args:{
				"doctype":doc,
				"document":item
			},
			callback:function(r){

					if(r.message){
						msgprint ("Hmm. Something Went Wrong.");
					}
			}
		});
	}
});


frappe.ui.form.on("Advanced Settings", "revert_doc", function (frm){
	var progress_bar = '<span id="progress_bar_text">Cancelling...</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	var args = {
			"r_doctype":frm.doc.r_doctype,
			"r_id":frm.doc.r_id
		};
	console.log(args);
	frappe.call({
		method : "gaisano.gaisano.doctype.advanced_settings.advanced_settings.cancel_doc_with_sle",
		args:args,
		callback:function(r){
			    progress.hide();
			frappe.msgprint(r.message,"Done.", "blue");
		}
	})
});


frappe.ui.form.on("Advanced Settings", "update_categories", function (frm){
	var progress_bar = '<span id="progress_bar_text">Updating Subclass and Classifications</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frappe.call({
		method : "gaisano.gaisano.doctype.advanced_settings.advanced_settings.update_categories",
		args:{},
		callback:function(r){
			progress.hide();
			frappe.msgprint(r.message,"Done.", "blue");
		}
	})
});

frappe.ui.form.on("Advanced Settings", "delete_discount", function (frm){
	var progress_bar = '<span id="progress_bar_text">Deleting Supplier Discount</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frappe.call({
		method : "gaisano.gaisano.doctype.advanced_settings.advanced_settings.delete_discount",
		args:{
			"discount": frm.doc.discount_for_delete
		},
		callback:function(r){
			progress.hide();
			frappe.msgprint(r.message,"Done.", "blue");
		}
	})
});

frappe.ui.form.on("Advanced Settings", "update_po_date", function (frm) {
	var progress_bar = '<span id="progress_bar_text">Updating PO Dates</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    var po = frm.doc.po_wrong_date;
    frappe.call({
        method: "gaisano.usevents.update_po_dates",
        args:{
            "po":po
        },
        callback: function (r) {
            progress.hide();
        	if (r.message){
                frappe.msgprint(r.message, "Errors");
            }
            else{
                frappe.msgprint("Success!", "Message");

            }
        }
    });
});

frappe.ui.form.on("Advanced Settings", "delete_rename_button", function (frm) {
	var progress_bar = '<span id="progress_bar_text">Deleting the Old and Renaming the New...</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
    var args = {
    	"doctype": frm.doc.transfer_doctype,
		"old_doc": frm.doc.original_doc,
		"new_doc": frm.doc.new_doc
    };

    frappe.call({
        method: "gaisano.gaisano.doctype.advanced_settings.advanced_settings.delete_and_rename",
        args:args,
        callback: function (r) {
            progress.hide();
        	if (r.message){
                frappe.msgprint(r.message, "Errors");
            }
            else{
                frappe.msgprint("Success!", "Message");

            }
        }
    });
});




# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime

class ItemSubClassification(Document):
	pass


@frappe.whitelist()
def update_item_classification(category=None, classification=None, subclass=None):
	print category, classification, subclass
	rows = frappe.db.sql("""SELECT name, category, classification, parent from `tabItem Class Each` where sub_class =%s""", subclass)
	for row in rows:
		if (row[2] == classification) and (row[1]== category):
			continue
		else:
			if (category == "") or (category == None):
				frappe.db.sql("""UPDATE `tabItem Class Each` set category = NULL, classification = %s where name = %s""",
							  (classification, row[0]))
				frappe.db.sql("""UPDATE `tabItem Classification` set category = NULL where name = %s""", classification)
				frappe.db.sql("""DELETE from `tabCategory Classes` where classification = %s""", classification)
			else:
				frappe.db.sql("""UPDATE `tabItem Class Each` set category = %s, classification = %s where name = %s""", (category, classification, row[0]))
				frappe.db.sql("""UPDATE `tabItem Classification` set category = %s where name = %s""", (category, classification))
				frappe.db.sql("""DELETE from `tabCategory Classes` where classification =  %s""",classification)

				count = int(frappe.db.sql("""SELECT count(name) from `tabCategory Classes` where parent = %s""", category)[0][0]) + 1
				now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
				user = frappe.get_user().name
				frappe.db.sql("""INSERT INTO `tabCategory Classes` (name, creation, modified, modified_by, owner,
					docstatus, parent, parentfield, parenttype, idx, classification) VALUES
					(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
					(classification, now, now, user, user, 0, category, "classifications", "Item Category", count, classification))
			frappe.db.commit()
			item_doc = frappe.get_doc("Item", row[3])
			try:
				item_doc.save()
				frappe.db.commit()
			except:
				print "ERROR!"
				continue
	return "Finished."

@frappe.whitelist()
def update_item_subclass(newsubclass, subclass):
	rows = frappe.db.sql("""SELECT name, parent from `tabItem Class Each` where sub_class =%s""", subclass)
	for row in rows:
		frappe.db.sql("""UPDATE `tabItem Class Each` set sub_class = %s where name = %s""", (newsubclass, row[0]))
		frappe.db.commit()
		item_doc = frappe.get_doc("Item", row[1])
		try:
			item_doc.save()
			frappe.db.commit()
		except:
			print "ERROR!"
			continue
	return "Finished."
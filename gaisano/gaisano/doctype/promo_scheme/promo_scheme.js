// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Promo Scheme', "onload", function(frm){
		if(frm.doc.__islocal)
			frm.set_value("date_created", frappe.datetime.get_today());

		cur_frm.set_query("cellophane_type", function () {
            return {
                "filters": {
                    "type": "Cellophane"
                }
            }
        });
		cur_frm.set_query("barcode_type", function () {
		return {
			"filters": {
				"type": "Barcode"
			}
		};
		});
});

frappe.ui.form.on('Promo Scheme', {
	//UPDATE TOTAL PIECES AND SRP
	"packing": function(frm) {
		update_totals_and_srp(frm);
	},
	"allocation": function(frm){
		update_totals_and_srp(frm);
		if(is_not_empty(frm.doc.barcode_type))
			set_barcode_cost(frm);
	},
	"no_of_cases": function(frm){
		update_totals_and_srp(frm);
	},
	"take_x": function(frm){
		frm.set_value("allocation", parseFloat(frm.doc.take_x));
		set_promo_scheme_name(frm);
	},
	"buy_x":function(frm){
		set_promo_scheme_name(frm);
	},
	//UPDATE OTHER COSTS: Cellophane
	"cellophane_type":function(frm){
		if(is_not_empty(frm.doc.cellophane_type))
			set_cellophane_cost(frm, false);
		else
			set_cellophane_cost(frm, true);
	},
	"qty_cellophane":function(frm){
		if(is_not_empty(frm.doc.cellophane_type))
			set_cellophane_cost(frm, false);
		else
			set_cellophane_cost(frm, true);
	},
	//UPDATE OTHER COSTS: Barcode
	//UPDATE OTHER COSTS: Cellophane
	"barcode_type":function(frm){
		if(is_not_empty(frm.doc.barcode_type))
			set_barcode_cost(frm, false);
		else
			set_barcode_cost(frm, true)
	},
	"labor": function(frm){
		set_labor_cost(frm)
	},
	"qty_scotch_tape":function(frm){
		set_scotch_tape_cost(frm)
	},
	"sealer_hours": function(frm){
		set_sealer_cost(frm)
	}

});

//Get Total Value
frappe.ui.form.on("Promo Scheme",{
	"srp":function (frm) {
		get_total_value(frm);
    },
	"cellophane_cost":function (frm) {
		get_total_value(frm);
    },
	"barcode_cost":function (frm) {
		get_total_value(frm);
    },
	"labor_cost":function (frm) {
		get_total_value(frm);
    },
	"scotch_tape_cost":function (frm) {
		get_total_value(frm);
    },
	"sealer_cost":function (frm) {
		get_total_value(frm);
    }
});

frappe.ui.form.on("Promo Scheme", "host_item", function(frm){
	frappe.call({
		method: "frappe.client.get",
		args:{
			"doctype": "Item",
			"name": frm.doc.host_item
		},
		callback: function(r){
			if (r.message){
				console.log(r.message);
				//TODO: If from supplier :  Item Cost. if from Grocery: Item Price
				frm.set_value("host_item_srp", r.message['item_price']);
				set_promo_scheme_name(frm);
			}
		}
	})
});


frappe.ui.form.on("Promo Scheme", "free_item", function(frm){
	frappe.call({
		method: "frappe.client.get",
		args:{
			"doctype": "Item",
			"name": frm.doc.free_item
		},
		callback: function(r){
			if (r.message){
				console.log(r.message);
				//TODO: If from supplier :  Item Cost. if from Grocery: Item Price
				frm.set_value("free_item_srp", r.message['item_price']);
				frm.set_value("packing", r.message['packing']);
				set_promo_scheme_name(frm);
			}
		}
	})
});

function update_totals_and_srp(frm){
	if(frm.doc.scheme_type == "Buy X Take X"){
		frm.set_value("total_pieces", get_total_pieces(frm));
		frm.set_value("srp", get_total_srp(frm));
	}
	else if(frm.doc.scheme_type == "Save X Php"){
		//TODO:change SRP calculation based on items table.
	}
	else if(frm.doc.scheme_type == "Less X %"){
		//TODO:change SRP calculation : 1 item only.
	}
}
function get_total_pieces(frm){
	var packing = parseInt(frm.doc.packing);
	var alloc = parseInt(frm.doc.allocation);
	var cases = parseInt(frm.doc.no_of_cases);
	return packing*alloc*cases;
}

function get_total_srp(frm){
	var total_pieces = get_total_pieces(frm);
	var total_srp = get_srp(frm);
	return total_pieces*total_srp;
}

function get_srp(frm){
	var srp = frm.doc.free_item_srp;
	return srp;
}

function set_promo_scheme_name(frm){
	var buy_x = frm.doc.buy_x;
	var take_x = frm.doc.take_x;
	var host_item = frm.doc.host_item;
	var free_item = frm.doc.free_item;
	if(frm.doc.scheme_type == "Buy X Take X"){
		if((is_not_empty(free_item))&&(is_not_empty(host_item))&&(is_not_empty(buy_x))&&(is_not_empty(take_x))){
			var temp_string = "*BUY "+buy_x+" "+host_item+" TAKE "+take_x+" "+free_item;
			frm.set_value("scheme_name",temp_string);
		}
	}
	else if(frm.doc.scheme_type == "Save X Php"){
		//TODO:change SRP calculation based on items table.
	}
	else if(frm.doc.scheme_type == "Less X %"){
		//TODO:change SRP calculation : 1 item only.
	}

}

function is_not_empty(str){
	if((str!=undefined)&&(str!=0)&&(str!=""))
		return true;
	else
		return false;
}

function set_cellophane_cost(frm, set_zero){
	if (!set_zero)
		frappe.call({
			method : "frappe.client.get",
			args:{
				"doctype":"Bundling Item",
				name: frm.doc.cellophane_type
			},
			callback:function(r){
				frm.set_value("cellophane_cost", get_cost(parseFloat(frm.doc.qty_cellophane),parseFloat(r.message['cost'])));
			}
		});
	else{
		frm.set_value("cellophane_cost", 0);
	}
}

function set_barcode_cost(frm , set_zero){
	if(!set_zero)
		frappe.call({
			method : "frappe.client.get",
			args:{
				"doctype":"Bundling Item",
				name: frm.doc.barcode_type
			},
			callback:function(r){
				frm.set_value("barcode_price", parseFloat(r.message['cost']));
				frm.set_value("barcode_cost", get_cost(parseFloat(frm.doc.total_pieces),parseFloat(r.message['cost'])));
			}
		});
	else{
		frm.set_value("barcode_price", 0);
		frm.set_value("barcode_cost",0);
	}
}
function set_labor_cost(frm){
	frm.set_value("labor_cost", get_cost(parseFloat(frm.doc.total_pieces),parseFloat(frm.doc.labor)));
}
function set_scotch_tape_cost(frm){
	frappe.call({
		method : "frappe.client.get",
		args :{
			"doctype" : "Bundling Item",
			"name": "Scotch Tape"
		},
		callback:function(r){
			if(r.message){
				frm.set_value("scotch_tape_cost", get_cost(parseFloat(frm.doc.qty_scotch_tape),parseFloat(r.message['cost'])));
			}
			else{
				frappe.msgprint("Please Create an Item for Scotch Tape");
			}
		}
	});
}
function set_sealer_cost(frm){
	frappe.call({
		method : "frappe.client.get",
		args :{
			"doctype" : "Bundling Item",
			"name": "Sealer"
		},
		callback:function(r){
			if(r.message){
				frm.set_value("sealer_cost", get_cost(parseFloat(frm.doc.sealer_hours),parseFloat(r.message['cost'])));
			}
			else{
				frappe.msgprint("Please Create an Item for Sealer");
			}
		}
	});
}

function get_cost(qty,price){
		return (qty*price);
}

function get_total_value(frm){
	var cellophane = parseFloat(frm.doc.cellophane_cost);
	var barcode = parseFloat(frm.doc.barcode_cost);
	var labor = parseFloat(frm.doc.labor_cost);
	var tape = parseFloat(frm.doc.scotch_tape_cost);
	var sealer = parseFloat(frm.doc.sealer_cost);
	var srp = parseFloat(frm.doc.srp);
	var total = cellophane + barcode + labor + tape + sealer+srp;
	console.log(total);
	frm.set_value("total_scheme_value", total);
}

/**
 * PROMO SCHEME ITEMS FOR SAVE x PHP CODE
 * */

frappe.ui.form.on("Promo Scheme Item", "item_code", function(frm, cdt, cdn){
	var item = locals[cdt][cdn];
	frappe.call({
		method : "frappe.client.get",
		args:{
			"doctype": "Item",
			"name": item["item_code"]
		},
		callback: function(r){
			if(r.message){
				//TODO: If from supplier :  Item Cost. if from Grocery: Item Price
				if(frm.doc.taken_from == "Grocery") {
					frappe.model.set_value(cdt, cdn, "item_cost", r.message["item_price"]);
                    frm.refresh_field("items");
                }
				else if (frm.doc.taken_from == "Supplier") {
                    frappe.model.set_value(cdt, cdn, "item_cost", r.message["item_cost_with_freight"]);
                	frm.refresh_field("items");
				}
				else
					frappe.throw("Please identify if stocks are from Grocery or from Supplier in the Drop Down above.")

			}
		}
	});
});
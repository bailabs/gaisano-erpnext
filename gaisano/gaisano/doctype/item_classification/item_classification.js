// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Item Classification', {
	refresh: function(frm) {

	}
});

frappe.ui.form.on('Item Classification', 'transfer_items', function(frm){
	var subclass = frm.doc.subclass;

	if (!frm.doc.subclass){
		console.log("No subclass.");
		subclass = ""
	}
	var progress_bar = '<span id="progress_bar_text">Updating Item Classification</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frappe.call({
		method: "gaisano.gaisano.doctype.item_classification.item_classification.update_item_classification",
		args:{
			"old_classification":frm.doc.class_name,
			"new_classification":frm.doc.classification,
			"subclass": subclass
		},
		callback:function(r){
			progress.hide();
			if(r.message){
				frappe.msgprint(r.message,"Done", "Blue");
			}
		}
	});
});
// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on("Price Tag Generator", "get_items_with_srp_changes", function(frm){

	var progress_bar = '<span id="progress_bar_text">Fetching items with updated SRP</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frm.clear_table("items");
	frm.refresh_field("items");
	frappe.call({
		method: "gaisano.gaisano.doctype.price_tag_generator.price_tag_generator.get_items_with_srp_changes",
		args:{
			"modified":frm.doc.date_filter,
			"item_supplier":frm.doc.supplier,
			"modified_by":frm.doc.user_filter
		},
		callback: function(r){
			if(r.message){
				for(var i = 0; i < r.message.length; i++){
					var newrow = frm.add_child("items");
					newrow.item = r.message[i].item;
					newrow.item_description = r.message[i].item_description;
					newrow.barcode = r.message[i].barcode;
					newrow.srp = r.message[i].srp;
					newrow.user_modified = r.message[i].user_modified;
					console.log(r.message[i].item+" | ", r.message[i].srp);
					progress.hide();
					frm.refresh_field("items");

				}
			}
			else{
				frappe.msgprint("No Items were Updated on "+frm.doc.date_filter+"")
				progress.hide()
			}
		}
	});
});

// frappe.ui.form.on('Price Tag Generator', {
	// after_save: function(frm) {
		// open_print_window(frm.doc.tag_type);
	// }
// });

frappe.ui.form.on("Price Tag Generator", "onload", function(frm){
	var dateObj = new Date();
	var month = dateObj.getUTCMonth() + 1;
	var day = dateObj.getUTCDate() ;
	var year = dateObj.getUTCFullYear();
	console.log(day < 10 ? '0' + date : date);
	newdate = year + "-" + month + "-" + day;
	frm.set_value("date_filter",newdate);
})

frappe.ui.form.on("Price Tag Generator", "get_items", function(frm){
	var progress_bar = '<span id="progress_bar_text">Fetching items with updated SRP</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    });
    progress.fields_dict.progress.$wrapper.html(progress_bar);
    progress.show();
	frm.clear_table("items");
	frm.refresh_field("items");
	frappe.call({
		method: "gaisano.gaisano.doctype.price_tag_generator.price_tag_generator.get_items",
		args:{
			"modified":frm.doc.date_filter,
			"item_supplier":frm.doc.supplier,
			"modified_by":frm.doc.user_filter
		},
		callback: function(r){
			if(r.message){
				for(var i = 0; i < r.message.length; i++){
					var newrow = frm.add_child("items");
					newrow.item = r.message[i].item;
					newrow.item_description = r.message[i].item_description;
					newrow.barcode = r.message[i].barcode;
					newrow.srp = r.message[i].srp;
					newrow.user_modified = r.message[i].user_modified;
					console.log(r.message[i].item+" | ", r.message[i].srp);
					progress.hide();
					frm.refresh_field("items");

				}
			}
			else{
				frappe.msgprint("No Items Under "+frm.doc.supplier+" were Updated on "+frm.doc.date_filter+"")
				progress.hide()
			}
		}
	});
});

frappe.ui.form.on("Price Tag Generator", "reset_form", function(frm){
	frm.clear_table("items");
	frm.refresh_field("items");
	frm.set_value("date_filter","");
	frm.set_value("user_filter","");
	frm.set_value("supplier","");
});

frappe.ui.form.on("Price Tag Generator", "generate_print", function(frm){
	console.log(frm.doc.__unsaved);
	frm.save();
});

frappe.ui.form.on("Price Tag Item", "item", function(frm, cdt, cdn){
	frappe.call({
		method: "frappe.client.get",
		args:{
			"doctype":"Item",
			"name":locals[cdt][cdn].item
		},
		callback: function(r){
			if(r.message){
				var ean_barcode = (r.message["item_price_retail_with_margin"].toFixed(2)).replace(".","");
				ean_barcode = barcode(ean_barcode);
				frappe.model.set_value(cdt, cdn, "item",r.message["name"]);
				frappe.model.set_value(cdt, cdn, "srp",r.message["item_price_retail_with_margin"]);
				frappe.model.set_value(cdt, cdn, "item_description",r.message["item_name_dummy"]);
				frappe.model.set_value(cdt, cdn, "barcode",r.message["barcode_retial"]);
				frappe.model.set_value(cdt, cdn, "ean_barcode", ean_barcode);
				frappe.model.set_value(cdt, cdn, "user_modified",r.message["modified_by"]);
				frm.refresh_field("items");
			}
			if (frm.doc.checking_mode == 1) {
				frappe.prompt([
						{'fieldname': 'barcode', 'fieldtype': 'Data', 'label': 'Barcode', 'reqd': 1}
					],
					function (values) {
						var scanned_srp = parseInt(values.barcode);
						scanned_srp = scanned_srp.toString();
						scanned_srp = scanned_srp.slice(0, -1);
						scanned_srp = scanned_srp.substring(0, scanned_srp.length - 2) + "." + scanned_srp.substring(scanned_srp.length - 2);
						if (r.message["item_price_retail_with_margin"] != scanned_srp) {
							frappe.msgprint("This Item price has updated. Please print");
						}
					},
					'Ean 8'
				)
			}

		}
	});
});


function barcode(ean_barcode) {
	while(ean_barcode.length < 7)
	{
		ean_barcode = "0" + ean_barcode;
	}
	odd = parseInt(ean_barcode.charAt(0)) + parseInt(ean_barcode.charAt(2)) + parseInt(ean_barcode.charAt(4)) + parseInt(ean_barcode.charAt(6));
	even = parseInt(ean_barcode.charAt(1)) + parseInt(ean_barcode.charAt(3)) + parseInt(ean_barcode.charAt(5));
	var last_digit= ( 10 - [ (3 * odd + even) % 10 ] ) % 10
	ean_barcode = ean_barcode + last_digit
	return ean_barcode;
}


function open_print_window(tag_type){
	if(tag_type=="Regular Price Tag")
		window.open("print?doctype=Price%20Tag%20Generator&name=Price%20Tag%20Generator&trigger_print=1&format=Price%20Tag%20Format&no_letterhead=0");
	else
		window.open("print?doctype=Price%20Tag%20Generator&name=Price%20Tag%20Generator&format=Promo%20Tag&no_letterhead=0")
}
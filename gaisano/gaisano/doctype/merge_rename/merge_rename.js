// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Merge Rename', {
	refresh: function(frm) {
		console.log(frm.doc);
		cur_frm.set_query("source_doctype", function(frm) {
        return {
            "filters": [
            	['DocType', 'name', 'in', 'Item, Supplier, Warehouse']
			]
        };
	});
	},
	"source_doctype":function (frm) {
		frm.clear_table("rename_items");
		frm.clear_table("merge_items");
		frm.refresh_field("rename_items");
		frm.refresh_field("merge_items");
    },
	"type":function (frm) {
		if(frm.doc.type =="Rename")
			transfer_items("merge_items", "rename_items", frm);
		else if(frm.doc.type =="Merge")
			transfer_items("rename_items", "merge_items", frm);
		else{
			//do nothing
		}
    }
});

frappe.ui.form.on("Merge Rename", "before_save", function(frm){
	frm.clear_table("merge_rename_items");
	if(frm.doc.rename_items.length){
		var rows = frm.doc.rename_items;
		for(var i = 0; i<rows.length; i++){
			var newrow = frm.add_child("merge_rename_items");
			newrow.source_doctype = rows[i].source_doctype;
			newrow.old_name = rows[i].wrong_doc;
			newrow.new_name = rows[i].correct_name;
			newrow.is_merge = 0;
		}
		frm.refresh_field("merge_rename_items");
	}
	else{
		var rows = frm.doc.merge_items;
		for(var i = 0; i<rows.length; i++){
			var newrow = frm.add_child("merge_rename_items");
			newrow.source_doctype = rows[i].source_doctype;
			newrow.old_name = rows[i].wrong_doc;
			newrow.new_name = rows[i].correct_doc;
			newrow.is_merge = 1;
		}
		frm.refresh_field("merge_rename_items");
	}
});

frappe.ui.form.on("Merge Rename", "apply_changes",function(frm){
	var progress_bar = '<span id="progress_bar_text">Updating Data</span><div class="progress">' +
		'<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
		'</div></div>';
	var progress = new frappe.ui.Dialog({
		title: "Loading...",
		fields: [
			{"fieldtype": "HTML", "fieldname": "progress"}]
	});
	progress.fields_dict.progress.$wrapper.html(progress_bar);
	progress.show();
	frappe.call({
		"method":"gaisano.gaisano.doctype.merge_rename.merge_rename.rename_and_submit",
		"args":{
			"docname":frm.doc.name,
			"item_list":frm.doc.merge_rename_items
		},
		"callback":function(r){
			progress.hide();
			frappe.msgprint(r.message);
			frm.reload_doc();
		}
	});
});

frappe.ui.form.on("Rename Item","rename_items_add", function (frm,cdt,cdn) {
	var source_doctype = frm.doc.source_doctype;
	frappe.model.set_value(cdt, cdn, "source_doctype",source_doctype);
	frm.refresh_field("rename_items");
});

frappe.ui.form.on("Merge Item","merge_items_add", function (frm,cdt,cdn) {
	var source_doctype = frm.doc.source_doctype;
	frappe.model.set_value(cdt, cdn, "source_doctype",source_doctype);
	frm.refresh_field("merge_items");
});

function transfer_items(from_table, to_table, frm){
	var from_=frm.doc[from_table];
	var to_= frm.doc[to_table];

	console.log(from_table, to_table);
	if(from_.length){
		frm.clear_table(to_table);
		for(var i = 0; i< from_.length; i++){
			var new_row = cur_frm.add_child(to_table);
			new_row.source_doctype = from_[i].source_doctype;
			new_row.wrong_doc = from_[i].wrong_doc;
		}
		frm.clear_table(from_table);
		frm.refresh_field(from_table);
		frm.refresh_field(to_table);
	}
}
// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Transfer Items in Supplier Discounts', {
	refresh: function(frm) {

	},
	"transfer_discount":function(frm){
		var progress_bar = '<span id="progress_bar_text">Updating Items</span><div class="progress">' +
		'<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
		'</div></div>';
		var progress = new frappe.ui.Dialog({
			title: "Loading...",
			fields: [
				{"fieldtype": "HTML", "fieldname": "progress"}]
		});
		progress.fields_dict.progress.$wrapper.html(progress_bar);
		progress.show();
		frappe.call({
			"method": "gaisano.gaisano.doctype.transfer_items_in_supplier_discounts.transfer_items_in_supplier_discounts.transfer_items_in_discount",
			"args":{
				"items":frm.doc.items,
				"doc": frm.doc
			},
			"callback":function(r){
				progress.hide();
				frm.reload_doc();
			}
		});
	},
	"transfer_supplier":function(frm){
		frappe.call({
			"method": "gaisano.gaisano.doctype.transfer_items_in_supplier_discounts.transfer_items_in_supplier_discounts.transfer_items_in_supplier",
			"args":{
				"from_supplier":frm.doc.source_supplier,
				"to_supplier":frm.doc.target_supplier,
				"doc": frm.doc
			},
			"callback":function(r){
				frm.reload_doc();
			}
		});
	}
});

frappe.ui.form.on("Transfer Items in Supplier Discounts", "create_rows", function(frm){
    frappe.call({
        "method": "gaisano.events.get_all_branches",
        "args" : {},
        "callback": function (r) {
            if (r.message){
                frm.clear_table("items");
                frm.refresh_field("items");
                for(var i = 0; i< r.message.length; i++)
                {
                    //console.log(r.message[i]);
                    var newrow = cur_frm.add_child("items");
                    newrow.branch = r.message[i];
                    frm.refresh_field("items");
                }
            }
        }
    });
});

frappe.ui.form.on("Discount For Transfer", "source_discount",function(frm, cdt, cdn){
	console.log('Source Discount');
	var discount = locals[cdt][cdn].source_discount;
	frappe.call({
		"method":"frappe.client.get",
		"args": {"doctype":"Supplier Discounts", "name": discount},
		"callback": function (r) {
			if(r.message){
				frappe.model.set_value(cdt, cdn, "source_discount_name", r.message.supplier_discount_name);
			}
        }
	});
});

frappe.ui.form.on("Discount For Transfer", "target_discount", function(frm, cdt, cdn){
	console.log('Target Discount');
	var discount = locals[cdt][cdn].target_discount;
	frappe.call({
		"method":"frappe.client.get",
		"args": {"doctype":"Supplier Discounts", "name": discount},
		"callback": function (r) {
			if(r.message){
					frappe.model.set_value(cdt, cdn, "target_discount_name", r.message.supplier_discount_name);
			}
		}
	});
});


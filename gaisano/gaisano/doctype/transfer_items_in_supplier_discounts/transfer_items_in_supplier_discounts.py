# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, json
from frappe.model.document import Document
from gaisano.events import insert_item_to_supplier_discount

class TransferItemsinSupplierDiscounts(Document):
	pass

@frappe.whitelist()
def transfer_items_in_discount(items, doc):
	doc = json.loads(doc)
	try:
		if doc['__islocal']:
			frappe.throw("Please Save the Document")
	except:
		if "New Transfer Items" not in doc['name']:
			print "ITEM IS SAVED"
			data = []
			doc_name = doc['name']
			items = json.loads(items)
			print doc_name
			for item in items:
				branch, from_discount, to_discount, new_discount_name = item['branch'], item['source_discount'], item['target_discount'], item['target_discount_name']
				item_list = frappe.db.sql("""SELECT parent from `tabItem Branch Discount` where branch = %s and supplier_discount = %s""",(branch, from_discount))
				update_items_modified(item_list)
				frappe.db.sql("""UPDATE `tabItem Branch Discount` set supplier_discount = %s, supplier_discount_name = %s
									where branch = %s and supplier_discount = %s""",(to_discount, new_discount_name, branch, from_discount))
				data = create_item_list(data, item_list)
				frappe.db.commit()
			add_to_supplier_discount_items(data)
			submit_document(doc_name)

def update_items_modified(items):
	user = frappe.get_user()
	print user.name
	for item in items:
		frappe.db.sql("""UPDATE `tabItem` set modified = NOW(), modified_by=%s where name = %s""",(user.name,item[0]))

def submit_document(doc_name):
	frappe.db.sql("""UPDATE `tabTransfer Items in Supplier Discounts` set docstatus = 1 where name = %s""", doc_name)
	frappe.db.sql("""UPDATE `tabDiscount For Transfer` set docstatus = 1 where parent = %s""", doc_name)

@frappe.whitelist()
def transfer_items_in_supplier(doc, from_supplier, to_supplier):
	doc = json.loads(doc)
	try:
		if doc['__islocal']	:
			frappe.throw("Please Save the Document")
	except:
		if "New Transfer Items" not in doc['name']:
			print "Not a new doc"
			items = frappe.db.sql("""SELECT parent from `tabItem Supplier` where supplier = %s""", from_supplier)
			frappe.db.sql("""UPDATE `tabItem Supplier` set supplier = %s where supplier = %s""",(to_supplier, from_supplier))
			update_items_modified(items)
			frappe.db.commit()
			submit_document(doc['name'])

def create_item_list(data, items):
	for item in items:
		if item[0] not in data:
			data.append(item[0])
	return data


def add_to_supplier_discount_items(item_list):
	for item in item_list:
		item_doc = frappe.get_doc("Item", item)
		insert_item_to_supplier_discount(item_doc)
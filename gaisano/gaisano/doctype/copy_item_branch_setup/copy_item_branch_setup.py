# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class CopyItemBranchSetup(Document):
	def validate(self):
		if not((self.copy_supplier_discounts) or (self.copy_branch_availability)):
			frappe.throw("Please check at least one of the checkboxes!")
		if self.source_branch == self.to_branch:
			frappe.throw("Source Branch (Copy From) SHOULD NOT be the same as the Copy To branch!")
	pass

@frappe.whitelist()
def copy_item_setup_to_branch(name, from_branch, to_branch, copy_supplier_discounts = None, copy_branch_availability = None):
	if from_branch == to_branch:
		frappe.throw("Source Branch (Copy From) SHOULD NOT be the same as the Copy To branch!")
	if not ((copy_supplier_discounts) or (copy_branch_availability)):
		frappe.throw("Please check at least one of the checkboxes!")
	if copy_supplier_discounts==str(1):
		print "copy supplier discounts", from_branch, to_branch
		insert_branch_discount(from_branch, to_branch)
	if copy_branch_availability==str(1):
		print "copy branch availability"
		insert_branch_availability(from_branch, to_branch)
	this_doc = frappe.get_doc("Copy Item Branch Setup",name)
	this_doc.submit()
	frappe.db.commit()
	return "DONE"

def insert_branch_discount(from_branch, to_branch):
	items=frappe.db.sql("""SELECT disc.* from `tabItem Branch Discount` disc join `tabItem` itm on itm.name = disc.parent
							where disc.branch = %s and itm.type != 'Disabled' and itm.type!='Company Bundling'""", from_branch, as_dict= True)
	for item in items:
		item_code = item['parent']
		discount, discount_name = item['supplier_discount'], item['supplier_discount_name']
		if branch_discount_exists(item_code, to_branch):
			print "UPDATE ONLY.", item_code, to_branch
			frappe.db.sql("""UPDATE `tabItem Branch Discount` set supplier_discount=%s, supplier_discount_name=%s where branch = %s and parent = %s """,
						  (discount, discount_name, to_branch, item_code))
		else:
			item['branch']=to_branch
			item['doctype']="Item Branch Discount"
			del item['name']
			idx = get_idx('tabItem Branch Discount', item_code)
			item['idx'] = (idx if idx is not None else 0)+1
			print item
			branch_discount = frappe.get_doc(item)
			branch_discount.insert()
			#print "INSERT into:", item_code, to_branch, discount
		#frappe.db.sql("""UPDATE `tabItem` set modified = NOW(), modified_by="Administrator" where name = %s""",item_code)
		#frappe.db.commit()

def insert_branch_availability(from_branch, to_branch):
	items=frappe.db.sql("""SELECT branch.* from `tabItem Branch` branch join `tabItem` itm on itm.name = branch.parent
						  where branch.parenttype = 'Item' and branch.branch = %s and itm.type!='Disabled' and
						  itm.type!='Company Bundling'""", from_branch, as_dict= True)
	for item in items:
		item_code = item['parent']
		if branch_availability_exists(item_code, to_branch):
			#print "EXISTS FOR ",item_code, to_branch, ". Continue."
			continue
		else:
			item['branch'] = to_branch
			item['doctype'] = "Item Branch"
			del item['name']
			idx = get_idx('tabItem Branch', item_code)
			item['idx'] = (idx if idx is not None else 0) + 1
			#print item
			branch_availability= frappe.get_doc(item)
			branch_availability.insert()
			#print "INSERT into:", item_code, to_branch
		#frappe.db.sql("""UPDATE `tabItem` set modified = NOW(), modified_by="Administrator" where name = %s""",item_code)
		#frappe.db.commit()

def branch_discount_exists(item_code, branch):
	count_ = frappe.db.sql("""SELECT COUNT(*) from `tabItem Branch Discount` where parent = %s and branch = %s""",(item_code, branch))
	print count_
	if count_[0][0] > 0:
		return True
	else:
		return False

def branch_availability_exists(item_code, branch):
	count_ = frappe.db.sql("""SELECT COUNT(*) from `tabItem Branch` where parent = %s and branch = %s""",(item_code, branch))
	if count_[0][0] > 0:
		return True
	else:
		return False

def get_idx(table_name, parent):
	return frappe.db.sql("""SELECT MAX(idx) from `{0}` where parent = '{1}'""".format(table_name, parent))[0][0]
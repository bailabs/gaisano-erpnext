# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from gaisano.events import get_branch_discount
import time
from datetime import datetime, date, time, timedelta

class SupplierDiscounts(Document):
    def validate(self):
        print "VALIDATE Function"
        temp_name = self.supplier_name + " " + self.disc_name_dummy
        self.supplier_discount_name = temp_name
        branch = frappe.db.get_value("Server Information", None, "branch")
        print self.discount_id, self.discount_code, "existing data for insert/save"
        if branch == "CDO Main":
            if self.is_new():
                new_id = get_last_id()
                print new_id, "NEW_ID"
                if self.discount_id != new_id:
                    self.discount_id = get_last_id()
                    self.discount_code = "DISC-" + str(self.discount_id).zfill(6)
                    self.name = self.discount_code
            else:
                create_discount_history(self)
                update_item_branch_discount_name(self.name, self.supplier_discount_name)
        else:
            self.discount_code = "DISC-" + str(self.discount_id).zfill(6)
            self.name = self.discount_code
        print self.name, "-Discount Name from Validate"
        temp_id = int(str(self.name)[5:])
        if (self.discount_id != temp_id):
            self.discount_id = temp_id
            print temp_id, "Update Discount ID--------------___#####"
        itemlist = create_item_list(self.items)
        print(itemlist)

        for item in self.items:
            print(item)
            check_dup_items(item.items, itemlist)
    def run_post_save_methods(self):
        create_discount_history(self)

#check if duplicate items exist on save
def check_dup_items(item, itemlist):
    item_count = itemlist.count(item)
    error_str = "Duplicate entries found: " +str(item_count-1) + "duplicate for " + str(item)
    if item_count>1:
        frappe.throw(error_str)

def create_item_list(items):
    itemlist = []
    for item in items:
        itemlist.append(item.items)
    return itemlist

@frappe.whitelist()
def update_supplier_discount_items(discount):
    supplier_discount = frappe.get_doc("Supplier Discounts", discount)
    branch = frappe.db.get_value("Server Information",None, "branch")
    items = supplier_discount.items
    temp_name = supplier_discount.supplier_name + " " + supplier_discount.disc_name_dummy
    if (supplier_discount.name != temp_name) and ("New Supplier Discounts" in supplier_discount.name):
        frappe.throw("Please Save the Document.")
    else:
        item_discount = supplier_discount.name

    print "ITEM DISCOUNT IS: " + item_discount
    for item in items:
        this_item = frappe.get_doc("Item", item.items)
        delete_existing_discounts(this_item.name, item_discount)
        this_item.supplier_discount = item_discount
        #print this_item.name, this_item.supplier_discount, this_item.supplier_items
        frappe.db.sql("""UPDATE `tabItem Branch Discount` set supplier_discount = %s, supplier_discount_name = %s where name = %s""",
                      (item_discount, this_item.name))
        frappe.db.commit()
        if len(this_item.supplier_items) == 1:
            if this_item.supplier_items[0].supplier != supplier_discount.supplier_name:
                frappe.db.sql("""UPDATE `tabItem Supplier` set supplier = %s where name = %s""",
                              (supplier_discount.supplier_name, this_item.supplier_items[0].name))
        frappe.db.commit()
        save_item(this_item)
    return "Finished updating Items"

def save_item(doc):
    try:
        doc.save()
    except:
        print "error on save for : "+doc.name
    else:
        doc.save()

def delete_existing_discounts(itemcode, supplier_discount):
    print "LOOKING FOR DUPLICATES for "+itemcode+ " with new discount "+ supplier_discount
    other_discounts = frappe.db.sql("""SELECT name, parent from `tabSupplier Discount Items` where parent != %s and items = %s""", (supplier_discount, itemcode))
    for discount in other_discounts:
        print "found duplicate in : "+ discount[1]
        frappe.db.sql("""DELETE FROM `tabSupplier Discount Items` where name = %s""", discount[0])
        frappe.db.commit()

@frappe.whitelist()
def next_po_date(po_date, frequency, lead_time):
    lead_time = int(lead_time)
    date = po_date
    converted_date = datetime.strptime(date, '%Y-%m-%d').date()
    schedule = converted_date.strftime("%A")
    left = ""

    if frequency == "Monthly":
        number_of_days = 30
        next_po_date = converted_date + timedelta(days=30)

        for y in range(1, 7):
            left_date = next_po_date - timedelta(days=y)
            if left_date.strftime('%A') == schedule:
                left = y

        next_po_date = converted_date + timedelta(days=30)
        rr_date = converted_date + timedelta(days=lead_time)
        next_rr_date = converted_date + timedelta(days=30) + timedelta(days=lead_time)

    else:
        if frequency == "Weekly":
            days = 7
        elif frequency == "Bi-monthly":
            days = 14

        next_po_date = converted_date + timedelta(days=days)
        rr_date = converted_date + timedelta(days=lead_time)
        next_rr_date = converted_date + timedelta(days=days) + timedelta(days=lead_time)

    return {"next_po_date": next_po_date, "po_schedule": schedule, "rr_date": rr_date, "next_rr_date": next_rr_date}

#TODO: On new discount, auto-update discount_id.
@frappe.whitelist()
def get_last_id():
    last_id = frappe.db.sql("""SELECT MAX(discount_id) from `tabSupplier Discounts`""")
    temp = check_existing(last_id[0][0])
    #print temp
    return temp

def check_existing(discount_id):
    count_discounts = frappe.db.sql("""SELECT count(discount_id) from `tabSupplier Discounts` where discount_id = %s""", discount_id)
    #print count_discounts[0][0], " existing for ", discount_id
    if (count_discounts[0][0])>0:
        discount_id +=1
        discount_id = check_existing(discount_id)
    else:
        #print discount_id, "Okay!"
        discount_id = discount_id
    return discount_id

def create_discount_history(doc):
    previous_record = check_existing_discount_history(doc.name)
    if previous_record is not None:
        if (previous_record['new_disc_1']==doc.disc_1) and (previous_record['new_disc_2']==doc.disc_2) and (previous_record['new_disc_3'] == doc.disc_3) and (previous_record['new_disc_4']==doc.disc_4) and (previous_record['new_bo_1']==doc.bo_disc_1) and (previous_record['new_bo_2']==doc.bo_disc_2) and (previous_record['new_bo_3']==doc.bo_disc_3) and (previous_record['new_bo_4']==doc.bo_disc_4):
            print "DISCOUNTS MATCH. DO NOT CREATE NEW."
        else:
            #print "create new."
            new_edit_history = {
                "doctype": "Supplier Discount Edit History",
                "supplier_discount": doc.name,
                "supplier_discount_name": doc.supplier_discount_name,
                "updated_by": frappe.get_user().name,
                "updated_on": datetime.now(),
                "prev_disc_1": previous_record['new_disc_1'],
                "prev_disc_2": previous_record['new_disc_2'],
                "prev_disc_3": previous_record['new_disc_3'],
                "prev_disc_4": previous_record['new_disc_4'],
                "prev_bo_1": previous_record['new_bo_1'],
                "prev_bo_2": previous_record['new_bo_2'],
                "prev_bo_3": previous_record['new_bo_3'],
                "prev_bo_4": previous_record['new_bo_4'],
                "new_disc_1": doc.disc_1,
                "new_disc_2": doc.disc_2,
                "new_disc_3": doc.disc_3,
                "new_disc_4": doc.disc_4,
                "new_bo_1": doc.bo_disc_1,
                "new_bo_2": doc.bo_disc_2,
                "new_bo_3": doc.bo_disc_3,
                "new_bo_4": doc.bo_disc_4
            }
            #print new_edit_history
            history_doc = frappe.get_doc(new_edit_history)
            history_doc.insert()
            frappe.db.commit()
            print "Created New Discount History."
            update_item_actual_margin_rates(doc)

    else:
        print "INSERT NEW EXISTING DISCOUNT HISTORY"
        new_edit_history = {
            "doctype": "Supplier Discount Edit History",
            "supplier_discount": doc.name,
            "supplier_discount_name": doc.supplier_discount_name,
            "updated_by": frappe.get_user().name,
            "updated_on": datetime.now(),
            "prev_disc_1": "",
            "prev_disc_2": "",
            "prev_disc_3": "",
            "prev_disc_4": "",
            "prev_bo_1": "",
            "prev_bo_2": "",
            "prev_bo_3": "",
            "prev_bo_4": "",
            "new_disc_1": doc.disc_1,
            "new_disc_2": doc.disc_2,
            "new_disc_3": doc.disc_3,
            "new_disc_4": doc.disc_4,
            "new_bo_1": doc.bo_disc_1,
            "new_bo_2": doc.bo_disc_2,
            "new_bo_3": doc.bo_disc_3,
            "new_bo_4": doc.bo_disc_4
        }
        print new_edit_history
        history_doc = frappe.get_doc(new_edit_history)
        history_doc.insert()
        update_item_actual_margin_rates(doc)
        frappe.db.commit()


def check_existing_discount_history(supplier_discount):
    recent_discount_history = frappe.db.sql("""SELECT * from `tabSupplier Discount Edit History` where supplier_discount =%s order by updated_on desc limit 1""", supplier_discount, as_dict = True)
    if len(recent_discount_history)>0:
        return recent_discount_history[0]
    else:
        return None

def update_item_branch_discount_name(discount, discount_name):
    print "------------UPDATE SUPPLIER DISCOUNT NAME IN ITEMS---------------"
    frappe.db.sql("""UPDATE `tabItem` set modified = NOW() where name in (SELECT parent from `tabItem Branch Discount` where supplier_discount = %s and supplier_discount_name !=%s)""", (discount, discount_name))
    frappe.db.sql("""UPDATE `tabItem Branch Discount` set supplier_discount_name = %s where supplier_discount = %s""",
                  (discount_name, discount))
    frappe.db.commit()

@frappe.whitelist()
def update_item_supplier(discount, supplier):
    print "_____________________ UPDATE ITEM SUPPLIER ________________________"
    items = frappe.db.sql("""SELECT DISTINCT (disc.parent) from `tabItem Branch Discount` disc inner join `tabItem Supplier` sup
                            on sup.parent = disc.parent where disc.supplier_discount = %s and sup.supplier !=%s""",(discount,supplier))
    for item in items:
        print item[0]
        item_code = item[0]
        frappe.db.sql("""UPDATE `tabItem Supplier` sup, `tabItem` itm set sup.supplier = %s, itm.modified = NOW() where
                          sup.parent = itm.name and sup.parent = %s and sup.supplier != %s""", (supplier,item_code,supplier))
        frappe.db.commit()

def update_item_actual_margin_rates(disc):
    server_branch = frappe.db.get_value("Server Information", None, "branch")
    print server_branch, disc.name, "UPDATE ITEM ACTUAL MARGIN RATE"
    items = frappe.db.sql("""SELECT distinct(parent) from `tabItem Branch Discount` where branch = %s and supplier_discount =%s""",(server_branch, disc.name))
    print disc.name, disc.supplier_discount_name, disc.disc_1, disc.disc_2, disc.disc_3, disc.disc_4
    for item in items:
        item_doc = frappe.get_doc("Item", item[0])
        net_cost = item_doc.item_cost_with_freight * (1 - (float(disc.disc_1)/100)) * (1 - (float(disc.disc_2)/100)) * (1 - (float(disc.disc_3)/100)) * (1 - (float(disc.disc_4)/100))
        print item[0], net_cost
        net_cost_retail = net_cost/item_doc.packing
        actual_ws_margin = 0 if net_cost == 0 else ((item_doc.item_price - net_cost)/net_cost)*100
        actual_ret_margin = 0 if net_cost_retail == 0 else ((item_doc.item_price_retail_with_margin - net_cost_retail) / net_cost_retail) * 100
        frappe.db.sql("""UPDATE `tabItem` set net_cost = %s, actual_retail_rate =%s, actual_wholesale_rate = %s, modified = NOW() where name = %s""",(net_cost, actual_ret_margin, actual_ws_margin, item[0]))
        frappe.db.commit()
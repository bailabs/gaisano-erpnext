# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class UploadOfftake(Document):
	pass


@frappe.whitelist()
def get_item_code(barcode,name):
	item = frappe.db.sql("""SELECT name, item_code from `tabItem` where barcode_retial = %s""", barcode)
	item_name = ""
	if len(item) != 0:
		for i in item: #I think there should be only 1 item. But welp.
			item_name = i[0]
		return item_name
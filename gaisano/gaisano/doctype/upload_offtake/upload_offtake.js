// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Upload Offtake', {
	refresh: function(frm) {
		var barcode = frm.doc.barcode;
		if ((barcode !="")&&(barcode!=undefined)){
			setItem(barcode, frm);
		}
	}
});

frappe.ui.form.on('Upload Offtake', "item", function(frm){
	item = frm.doc.item;
	frappe.call({
		method : "frappe.client.get",
		args:{
			doctype : "Item",
			name:item
		},
		callback: function(data){
			if (data.message){
				console.log(data.message)
				frm.set_value("barcode", data.message.barcode_retial);
			}
		}
	});

});

function setItem(barcode, frm){
	frappe.call({
		method: "gaisano.gaisano.doctype.upload_offtake.upload_offtake.get_item_code",
		args: {
			"barcode":barcode,
			"name": frm.doc.name
		},
		callback:function(data){
			if(data.message)
			{
				console.log(data.message)
				frm.set_value("item", data.message);
			}


		}
	});
}
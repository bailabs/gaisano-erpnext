# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class DepartmentCode(Document):
	def validate(self):
		owner = frappe.get_doc("User", self.owner).first_name
		modified_by = frappe.get_doc("User", self.modified_by).first_name
		self.modify = modified_by
		self.date_mod = self.modified
		self.append = owner
		self.date_app = self.creation
	pass

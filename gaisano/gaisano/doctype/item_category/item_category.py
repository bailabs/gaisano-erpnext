# -*- coding: utf-8 -*-
# Copyright (c) 2015, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json

class ItemCategory(Document):
	pass

@frappe.whitelist()
def update_everything(class_list, category):
	class_list = json.loads(class_list)
	for item in class_list:
		print item["classification"]
		frappe.db.sql("""UPDATE `tabItem Class Each` set category = %s where classification = %s """, (category, item["classification"]))
		frappe.db.sql("""UPDATE `tabItem Classification` set category = %s where name = %s """, (category, item["classification"]))
		frappe.db.sql("""UPDATE `tabItem Sub Classification` set category = %s where classification = %s""", (category, item["classification"]))
		frappe.db.commit()
		check_for_existing(item["classification"], category)
	return "Done."

def check_for_existing(classification, category):
	duplicates = frappe.db.sql("""SELECT name from `tabCategory Classes` where parent!=%s and classification =%s""", (category, classification))
	if len(duplicates)>0:
		frappe.db.sql("""DELETE from `tabCategory Classes` where parent!=%s and classification =%s""",
					  (category, classification))
		frappe.db.commit()

def update_items(classification):
	for item in frappe.db.sql("""Select parent, name from `tabItem Class Each` where classification = %s""", classification):
		try:
			item_doc = frappe.get_doc("Item", item[0])
			item_doc.save()
		except:
			continue
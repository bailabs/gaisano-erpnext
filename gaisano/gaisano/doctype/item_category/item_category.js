// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.ui.form.on('Item Category', {
	refresh: function(frm) {

	}
});

frappe.ui.form.on('Item Category', {
	"update_items": function(frm) {
		var class_list = frm.doc.classifications;
		var category_name = frm.doc.category_name;
		var args = {
				"class_list":class_list,
				"category":category_name
			};

		var progress_bar = '<span id="progress_bar_text">Updating Items</span><div class="progress">' +
        '<div id = "progress_bar" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="01" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
        '</div></div>';
    	var progress = new frappe.ui.Dialog({
        title: "Loading...",
        fields: [
            {"fieldtype": "HTML", "fieldname": "progress"}]
    	});
		progress.fields_dict.progress.$wrapper.html(progress_bar);
		progress.show();

		frappe.call({
			method: "gaisano.gaisano.doctype.item_category.item_category.update_everything",
			args: args,
			callback:function(r){
				progress.hide();
				frappe.msgprint(r.message, "Done", "blue");
			}
		});
	}
});
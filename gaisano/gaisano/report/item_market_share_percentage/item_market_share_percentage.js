// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Item Market Share Percentage"] = {
	"filters": [
		{
            "fieldname": "from_date",
            "label": __("From Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "to_date",
            "label": __("To Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "category",
            "label": __("Item Classification"),
            "fieldtype": "Link",
			"options": "Item Classification",
            "reqd": 1
        },
        {
            "fieldname": "subclass",
            "label": __("Subclass"),
            "fieldtype": "Link",
			"options": "Item Sub Classification",
            "reqd": 0
        },
        {
            "fieldname": "volumne",
            "label": __("Volume Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
            "default":40
        },
        {
            "fieldname": "peso_value",
            "label": __("Peso Value Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
            "default":40
        },
        {
            "fieldname": "profit",
            "label": __("Profit Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
            "default":20
        },
        {
            "fieldname": "get_totals",
            "label": __("Total Per Company and Per Subclass?"),
            "fieldtype": "Check",
            "reqd": 0
        },
        {
            "fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "reqd": 1
        },
        {
			"fieldname" : "report_password",
			"fieldtype" : "Password",
			"label" : "Password for Show Peso Value",
			"reqd" : 0
		},
        {
            "fieldname":"show_peso_value",
            "label": "Show Peso Value",
            "fieldtype": "Check"
        }
	]
};

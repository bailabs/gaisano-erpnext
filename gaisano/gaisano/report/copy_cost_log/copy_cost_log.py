# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from gaisano.itemevents import get_logfile_data

def execute(filters=None):
	columns =[
		{"label": "User ", 'width': 200, "fieldname": "user"},
		{"label": "Updated ", 'width': 250, "fieldname": "updated"},
	]

	data = get_logfile_data()
	return columns, data

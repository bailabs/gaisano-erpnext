// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Service Level Report"] = {
	"filters": [
		{
            "fieldname": "from_date",
            "label": __("From Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "to_date",
            "label": __("To Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "reqd": 1
        },
        {
            "fieldname": "supplier_name",
            "label": __("Supplier?"),
            "fieldtype": "Link",
            "options": "Supplier",
            "reqd": 0
        },
        {
            "fieldname": "discount_name",
            "label": __("Supplier Discount"),
            "fieldtype": "Link",
            "options":"Supplier Discounts",
            "reqd": 0,
            "get_query": function(){
				var branch = frappe.query_report_filters_by_name.branch.get_value();
				var supplier = frappe.query_report_filters_by_name.supplier_name.get_value();
				var filters = get_discount_filters(branch,supplier);
				return{
					"filters":filters
        		}
    		},
            "on_change": function(query_report){
                var discount = frappe.query_report_filters_by_name.discount_name.get_value();
                if (discount){
                    return frappe.call({
                        method : "frappe.client.get",
                        args :{
                            "doctype" : "Supplier Discounts",
                            "name" : discount
                        },
                        callback: function(r){
                            frappe.query_report_filters_by_name.supplier_discount_name.set_value(r.message['supplier_discount_name']);
                            frappe.query_report.refresh();
                        }
                    })
                }
                else{
                    frappe.query_report_filters_by_name.supplier_discount_name.set_value("");
                    frappe.query_report.refresh();
                }
            }
        },
		{
            "fieldname": "ref_service_level",
            "label": __("Reference Service Level"),
            "fieldtype": "Float",
            "reqd": 1
        },
        {
            "fieldname": "supplier",
            "label": __("Per Supplier?"),
            "fieldtype": "Check",
            "reqd": 0
        },
        {
            "fieldname": "supplier_discount",
            "label": __("Per Supplier Discount?"),
            "fieldtype": "Check",
            "reqd": 0
        },
		{
			"fieldname": "show_po",
			"label": __("Per PO's?"),
			"fieldtype": "Check",
			"reqd":0
		},
        {
			"fieldname": "show_rr",
			"label": __("Show RR's?"),
			"fieldtype": "Check",
			"reqd":0
		},
        {
			"fieldname": "supplier_discount_name",
			"label": __("Supplier Discount Name"),
			"fieldtype": "Data",
			"reqd":0,
            "read_only":1
        }
	]
};

function get_discount_filters(branch, supplier){

    if(branch == "CDO Main")
        var filters = {"disable":0,"disabled_in_main":0,"supplier_name":supplier};
    else
        var filters = {"disable": 0, "supplier_name":supplier};
    return filters
}

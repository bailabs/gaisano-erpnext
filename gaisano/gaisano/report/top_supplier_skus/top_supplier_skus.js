// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Top Supplier SKUS"] = {
	"filters": [
		{
		 	"fieldname":"from_date",
		 	"label": __("From Date"),
		 	"fieldtype": "Date",
		 	"reqd": 1
		},
		{
		 	"fieldname":"to_date",
		 	"label": __("To Date"),
		 	"fieldtype": "Date",
		 	"reqd": 1
		},
		{
		 	"fieldname":"branch",
		 	"label": __("Branch"),
		 	"fieldtype": "Link",
		 	"options": "Branch",
		 	"reqd": 1
		},
		{
		 	"fieldname":"supplier",
		 	"label": __("Supplier"),
		 	"fieldtype": "Link",
		 	"options": "Supplier",
		 	"reqd": 1
		},
		{
		 	"fieldname":"category",
		 	"label": __("Category"),
		 	"fieldtype": "Link",
		 	"options": "Item Category",
		 	"reqd": 0
		},
		{
		 	"fieldname":"classification",
		 	"label": __("Classification"),
		 	"fieldtype": "Link",
		 	"options": "Item Classification",
		 	"reqd": 1
		},
		{
		 	"fieldname":"subclass",
		 	"label": __("Subclassification"),
		 	"fieldtype": "Link",
		 	"options": "Item Sub Classification",
		 	"reqd": 0
		},
		        {
            "fieldname": "volume_perc",
            "label": __("Volume Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
			"default": 40
        },
        {
            "fieldname": "peso_perc",
            "label": __("Peso Value Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
			"default": 40
        },
        {
            "fieldname": "profit_perc",
            "label": __("Profit Percentage"),
            "fieldtype": "Int",
            "reqd": 1,
			"default": 20
        },
		{
            "fieldname": "show_values",
            "label": __("Show Values?"),
            "fieldtype": "Check",
            "reqd": 0
        }
	]
};

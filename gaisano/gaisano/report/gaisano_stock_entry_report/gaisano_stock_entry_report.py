# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, decimal

def execute(filters=None):
	data = []
	se_type = filters.get("se_type")
	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	show_memo = filters.get("show_memo")
	show_dm = filters.get("show_dm")
	branch = filters.get("branch")

	columns = get_columns(se_type,show_memo,show_dm)

	if se_type != "Bad Order":
		data = get_data(branch, se_type, from_date, to_date)
		total = get_totals(branch, se_type, from_date, to_date)
		data.append({'name':'TOTAL:', 'total_amount':total[0][0]})

	else:
		data = get_bo_data(branch, from_date, to_date)
		total = get_totals(branch, se_type, from_date, to_date)
		data.append({'name': 'BO TOTAL:', 'total_amount': total[0][0], "grand_total_less_discount":total[0][1]})
	return columns, data


def get_data(branch, se_type, from_date, to_date):
	return frappe.db.sql("""SELECT * from `tabStock Entry` where branch = %s and type = %s and posting_date >=%s and posting_date <=%s""",
						 (branch, se_type, from_date, to_date),as_dict = True)

def get_totals(branch, se_type, from_date, to_date):
	return frappe.db.sql("""SELECT sum(total_amount) as total_amount, sum(grand_total_less_discount) as grand_total_less_discount from `tabStock Entry` where branch = %s and type = %s and posting_date >=%s and posting_date <=%s and docstatus = 1""",
						 (branch, se_type, from_date, to_date))

def get_bo_data(branch,from_date, to_date):
	current_type = None
	total = 0
	total_less_discount = 0
	data = []
	records = frappe.db.sql("""SELECT se.* from `tabStock Entry` se where se.type = 'Bad Order' and se.branch = %s and se.posting_date >=%s
							  and se.posting_date <=%s order by se.name asc""", (branch, from_date, to_date),as_dict = True)
	i=0
	for record in records:
		record['grand_total_less_discount']= float(decimal.Decimal(str(record['grand_total_less_discount'])).quantize(decimal.Decimal('.01'), rounding=decimal.ROUND_HALF_UP))
		data.append(record)
			#print "i",i,"append total, Change Current Type", current_type, total
		#print record['name'],record['type'], record['total_amount'], record['supplier_type']
	return data

def get_columns(se_type, show_memo=None, show_dm=None):
	columns = [
		{"fieldname": "name", "label": "Series #", "fieldtype": "Link", "options": "Stock Entry", "width": 150},
		{"fieldname": "type", "label": "Type", "fieldtype": "Data", "width": 80},
		{"fieldname": "docstatus", "label": "Status", "fieldtype": "Data", "width": 90},
		{"fieldname": "owner", "label": "Created By", "fieldtype": "Data", "width": 140},
		{"fieldname": "posting_date", "label": "Posting Date", "fieldtype": "Date", "width": 80},
		{"fieldname": "posting_time", "label": "Posting Time", "fieldtype": "Time", "width": 80},
		{"fieldname": "total_amount", "label": "Total Amount", "fieldtype": "Currency", "precision": 2, "width": 90}
	]
	if se_type == "Bad Order":
		columns.append(
			{"fieldname": "supplier_discount_name", "label": "Supplier Discount", "fieldtype": "Data", "width": 170})
		columns.append(
			{"fieldname": "grand_total_less_discount", "label": "Total Less Discounts", "fieldtype": "Currency",
			 "precision": 2, "width": 90})
	if show_memo == 1:
		columns.append({"fieldname": "memo", "label": "Memo", "fieldtype": "Data", "width": 100})
	if show_dm == 1:
		columns.append({"fieldname": "dm_number", "label": "DM#", "fieldtype": "Data", "width": 100})
	return  columns

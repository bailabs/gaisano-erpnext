// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Item Cost Changes"] = {
	"filters": [
        {
            "fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "reqd":1,
        },
		{
            "fieldname": "reference_date",
            "label": __("Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
		{
            "fieldname": "supplier",
            "label": __("Supplier"),
            "fieldtype": "Link",
			"options":"Supplier",
            "reqd": 0
        },
        {
            "fieldname": "supplier_discount",
            "label": __("Supplier Discount"),
            "fieldtype": "Link",
            "options":"Supplier Discounts",
            "reqd": 0,
      "get_query": function () {
                var branch = frappe.query_report_filters_by_name.branch.get_value();
                var supplier = frappe.query_report_filters_by_name.supplier.get_value();
                var filters = get_discount_filters(branch, supplier);
                return {
                    "filters": filters
                }
            },
            "on_change": function(query_report){
                var discount = frappe.query_report_filters_by_name.supplier_discount.get_value();
                if (discount){
                    return frappe.call({
                        method : "frappe.client.get",
                        args :{
                            "doctype" : "Supplier Discounts",
                            "name" : discount
                        },
                        callback: function(r){
                            frappe.query_report_filters_by_name.supplier_discount.set_value(r.message['supplier_discount']);
                            frappe.query_report.refresh();
                        }
                    })
                }
                else{
                    frappe.query_report_filters_by_name.supplier_discount.set_value("");
                    frappe.query_report.refresh();
                }
            }
        },
        		{
            "fieldname": "category",
            "label": __("Category"),
            "fieldtype": "Link",
			"options":"Item Category",
            "reqd": 0
        },
	]
};

function get_discount_filters(branch, supplier){
    if(branch == "CDO Main")
        var filters = {"disable":0,"disabled_in_main":0,"supplier_name":supplier};
    else
        var filters = {"disable": 0, "supplier_name":supplier};
    return filters
}

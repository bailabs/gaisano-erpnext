# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import random, decimal
from gaisano.events import get_rounded_price

def execute(filters=None):
	columns, data = [], []

	report_type = filters.get('report_type')
	columns = get_columns(report_type)
	warehouse = filters.get('warehouse')
	from_date = filters.get('from_date')
	to_date = filters.get('to_date')

	if report_type == "Upload Product CSV no wholesale":
		data = get_items(warehouse)
	elif report_type == "Upload Product CSV with wholesale":
		data = get_items_wholesale(warehouse)
	elif report_type == "Upload Fresh and Produce CSV":
		data = get_ladlad()
	else:
		if not(from_date) or not(to_date):
			frappe.throw("From Date and To Date are required for this report.")
		data = get_sales_per_department(from_date, to_date)
	return columns, data

def get_columns(report_type):
	columns = []
	if report_type == "Upload Product CSV no wholesale":
		columns = [
			{"label": "barcode", 'width': 200, "fieldname": "barcode"},
			{"label": "name", 'width': 150, "fieldname": "item_name"},
			{"label": "price", 'width': 150, "fieldname": "price"},
			{"label": "category", 'width': 150, "fieldname": "category"},
			{"label": "subcategory", 'width': 100, "fieldname": "subcategory"},
			{"label": "status", 'width': 150, "fieldname": "status"}
		]
	elif report_type == "Upload Product CSV with wholesale":
		columns = [
			{"label": "barcode", 'width': 200, "fieldname": "barcode"},
			{"label": "name", 'width': 150, "fieldname": "item_name"},
			{"label": "price", 'width': 150, "fieldname": "price"},
			{"label": "wholesale_barcode", 'width': 100, "fieldname": "wholesale_barcode"},
			{"label": "wholesale_price", 'width': 100, "fieldname": "wholesale_price"},
			{"label": "wholesale_content_quantity", 'width': 100, "fieldname": "wholesale_content_quantity"},
			{"label": "wholesale_unit", 'width': 100, "fieldname": "wholesale_unit"},
			{"label": "category", 'width': 150, "fieldname": "category"},
			{"label": "subcategory", 'width': 100, "fieldname": "subcategory"},
			{"label": "status", 'width': 150, "fieldname": "status"}
		]
	elif report_type == "Upload Fresh and Produce CSV":


		columns = [
			{"label": "barcode", 'width': 200, "fieldname": "barcode"},
			{"label": "name", 'width': 150, "fieldname": "item_name"},
			{"label": "price", 'width': 150, "fieldname": "price"},
			{"label": "approx_weight", 'width': 150, "fieldname": "approx_weight"},
			{"label": "unit_in_weight", 'width': 150, "fieldname": "unit_in_weight"},
			{"label": "price_per_unit", 'width': 150, "fieldname": "price_per_unit"},
			{"label": "category", 'width': 150, "fieldname": "category"},
			{"label": "subcategory", 'width': 100, "fieldname": "subcategory"},
			{"label": "variable", 'width': 150, "fieldname": "variable"}
		]
	elif report_type == "Sales per Department":
		columns = [
			{"label": "Department", 'width': 200, "fieldname": "department"},
			{"label": "Sales", 'width': 150, "fieldname": "sales", "fieldtype":"Currency"}
		]
	return columns

def get_items(warehouse):
	data = []

	items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_name_dummy, itm.item_price_retail_with_margin,
							cat.category, cat.classification, itm.name from `tabItem` itm left join `tabItem Class Each` cat on cat.parent = itm.name
							where itm.type!='Disabled' and itm.packing =1 and itm.my_suki=1 and
							itm.ladlad!=1""")

	for item in items:
		try:
			item_doc = frappe.get_doc("Item",item[5])
			has_custom_margin = item_doc.mysuki_custom_margin
			custom_margin = item_doc.custom_margin/100
		except:
			continue
		else:
			markup = 0
			if has_custom_margin ==1 and markup is not None:
				markup = custom_margin
			else:
				markup = random.uniform(0.030, 0.035)
			item_price = item[2]*(1+markup)
			item_price_fin = get_rounded_price(item_price)
			item_price_rounded = decimal.Decimal(str(item_price_fin)).quantize(decimal.Decimal('.01'), rounding=decimal.ROUND_HALF_UP)

			data.append({"barcode":item[0],
						 "item_name":item[1].upper(),
						 "price":float(item_price_rounded),
						 "category":"" if item[3] is None else item[3].upper(),
						 "subcategory":"" if item[4] is None else item[4].upper(),
						 "status":"A"
						 })
			print item[5], item_price_fin
	return data

def get_items_wholesale(warehouse):
	data = []

	items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_name_dummy, itm.item_price_retail_with_margin,
							cat.category, cat.classification, itm.barcode_pack, itm.item_price, itm.packing,
							itm.uom, itm.name from `tabItem` itm left join `tabItem Class Each` cat on cat.parent = itm.name
							where itm.type!='Disabled' and itm.packing >1 and itm.my_suki=1 and
							itm.ladlad!=1""")

	for item in items:
		try:
			item_doc = frappe.get_doc("Item",item[9])
			has_custom_margin = item_doc.mysuki_custom_margin
			custom_margin = item_doc.custom_margin/100
		except:
			continue
		else:
			markup = 0
			if has_custom_margin ==1 and markup is not None:
				markup = custom_margin
			else:
				markup = random.uniform(0.030, 0.035)
			item_price = item[2] * (1 + markup)
			item_price_fin = get_rounded_price(item_price)
			item_price_rounded = decimal.Decimal(str(item_price_fin)).quantize(decimal.Decimal('.01'),rounding=decimal.ROUND_HALF_UP)

			data.append({"barcode":item[0],
						 "item_name":item[1].upper(),
						 "price":float(item_price_rounded),
						 "category":"" if item[3] is None else item[3].upper(),
						 "subcategory":"" if item[4] is None else item[4].upper(),
						 "wholesale_barcode": item[5] if (item[5] is not None) and (item[5] != "") else item[0],
						 "wholesale_price":item[6],
						 "wholesale_content_quantity":item[7],
						 "wholesale_unit":item[8],
						 "status": "A"
						 })
			print item[9], item_price_fin
	return data


def get_ladlad():
	data =[]
	# items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_name_dummy, itm.item_price_retail_with_margin,
	# 						cat.category, cat.classification, itm.name from `tabItem` itm left join `tabItem Class Each` cat on cat.parent = itm.name
	# 						where itm.type!='Disabled' and itm.type!='Phased Out' and itm.packing =1 and itm.my_suki=1
	# 						and itm.ladlad=1 and cat.category = 'Fresh'""")
	items = frappe.db.sql("""SELECT itm.barcode_retial, itm.item_name_dummy, itm.item_price_retail_with_margin,
							cat.category, cat.classification, itm.name from `tabItem` itm left join `tabItem Class Each` cat on cat.parent = itm.name
							where itm.my_suki=1 and itm.ladlad=1 and cat.category = 'Fresh'""")
	for item in items:
		try:
			item_doc = frappe.get_doc("Item",item[5])
			has_custom_margin = item_doc.mysuki_custom_margin
			custom_margin = item_doc.custom_margin/100
			approx_weight = item_doc.approx_weight
		except:
			continue
		else:
			markup = 0
			if has_custom_margin ==1 and markup is not None:
				markup = custom_margin
			else:
				markup = random.uniform(0.030, 0.035)
			item_price = item[2]*(1+markup)
			item_price_fin = get_rounded_price(item_price)
			item_price_rounded = decimal.Decimal(str(item_price_fin)).quantize(decimal.Decimal('.01'), rounding=decimal.ROUND_HALF_UP)

			price_per_approx_weight = float(item_price_rounded) * float(approx_weight)
			weighted_price_fin = get_rounded_price(price_per_approx_weight)
			weighted_price_rounded = decimal.Decimal(str(weighted_price_fin)).quantize(decimal.Decimal('.01'),
																			   rounding=decimal.ROUND_HALF_UP)

			data.append({"barcode":item[0][1:6],
						 "item_name":item[1].upper(),
						 "price":float(weighted_price_rounded),
						 "approx_weight":approx_weight,
						 "unit_in_weight":"KG",
						 "price_per_unit":float(item_price_rounded),
						 "category":"" if item[3] is None else item[3].upper(),
						 "subcategory":"" if item[4] is None else item[4].upper(),
						 "status":"A",
						 "variable": 1
						 })
			print item[5], item_price_fin
	return data

def get_sales_per_department(from_date, to_date):
	data=[]
	item_sales=frappe.db.sql("""SELECT sum(pos.amount), itm.item_category from `tabUpload POS` pos join `tabItem` itm
								on pos.barcode = itm.barcode_retial where pos.trans_date >= %s and pos.trans_date <=%s
								and pos.my_suki = 1 group by itm.item_category""",(from_date, to_date))
	for sales in item_sales:
		print sales[0], sales[1]
		data.append({"department":sales[1],"sales":sales[0]})
	return data
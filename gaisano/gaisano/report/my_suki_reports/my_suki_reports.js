// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["My Suki Reports"] = {
	"filters": [
		{
            "fieldname": "report_type",
            "label": __("Report Type"),
            "fieldtype": "Select",
			"options": ["Upload Product CSV no wholesale", "Upload Product CSV with wholesale", "Upload Fresh and Produce CSV", "Sales per Department"],
            "reqd": 1
        },
        {
            "fieldname":"from_date",
            "label": __("From Date"),
            "fieldtype": "Date",
            "reqd": 0
        },
        {
            "fieldname":"to_date",
            "label": __("To Date"),
            "fieldtype": "Date",
            "reqd": 0
        }
        //,
        // {
        //     "fieldname": "warehouse",
        //     "label": __("warehouse"),
        //     "fieldtype": "Link",
			// "options": "Warehouse",
        //     "reqd": 1
        // }
    ]
};

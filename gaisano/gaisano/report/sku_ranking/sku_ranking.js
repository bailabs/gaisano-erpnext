// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["SKU Ranking"] = {
	"filters": [
		{
			"fieldname": "branch",
			"label": __("Branch"),
			"fieldtype": "Link",
			"options": "Branch",
			"reqd": 1
		},
		{
            "fieldname": "from_date",
            "label": __("From Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "to_date",
            "label": __("To Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "report_type",
            "label": __("Report Type"),
            "fieldtype": "Select",
            "options":["", "POS Sales Only", "Wholesale Sales Only", "POS + Wholesale Sale"],
            "reqd": 1
        },
        {
	        "fieldname" : "report_password",
	        "fieldtype" : "Password",
	        "label" : "Password for Show Peso Value",
	        "reqd" : 0
        },
        {
            "fieldname":"show_peso_value",
            "label": "Show Peso Value",
            "fieldtype": "Check"
        },
        {
            "fieldname":"exclude_ladlad",
            "label": "Exclude Ladlad",
            "fieldtype": "Check"
        }
	]
}

# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from operator import itemgetter
from gaisano.popupevents import get_report_password

def execute(filters=None):
	columns, data = [], []
	branch = filters.get("branch")
	from_date = filters.get('from_date')
	to_date = filters.get('to_date')
	show_peso_value = filters.get('show_peso_value')
	password = filters.get('report_password')
	report_type = filters.get('report_type')
	exclude_ladlad = filters.get('exclude_ladlad')
	
	if show_peso_value == True:
		if not get_report_password(password):
			report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
			frappe.throw(report_msg)

	columns = get_columns(show_peso_value)

	if exclude_ladlad:
		data = get_data_without_ladlad(branch, from_date, to_date, show_peso_value, report_type)
	else:
		data = get_data(branch, from_date, to_date, show_peso_value, report_type)
	return columns, data

def get_columns(show_peso_value):
	if show_peso_value == True:
		columns=[
			{"label": 'Item Name', "fieldname": 'item_name', "fieldtype": 'Data', "width": 200},
			{"label": 'Barcode', "fieldname": 'item_barcode', "fieldtype": "Data", "width": 150},
			{"label": 'Peso Value', "fieldname": 'peso_value', "fieldtype": 'Currency'},
			{"label": 'Percentage', "fieldname": 'percentage', "fieldtype": 'Percent'},
		]
	else:
			columns=[
			{"label": 'Item Name', "fieldname": 'item_name', "fieldtype": 'Data', "width": 200},
			{"label": 'Barcode', "fieldname": 'item_barcode', "fieldtype": "Data", "width": 150},
			{"label": 'Percentage', "fieldname": 'percentage', "fieldtype": 'Percent'},
		]
	return columns


def get_data(branch, from_date, to_date, show_peso_value, report_type):
	data = []
	if report_type == "POS Sales Only":
		total = get_total_sales(branch, from_date, to_date)
		items = frappe.db.sql(
			"""SELECT name, item_name_dummy, barcode_retial, type FROM `tabItem` WHERE type != 'Disabled'""")
		for item in items:
			sales = get_item_sales(branch, item[2], from_date, to_date)
			if sales >0:
				#data.append({"item_name": item[1], "item_barcode": item[2], "peso_value": sales})
				percentage = (sales/total) * 100
				#total += sales
				data.append({"item_name": item[1], "item_barcode": item[2], "peso_value": sales, "percentage":percentage})
		data=sorted(data, key=itemgetter("peso_value"), reverse=True)
		if show_peso_value == True:
			data.append ({'peso_value': total})

	elif report_type == "Wholesale Sales Only":
		total_temp = 0
		items = frappe.db.sql("""select distinct(itm.item_code), itm.item_description, item_master.barcode_retial from
							  `tabStock Entry Detail` itm join `tabStock Entry` se on se.name = itm.parent join `tabItem` item_master on item_master.name = itm.item_code
							  where se.branch = %s and se.posting_date >=%s and se.posting_date <=%s and se.docstatus = 1 and se.type ='Wholesale' """, (branch, from_date, to_date))
		total_wholesale = frappe.db.sql("""select sum(total_amount) from `tabStock Entry` where branch = %s and posting_date >=%s and posting_date <=%s and docstatus = 1 and type ='Wholesale'""",(branch, from_date, to_date))
		for item in items:
			print item
			item_sales = get_wholesale_sales(item[0], branch, from_date, to_date)
			percentage = ((item_sales/total_wholesale[0][0])*100)
			data.append({"item_name": item[1], "item_barcode": item[2], "peso_value": item_sales, "percentage": percentage})
			data = sorted(data, key=itemgetter("peso_value"), reverse=True)
		if show_peso_value == True:
			data.append ({'peso_value': total_wholesale, "percentage":1})

	elif report_type == "POS + Wholesale Sale":
		total_pos = get_total_sales(branch, from_date, to_date)
		total_wholesale_sales = frappe.db.sql("""select sum(total_amount) from `tabStock Entry` where branch = %s and posting_date >=%s and posting_date <=%s and docstatus = 1 and type ='Wholesale'""",(branch, from_date, to_date))
		grand_total = total_pos+total_wholesale_sales[0][0]
		items = frappe.db.sql(
			"""SELECT name, item_name_dummy, barcode_retial, type FROM `tabItem` WHERE type != 'Disabled'""")
		for item in items:
			print item
			pos_sales = get_item_sales(branch, item[2], from_date, to_date)
			wholesale_sales = get_wholesale_sales(item[0],branch, from_date,to_date)
			item_sales = pos_sales + wholesale_sales
			if item_sales >0:
				percentage = (item_sales / grand_total) * 100
				data.append(
						{"item_name": item[1], "item_barcode": item[2], "peso_value": item_sales, "percentage": percentage})
		data = sorted(data, key=itemgetter("peso_value"), reverse=True)
		if show_peso_value == True:
			data.append({'peso_value': grand_total, "percentage":1})
	else:
		frappe.throw("Please select a report type")
	return data


def get_data_without_ladlad(branch, from_date, to_date, show_peso_value, report_type):
	data = []
	if report_type == "POS Sales Only":
		total = get_total_sales_without_ladlad(branch, from_date, to_date)
		items = frappe.db.sql(
			"""SELECT name, item_name_dummy, barcode_retial, type FROM `tabItem` WHERE type != 'Disabled' and ladlad !=1""")
		for item in items:
			sales = get_item_sales(branch, item[2], from_date, to_date)
			if sales>0:
				# data.append({"item_name": item[1], "item_barcode": item[2], "peso_value": sales})
				percentage = (sales / total) * 100
				# total += sales
				data.append(
					{"item_name": item[1], "item_barcode": item[2], "peso_value": sales, "percentage": percentage})
		data = sorted(data, key=itemgetter("peso_value"), reverse=True)
		if show_peso_value == True:
			data.append({'peso_value': total})

	elif report_type == "Wholesale Sales Only":
		total_temp = 0
		items = frappe.db.sql("""select distinct(itm.item_code), itm.item_description, item_master.barcode_retial from
    						  `tabStock Entry Detail` itm join `tabStock Entry` se on se.name = itm.parent join `tabItem` item_master on item_master.name = itm.item_code
    						  where se.branch = %s and se.posting_date >=%s and se.posting_date <=%s and se.docstatus = 1 and se.type ='Wholesale' """,
							  (branch, from_date, to_date))
		total_wholesale = frappe.db.sql(
			"""select sum(total_amount) from `tabStock Entry` where branch = %s and posting_date >=%s and posting_date <=%s and docstatus = 1 and type ='Wholesale'""",
			(branch, from_date, to_date))
		for item in items:
			item_sales = get_wholesale_sales(item[0], branch, from_date, to_date)
			percentage = ((item_sales / total_wholesale[0][0]) * 100)
			data.append(
				{"item_name": item[1], "item_barcode": item[2], "peso_value": item_sales, "percentage": percentage})
			data = sorted(data, key=itemgetter("peso_value"), reverse=True)
		if show_peso_value == True:
			data.append({'peso_value': total_wholesale, "percentage": 1})

	elif report_type == "POS + Wholesale Sale":
		total_pos = get_total_sales_without_ladlad(branch, from_date, to_date)
		total_wholesale_sales = frappe.db.sql(
			"""select sum(total_amount) from `tabStock Entry` where branch = %s and posting_date >=%s and posting_date <=%s and docstatus = 1 and type ='Wholesale'""",
			(branch, from_date, to_date))
		grand_total = total_pos + total_wholesale_sales[0][0]
		items = frappe.db.sql(
			"""SELECT name, item_name_dummy, barcode_retial, type FROM `tabItem` WHERE type != 'Disabled' and ladlad !=1""")
		for item in items:
			pos_sales = get_item_sales(branch, item[2], from_date, to_date)
			wholesale_sales = get_wholesale_sales(item[0], branch, from_date, to_date)
			item_sales = pos_sales + wholesale_sales
			if item_sales > 0:
				percentage = (item_sales / grand_total) * 100
				data.append(
					{"item_name": item[1], "item_barcode": item[2], "peso_value": item_sales, "percentage": percentage})
		data = sorted(data, key=itemgetter("peso_value"), reverse=True)
		if show_peso_value == True:
			data.append({'peso_value': grand_total, "percentage": 1})
	else:
		frappe.throw("Please select a report type")
	return data

def get_item_sales(branch, barcode, from_date, to_date):
	sales = frappe.db.sql("""SELECT SUM(amount) AS total FROM `tabUpload POS` WHERE barcode = %s and trans_date >= %s and trans_date <= %s and branch = %s""",(barcode, from_date, to_date, branch))
	return sales[0][0] if sales[0][0] is not None else 0

def get_total_sales(branch, from_date, to_date):
	total = frappe.db.sql("""SELECT SUM(amount) AS total FROM `tabUpload POS` WHERE trans_date >= %s and trans_date <= %s and branch = %s""",(from_date, to_date, branch))
	return total[0][0] if total[0][0] is not None else 0

def get_wholesale_sales(item_code, branch, from_date, to_date):
	sales = frappe.db.sql("""SELECT sum(amount) from `tabStock Entry Detail` itm join `tabStock Entry` se on
						  se.name = itm.parent where itm.item_code = %s and se.branch =%s and se.posting_date >=%s and se.posting_date <=%s AND
						  se.docstatus =1 and type = 'Wholesale'""",(item_code, branch, from_date, to_date))
	return sales[0][0] if sales[0][0] is not None else 0

def get_total_sales_without_ladlad(branch, from_date, to_date):
	total = frappe.db.sql(
		"""SELECT SUM(amount) AS total FROM `tabUpload POS` WHERE trans_date >= %s and trans_date <= %s and branch = %s and barcode not in(select barcode_retial from `tabItem` where ladlad = 1)""",
		(from_date, to_date, branch))
	return total[0][0] if total[0][0] is not None else 0
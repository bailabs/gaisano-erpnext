# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import calendar
from gaisano.popupevents import get_report_password


def execute(filters=None):
    columns = [
        {"label": "Month", 'width': 150, "fieldname": "month"},
        {"label": "Peso Value", "width": 150, "fieldname": "peso_value", "fieldtype": "Currency"}
    ]
    password = filters.get("report_password")
    supplier = filters.get("Supplier")

    data = []
    Year = filters.get("Year")
    branch = filters.get("Branch")

    if supplier is None:
        if not get_report_password(password):
            report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
            frappe.throw(report_msg)

    if (filters.get("OrderBy") in ["Purchase Receipt", "Bad Order"]):
        columns.append({"label": "Total(Less Discounts)", "width": 150, "fieldname": "net_total", "fieldtype": "Currency"})

    for x in range(1, 13):
        year_month = "" + str(Year) + "-" + str(x)+ "-01"
        next_month = "" + str(Year) + "-" + str(x+1)+ "-01"
        if x == 12:
            new_year = int(Year) + 1
            next_month = "" + str(new_year) + "-01-01"
        print year_month, next_month
        query = get_query(filters, year_month, next_month)
        row = frappe.db.sql(query)
        value = {"month": calendar.month_name[x], "peso_value": row[0]}
        if (filters.get("OrderBy") == "Purchase Receipt"):
            net_total = get_rr_net_total(year_month, next_month, branch, filters.get("Supplier"), filters.get("Supplier Discounts"))
            value['net_total']=net_total
        if (filters.get("OrderBy") == "Bad Order"):
            net_total = get_bo_net_total(year_month, next_month, branch, filters.get("Supplier"), filters.get("Supplier Discounts"))
            value['net_total']=net_total
        data.append(value)
    total_row = {"month":"TOTAL","peso_value":get_totals(data)}
    if (filters.get("OrderBy") in ["Purchase Receipt", "Bad Order"]):
        total_row['net_total'] = get_overall_net_total(data)
    data.append(total_row)

    return columns, data

def get_query(filters, year_month, next_month):

    OrderBy = filters.get("OrderBy") #"tab" + filters.get("OrderBy")
    Year = filters.get("Year")
    Branch = filters.get("Branch")
    Supplier = filters.get("Supplier") if (filters.get("Supplier") !=None) else ""
    Supplier_Discounts = filters.get("Supplier Discounts") if (filters.get("Supplier Discounts") !=None) else ""

    query =""

    print OrderBy, Year
    if OrderBy in ["Purchase Receipt","Purchase Order"]:
        OrderBy = "tab" + filters.get("OrderBy")

        if "Purchase Order" in OrderBy:
            date_str = "transaction_date"
        else:
            date_str = "posting_date"

        if (Supplier !=None) and (Supplier !=""):
            query = """Select SUM(grand_total) as total from `{0}` WHERE {4} >= '{1}' AND {4} < '{2}'
              AND branch = '{3}' AND supplier = '{5}' AND supplier_discount LIKE '{6}%' and docstatus=1""".format(
                OrderBy, year_month, next_month, Branch, date_str, Supplier, Supplier_Discounts)
        elif (Supplier_Discounts !=None) and (Supplier_Discounts !=""):
            query = """Select SUM(grand_total) as total from `{0}`
              WHERE {4} >= '{1}' AND {4} < '{2}'
              AND branch = '{3}' AND supplier like'{5}%' AND supplier_discount = '{6}' and docstatus=1""".format(
                OrderBy, year_month, next_month, Branch, date_str, Supplier, Supplier_Discounts)
        else:
            query = """Select SUM(grand_total) as total from `{0}`
                      WHERE {4} >= '{1}' AND {4} < '{2}' AND branch = '{3}' and docstatus =1 """.format(
                OrderBy, year_month, next_month, Branch, date_str)

    elif OrderBy == "Bad Order":
        if (Supplier !=None) and (Supplier !="") and (Supplier_Discounts is None or Supplier_Discounts == ""):
            query = """SELECT sum(se.total_amount) from `tabStock Entry` se inner join `tabSupplier Discounts` disc on
                              se.supplier_discount = disc.name where disc.supplier_name = '{0}' and se.branch='{1}' and se.posting_date>='{2}' and
                              se.posting_date<'{3}' and se.type = '{4}' and se.docstatus = 1""".format(Supplier, Branch, year_month, next_month, OrderBy)
        elif (Supplier_Discounts !=None) and (Supplier_Discounts !=""):
            print "With Discount"
            query = """SELECT sum(total_amount) from `tabStock Entry` where supplier_discount = '{0}' and branch = '{1}' and
                              posting_date>='{2}' and posting_date<'{3}' and type='{4}'
                              and docstatus = 1 """.format(Supplier_Discounts, Branch, year_month, next_month, OrderBy)
        else:
            query = """SELECT sum(total_amount) from `tabStock Entry` where branch='{0}' and posting_date>='{1}' and
                                          posting_date<'{2}' and type='{3}' and docstatus = 1
                                          """.format(Branch, year_month, next_month, OrderBy)
    else:
        if (Supplier !=None) and (Supplier !="") and (Supplier_Discounts is None or Supplier_Discounts == ""):
            query = """SELECT sum(itm.amount) from `tabStock Entry Detail` itm inner join `tabStock Entry` se on
                              se.name = itm.parent where itm.item_code in (SELECT distinct(parent) from `tabItem Supplier`
                              where supplier = '{0}') and se.branch='{1}' and se.posting_date>='{2}' and
                              se.posting_date<'{3}' and se.type = '{4}' and se.docstatus = 1""".format(Supplier, Branch, year_month, next_month, OrderBy)
        elif (Supplier_Discounts !=None) and (Supplier_Discounts !=""):
            print "With Discount"
            query = """SELECT sum(itm.amount) from `tabStock Entry Detail` itm inner join `tabStock Entry` se on
                              se.name = itm.parent where itm.item_code in (SELECT distinct(parent) from
                              `tabItem Branch Discount` where supplier_discount = '{0}' and branch = '{1}') and
                              se.branch='{1}' and se.posting_date>='{2}' and se.posting_date<'{3}' and se.type='{4}'
                              and se.docstatus = 1 """.format(Supplier_Discounts, Branch, year_month, next_month, OrderBy)
        else:
            query = """SELECT sum(itm.amount) from `tabStock Entry Detail` itm inner join `tabStock Entry` se on
                                          se.name = itm.parent where se.branch='{0}' and se.posting_date>='{1}' and
                                          se.posting_date<'{2}' and se.type='{3}' and se.docstatus = 1
                                          """.format(Branch, year_month, next_month, OrderBy)

    if OrderBy == "Stock Transfer":
        print "STT"
        query +=  """ and se.from_warehouse in (select name from `tabWarehouse` where branch = '{}')""".format(Branch)
    #print query
    return query

def get_totals(data):
    total = 0
    for item in data:
        total+= (0 if item['peso_value'][0] is None else item['peso_value'][0])
        #print total, item['peso_value'][0]
    return total

def get_rr_net_total(from_date, to_date, branch, supplier = None, supplier_discount = None):
    query = "SELECT SUM(grand_total_less_discount) from `tabPurchase Receipt` where posting_date >='"+from_date+"' and " \
            "posting_date < '"+to_date+"'and branch = '"+branch+"'and docstatus = 1"
    if supplier is not None:
        query+= " and supplier = '"+supplier+"'"
    if supplier_discount is not None:
        query += " and supplier_discount = '" + supplier_discount + "'"
    net_total = frappe.db.sql(query)
    print query
    return net_total[0][0]


def get_bo_net_total(from_date, to_date, branch, supplier = None, supplier_discount = None):
    query = ""
    if (supplier is not None) and (supplier_discount is None):
        query= "SELECT SUM(se.grand_total_less_discount) from `tabStock Entry` se inner join " \
                "`tabSupplier Discounts` disc on se.supplier_discount = disc.name where disc.supplier_name = '"\
               +supplier+"' and se.posting_date >='"+from_date+"' and se.posting_date < '" + to_date + "'and se.branch = '" + branch + \
                 "'and se.docstatus = 1 and se.type = '" + "Bad Order" + "'"
    elif supplier_discount is not None:
        query += "SELECT SUM(grand_total_less_discount) from `tabStock Entry` where supplier_discount = '" + \
                 supplier_discount + "' and posting_date >='"+from_date+"' and posting_date < '" + to_date + "'and branch = '" + branch + \
                 "'and docstatus = 1 and type = '" + "Bad Order" + "'"
    else:
        query += "SELECT SUM(grand_total_less_discount) from `tabStock Entry` where posting_date >='" + from_date + "' and posting_date < '" + to_date + "'and branch = '" + branch + \
                 "'and docstatus = 1 and type = '" + "Bad Order" + "'"
    net_total = frappe.db.sql(query)
    print query
    return net_total[0][0]


def get_overall_net_total(data):
    total = 0
    for item in data:
        total += (0 if item['net_total'] is None else item['net_total'])
    return total
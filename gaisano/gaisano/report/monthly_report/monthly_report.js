// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Monthly Report"] = {
	"filters": [
		{
			"fieldname":"OrderBy",
			"label": __("Order By"),
			"fieldtype": "Select",
			"options": ["Purchase Order", "Purchase Receipt", "Withdrawals", "Stock Transfer", "Bad Order", "Wholesale"],
			"width":150,
			"reqd": 1
		},

		{
			"fieldname":"Year",
			"label": __("Year"),
			"fieldtype": "Link",
			"options": "Fiscal Year",
			"width":150,
			"reqd": 1
		},

        {
            "fieldname": "Branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "width": 150,
            "reqd": 1
        },

		{
			"fieldname": "Supplier",
			"label": __("Supplier"),
			"fieldtype": "Link",
			"options": "Supplier",
			"width": 150,
			"reqd": 0
		},

		{
			"fieldname": "Supplier_Discounts",
			"label": __("Supplier Discount"),
			"fieldtype": "Link",
			"options": "Supplier Discounts",
			"width": 150,
			"reqd": 0,
			"get_query": function(){
				var branch = frappe.query_report_filters_by_name.Branch.get_value();
				var supplier = frappe.query_report_filters_by_name.Supplier.get_value();
				console.log(branch,supplier);
				var filters = get_discount_filters(branch,supplier);
				return{
					"filters":filters
        		}
    		},
			"on_change": function(query_report){
                var discount = frappe.query_report_filters_by_name.Supplier_Discounts.get_value();
                if (discount){
                    return frappe.call({
                        method : "frappe.client.get",
                        args :{
                            "doctype" : "Supplier Discounts",
                            "name" : discount
                        },
                        callback: function(r){
                            frappe.query_report_filters_by_name.supplier_discount_name.set_value(r.message['supplier_discount_name']);
                            frappe.query_report.refresh();
                        }
                    })
                }
                else{
                    frappe.query_report_filters_by_name.supplier_discount_name.set_value("");
                    frappe.query_report.refresh();
                }
            }
		},
		{
			"fieldname": "supplier_discount_name",
			"fieldtype": "Data",
			"reqd":0,
            "read_only":1
		},
		{
        "fieldname" : "report_password",
        "fieldtype" : "Password",
        "label" : "Password to show Branch Totals",
        "reqd" : 0
        }

	]
};

function get_discount_filters(branch, supplier){
    if(branch == "CDO Main")
        var filters = {"disable":0,"disabled_in_main":0,"supplier_name":supplier};
    else
        var filters = {"disable": 0, "supplier_name":supplier};
    return filters
}
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import frappe
from frappe import _


class AttributeDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__


def execute(filters=None):
    columns = get_columns()
    sl_entries = get_stock_ledger_entries(filters)
    item_details = get_item_details(filters)

    data = []
    running_balance = 0
    for sle in sl_entries:
        sle = AttributeDict(dict(sle))
        if sle.item_code == "Opening Balance":
            item_detail = {"brand": "********", "description": "********", "item_group": "********",
                           "item_name": "********", "brand": "********",
                           "stock_uom": "********"}
            item_detail = AttributeDict(dict(item_detail))
            running_balance = sle.qty_after_transaction
        else:
            item_detail = item_details[sle.item_code]
            if not sle.actual_qty:
                sle.actual_qty = 0
            if not running_balance:
                running_balance = 0
            running_balance += float(sle.actual_qty)

        if sle.voucher_type == 'Stock Reconciliation':
            running_balance = float(sle.qty_after_transaction)
        if "Opening Balance" not in sle.item_code:
            item = frappe.get_doc("Item",sle.item_code)
            item_desc = item.item_name_dummy
        else:
            item_desc=sle.item_code
        data.append([sle.date,
                     item_desc,
                     #sle.item_code,
                     item_detail.stock_uom,
                     sle.actual_qty,
                     running_balance,
                     sle.voucher_type, sle.voucher_no,
                     sle.batch_no, sle.serial_no, sle.company])

    return columns, data


def get_columns():
    return [_("Date") + ":Datetime:95",
            _("Item") + ":Data:330",
            _("Stock UOM") + ":Link/UOM:100",
            _("Qty") + ":Float:50",
            _("Balance Qty") + ":Float:100",
            _("Voucher Type") + "::150", _("Voucher #") + ":Dynamic Link/" + _("Voucher Type") + ":100",
            ]


def get_stock_ledger_entries(filters):
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    stock_ledger = frappe.db.sql("""select concat_ws(" ", posting_date, posting_time) as date,
			item_code, warehouse, actual_qty, qty_after_transaction, incoming_rate, valuation_rate,
			stock_value, voucher_type, voucher_no, batch_no, serial_no, company
		from `tabStock Ledger Entry` sle
		where company = %(company)s and
			posting_date between %(from_date)s and %(to_date)s
			{sle_conditions}
			order by posting_date asc, posting_time asc, name asc""" \
                                 .format(sle_conditions=get_sle_conditions(filters)), filters, as_dict=1)

    # diri ka butang sa imo query sa pos and loop dayun

    if filters.get("warehouse"):
        is_display = frappe.db.sql("""Select is_display from `tabWarehouse` where name = %s""",
                                   (filters.get("warehouse")))

        bal_dict = {
            'stock_value': 0,
            'company': 'Gaisano Grocery',
            'serial_no': '',
            'incoming_rate': '',
            'valuation_rate': '',
            'item_code': 'Opening Balance',
            'voucher_type': '',
            'warehouse': '',
            'batch_no': '',
            'date': '',
            'voucher_no': '',
            'qty_after_transaction': get_balance(filters),
            'actual_qty': ''

        }

        print bal_dict
        stock_ledger.append(bal_dict)

        if (is_display[0][0] == 1):

            pos = frappe.db.sql("""Select barcode, branch, trans_date, name, qty, cost from `tabUpload POS`
                          where trans_date >= %s and trans_date <= %s and barcode = (Select barcode_retial from `tabItem` where item_code = %s)""",
                                (from_date, to_date, filters.get("item_code")))

            for p in pos:
                barcode = str(p[0])
                branch = str(p[1])
                date = str(p[2])
                pos_name = str(p[3])
                qty = str(p[4])
                cost = str(p[5])

                item = frappe.db.sql("""Select item_code, item_cost from `tabItem` where barcode_retial = %s""",
                                     (barcode))

                for i in item:
                    itm_code = str(i[0])
                    itm_cost = str(i[1])

                    try:
                        factor = float(itm_cost) / float(cost)
                        total_qty = float(qty) / float(factor)
                    except:
                        total_qty = 0

                    warehouse = frappe.db.sql(
                        """Select name from `tabWarehouse` where branch = %s and is_display = 1 and name = %s""",
                        (branch, str(filters.get("warehouse"))))

                    for w in warehouse:
                        wr_name = str(w[0])

                        pos_dict = {
                            'stock_value': 0,
                            'company': 'Gaisano Grocery',
                            'serial_no': '',
                            'incoming_rate': itm_cost,
                            'valuation_rate': itm_cost,
                            'item_code': itm_code,
                            'voucher_type': 'POS',
                            'warehouse': wr_name,
                            'batch_no': '',
                            'date': date,
                            'voucher_no': pos_name,
                            'qty_after_transaction': 0,
                            'actual_qty': float(total_qty * -1)
                        }
                        stock_ledger.append(pos_dict)
        stock_ledger = sorted(stock_ledger, key=lambda k: k['date'])
        return stock_ledger

    else:
        pos = frappe.db.sql("""Select barcode, branch, trans_date, name, qty, cost from `tabUpload POS`
              where trans_date >= %s and trans_date <= %s and barcode = (Select barcode_retial from `tabItem` where item_code = %s)""",
                            (from_date, to_date, filters.get("item_code")))

        for p in pos:
            barcode = str(p[0])
            branch = str(p[1])
            date = str(p[2])
            pos_name = str(p[3])
            qty = str(p[4])
            cost = str(p[5])

            item = frappe.db.sql("""Select item_code, item_cost from `tabItem` where barcode_retial = %s""", (barcode))

            for i in item:
                itm_code = str(i[0])
                itm_cost = str(i[1])

                try:
                    factor = float(itm_cost) / float(cost)
                    total_qty = float(qty) / float(factor)
                except:
                    total_qty = 0

                warehouse = frappe.db.sql("""Select name from `tabWarehouse` where branch = %s and is_display = 1""",
                                          (branch))

                for w in warehouse:
                    wr_name = str(w[0])

                    pos_dict = {
                        'stock_value': 0,
                        'company': 'Gaisano Grocery',
                        'serial_no': '',
                        'incoming_rate': itm_cost,
                        'valuation_rate': itm_cost,
                        'item_code': itm_code,
                        'voucher_type': 'Delivery Note',
                        'warehouse': wr_name,
                        'batch_no': '',
                        'date': date,
                        'voucher_no': pos_name,
                        'qty_after_transaction': 0,
                        'actual_qty': float(total_qty * -1)
                    }
                    stock_ledger.append(pos_dict)
        stock_ledger = sorted(stock_ledger, key=lambda k: k['date'])
        return stock_ledger


def get_item_details(filters):
    item_details = {}
    for item in frappe.db.sql("""select name, item_name, description, item_group,
			brand, stock_uom from `tabItem` {item_conditions}""" \
                                      .format(item_conditions=get_item_conditions(filters)), filters, as_dict=1):
        item_details.setdefault(item.name, item)

    return item_details


def get_item_conditions(filters):
    conditions = []
    if filters.get("item_code"):
        conditions.append("name=%(item_code)s")
    if filters.get("brand"):
        conditions.append("brand=%(brand)s")

    return "where {}".format(" and ".join(conditions)) if conditions else ""


def get_sle_conditions(filters):
    conditions = []
    item_conditions = get_item_conditions(filters)
    if item_conditions:
        conditions.append("""item_code in (select name from tabItem
			{item_conditions})""".format(item_conditions=item_conditions))
    if filters.get("warehouse"):
        conditions.append(get_warehouse_condition(filters.get("warehouse")))
    if filters.get("voucher_no"):
        conditions.append("voucher_no=%(voucher_no)s")

    return "and {}".format(" and ".join(conditions)) if conditions else ""


def get_opening_balance(filters, columns):
    if not (filters.item_code and filters.warehouse and filters.from_date):
        return

    from erpnext.stock.stock_ledger import get_previous_sle
    last_entry = get_previous_sle({
        "item_code": filters.item_code,
        "warehouse": get_warehouse_condition(filters.warehouse),
        "posting_date": filters.from_date,
        "posting_time": "00:00:00"
    })

    row = [""] * len(columns)
    row[1] = _("'Opening'")
    for i, v in ((9, 'qty_after_transaction'), (11, 'valuation_rate'), (12, 'stock_value')):
        row[i] = last_entry.get(v, 0)

    return row


def get_warehouse_condition(warehouse):
    warehouse_details = frappe.db.get_value("Warehouse", warehouse, ["lft", "rgt"], as_dict=1)
    if warehouse_details:
        return " exists (select name from `tabWarehouse` wh \
			where wh.lft >= %s and wh.rgt <= %s and sle.warehouse = wh.name)" % (warehouse_details.lft,
                                                                                 warehouse_details.rgt)

    return ''


def get_balance(filters):
    warehouse = filters.get("warehouse")
    item_code = filters.get("item_code")
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")

    recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
                where warehouse = %s and
                item_code = %s and
                posting_date <= %s and
                voucher_type = 'Stock Reconciliation' ORDER BY posting_date DESC limit 1""",
                          (warehouse, item_code, from_date))

    for r in recon:
        date = str(r[0])
        qty = str(r[1])

        rem_total = frappe.db.sql("""Select sum(actual_qty) from `tabStock Ledger Entry`
                where warehouse = %s and
                item_code = %s and
                posting_date <= %s and
                voucher_type != 'Stock Reconciliation'""", (warehouse, item_code, date))
        try:
            rem_sum = str(rem_total[0][0])
        except:
            rem_sum = 0

        pos_total = frappe.db.sql("""Select sum(qty * cost) from `tabUpload POS`
                where
                branch = (Select branch from `tabWarehouse` where name = %s) and
                barcode = (Select barcode_retial from `tabItem` where item_code = %s) and
                trans_date <= %s""", (warehouse, item_code, date))

        try:
            pos_sum = str(pos_total[0][0])
        except:
            pos_sum = 0

        itm_factor = frappe.db.sql("""Select item_cost from `tabItem` where item_code = %s""", (item_code))

        try:
            factor = str(itm_factor[0][0])
        except:
            factor = 1

        try:
            balance = float(qty) + float(rem_sum) + ((float(pos_sum) / float(factor)) * -1)
        except:
            balance = 0
        return balance

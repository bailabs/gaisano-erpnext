# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe


def execute(filters=None):
    columns = [
        {"label": "Item Code", 'width': 300, "fieldname": "item_code"},
        {"label": "PO Number", 'width': 150, "fieldname": "po_name"},
        {"label": "Order Qty", 'width': 150, "fieldname": "po_qty", "fieldtype": "Float"},
        {"label": "Received Qty", 'width': 150, "fieldname": "rr_qty", "fieldtype": "Float"},
        {"label": "Percentage", 'width': 100, "fieldname": "percent", "fieldtype": "Percent"},
    ]
    data = []
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    supplier = filters.get("Supplier")
    branch = filters.get("Branch")

    try:
        if from_date > to_date:
            frappe.throw("Error: From date is higher than To Date ")
    except:
         print "continue"

    po = frappe.db.sql("""Select name from `tabPurchase Order` where
                       transaction_date >= %s and transaction_date <= %s and supplier = %s and branch = %s""",
                       (from_date, to_date, supplier, branch))

    for s in po:
        po_name = str(s[0])

        doc = frappe.db.sql("""Select sum(a.qty), a.item_code, sum(b.qty), ((sum(b.qty)/sum(a.qty))*100)
               from `tabPurchase Order Item` as a, `tabPurchase Receipt Item` as b where a.parent = b.purchase_order
               and a.item_code = b.item_code and a.parent = %s group by a.item_code""", (po_name))

        for d in doc:
            po_qty = str(d[0])
            po_item = str(d[1])
            rr_qty = str(d[2])
            rr_perc = str(d[3])
            item_doc = frappe.get_doc("Item",po_item)

            status = frappe.db.sql("""Select status from `tabPurchase Receipt` where name in (Select parent from `tabPurchase Receipt Item` where purchase_order = %s
                                          and item_code = %s group by item_code)""", (po_name, po_item))

            for st in status:
                doc_st = str(st[0])

                if (doc_st == 'To Bill'):

                    data.append({"item_code": item_doc.item_name_dummy, "po_name": po_name, "po_qty": po_qty,
                     "rr_qty": rr_qty, "percent": rr_perc})


    return columns, data

# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from operator import itemgetter

def execute(filters=None):
	from_date = filters.get('from_date')
	to_date = filters.get('to_date')
	branch = filters.get('branch')
	warehouse = filters.get('warehouse')

	columns = [
		{"label": "Date", "width": 80, "fieldname": "posting_date", "fieldtype": "Data"},
		{"label": "STT #", "width": 180, "fieldname": "parent", "fieldtype": "Data"},
		{"label": "to Warehouse", "width": 200, "fieldname": "t_warehouse", "fieldtype": "Data"},
		{"label": "Dep Code", "width": 80, "fieldname": "dep_code", "fieldtype": "Data"},
		{"label": "Item", "width": 250, "fieldname": "item_description", "fieldtype": "Data"},
		{"label": "QTY", "width": 80, "fieldname": "qty_dummy", "fieldtype": "Float", "precision":2},
		{"label": "Price", "width": 100, "fieldname": "price", "fieldtype": "Currency", "precision": 2},
		{"label": "Amount", "width": 100, "fieldname": "amount", "fieldtype": "Currency", "precision": 2}
	]
	data = get_data(from_date, to_date, branch, warehouse)
	data = sorted(data, key=itemgetter('dep_code'), reverse=False)
	data = get_totals(data)
	data = get_total_row(data)
	return columns, data


def get_data(from_date, to_date, branch, warehouse):
	data =[]

	se_list = frappe.db.sql("""SELECT item_code, item_description, qty_dummy, price, amount, parent, t_warehouse from `tabStock Entry Detail` WHERE 
								t_warehouse = %s and parent in (select name from `tabStock Entry` where posting_date >=%s and posting_date<=%s
								and branch = %s and docstatus = 1)""",
							(warehouse, from_date, to_date, branch), as_dict = True)

	for row in se_list:
		#row['subclass'] = get_item_subclass(row['item_code'])
		row['dep_code'] = get_item_depcode(row['item_code'])
		row['posting_date'] = get_se_date(row['parent'])
		data.append(row)

	return data

def get_se_date(se):
	return frappe.db.get_value("Stock Entry",se, "posting_date")

# def get_item_subclass(item):
# 	subclass = frappe.db.sql("""SELECT sub_class from `tabItem Class Each` where parent = %s limit 1""", item)
# 	return subclass[0][0] if len(subclass)>0 else ""

def get_item_depcode(item):
	subclass = frappe.db.sql("""SELECT CONCAT(dep.department,' (',dep.name,')') from `tabItem` itm join `tabDepartment Code` dep on dep.name = itm.dep_code where itm.name = %s limit 1""", item)
	return subclass[0][0] if len(subclass)>0 else ""

def get_totals(data):
	new_list = []
	current_subclass = ""
	subclass_total = 0
	for row in data:
		if current_subclass == "":
			current_subclass = row['dep_code']
		if current_subclass!= row['dep_code']:
			new_list.append({
				"parent": "TOTAL FOR "+current_subclass,
				"amount": subclass_total
			})
			current_subclass = row['dep_code']
			subclass_total = row['amount']
		else:
			subclass_total += float(row['amount'])
		new_list.append(row)
	new_list.append({
		"parent": "TOTAL FOR " + current_subclass,
		"amount": subclass_total
	})

	return new_list


def get_total_row(data):
	total_amount = 0

	for row in data:
		if "TOTAL FOR" in row['parent']:
			continue
		else:
			total_amount+=float(row['amount'])
	data.append({"parent":"GRAND TOTAL OF STTS","amount":total_amount})

	return data
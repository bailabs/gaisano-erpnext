# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe


def execute(filters=None):
    columns = [
        {"label": "Item Code", 'width': 280, "fieldname": "item_code"},
        {"label": "Barcode", 'width': 110, "fieldname": "barcode"},
        {"label": "Paccking List", 'width': 100, "fieldname": "packing_list"},
        {"label": "Beginning", 'width': 100},
        {"label": "Withdrawals", 'width': 100, "fieldname": "withdrawals"},
        {"label": "Withdrawals in Pcs", 'width': 100, "fieldname": "with_pcs"},
        {"label": "Bad Orders", 'width': 100, "fieldname": "bad_order"},
        {"label": "Ending", 'width': 100},
        {"label": "Actual POS", 'width': 100},
        {"label": "System POS", 'width': 100, "fieldname": "pos"},
        {"label": "Difference", 'width': 100, "fieldname": "offtake"},

    ]
    data = []
    date = filters.get("date")
    supplier = str(filters.get("supplier"))
    warehouse = filters.get("warehouse")
    branch = filters.get("branch")

    item_under_supplier = frappe.db.sql("""Select sup.parent, itm.item_name_dummy from `tabItem Supplier` sup
                                      inner join `tabItem` itm on itm.name = sup.parent where supplier = %s""", (supplier))

    for itm in item_under_supplier:
        itm_code = str(itm[0])
        item_desc = str(itm[1])
        packing_list = frappe.db.sql("""select max(conversion_factor) from `tabUOM Conversion Detail` where parent = %s""", (itm_code))
        pkg = str(packing_list[0][0])

        withdrawal = frappe.db.sql("""select sum(transfer_qty) from `tabStock Entry Detail` as a JOIN `tabStock Entry` as b
                    where a.parent = b.name and a.t_warehouse = %s and branch = %s and
                    b.posting_date = %s and b.docstatus = 1 and a.item_code = %s""", (warehouse, branch , date, itm_code))
        for with_qty in withdrawal:
            withdraw = str(with_qty[0])
            if (withdraw != 'None'):
                with_pcs = float(withdraw) * float(pkg)
            else :
                with_pcs = 0


            badorder = frappe.db.sql("""select sum(transfer_qty) from `tabStock Entry Detail` as a JOIN `tabStock Entry` as b
                                where a.parent = b.name and a.t_warehouse = 'Bad Orders - GG' and branch = %s and
                                b.posting_date = %s and b.docstatus = 1 and a.item_code = %s""", (branch, date, itm_code))

            for bad in badorder:
                bad_qty = str(bad[0])

                item_barcode = frappe.db.sql("""Select barcode_retial from `tabItem` where item_code = %s""", (itm_code))
                for bar in item_barcode:
                    barcode = str(bar[0])

                    posdata = frappe.db.sql("""select sum(qty) from `tabUpload POS` where trans_date = %s and barcode = %s"""
                                            ,(date, barcode))
                    for pos in posdata:
                        pos_qty = str(pos[0])
                        data.append({"packing_list": pkg, "item_code": item_desc,
                                     "withdrawals": withdraw, "with_pcs": with_pcs, "bad_order": bad_qty,
                                     "pos": pos_qty, "barcode": barcode})


    return columns, data

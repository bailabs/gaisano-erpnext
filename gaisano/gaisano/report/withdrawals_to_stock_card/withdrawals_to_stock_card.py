# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from datetime import datetime, date, time, timedelta

def execute(filters=None):
	columns = [
		{"label": "Date", "width": 200, "fieldname": "date"},
		{"label": "Item Name", "width": 200, "fieldname": "item_name"},
		{"label": "Qty Withdrawn", "width": 200, "fieldname": "qty_withdrawn"},
		{"label": "Qty Sold", "width": 200, "fieldname": "qty_sold"}
	]

	branch = filters.get("branch")
	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	item = filters.get("item")

	data = stock_card_report(branch, from_date, to_date, item)

	return columns, data

def stock_card_report(branch, from_date, to_date, item):
	data = []
	branch_code = frappe.db.sql("""select branch_code from `tabBranch` where name = %s""", branch)
	branch_code = "W-" + branch_code[0][0] + "%"
	from_date = datetime.strptime(from_date, '%Y-%m-%d')
	to_date = datetime.strptime(to_date, '%Y-%m-%d')

	if (item):
		total_sle_qty = 0
		total_pos_qty = 0
		item = frappe.db.sql("select name, item_name_dummy, barcode_retial, stock_card from `tabItem` where name = %s", (item))
		if (item[0][3] == 1):
			while (from_date <= to_date):
				from_date = datetime.strftime(from_date, '%Y-%m-%d')

				sle = frappe.db.sql(
					"""select sum(actual_qty) from `tabStock Ledger Entry` where posting_date = %s and item_code = %s and actual_qty > 0 and voucher_no like %s""",
					(from_date, item[0][0], branch_code))

				pos = frappe.db.sql(
					"""select sum(qty) from `tabUpload POS` where barcode = %s and trans_date = %s and branch = %s""",
					(item[0][2], from_date, branch))

				print "renzo"
				print sle

				if (sle[0][0]):
					sle_qty = sle[0][0]
				else:
					sle_qty = 0

				if (pos[0][0]):
					pos_qty = pos[0][0]
				else:
					pos_qty = 0

				total_sle_qty = total_sle_qty + sle_qty
				total_pos_qty = total_pos_qty + pos_qty

				data.append({"date": from_date, "item_name": item[0][1], "qty_withdrawn": sle_qty, "qty_sold": pos_qty})
				from_date = datetime.strptime(from_date, '%Y-%m-%d') + timedelta(days=1)
			data.append({"date": " ", "item_name": "<b>Total</b>", "qty_withdrawn":'<b>' + str(total_sle_qty) +'</b>', "qty_sold": '<b>' + str(total_pos_qty) + '</b>'})

		else:
			frappe.throw("Item is not a Stock Card. Report won't generate")

	else:
		item_stock_card = frappe.db.sql("""select name, item_name_dummy, barcode_retial from `tabItem` where stock_card = 1""")
		total_sle_qty = 0
		total_pos_qty = 0
		while (from_date <= to_date):

			from_date = datetime.strftime(from_date, '%Y-%m-%d')

			data.append({"date": from_date})
			for item_stock_cards in item_stock_card:
				sle = frappe.db.sql("""select sum(actual_qty) from `tabStock Ledger Entry` where posting_date = %s and item_code = %s and actual_qty > 0 and voucher_no like %s""", (from_date, item_stock_cards[0], branch_code))
				pos = frappe.db.sql("""select sum(qty) from `tabUpload POS` where barcode = %s and trans_date = %s and branch = %s""", (item_stock_cards[2], from_date, branch))

				if (sle[0][0]):
					sle_qty = sle[0][0]
				else:
					sle_qty = 0

				if (pos[0][0]):
					pos_qty = pos[0][0]
				else:
					pos_qty = 0

				total_sle_qty = total_sle_qty + sle_qty
				total_pos_qty = total_pos_qty + pos_qty

				data.append({"date": " ", "item_name": item_stock_cards[1], "qty_withdrawn":sle_qty, "qty_sold": pos_qty})

			from_date = datetime.strptime(from_date, '%Y-%m-%d') + timedelta(days=1)
		data.append({"date": " ", "item_name": "<b>Total</b>", "qty_withdrawn": '<b>' + str(total_sle_qty) + '</b>',"qty_sold": '<b>' + str(total_pos_qty) + '</b>'})


	return data
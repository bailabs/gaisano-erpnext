# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe


def execute(filters=None):
    columns = [
        {"label": "Percentage", 'width': 100, "fieldname": "percent", "fieldtype": "Percent"},
    ]
    data = []

    upload_pos = frappe.db.sql("""select count(distinct barcode) from `tabUpload POS`""")
    item = frappe.db.sql("""Select count(distinct barcode) from `tabUpload POS` where barcode IN (select barcode_retial from `tabItem`)""")


    itm = str(item[0][0])
    nf = str(upload_pos[0][0])

    percent = ((float(itm)/float(nf)) * 100)

    data.append({"percent": percent})


    return columns, data

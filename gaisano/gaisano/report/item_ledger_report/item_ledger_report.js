// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt

frappe.query_reports["Item Ledger Report"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			"reqd": 1
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
			"reqd": 1
		},
		{
			"fieldname":"warehouse",
			"label": __("Warehouse"),
			"fieldtype": "Link",
			"options": "Warehouse",
			"reqd": 1
		},
        {
            "fieldname": "item_code",
            "label": __("Item"),
            "fieldtype": "Link",
            "options": "Item",
            "reqd": 1
        },
		// },
		{
			"fieldname":"supplier",
			"label": __("Supplier"),
			"fieldtype": "Link",
			"options": "Supplier"
		},
		{
			"fieldname":"report_type",
			"label": __("report_type"),
			"fieldtype": "Select",
			"options": ["SLE", "Transactions"],
			"reqd":1
		}
		// {
		// 	"fieldname":"supplier_discount",
		// 	"label": __("Supplier Discount"),
		// 	"fieldtype": "Link",
		// 	"options": "Supplier Discounts",
		// 	"get_query": function () {
         //        var supplier = frappe.query_report_filters_by_name.supplier.get_value();
         //        return {
         //            "filters": {
         //            	"supplier_name": supplier
		// 			}
         //        }
         //    }
		// },
		// {
		// 	"fieldname":"voucher_no",
		// 	"label": __("Voucher #"),
		// 	"fieldtype": "Data"
		// }
	]
};



// $(function() {
// 	$(wrapper).bind("show", function() {
// 		frappe.query_report.load();
// 	});
// });
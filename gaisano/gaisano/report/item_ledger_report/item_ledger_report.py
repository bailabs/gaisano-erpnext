# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
import datetime

def execute(filters=None):
    report_type = filters.get("report_type")
    if report_type == "SLE":
        return execute_sle(filters)
    else:
        return execute_transactions(filters)

def execute_sle(filters=None):
    filters['company'] = frappe.db.get_value("Global Defaults", None, "default_company")
    columns = get_columns()
    sl_entries = get_stock_ledger_entries(filters)
    item_details = get_item_details(filters)
    #opening_row = get_opening_balance(filters, columns)

    data = []

    #if opening_row:
    #    data.append(opening_row)
    discount = get_supplier_discount(filters.get("item_code"))

    if discount != None:
        discount_doc = frappe.get_doc("Supplier Discounts", discount)
        discount_name = discount_doc.supplier_discount_name
    else:
        discount_name = "None"

    for sle in sl_entries:
        print sle.item_code, sle.supplier_discount
        item_detail = item_details[sle.item_code]
        item_doc = frappe.get_doc("Item", sle.item_code)
        voucher_type = sle.voucher_type
        print voucher_type

        voucher_type = voucher_type.replace(" ", "%20")
        sle_link = "<a href=%s target='_blank'>" % ("/desk#Form/"+voucher_type+"/" + (sle.voucher_no)) + (sle.voucher_no + "</a>")

        temp = [sle.date, item_doc.item_name_dummy, sle.warehouse,
                     item_detail.stock_uom, sle.actual_qty, sle.qty_after_transaction, discount_name,sle_link
                    ]
        data.append(temp)
    return columns, data


def get_columns():
    return [_("Date") + ":Datetime:90", _("Item") + ":Data:280", #_("Item Name") + "::100",
            _("Warehouse") + ":Link/Warehouse:180",
            _("UOM") + ":Link/UOM:60",
            _("Qty") + ":Float:50",
            _("Balance Qty") + ":Float:100",
            #_("Supplier") + ":Link/Supplier:170",
            _("Supplier Discount") + ":Data:200",
            #_("Incoming Rate") + ":Currency:110", _("Valuation Rate") + ":Currency:110",
            #_("Balance Value") + ":Currency:110",#_("Voucher Type") + "::110",
            _("Voucher #") + ":Data:200" #+ _("Voucher Type") + ":100",
            #_("Batch") + ":Link/Batch:100",
            #_("Serial #") + ":Link/Serial No:100", _("Company") + ":Link/Company:100"
            ]


def get_stock_ledger_entries(filters):
    return frappe.db.sql("""select concat_ws(" ", posting_date, posting_time) as date,
			item_code, warehouse, actual_qty, qty_after_transaction, incoming_rate, valuation_rate,
			stock_value, voucher_type, voucher_no, batch_no, serial_no, company
		from `tabStock Ledger Entry` sle left join `tabItem Supplier` ts on sle.item_code = ts.parent
		 where company = %(company)s and posting_date between %(from_date)s and %(to_date)s
			{sle_conditions}
			order by posting_date asc, posting_time asc, sle.name asc""" \
                         .format(sle_conditions=get_sle_conditions(filters)), filters, as_dict=1)


def get_item_details(filters):
    item_details = {}
    for item in frappe.db.sql("""select name, item_name, description, item_group,
			brand, stock_uom from `tabItem` {item_conditions}""" \
                                      .format(item_conditions=get_item_conditions(filters)), filters, as_dict=1):
        item_details.setdefault(item.name, item)

    return item_details


def get_item_conditions(filters):
    conditions = []
    if filters.get("item_code"):
        conditions.append("name=%(item_code)s")
    if filters.get("brand"):
        conditions.append("brand=%(brand)s")

    return "where {}".format(" and ".join(conditions)) if conditions else ""


def get_sle_conditions(filters):
    conditions = []
    item_conditions = get_item_conditions(filters)
    if item_conditions:
        conditions.append("""sle.item_code in (select name from tabItem
			{item_conditions})""".format(item_conditions=item_conditions))
    if filters.get("warehouse"):
        conditions.append(get_warehouse_condition(filters.get("warehouse")))
    if filters.get("voucher_no"):
        conditions.append("voucher_no=%(voucher_no)s")
    if filters.get("supplier"):
        conditions.append("ts.supplier=%(supplier)s")
    return "and {}".format(" and ".join(conditions)) if conditions else ""


def get_warehouse_condition(warehouse):
    warehouse_details = frappe.db.get_value("Warehouse", warehouse, ["lft", "rgt"], as_dict=1)
    if warehouse_details:
        return " exists (select name from `tabWarehouse` wh \
			where wh.lft >= %s and wh.rgt <= %s and sle.warehouse = wh.name)" % (warehouse_details.lft,
                                                                                warehouse_details.rgt)

    return ''


def get_supplier_discount(item_code):
    branch = frappe.db.get_value("Server Information", None, "branch")
    print branch, item_code
    rows = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where branch = %s and parent = %s limit 1""",(branch, item_code))
    return rows[0][0] if len(rows)>0 else None


#####OLD CODE: Based on transactions

def execute_transactions(filters=None):
    filters['company'] = frappe.db.get_value("Global Defaults", None, "default_company")
    columns = get_columns_trans()
    data = []

    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    warehouse = filters.get("warehouse")
    item_code = filters.get("item_code")

    item_name = get_item_name(item_code)

    previous_balance = get_previous_balance(from_date, warehouse, item_code)
    transactions = get_transactions(warehouse, item_code, from_date, to_date)

    opening_row = {"voucher_no": "Previous Balance", "item_code": item_name, "balance_qty": previous_balance}
    data.append(opening_row)

    for transaction in transactions:
        transaction['item_code'] = item_name
        if transaction['voucher_type'] == 'Stock Reconciliation':
            previous_balance = float(transaction['qty'])
            transaction['balance_qty'] = float(transaction['qty'])
        else:
            transaction['balance_qty'] = float(previous_balance) + float(transaction['qty'])
            previous_balance = transaction['balance_qty']
        data.append(transaction)

    return columns, data


def get_columns_trans():

    return [
        {"label": "Date", 'width': 150, "fieldname": "timestamp", "fieldtype": "Datetime"},
        {"label": "Voucher #", 'width': 150, "fieldname": "voucher_no"},
        {"label": "Item Name", 'width': 280, "fieldname": "item_code"},
        {"label": "UOM", 'width': 60, "fieldname": "uom"},
        {"label": "Qty", 'width': 60, "fieldname": "qty", "fieldtype": "float"},
        {"label": "Balance Qty", 'width': 100, "fieldname": "balance_qty", "fieldtype": "float"}
    ]


def get_item_name(item_code):
    return frappe.db.get_value("Item", item_code, "item_name_dummy")


def get_previous_balance(trans_date, warehouse, item_code):
    #time_now = datetime.datetime.now().time()
    to_date = str(trans_date)# + " " + str(time_now)
    from_date = "2015-01-01 00:00:00"
    recon = 0

    sr = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty from `tabStock Reconciliation Item` itm
                            join `tabStock Reconciliation` sr on sr.name = itm.parent where itm.warehouse = %s and itm.item_code = %s
                            and sr.posting_date< %s and sr.docstatus = 1 order by posting_date desc, posting_time desc, sr.name desc limit 1""",
                       (warehouse, item_code, trans_date))

    if len(sr)>0:
        if sr[0][0]:
            from_date = sr[0][0]
            recon = sr[0][1]

    print(str(from_date), str(to_date))

    rrs = frappe.db.sql("""select sum(itm.stock_qty) from `tabPurchase Receipt Item` itm join `tabPurchase Receipt` rr on
                        rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s and rr.docstatus = 1
                        and TIMESTAMP(rr.posting_date, rr.posting_time)>=%s and TIMESTAMP(rr.posting_date, rr.posting_time)< %s""",
                        (warehouse, item_code, from_date, to_date))

    ses_minus = frappe.db.sql("""select sum(-1*itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time)>= %s and TIMESTAMP(se.posting_date, se.posting_time)<%s
                              and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))

    ses_plus = frappe.db.sql("""select sum(itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time)>= %s and TIMESTAMP(se.posting_date, se.posting_time)<%s
                              and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))

    total_rr = rrs[0][0] if rrs[0][0] is not None else 0
    total_se_minus = ses_minus[0][0] if ses_minus[0][0] is not None else 0
    total_se_plus = ses_plus[0][0] if ses_plus[0][0] is not None else 0

    inv = total_rr + total_se_minus + total_se_plus + recon

    return inv


def get_transactions(warehouse, item_code, from_date, to_date):

    transactions = []
    srs = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty, sr.name, item.uom from `tabStock Reconciliation Item` itm
                        join `tabStock Reconciliation` sr on sr.name = itm.parent join `tabItem` item on item.name = itm.item_code
                        where itm.warehouse = %s and itm.item_code = %s and sr.docstatus = 1
                        and sr.posting_date between %s and %s """,
                       (warehouse, item_code, from_date, to_date))
    for sr in srs:
        url = "<a href=%s target='_blank'>" % ("/desk#Form/Stock%20Reconciliation/" + (sr[2])) + (
            sr[2] + "</a>")
        transactions.append({"timestamp":sr[0], "qty":sr[1], "voucher_no":url,"uom":sr[3], "voucher_type": "Stock Reconciliation"})

    rrs = frappe.db.sql("""select timestamp(rr.posting_date, rr.posting_time), itm.stock_qty, rr.name, itm.stock_uom
                        from `tabPurchase Receipt Item` itm
                        join `tabPurchase Receipt` rr on rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s
                        and rr.posting_date between %s and %s and rr.docstatus = 1""",(warehouse, item_code, from_date, to_date))

    for rr in rrs:
        url = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Receipt/" + (rr[2])) + (
            rr[2] + "</a>")
        transactions.append({"timestamp":rr[0], "qty":rr[1], "voucher_no":url, "uom":rr[3], "voucher_type": "Purchase Receipt"})

    ses_minus = frappe.db.sql("""select timestamp(se.posting_date, se.posting_time), -1*itm.transfer_qty, se.name,itm.stock_uom
                        from `tabStock Entry Detail` itm
                        join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
                        and se.posting_date between %s and %s and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))

    for se in ses_minus:
        url = "<a href=%s target='_blank'>" % ("/desk#Form/Stock%20Entry/" + (se[2])) + (
            se[2] + "</a>")
        transactions.append({"timestamp":se[0], "qty":se[1], "voucher_no":url, "uom": se[3], "voucher_type": "Stock Entry"})

    ses_plus = frappe.db.sql("""select timestamp(se.posting_date, se.posting_time), itm.transfer_qty, se.name,itm.stock_uom
                            from `tabStock Entry Detail` itm
                            join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
                            and se.posting_date between %s and %s and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))

    for se in ses_plus:
        url = "<a href=%s target='_blank'>" % ("/desk#Form/Stock%20Entry/" + (se[2])) + (
            se[2] + "</a>")
        transactions.append({"timestamp": se[0], "qty": se[1], "voucher_no": url, "uom": se[3], "voucher_type": "Stock Entry"})

    transactions = sorted(transactions, key=lambda k: k['timestamp'], reverse=False)

    return transactions

# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	expected_date = filters.get("date")
	report_type = filters.get("report_type")
	data = daily(expected_date,report_type)
	columns = get_columns(report_type)
	return columns, data

def get_columns(report_type):
	if report_type == "Service Level per RR":
		columns = [
			{"label": "Purchase Receipt", 'width': 150, "fieldname": "purchase_receipt"},
			{"label": "Supplier Discount", 'width': 350, "fieldname": "supplier_discount"},
			{"label": "Service Level", 'width': 150, "fieldname": "service_level", "fieldtype": "Float", "precision": 2}
		]
	else:
		columns = [
			{"label": "Supplier", 'width': 150, "fieldname": "supplier"},
			{"label": "Supplier Discount", 'width': 350, "fieldname": "supplier_discount"},
			{"label": "Service Level", 'width': 150, "fieldname": "service_level", "fieldtype": "Float", "precision": 2}
		]
	return columns

def daily(expected_date,report_type):
	data = []

	if (report_type == "Service Level per Supplier"):
		supplier = frappe.db.sql(
			"""select distinct(supplier) from `tabPurchase Receipt` where posting_date = %s""", expected_date)
		for suppliers in supplier:
			purchase_receipt = frappe.db.sql(
				"""select avg(supplier_discount_total), supplier, supplier_discount_name FROM(select sum(service_level) as
					supplier_discount_total, supplier, supplier_discount_name from `tabPurchase Receipt` where rr_type = "Receive PO from this Branch" and
					supplier = %s and posting_date = %s and docstatus =1  group by purchase_order) as average""",(suppliers[0], expected_date))
			for purchase_receipts in purchase_receipt:
				print purchase_receipts[1]
				print purchase_receipts[2]
				print purchase_receipts[0]
				data.append({"supplier": purchase_receipts[1], "supplier_discount": purchase_receipts[2], "service_level": purchase_receipts[0]})

	elif (report_type == "Service Level per Supplier Discount"):
		purchase_receipt = frappe.db.sql(
			"""select sum(service_level_qty), supplier, supplier_discount_name from `tabPurchase Receipt` where rr_type = "Receive PO from this Branch" and docstatus =1 and posting_date = %s  group by purchase_order""",
			expected_date)
		for purchase_receipts in purchase_receipt:
			data.append({"supplier": purchase_receipts[1], "supplier_discount": purchase_receipts[2],
						 "service_level": purchase_receipts[0]})

	elif (report_type == "Service Level Scheduled Delivery"):
		purchase_order = frappe.db.sql(
			"""select name, total_case, supplier_discount_name, supplier from `tabPurchase Order` where expected_delivery = %s""",
			expected_date)

		for purchase_orders in purchase_order:
			purchase_receipt = frappe.db.sql(
				"""select name, sum(total_case) from `tabPurchase Receipt` where purchase_order = %s and posting_date <= %s group by purchase_order""",
				(purchase_orders[0], expected_date))
			if purchase_receipt:
				po_total_case = purchase_orders[1]
				pr_total_case = purchase_receipt[0][1]
				service_lvl = round((float(pr_total_case) / float(po_total_case) * 100), 2)
				data.append({"supplier": purchase_orders[3], "supplier_discount": purchase_orders[2],
							 "service_level": service_lvl})
			else:
				data.append({"supplier": purchase_orders[3], "supplier_discount": purchase_orders[2],
							 "service_level": 0})
	else:
		purchase_receipts = frappe.db.sql("""SELECT name, service_level_qty, supplier_discount_name from `tabPurchase Receipt` where posting_date =%s""",expected_date)
		for pr in purchase_receipts:
			data.append({"purchase_receipt":pr[0] , "supplier_discount": pr[2],
						 "service_level": pr[1]})
	return data
# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

from operator import itemgetter
from gaisano.popupevents import get_report_password

def execute(filters=None):
    columns = []
    data = []
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    category_filter = filters.get("category")
    type = filters.get("report_type")
    branch = filters.get("branch")

    print branch
    if branch == None:
        branch = ""
    if category_filter == None:
        category_filter = ""


    v_perc = float(filters.get("volume")) / 100
    p_perc = float(filters.get("peso_value")) / 100
    pr_perc = float(filters.get("profit")) / 100

    show_peso_value = filters.get("show_peso_value")
    password = filters.get("report_password")
    if type == "Rank Categories":

        if show_peso_value == 1:
            if not get_report_password(password):
                report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
                frappe.throw(report_msg)

        sum_volume, sum_amount, sum_profit = get_overall_sales(branch, from_date, to_date)
        temp1, temp2, temp3 = 0, 0, 0
        if show_peso_value == 1:
            columns = [
                {"label": "Category", 'width': 200, "fieldname": "category"},
                {"label": "Volume Sales", 'width': 150, "fieldname": "volume", "fieldtype": "float"},
                {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
                {"label": "Revenue Value", 'width': 150, "fieldname": "revenue", "fieldtype": "Currency"},
                {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
                {"label": "Profit Value", 'width': 170, "fieldname": "profit", "fieldtype": "Currency"},
                {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
                {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
            ]
        else:
            columns = [
                {"label": "Category", 'width': 200, "fieldname": "category"},
                {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
                {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
                {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
                {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
            ]

        category_list = frappe.db.sql("""SELECT name from `tabItem Category`""")
        for category in category_list:
            try:
                overall = 0
                temp_volume, temp_peso, temp_profit = get_category_sales(branch, from_date, to_date, category[0])
                if temp_volume == None:
                    temp_volume = 0
                if temp_peso == None:
                    temp_peso = 0
                if temp_profit == None:
                    temp_profit = 0

                vol_perc = (float(temp_volume) / float(sum_volume)) * 100
                peso_perc = (float(temp_peso) / float(sum_amount)) * 100
                prof_perc = (float(temp_profit) / float(sum_profit)) * 100
                end_vol_percent = ((float(vol_perc)) * v_perc)
                end_peso_percent = ((float(peso_perc)) * p_perc)
                end_prof_percent = ((float(prof_perc)) * pr_perc)
                overall = end_vol_percent + end_peso_percent + end_prof_percent

                temp1 += temp_volume
                temp2 += temp_peso
                temp3 += temp_profit
            except:
                #print "ERROR on Category = "+category[0]
                temp = {"category": category[0], "volume": 0, "volume_perc": 0,
                        "revenue": 0, "peso_perc": 0, "profit_perc": 0, "profit": 0,
                        "overall": 0}
                #print temp
                data.append(temp)
                continue
            else:
                #temp = {"category": category[0], "volume": temp_volume,  "revenue": temp_peso, "profit": temp_profit}
                temp = {"category": category[0], "volume": temp_volume, "volume_perc": vol_perc,
                        "revenue": temp_peso, "peso_perc":peso_perc, "profit_perc":prof_perc, "profit": temp_profit, "overall": overall}
                data.append(temp)

        #print data
        #data= sorted(data, key = lambda k: k["overall"], reverse=False)
        data =  sorted(data, key=itemgetter("overall"), reverse=True)
        temp = {"category": "TOTALS", "volume": temp1,"volume_perc": 100,"revenue": temp2, "peso_perc":100,
            "profit": temp3, "profit_perc":100, "overall": 100}
        data.append(temp)
    elif type == "Rank Classifications":
        if show_peso_value == 1:
            if not get_report_password(password):
                report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
                frappe.throw(report_msg)
        columns, data = get_classification_ranking(filters, branch, from_date, to_date, show_peso_value)
    elif type == "Market Share By Category (Hide Subclass)":
        if category_filter =="":
            frappe.msgprint("Please Pick a Category", "Error", "Red")
        else:
            columns, data = get_category_market_share(filters, branch, from_date, to_date, category_filter, False)
        #get_category_market_share(v_perc, p_perc, pr_perc, branch, from_date, to_date, category, False)

    elif type == "Market Share with Suppliers (By Classification)" and show_peso_value == 1:
        if category_filter =="":
            frappe.msgprint("Please Pick a Category", "Error", "Red")

        if not get_report_password(password):
            report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
            frappe.throw(report_msg)
        else:
            columns, data = get_category_market_share_show_peso_value(filters, branch, from_date, to_date, category_filter, "mktshare_class")
    elif type == "Market Share with Suppliers (By Classification)":

        if category_filter =="":
            frappe.msgprint("Please Pick a Category", "Error", "Red")

        else:
            columns, data = get_category_market_share(filters, branch, from_date, to_date, category_filter, "mktshare_class")
        #get_category_market_share(v_perc, p_perc, pr_perc, branch, from_date, to_date, category, False)

    elif type == "Market Share with Suppliers (Entire Category)" and show_peso_value == 1:
        if category_filter == "":
            frappe.msgprint("Please Pick a Category", "Error", "Red")

        if not get_report_password(password):
            report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
            frappe.throw(report_msg)
        else:
            columns, data = get_category_market_share_show_peso_value(filters, branch, from_date, to_date, category_filter, "mktshare_cat")

    elif type == "Market Share with Suppliers (Entire Category)":
            if category_filter == "":
                frappe.msgprint("Please Pick a Category", "Error", "Red")
            else:
                columns, data = get_category_market_share(filters, branch, from_date, to_date, category_filter, "mktshare_cat")
                # get_category_market_share(v_perc, p_perc, pr_perc, branch, from_date, to_date, category, False)

    elif type == "Market Share by Category with Subclass":
        if category_filter == "":
            frappe.msgprint("Please Pick a Category", "Error", "Red")
        else:
            columns, data = get_category_market_share(filters, branch, from_date, to_date, category_filter, True)
        #get_category_market_share(v_perc, p_perc, pr_perc, branch, from_date, to_date, category, True)
    else:
        columns, data = get_category_market_share(filters, branch, from_date, to_date, category_filter, "eh")

    return columns, data


def check_totals(new_data):
    temp1, temp2, temp3, temp4, = 0, 0, 0, 0
    for i, item in enumerate(new_data):
        temp1 += item['volume_perc']
        temp2 += item['peso_perc']
        temp3 += item['profit_perc']
        temp4 += item['overall']

    new_data.append({"supplier": "<B>TOTAL based on Totals</B>", "volume_perc": temp1,
                     "peso_perc": temp2, "profit_perc": temp3,
                     "overall": temp4
                     })
    return new_data



def sort_by_filter(temp_list, filter, is_reverse):
    if filter == 'overall':
        print "overall"
        return sorted(temp_list, key=itemgetter("overall"), reverse=is_reverse)
    else:
        print "supplier"
        return sorted(temp_list, key=lambda k: k[filter], reverse=is_reverse)


def get_total_sales(branch, from_date, to_date, category):
    total_qty, total_amount, total_profit = 0, 0 ,0
    items = frappe.db.sql("""SELECT item.name, item.barcode_retial, item.item_discount from
        `tabItem` item LEFT JOIN `tabItem Class Each` item_class ON item_class.parent = item.name where
        item_class.classification = %s""", category)
    for item in items :
        try:
            #total_sales = frappe.db.sql( """select sum(qty), sum(amount), ((sum(amount))-(sum(qty*cost*(1-%s)))) from
            #`tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s AND barcode = %s""",
            #                             (item[2], '%' + branch + '%', str(from_date), str(to_date), item[1]) )
            total_sales = get_idv_sales(branch, from_date, to_date, item[0])
            #print total_sales
            total_qty +=float(total_sales[0])
            total_amount +=float(total_sales[1])
            total_profit +=float(total_sales[2])
        except:
            continue
    return  total_qty, total_amount, total_profit


def get_overall_sales(branch, from_date, to_date):
    print "----------------GET OVERALL SALES------------------"
    sum_volume, sum_amount, sum_profit = 0, 0, 0
    print branch, from_date, to_date
    categories = frappe.db.sql("""SELECT name from `tabItem Category`""")
    for category in categories:
        print category[0]
        #print get_total_sales(branch, from_date, to_date, category[0])
        temp_volume, temp_peso, temp_profit = get_category_sales(branch, from_date, to_date, category[0])
        sum_volume += temp_volume
        sum_amount += temp_peso
        sum_profit += temp_profit
    return  sum_volume, sum_amount, sum_profit

def get_category_sales(branch, from_date, to_date, category):
    total_qty, total_amount, total_profit = 0, 0 ,0
    items = frappe.db.sql("""SELECT item.name, item.barcode_retial, item.item_discount from
        `tabItem` item LEFT JOIN `tabItem Class Each` item_class ON item_class.parent = item.name where
        item_class.category = %s""", category)
    for item in items :
        try:
            #total_sales = frappe.db.sql( """select sum(qty), sum(amount), ((sum(amount))-(sum(qty*cost*(1-%s)))) from
            #`tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s AND barcode = %s""",
            #                             (item[2],'%' + branch + '%', str(from_date), str(to_date), item[1]) )
            #print total_sales
            #total_qty +=float(total_sales[0][0])
            #total_amount +=float(total_sales[0][1])
            #total_profit += float(total_sales[0][2])
            total_sales = get_idv_sales(branch, from_date, to_date, item[0])
            # print total_sales
            total_qty += float(total_sales[0])
            total_amount += float(total_sales[1])
            total_profit += float(total_sales[2])
        except:
            continue

    return  total_qty, total_amount, total_profit


def get_classification_ranking(filters, branch, from_date, to_date, show_peso_value):
    columns = []
    data = []

    v_perc = float(filters.get("volume")) / 100
    p_perc = float(filters.get("peso_value")) / 100
    pr_perc = float(filters.get("profit")) / 100

    if show_peso_value == 1:
        columns = [
            {"label": "Category", 'width': 200, "fieldname": "category"},
            {"label": "Classification", 'width': 200, "fieldname": "classification"},
            {"label": "Volume Sales", 'width': 150, "fieldname": "volume", "fieldtype": "float"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Revenue Value", 'width': 150, "fieldname": "revenue", "fieldtype": "Currency"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            {"label": "Profit Value", 'width': 170, "fieldname": "profit", "fieldtype": "Currency"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
    else:
        columns = [
            {"label": "Category", 'width': 200, "fieldname": "category"},
            {"label": "Classification", 'width': 200, "fieldname": "classification"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
    sum_volume, sum_amount, sum_profit = get_overall_sales(branch, from_date, to_date)
    print "TOTALS"
    print sum_volume, sum_amount, sum_profit
    classifications = frappe.db.sql("""select distinct(category), classification from `tabItem Class Each` where
                                    classification is not NULL and classification != "" order by category asc""")

    for classification in classifications:
        volume, revenue, profit = 0, 0, 0
        item_cat = classification[0]
        item_class = classification[1]
        items = frappe.db.sql(
            """SELECT parent from `tabItem Class Each` where category = %s and classification=%s""",
            (classification[0], classification[1]))
        for item in items:
            item_volume, item_peso_value, item_profit = get_idv_sales(branch, from_date, to_date, item[0])
            volume += item_volume
            revenue += item_peso_value
            profit += item_profit

        print volume, revenue, profit
        volume_perc = 0 if sum_volume == 0 else (volume / sum_volume) * 100
        peso_perc = 0 if sum_amount == 0 else(revenue / sum_amount) * 100
        profit_perc = 0 if sum_profit == 0 else(profit / sum_profit) * 100
        total_perc = 0 if volume_perc == 0 else(v_perc * volume_perc) + (p_perc * peso_perc) + (pr_perc * profit_perc)
        temp = {"category": str(item_cat), "classification": item_class, "volume": volume,
                "volume_perc":volume_perc, "revenue":revenue, "peso_perc":peso_perc, "profit":profit,
                "profit_perc":profit_perc, "overall":total_perc}
        data.append(temp)

    return columns, data


def get_category_market_share(filters=None, branch = None, from_date = None, to_date = None, category = None, with_subclass = None):

    data = None
    if with_subclass == False:
        columns = [
            {"label": "classification", 'width': 200, "fieldname": "classification"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
        data = get_classifications_marketshare(filters, branch, from_date, to_date, category, False)
        #print columns, get_classifications_marketshare(filters, branch, from_date, to_date, category, False)

    elif with_subclass == True :
        columns = [
            {"label": "classification", 'width': 200, "fieldname": "classification"},
            {"label": "Sub Class", 'width': 200, "fieldname": "subclass"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
        data = get_classifications_marketshare(filters, branch, from_date, to_date, category, True)

    elif with_subclass == "mktshare_class":
        columns = [
            {"label": "classification", 'width': 200, "fieldname": "classification"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            # {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
        data = get_item_marketshare(filters, branch, from_date, to_date, category, True)

    elif with_subclass == "mktshare_cat":
        columns = [
            {"label": "classification", 'width': 200, "fieldname": "classification"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            # {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
        data = get_item_marketshare(filters, branch, from_date, to_date, category, False)

    else:
        columns = [
            {"label": "classification", 'width': 200, "fieldname": "classification"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Item", 'width': 200, "fieldname": "item"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
        data = get_item_marketshare(filters, branch, from_date, to_date, category, "eh")

    return columns, data

def get_category_market_share_show_peso_value(filters=None, branch = None, from_date = None, to_date = None, category = None, with_subclass = None):

    data = None

    if with_subclass == "mktshare_class":
        print "SULOD PUD KO DIRI DAYON HA~!"
        columns = [
            {"label": "classification", 'width': 200, "fieldname": "classification"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
        data = get_item_marketshare(filters, branch, from_date, to_date, category, True)

    elif with_subclass == "mktshare_cat":
        columns = [
            {"label": "classification", 'width': 200, "fieldname": "classification"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "volume_perc", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_perc", "fieldtype": "Percent"},
            {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
        data = get_item_marketshare(filters, branch, from_date, to_date, category, False)

    return columns, data

def get_classifications_marketshare(filters = None, branch = None, from_date = None, to_date = None, category = None, with_sublass = None):
    list_to_pass = []
    class_type = ""
    current_class = None

    v_perc = float(filters.get("volume")) / 100
    p_perc = float(filters.get("peso_value")) / 100
    pr_perc = float(filters.get("profit")) / 100

    if with_sublass == True:
        class_type = "subclass"
    else:
        class_type = "classification"
    try:
        overall = 0
        sum_volume, sum_amount, sum_profit = get_category_sales(branch, from_date, to_date, category)
        if sum_volume == None:
            sum_volume = 0
        if sum_amount == None:
            sum_amount = 0
        if sum_profit == None:
            sum_profit = 0

        print sum_volume, sum_amount, sum_profit

        classifications = frappe.db.sql("""SELECT DISTINCT classification from `tabItem Class Each` where
            category = %s ORDER BY name ASC""", category)
        for row in classifications:

            try:
                temp_volume, temp_peso, temp_profit = get_classification_sales(branch, from_date, to_date, category, "classification", row[0])
                vol_perc = (float(temp_volume) / float(sum_volume)) * 100
                peso_perc = (float(temp_peso) / float(sum_amount)) * 100
                prof_perc = (float(temp_profit) / float(sum_profit)) * 100
                end_vol_percent = ((float(vol_perc)) * v_perc)
                end_peso_percent = ((float(peso_perc)) * p_perc)
                end_prof_percent = ((float(prof_perc)) * pr_perc)
                overall = end_vol_percent + end_peso_percent + end_prof_percent

                #temp = {"category": category[0], "volume": temp_volume,  "revenue": temp_peso, "profit": temp_profit, "overall":overall}
                print temp_volume, temp_peso, temp_profit, overall
            except:
                print "EH"
                temp = {"classification": "TOTAL FOR "+str(row[0]).capitalize(), "volume_perc": 0,
                        "peso_perc": 0, "profit_perc": 0, "overall": 0}
                list_to_pass.append(temp)
                #append zeroes
            else:
                print "Append temp to data."
                temp = {"classification": "TOTAL FOR "+str(row[0]).capitalize(), "volume_perc": vol_perc,
                        "peso_perc": peso_perc, "profit_perc": prof_perc, "overall": overall}
                list_to_pass.append(temp)

            if class_type == "subclass":
                rows = frappe.db.sql(
                    """Select name, classification from `tabItem Sub Classification` where classification = %s and category = %s""",
                    (row[0], category))
                print "============subclass mi lord============"
                for subclass in rows:
                    sum_subclass_volume, sum_subclass_peso, sum_subclass_profit, sum_subclass_overall = 0, 0, 0, 0
                    try:
                        print subclass
                        subclass_volume, subclass_peso, subclass_profit = get_classification_sales(branch, from_date,
                            to_date, category,"subclass", subclass[0])
                        print "---------ballluuueeeesss-------------------"
                        print sum_volume, sum_amount, sum_profit
                        print subclass_volume, subclass_peso, subclass_profit
                        s_vol_perc = (float(subclass_volume) / float(sum_volume)) * 100
                        s_peso_perc = (float(subclass_peso) / float(sum_amount)) * 100
                        s_prof_perc = (float(subclass_profit) / float(sum_profit)) * 100
                        end_vol_percent = ((float(s_vol_perc)) * v_perc)
                        end_peso_percent = ((float(s_peso_perc)) * p_perc)
                        end_prof_percent = ((float(s_prof_perc)) * pr_perc)
                        overall = end_vol_percent + end_peso_percent + end_prof_percent
                        sum_subclass_volume +=s_vol_perc
                        sum_subclass_peso +=s_peso_perc
                        sum_subclass_profit +=s_prof_perc
                        sum_subclass_overall += overall
                        print "=================SUM SUBCLASS ================="
                        print  sum_subclass_volume, sum_subclass_peso, sum_subclass_profit, sum_subclass_overall
                    except:
                        print "EH"
                        temp = {"classification": row[0],"subclass": subclass[0], "volume_perc": 0,
                                "peso_perc": 0, "profit_perc": 0, "overall": 0}
                        list_to_pass.append(temp)
                        # append zeroes
                    else:
                        print "Append subclass to data."
                        temp = {"classification": row[0],"subclass": subclass[0], "volume_perc": s_vol_perc,
                                "peso_perc": s_peso_perc, "profit_perc": s_prof_perc, "overall": overall}
                        list_to_pass.append(temp)

                    #temp_volume, temp_peso, temp_profit

    except:
        # print "ERROR on Category = "+category[0]
        temp = {"category": category[0], "volume": 0, "volume_perc": 0,
                "revenue": 0, "peso_perc": 0, "profit_perc": 0, "profit": 0,
                "overall": 0}
        # print temp
        list_to_pass.append(temp)

    list_to_pass = reorder_list(list_to_pass, "overall", 'classification',True)
    return list_to_pass

def get_item_marketshare(filters, branch, from_date, to_date, category, by_classification):
    data = []
    # is_summary = filters.get("summary")

    v_perc = float(filters.get("volume")) / 100
    p_perc = float(filters.get("peso_value")) / 100
    pr_perc = float(filters.get("profit")) / 100

    itm_cat = frappe.db.sql("""Select DISTINCT parent, sub_class from `tabItem Class Each` where classification = %s""",
                            (category))
    whole_sale, whole_sale_peso, whole_profit = get_category_sales(branch, from_date, to_date, category)
    print "total sales data"
    print whole_sale, whole_sale_peso, whole_profit

    items = frappe.db.sql("""SELECT item.name, item.barcode_retial, item_class.classification, item.item_discount from
            `tabItem` item LEFT JOIN `tabItem Class Each` item_class ON item_class.parent = item.name where
            item_class.category = %s ORDER BY item_class.classification ASC """, category)

    for item in items:
        try:
            #total_sales = frappe.db.sql("""select sum(qty), sum(amount), ((sum(amount))-(sum(qty*cost*(1-(%s))))) from
            #    `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s AND barcode = %s""",
            #                            (item[3],'%' + branch + '%', str(from_date), str(to_date), item[1]))
            itm_sales, itm_sales_peso, itm_profit = get_idv_sales(branch, from_date, to_date, item[0])
            #itm_sales = float(total_sales[0][0])
            #itm_sales_peso = float(total_sales[0][1])
            #itm_profit = float(total_sales[0][2])

            if (itm_sales != 'None'):
                # itm_profit_f = abs(float(itm_profit))
                itm_profit_f = (float(itm_profit))
                vol_perc = (float(itm_sales) / float(whole_sale)) * 100
                peso_perc = (float(itm_sales_peso) / float(whole_sale_peso)) * 100
                prof_perc = (float(itm_profit_f) / float(whole_profit)) * 100
                end_vol_percent = ((float(vol_perc)) * v_perc)
                end_peso_percent = ((float(peso_perc)) * p_perc)
                end_prof_percent = ((float(prof_perc)) * pr_perc)
                overall = end_vol_percent + end_peso_percent + end_prof_percent

                print "ITEM SALES   "
                print itm_profit_f, itm_sales, itm_sales_peso
                supplier = frappe.db.sql("""Select supplier from `tabItem Supplier` where parent = %s""", (item[0]))
                for s in supplier:
                    sup = s[0]
                    item_doc = frappe.get_doc("Item", item[0])
                    data.append({"category":category, "item":item_doc.item_name_dummy, "classification": item[2], "supplier": sup, "volume_perc": vol_perc,
                            "peso_perc": peso_perc, "profit_perc": prof_perc, "overall": overall, "peso_value":float(itm_sales_peso)
                                 })

        except:
            print "error at "+item[0]
    if by_classification == True:
        data = reorder_list(data, "supplier", 'classification', True)
        data = get_supplier_totals(data, 'classification')
        data = reorder_list(data, "overall", 'classification', True)
    elif by_classification == False:
        data = reorder_list(data, 'supplier', 'category', True)
        data = get_supplier_totals(data, 'category')
        data = reorder_list(data, "overall", 'category', True)
    else:
        data = reorder_list(data, "supplier", 'classification', True)

    return data
def calc_discount(from_date, to_date, category, branch, classification):
    if category == None:
        category = ""
    if classification == None:
        classification = ""

    itm_cat = frappe.db.sql("""Select parent, sub_class from `tabItem Class Each` where category LIKE %s and classification LIKE %s""",
                            ('%'+category+'%', '%'+classification+'%'))
    volume, peso, profit = 0
    for itm in itm_cat:
        itm_code = str(itm[0])
        #idv_sales = frappe.db.sql("""select ((sum(amount))-(sum((qty*cost) * (1-(Select item_discount from `tabItem` where item_code = %s and item_discount is not null)))))
        #                                          from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
        #                                          AND barcode = (Select barcode_retial from `tabItem` where item_code = %s)""",
        #                          (itm_code, '%' + branch + '%', str(from_date), str(to_date), itm_code))
        #for i in idv_sales:
        #    itm_profit = str(i[0])
        #    if itm_profit != 'None':
        #        profit += float(itm_profit)
        #    else:
        #        itm_profit = 0
        #        profit += float(itm_profit)
        temp = get_idv_sales(branch, from_date, to_date, itm[0])
        volume += temp[0]
        peso += temp[1]
        profit += temp[2]
    return volume, peso, profit

def get_classification_sales(branch, from_date, to_date, category, class_type, class_name):
    print "GET CLASS SALES METHOD"
    total_qty, total_amount, total_profit = 0, 0 ,0
    items = None

    if class_type == "classification":
        items = frappe.db.sql("""SELECT item.name, item.barcode_retial, item.item_discount from
            `tabItem` item LEFT JOIN `tabItem Class Each` item_class ON item_class.parent = item.name where
            item_class.category = %s AND item_class.classification = %s """, (category, class_name))

    else:
        items = frappe.db.sql("""SELECT item.name, item.barcode_retial, item.item_discount from
            `tabItem` item LEFT JOIN `tabItem Class Each` item_class ON item_class.parent = item.name where
            item_class.category = %s AND item_class.sub_class = %s """, (category, class_name))

    for item in items :
        try:
            #total_sales = frappe.db.sql( """select sum(qty), sum(amount), ((sum(amount))-(sum(qty*cost*(1-%s)))) from
            #`tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s AND barcode = %s""",
            #                             (item[2],'%' + branch + '%', str(from_date), str(to_date), item[1]) )
            #print total_sales
            total_sales = get_idv_sales(branch, from_date, to_date, item[0])
            total_qty += total_sales[0]
            total_amount += total_sales[1]
            total_profit += total_sales[2]
            #total_qty +=float(total_sales[0][0])
            #total_amount +=float(total_sales[0][1])
            #total_profit += float(total_sales[0][2])
        except:
            continue

    return total_qty, total_amount, total_profit



def reorder_list(newlist, filter, key, is_reverse):
    temp_list = []
    current_subclass = ""
    new_data = []

    for i, item in enumerate(newlist):

        if i == 0:
            current_subclass = item[key]
            temp_list.append(item)
        else:
            if ((i+1) != len(newlist)) and (current_subclass != item[key]):
                print "sort previous. Start new list."
                if filter == "supplier":
                    print item["supplier"], item[key]
                temp_list = sort_by_supplier(temp_list, filter, is_reverse)
                i = len(new_data)
                j = i + len(temp_list)
                new_data[i:j] = temp_list
                temp_list = []
                temp_list.append(item)
                current_subclass = item[key]

                #new_data.append(temp_data)
            elif ((i+1) != len(newlist)) and (current_subclass == item[key]):
                temp_list.append(item)

            elif (i + 1) == len(newlist):
                print "===============LAST ITEM!==================="
                if current_subclass != item[key]:
                    print "sort previous. Start new list."
                    temp_list = sort_by_supplier(temp_list, filter, is_reverse)
                    i = len(new_data)
                    j = i + len(temp_list)
                    new_data[i:j] = temp_list
                    temp_list = []
                    new_data.append(item)
                else:
                    temp_list.append(item)
                    temp_list = sort_by_supplier(temp_list, filter, is_reverse)
                    i = len(new_data)
                    j = i + len(temp_list)
                    new_data[i:j] = temp_list

    #return new_data
    return new_data

def sort_by_supplier(temp_list, filter, is_reverse):

    if filter == 'overall':
        print "overall"
        return sorted(temp_list, key=itemgetter('overall'), reverse=is_reverse)
    else:
        print "supplier"
        return sorted(temp_list, key=lambda k: k[filter], reverse=is_reverse)

def get_supplier_totals(newlist, key):
    print "=====================-GET SUPPLIER TOTALS!-==================="
    sup_perc, total_perc = 0,0
    sup_peso, total_peso = 0,0
    sup_profit, total_profit = 0,0
    sup_total, total_overall = 0,0
    sup_peso_value, total_peso_value = 0,0
    volume_perc = 0
    peso_perc = 0
    profit_perc = 0
    peso_value =0
    overall = 0
    new_data = []
    current_supplier = ""
    current_key = ""

    for i, item in enumerate(newlist):

        total_perc += item['volume_perc']
        total_peso += item['peso_perc']
        total_profit += item['profit_perc']
        total_overall += item['overall']
        total_peso_value += item['peso_value']
    #{"category":category, "classification": item[2], "supplier": sup, "volume_perc": vol_perc,
    #                        "peso_perc": peso_perc, "profit_perc": prof_perc, "overall": overall
    #                             }
        if i == 0:
            current_key = item[key]
            current_supplier = item['supplier']
            sup_perc, volume_perc = item['volume_perc'], item['volume_perc']
            sup_peso, peso_perc = item['peso_perc'], item['peso_perc']
            sup_profit, profit_perc = item['profit_perc'], item['profit_perc']
            sup_total, overall = item['overall'], item['overall']
            sup_peso_value, peso_value = item["peso_value"], item["peso_value"]

        else:

            if current_key != item[key]:
                #append current supplier data and totals

                temp_sup = {"category":item["category"], "item":item['item'], key: current_key, "supplier": current_supplier, "volume_perc": sup_perc,
                             "peso_perc": sup_peso, "profit_perc": sup_profit,
                             "overall": sup_total, "peso_value": sup_peso_value}
                new_data.append(temp_sup)
                temp_data = {key:"<b>TOTAL FOR " + current_key + "</b>", "volume_perc": volume_perc,
                             "peso_perc": peso_perc, "profit_perc": profit_perc, "peso_value": peso_value,
                             "overall": overall}
                new_data.append(temp_data)
                current_supplier = item['supplier']
                current_key = item[key]
                sup_perc, volume_perc = item['volume_perc'], item['volume_perc']
                sup_peso, peso_perc = item['peso_perc'], item['peso_perc']
                sup_profit, profit_perc = item['profit_perc'], item['profit_perc']
                sup_total, overall = item['overall'], item['overall']
                sup_peso_value, peso_value = item["peso_value"], item["peso_value"]


            else:
                #new_data.append(item)
                volume_perc += item['volume_perc']
                peso_perc += item['peso_perc']
                profit_perc += item['profit_perc']
                overall += item['overall']
                peso_value += item['peso_value']

                if current_supplier != item['supplier']:
                    temp_data = {"category": item['category'], "item":item['item'], key: current_key,"supplier":current_supplier,
                                 "volume_perc": sup_perc, "peso_perc": sup_peso, "profit_perc": sup_profit, "peso_value": sup_peso_value,
                                 "overall": sup_total}
                    new_data.append(temp_data)
                    current_supplier = item['supplier']
                    current_key = item[key]
                    sup_perc = item['volume_perc']
                    sup_peso = item['peso_perc']
                    sup_profit = item['profit_perc']
                    sup_total = item['overall']
                    sup_peso_value = item['peso_value']
                else:
                    sup_perc += item['volume_perc']
                    sup_peso += item['peso_perc']
                    sup_profit += item['profit_perc']
                    sup_total += item['overall']
                    sup_peso_value += item['peso_value']


            if (i + 1) == len(newlist):
                temp_data = {"category": item['category'],"item":item['item'], key: current_key, "supplier": current_supplier,
                             "volume_perc": sup_perc, "peso_perc": sup_peso, "profit_perc": sup_profit, "peso_value":sup_peso_value,
                             "overall": sup_total}
                new_data.append(temp_data)
                temp_data = {key: "<b>TOTAL FOR " + str(current_key) + "</b>", "volume_perc": volume_perc,
                             "peso_perc": peso_perc, "profit_perc": profit_perc, "peso_value":peso_value,
                             "overall": overall}
                new_data.append(temp_data)
    return new_data
    #return new_data

def get_idv_sales(branch, from_date, to_date, item_code):
    print "GET IDV SALES FUNCTION"
    volume , peso_value, profit = 0, 0, 0
    d1, d2, d3, d4 = get_supplier_discounts(item_code, branch)
    try:
        item_doc = frappe.get_doc("Item", item_code)
    except:
        print "ERROR"
    else:
        idv_sales = frappe.db.sql("""select sum(qty), sum(amount), sum(qty*cost) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
                              trans_date <= %s AND barcode = %s""",
                                  ('%' + branch + '%', str(from_date), str(to_date), str(item_doc.barcode_retial)))
        if len(idv_sales) > 0:
            for sales in idv_sales:
                volume = (sales[0] if sales[0] != None else 0)
                peso_value = (sales[1] if sales[1] != None else 0)
                total_cost = (sales[2] if sales[2] != None else 0)
                net_cost = ((((float(total_cost) * (1 - d1)) * (1 - d2)) * (1 - d3)) * (1 - d4))
                profit = peso_value - (net_cost)
    print volume , peso_value, profit
    return volume, peso_value, profit


def get_supplier_discounts(item_code, branch):
    discounts = frappe.db.sql("""Select supplier_discount from `tabItem Branch Discount` where parent = %s and branch = %s""",(item_code, branch) )
    disc1, disc2, disc3, disc4 = 0, 0, 0, 0
    if len(discounts) > 0:
        for discount in discounts:
            supplier_discount = frappe.get_doc("Supplier Discounts", discount[0])
            disc1 = (supplier_discount.disc_1 if supplier_discount.disc_1 != None else 0)
            disc2 = (supplier_discount.disc_2 if supplier_discount.disc_2 != None else 0)
            disc3 = (supplier_discount.disc_3 if supplier_discount.disc_3 != None else 0)
            disc4 = (supplier_discount.disc_4 if supplier_discount.disc_4 != None else 0)
    return float(disc1/100), float(disc2/100), float(disc3/100), float(disc4/100)

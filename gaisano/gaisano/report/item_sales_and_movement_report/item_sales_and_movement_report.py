# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from dateutil.relativedelta import relativedelta
import datetime


def execute(filters=None):
    columns = get_columns()
    data = get_results(filters)
    return columns, data


def get_columns():
    columns = [
        {"label": "Item Code", 'width': 280, "fieldname": "item_code"},
        {"label": "Packing", 'width': 100, "fieldname": "packing"},
        {"label": "POS|WS|ST", 'width': 150, "fieldname": "b1"},
        {"label": "1st 30 Days", 'width': 80, "fieldname": "m1"},
        {"label": "POS|WS|ST", 'width': 150, "fieldname": "b2"},
        {"label": "2nd 30 Days", 'width': 80, "fieldname": "m2"},
        {"label": "POS|WS|ST", 'width': 150, "fieldname": "b3"},
        {"label": "Last 30 Days", 'width': 80, "fieldname": "m3"},
        {"label": "Offtake", 'width': 50, "fieldname": "cisl"},
        {"label": "Cycle Days", 'width': 50, "fieldname": "cycle_days"},
        {"label": "CISL", 'width': 50, "fieldname": "offtake"}#,
        #{"label": "Old Offtake m1", 'width': 50, "fieldname": "old_m1"},
        #{"label": "Old Offtake m2", 'width': 50, "fieldname": "old_m2"},
        #{"label": "Old Offtake m3", 'width': 50, "fieldname": "old_m3"},
        #{"label": "Old Offtake/CISL", 'width': 50, "fieldname": "old_offtake"},
    ]
    return columns

def get_results(filters):
    data = []
    to_date = filters.get("to_date")
    m1_range = get_dates(to_date, 60)[2]
    m2_range = get_dates(to_date, 30)[2]
    m3_range = get_dates(to_date, 0)[2]

    dates = {"b1": m1_range, "b2": m2_range, "b3": m3_range }
    data.append(dates)
    if (filters.get("item_name")=="") or (filters.get("item_name")==None):
        data = get_items_from_supplier_discount(filters, data)
    else:
       data = get_item_details(filters, data)
    return data

def get_item_details(filters, data):
    item_code = filters.get("item_name")
    item_doc = frappe.get_doc("Item",item_code)
    branch = filters.get('branch')
    supplier = filters.get('supplier')
    to_date = filters.get('to_date')
    from_date = get_dates(to_date,60)[1]

    offtake = get_qty_sold(branch, item_code, supplier, from_date, to_date)[4]
    cisl = get_qty_sold(branch, item_code, supplier, from_date, to_date)[5]
    cycle_days = get_qty_sold(branch, item_code, supplier, from_date, to_date)[6]
    m1, b1 = get_m1(filters, item_code)
    m2, b2 = get_m2(filters, item_code)
    m3, b3 = get_m3(filters, item_code)
    packing = get_uom(item_code, supplier)
    temp = {"item_code": item_doc.item_name_dummy, "packing":packing, "b1":b1, "b2":b2, "b3":b3, "m1": m1, "m2": m2, "m3": m3, "cisl":cisl, "cycle_days":cycle_days, "offtake": offtake}
    data.append(temp)
    return data

def get_dates(to_date, days):

    now = datetime.datetime.strptime(to_date, '%Y-%m-%d').date()
    now -= datetime.timedelta(days=days)
    date = now - datetime.timedelta(days=30)
    dates = ""+str(date)+" to "+str(now)
    return  now, date, dates


def get_m1(filters, item_code):
    branch = filters.get('branch')
    supplier = filters.get('supplier')
    to_date = filters.get('to_date')

    total = 0
    eh = 0

    now, date, dates = get_dates(to_date, 60)

    s1, s2, s3 = 0, 0, 0
    total= get_qty_sold(branch, item_code, supplier, date, now)[0]
    s1, s2, s3 = get_qty_sold(branch, item_code, supplier, date, now)[1], get_qty_sold(branch, item_code, supplier, date, now)[2], get_qty_sold(branch, item_code, supplier, date, now)[3]
    eh = get_uom(item_code, supplier)
    total /= eh
    breakdown = str(int(s1/eh)) + " | " + str(int(s2/eh)) + " | " + str(int(s3/eh))

    return int(total), breakdown

def get_m2(filters, item_code):
    branch = filters.get('branch')
    supplier = filters.get('supplier')
    to_date = filters.get('to_date')
    total = 0
    eh = 0
    now, date, dates = get_dates(to_date, 30)
    date += datetime.timedelta(days=1)
    s1, s2, s3 = 0, 0, 0
    total= get_qty_sold(branch, item_code, supplier, date, now)[0]
    s1, s2, s3 = get_qty_sold(branch, item_code, supplier, date, now)[1], get_qty_sold(branch, item_code, supplier, date, now)[2], get_qty_sold(branch, item_code, supplier, date, now)[3]
    eh = get_uom(item_code, supplier)
    total /= eh
    breakdown = str(int(s1/eh)) + " | " + str(int(s2/eh)) + " | " + str(int(s3/eh))
    return int(total), breakdown

def get_m3(filters, item_code):
    branch = filters.get('branch')
    supplier = filters.get('supplier')
    to_date = filters.get('to_date')
    total = 0
    eh = 0
    now, date, dates = get_dates(to_date, 0)
    date += datetime.timedelta(days=1)
    s1, s2, s3 = 0, 0, 0
    total = get_qty_sold(branch, item_code, supplier, date, now)[0]
    s1, s2, s3 = get_qty_sold(branch, item_code, supplier, date, now)[1], get_qty_sold(branch, item_code, supplier, date, now)[2], get_qty_sold(branch, item_code, supplier, date, now)[3]
    eh = get_uom(item_code, supplier)
    total /= eh
    breakdown = str(int(s1/eh)) + " | " + str(int(s2/eh)) + " | " + str(int(s3/eh))
    return int(total), breakdown

def get_items_from_supplier_discount(filters, data):
    sup_disc = filters.get('supplier')
    to_date = filters.get('to_date')
    from_date = get_dates(to_date,60)[1]

    items = frappe.db.sql("""Select name, item_cost, uom, item_code, item_name_dummy from tabItem where name in
                          (Select items from `tabSupplier Discount Items` where parent = %s order by items asc)""", sup_disc)

    for item in items:
        item_code = item[3]
        offtake = get_qty_sold(filters.get('branch'), item[3], sup_disc, from_date, to_date)[4]
        cisl = get_qty_sold(filters.get('branch'), item[3], sup_disc, from_date, to_date)[5]
        cycle_days = get_qty_sold(filters.get('branch'), item[3], sup_disc, from_date, to_date)[6]
        m1,b1 = get_m1(filters, item_code)
        m2,b2 = get_m2(filters, item_code)
        m3,b3 = get_m3(filters, item_code)
        packing = get_uom(item_code, sup_disc)
        #old_m1, old_m2, old_m3, old_offtake = get_old_system_offtake(to_date, item_code, filters.get('branch'), get_uom(item_code, sup_disc))
        #old_offtake = (old_offtake * float(cycle_days))/90
        temp = {"item_code": item[4], "packing": packing, "b1":b1, "b2":b2, "b3":b3, "m1": m1, "m2": m2, "m3": m3, "cisl": cisl,
                "cycle_days": cycle_days, "offtake": offtake}#, "old_m1":old_m1, "old_m2":old_m2, "old_m3":old_m3, "old_offtake":old_offtake}
        data.append(temp)
    return data


def get_cisl(filters):
    cisl = 0
    cisl2 = 0
    branch = filters.get('branch')
    item_code = filters.get('item_name')
    supplier = filters.get('supplier')
    to_date = filters.get('to_date')

    total = 0
    eh = 0

    now = datetime.datetime.strptime(to_date, '%Y-%m-%d').date()
    date = now - datetime.timedelta(days=90)

    s1, s2, s3 = 0, 0, 0
    cisl = get_qty_sold(branch, item_code, supplier, date, now)[4]
    return cisl



def get_qty_sold(branch, item_code, supplier_disc, date, now):
    cisl, cisl2 = 0, 0
    con_f = 1
    s1_sum, s1_qty = 0, 0
    s2_sum, s2_qty = 0, 0
    s3_sum, s3_qty = 0, 0
    new_avg = 0
    uom_conv = 0
    sum = 0
    supplier = ""

    #supplier_temp= frappe.db.sql("""SELECT supplier_name, name from `tabSupplier Discounts` where name = %s""", supplier_disc)
    #for sup in supplier_temp:
    #    print sup
    #    supplier = sup[0]

    frequency = frappe.db.sql("""Select offtake_calculation_frequency from `tabBranch` where branch = %s""", (branch))
    freq = str(frequency[0][0])
    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier Discounts` where name = %s""",(supplier_disc))  # factor calculation
    for f in fac_calc:

        factor = str(f[0])
        #print "FACTOR = "+ factor
        con_factor = frappe.db.sql(
            """Select packing from `tabItem` where item_code = %s""", (item_code))

        for f in con_factor:
            con_f = str(f[0])
        if (str(con_f) == 'None') or (str(con_f) == ''):
            uom_conv = 1
        else:
            uom_conv = float(con_f)

        # ave qty: per piece/smallest UOM
        # count : # of records/days

    #print "========================compute======================"

    s1 = frappe.db.sql("""Select count(qty), sum(qty), uom from `tabUpload POS`
                                                              where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                              barcode = (Select barcode_retial from `tabItem` where item_code = %s)
                                                             and branch = %s""",
                           (date, now, item_code, branch))
    s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                 ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                 and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                 and `tabStock Entry Detail`.item_code = %s
                                 AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                           (date, now, item_code, branch))
    s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                and `tabStock Entry Detail`.item_code = %s
                                AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type ='Stock Transfer' AND
                                `tabStock Entry`.docstatus = 1 AND `tabStock Entry`.change_cost_for_wet_market = %s""",
                       (date, now, item_code, branch, "STOCK TRANSFER TO OTHER DEPARTMENTS (USE SRP)"))

    if str(s1[0][1]) == 'None':
        s1_sum, s1_qty = 0, 0
    else:
        s1_sum = s1[0][1]
        s1_qty = s1[0][0]

    if str(s2[0][1]) == 'None':
        s2_sum, s2_qty = 0, 0
    else:
        s2_sum = (s2[0][1] * uom_conv)
        s2_qty = s2[0][0]

    if str(s3[0][1]) == 'None':
        s3_sum, s3_qty = 0, 0
    else:
        s3_sum = (s3[0][1] * uom_conv)
        s3_qty = s3[0][0]

    divisor = 90 #90 days
            # if (s1_qty+s2_qty+s3_qty)!=0:
            # new_avg = (s1_sum+s2_sum+s3_sum)/(s1_qty+s2_qty+s3_qty)
    sum = s1_sum + s2_sum + s3_sum
    new_avg = sum / divisor  # 90
    if (str(factor) == 'None') or (str(factor)==''):
        factor = 1
    cisl2 = round((float(new_avg) / (float(con_f)) * float(factor)), 0)
    cisl_temp = round(new_avg/float(con_f),2)

    return sum, s1_sum, s2_sum, s3_sum, cisl2, cisl_temp, factor

def get_uom(item_code, supplier_disc):
    uom_conv = 1
    con_f = 1

    #supplier_temp = frappe.db.sql("""SELECT supplier_name, name from `tabSupplier Discounts` where name = %s""",
    #                              supplier_disc)
    #for sup in supplier_temp:
    #    print sup
    #    supplier = sup[0]

    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier Discounts` where name = %s""",
                             (supplier_disc))  # factor calculation
    for f in fac_calc:

        factor = str(f[0])

        con_factor = frappe.db.sql(
            """Select packing from `tabItem` where item_code = %s""", (item_code))

        for f in con_factor:
            con_f = str(f[0])
        #print "==========+CONF+==========="
        #print "conf " + con_f

        if (str(con_f) == 'None') or (str(con_f) == '') :
            uom_conv = 1
        else:
            uom_conv = float(con_f)
    return uom_conv



def get_old_system_offtake(now, item_code, branch, packing):
    barcode = ""
    barcodes = frappe.db.sql("""Select barcode, wholesale from `tabUpload Offtake` where item = %s""", item_code)
    offtake = 0
    for row in barcodes:
        barcode = row[0]

    m1, m2, m3 = 0, 0, 0

    rows = frappe.db.sql("""SELECT m1_qty, m2_qty, m3_qty from `tabUpload Offtake` where barcode = %s and branch = %s""", (barcode, branch))

    if len(rows) == 0:
        offtake = 0
    for data in rows:
        print data, data[0], data[1], data[2]
        if (data[0]==None) or (data[1]== None) or (data[2]==None):
            continue
        else:
            m1 = data[0]
            m2 = data[1]
            m3 = data[2]
            sum = (int(data[0]) + int(data[1]) + int(data[2]))
            offtake = (sum)
            print offtake

    return m1, m2, m3, offtake

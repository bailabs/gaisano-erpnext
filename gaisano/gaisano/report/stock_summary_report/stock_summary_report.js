// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Stock Summary Report"] = {
	"filters": [
		 {
		 	"fieldname":"branch",
		 	"label": __("Branch"),
		 	"fieldtype": "Link",
		 	"options": "Branch",
		 	"reqd": 1
		},
		{
			"fieldname":"warehouse",
			"label": __("Warehouse"),
			"fieldtype": "Link",
			"options": "Warehouse",
			"reqd": 1
		},
				{
			"fieldname":"item_supplier",
			"label": __("Supplier"),
			"fieldtype": "Link",
			"options": "Supplier",
			"reqd": 1

		},
		{
			"fieldname":"supplier",
			"label": __("Supplier Discount"),
			"fieldtype": "Link",
			"options": "Supplier Discounts",
			"reqd": 0,
			"get_query": function(){
				var branch = frappe.query_report_filters_by_name.branch.get_value();
				var supplier = frappe.query_report_filters_by_name.item_supplier.get_value();
				console.log(branch,supplier);
				var filters = get_discount_filters(branch,supplier);
				return{
					"filters":filters
        		}
    		},
			"on_change": function(query_report){
                var discount = frappe.query_report_filters_by_name.supplier.get_value();
                if (discount){
                    return frappe.call({
                        method : "frappe.client.get",
                        args :{
                            "doctype" : "Supplier Discounts",
                            "name" : discount
                        },
                        callback: function(r){
                            frappe.query_report_filters_by_name.supplier_discount_name.set_value(r.message['supplier_discount_name']);
                            frappe.query_report.refresh();
                        }
                    })
                }
                else{
                    frappe.query_report_filters_by_name.supplier_discount_name.set_value("");
                    frappe.query_report.refresh();
                }
            }
		},
		{
			"fieldname":"date",
			"label": __("Reference Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname":"show_qty",
			"label": __("Show QTY in Print?"),
			"fieldtype": "Check",
			"reqd": 0
		},
		{
			"fieldname":"phased_out",
			"label": __("Remove Phased Out?"),
			"fieldtype": "Check",
			"reqd": 0
		},
		{
			"fieldname":"exclude_zero_qty",
			"label":__("Remove Zero qty"),
			"fieldtype": "Check",
			"reqd": 0
		},
		{
			"fieldname": "supplier_discount_name",
			//"label": __("Show RR's?"),
			"fieldtype": "Data",
			"reqd":0,
            "read_only":1
		}
	]
};

function get_discount_filters(branch, supplier){
    if(branch == "CDO Main")
        var filters = {"disable":0,"disabled_in_main":0,"supplier_name":supplier};
    else
        var filters = {"disable": 0, "supplier_name":supplier};
    return filters
}

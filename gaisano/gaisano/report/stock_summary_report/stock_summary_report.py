# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import datetime

def execute(filters=None):
    columns = [
        {"label": "Item Code", 'width': 300, "fieldname": "item_code"},
        {"label": "Barcode", 'width': 150, "fieldname": "barcode"},
        {"label": "Barcode Wholesale", 'width': 150, "fieldname": "b_wholesale"},
        {"label": "Packing List", 'width': 100, "fieldname": "packing_list"},
        {"label": "UOM", 'width': 150, "fieldname": "uom"},
        {"label": "Current Qty", 'width': 100, "fieldname": "qty"},

    ]
    data = []
    warehouse = filters.get("warehouse")
    supplier_discount = filters.get("supplier")
    date = filters.get("date")
    item_supplier = filters.get("item_supplier")
    phased_out = filters.get("phased_out")
    exclude_zero_qty = filters.get("exclude_zero_qty")
    if supplier_discount == None:
        supplier_discount = ""
    if date == None:
        date = datetime.datetime.now()

    if supplier_discount == "":
        if phased_out != 1:
            itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom,
                item.barcode_pack, item.item_name_dummy from `tabItem` item inner join `tabItem Supplier` sup
                on sup.parent = item.name where item.type!= 'Disabled' and sup.supplier = %s ORDER BY item.type DESC, item.name ASC""",
                                        (item_supplier))
        else:
            itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom,
                item.barcode_pack, item.item_name_dummy from `tabItem` item inner join `tabItem Supplier` sup
                on sup.parent = item.name where item.type!= 'Disabled' AND item.type!= 'Phased Out' and sup.supplier = %s
                ORDER BY item.type DESC, item.name ASC""", (item_supplier))

    else:
        if phased_out != 1:
            itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom,
                item.barcode_pack, item.item_name_dummy from `tabItem` item inner join `tabItem Supplier` sup
                on sup.parent = item.name left join `tabSupplier Discount Items` disc on disc.items = item.item_code
                where disc.parent = %s and item.type!= 'Disabled' and sup.supplier = %s ORDER BY item.type DESC, item.name ASC""",
                                        (supplier_discount, item_supplier))
        else:
            itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom,
                item.barcode_pack, item.item_name_dummy from `tabItem` item inner join `tabItem Supplier` sup
                on sup.parent = item.name left join `tabSupplier Discount Items` disc on disc.items = item.item_code
                where disc.parent = %s and item.type!= 'Disabled' AND item.type!= 'Phased Out' and sup.supplier = %s
                ORDER BY item.type DESC, item.name ASC""", (supplier_discount, item_supplier))


    for det in itm_det:
        item_code = det[0]
        item_desc = det[5]
        barcode = det[1]
        pack = det[2]
        uom = det[3]
        b_wholesale = det[4]
        qty = get_balance(warehouse, item_code, date)
        if exclude_zero_qty == 1:
            if qty == 0 or qty == None:
                print "Throw"
            else:
                data.append({"item_code": item_desc, "packing_list": pack,
                             "barcode": barcode, "uom": uom,
                             "b_wholesale": b_wholesale, "qty": qty})

        else:
            print item_code, qty
            data.append({"item_code": item_desc, "packing_list": pack,
                 "barcode": barcode, "uom": uom,
                 "b_wholesale": b_wholesale, "qty": qty})

    return columns, data


def get_balance(warehouse, item_code, date):
        recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
                        where warehouse = %s and
                        item_code = %s and
                        posting_date <= %s
                        ORDER BY posting_date DESC, posting_time DESC, name DESC limit 1""", (warehouse, item_code, date))

        try:
            print recon[0][1]
        except:
            balance = 0
        else:
            balance = recon[0][1]
            return balance


# def get_balance2(warehouse, item_code, trans_date):
#     to_date = str(trans_date) + " 23:59:59"
#     from_date = "2015-01-01 00:00:00"
#     recon = 0
#
#
#     sr = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty from `tabStock Reconciliation Item` itm
#                             join `tabStock Reconciliation` sr on sr.name = itm.parent where itm.warehouse = %s and itm.item_code = %s
#                             and sr.posting_date<= %s and sr.docstatus = 1 order by posting_date desc, posting_time desc, sr.name desc limit 1""",
#                        (warehouse, item_code, trans_date))
#
#     if len(sr)>0:
#         if sr[0][0]:
#             from_date = sr[0][0]
#             recon = sr[0][1]
#
#     print(str(from_date), str(to_date))
#
#     rrs = frappe.db.sql("""select sum(itm.stock_qty) from `tabPurchase Receipt Item` itm join `tabPurchase Receipt` rr on
#                         rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s and rr.docstatus = 1
#                         and TIMESTAMP(rr.posting_date, rr.posting_time) between %s and %s""",
#                         (warehouse, item_code, from_date, to_date))
#
#     ses_minus = frappe.db.sql("""select sum(-1*itm.transfer_qty) from `tabStock Entry Detail` itm
#                               join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
#                               and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
#                               and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))
#
#     ses_plus = frappe.db.sql("""select sum(itm.transfer_qty) from `tabStock Entry Detail` itm
#                               join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
#                               and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
#                               and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))
#
#     total_rr = rrs[0][0] if rrs[0][0] is not None else 0
#     total_se_minus = ses_minus[0][0] if ses_minus[0][0] is not None else 0
#     total_se_plus = ses_plus[0][0] if ses_plus[0][0] is not None else 0
#
#     inv = total_rr + total_se_minus + total_se_plus + recon
#
#     return inv

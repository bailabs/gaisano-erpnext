# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime

def execute(filters=None):
	columns, data = [], []

	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	branch = filters.get("branch")
	supplier = filters.get("supplier")
	supplier_discount = filters.get("supplier_discount")
	report_type = filters.get("report_type")

	columns = get_columns(report_type)
	warehouse = filters.get("warehouse")
	data = get_data(from_date, to_date, branch, supplier, supplier_discount)
	date_now = datetime.date.today()

	if warehouse is not "None":
		data = get_inventory_data(data, date_now, warehouse)
	return columns, data

def get_columns(report_type):
	columns = []
	if report_type == "RR VS POS QTY ONLY":
		columns = [
			{"label": "Item", 'width': 200, "fieldname": "item", "fieldtype": "Data"},
			{"label": "UOM", 'width': 50, "fieldname": "uom"},
			{"label": "PACKING", 'width': 50, "fieldname": "packing", "fieldtype":"Int"},
			{"label": "QTY SOLD", "width": 90, "fieldname": "qty_sold_per_uom", "fieldtype": "Float", "precision":2},
			{"label": "POS SALES", "width": 150, "fieldname":"pos_sales", "fieldtype":"Currency"},
			{"label": "QTY RCVD", "width": 90, "fieldname": "rr_qty", "fieldtype": "Float", "precision": 2},
			{"label": "RR PESO VALUE", "width": 150, "fieldname": "rr_value", "fieldtype": "Currency"},
			{"label": "OFFTAKE(LAST 90 DAYS)", "width": 160, "fieldname": "offtake", "fieldtype": "Float", "precision": 2}
		]
	if report_type == "SHOW RR,POS QTY,INV":
		date_now = datetime.date.today()
		date_str = date_now.strftime("%B %d, %y")
		columns = [
			{"label": "Item", 'width': 200, "fieldname": "item", "fieldtype": "Data"},
			{"label": "UOM", 'width': 50, "fieldname": "uom"},
			{"label": "PACKING", 'width': 50, "fieldname": "packing", "fieldtype": "Int"},
			{"label": "QTY SOLD", "width": 90, "fieldname": "qty_sold_per_uom", "fieldtype": "Float", "precision": 2},
			{"label": "POS SALES", "width": 150, "fieldname": "pos_sales", "fieldtype": "Currency"},
			{"label": "QTY RCVD", "width": 90, "fieldname": "rr_qty", "fieldtype": "Float", "precision": 2},
			{"label": "RR PESO VALUE", "width": 150, "fieldname": "rr_value", "fieldtype": "Currency"},
			{"label": ("INV AS OF "+date_str), "width": 90, "fieldname": "inv_qty", "fieldtype": "Float", "precision": 2},
			{"label": "INV PESO VALUE", "width": 150, "fieldname": "inv_value", "fieldtype": "Currency"},
			{"label": "OFFTAKE(LAST 90 DAYS)", "width": 160, "fieldname": "offtake", "fieldtype": "Float", "precision": 2}
		]
	return columns


def get_data(from_date, to_date, branch, supplier, supplier_discount):
    data = []
    items = frappe.db.sql("""SELECT parent from `tabItem Supplier` where supplier = %s""", supplier)
    for item in items:
		if supplier_discount and not check_item_discount(item[0], branch, supplier_discount):
			continue
		else:
			item_doc = frappe.get_doc("Item", item[0])
			qty_sold, pos_sales = get_item_sales(from_date, to_date, item_doc.name, branch)
			offtake_amount, packing = get_offtake(from_date, to_date, item_doc.name, branch)

			offtake_amount = 0 if offtake_amount == None else offtake_amount
			packing = 0 if packing == None else packing

			offtake = 0 if packing == 0 else offtake_amount / packing

			qty_sold = 0 if qty_sold == None else qty_sold
			qty_sold_per_uom = 0 if qty_sold == 0 else qty_sold / packing
			pos_sales = 0 if pos_sales == None else pos_sales

			rr_qty, rr_value = get_item_rrs(from_date, to_date, item_doc.name, branch)

			rr_qty = 0 if rr_qty == None else rr_qty
			rr_value = 0 if rr_value == None else rr_value

			data.append(
			{"item": item_doc.item_name_dummy, "uom": item_doc.uom, "packing": item_doc.packing, "item_code": item_doc.name,
			 "qty_sold_per_uom": qty_sold_per_uom, "pos_sales": pos_sales, "rr_qty": rr_qty, "rr_value": rr_value, "offtake":offtake})

    return data

def get_item_sales(from_date, to_date, item, branch):
	sales = frappe.db.sql("""Select sum(pos.qty), sum(pos.amount) from `tabUpload POS` pos
								inner join `tabItem` item on pos.barcode = item.barcode_retial where item.name = %s and
								pos.trans_date >= %s and pos.trans_date <= %s and pos.branch = %s""", (item, from_date, to_date, branch))

	if len(sales)>0:
		return sales[0][0], sales[0][1]
	else:
		item = frappe.get_doc("Item", item)
		return 0, 0

def get_item_rrs(from_date, to_date, item, branch):
	rrs = frappe.db.sql("""SELECT sum(itm.qty), sum(itm.net_cost_amount) from `tabPurchase Receipt Item` itm inner join `tabPurchase Receipt` rr on itm.parent = rr.name
						where rr.posting_date >= %s and rr.posting_date < %s and itm.item_code = %s and rr.branch = %s
						and rr.docstatus = 1""",(from_date, to_date, item, branch))
        # print from_date
	if len(rrs)>0:
		#print "RR QTY | NET AMT"
		return rrs[0][0], rrs[0][1]
	else:
		return 0, 0

def get_inventory_data(data, to_date, warehouse):
	for row in data:
		item_doc = frappe.get_doc("Item", row['item_code'])
		item_cost = item_doc.item_cost
		#print item_doc
		print "--------------INVENTORY DATA----------------"
		inv = get_balance(warehouse, row['item_code'], to_date)
		peso_value = item_cost*inv
		print row['item_code'], inv, item_cost
		row['inv_qty'] = inv
		row['inv_value'] = peso_value
	return data


def get_balance(warehouse, item_code, date):
	print "get_balance", warehouse, item_code, date
	balance = 0
	recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
						where warehouse = %s and
						item_code = %s and
						posting_date <= %s
						ORDER BY posting_date DESC, posting_time DESC, name DESC limit 1""", (warehouse, item_code, date))
	try:
		print recon[0][1]
	except:
		balance = 0
	else:
		balance = recon[0][1]

	return balance if balance is not None else 0

def get_offtake(from_date, to_date, item, branch):
    filter_date = datetime.datetime.strptime(to_date, '%Y-%m-%d')
    previous_date = filter_date - datetime.timedelta(days=90)
    sales = frappe.db.sql("""Select sum(pos.qty), item.packing from `tabUpload POS` pos
    								inner join `tabItem` item on pos.barcode = item.barcode_retial where item.name = %s and
    								pos.trans_date >= %s and pos.trans_date <= %s and pos.branch = %s""",
                          (item, previous_date, to_date, branch))


    if len(sales) > 0:
        return sales[0][0], sales[0][1]
    else:
        item = frappe.get_doc("Item", item)
        return 0, item.packing

def check_item_discount(item, branch, discount):
	count_disc = frappe.db.sql("""SELECT COUNT(*) from `tabItem Branch Discount` where parent = %s and branch = %s and 
								supplier_discount = %s""",(item, branch, discount))
	print "COUNT DISCOUNT **********************>>>>> ", count_disc[0][0]
	if count_disc[0][0]>0:
		return True
	else:
		return False
// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["PO Status"] = {
	"filters": [
	    {
           "fieldname": "report_type",
           "label": __("Report Type"),
           "fieldtype": "Select",
           "options": ['PO Status', 'Overbooked POs', "Underbooked POs"],
           "reqd":1
       },
		{
           "fieldname": "from_date",
           "label": __("PO Start Date"),
           "fieldtype": "Date",
           "reqd": 1
       },
       {
           "fieldname": "to_date",
           "label": __("PO To Date"),
           "fieldtype": "Date",
           "reqd": 1
       },
       {
           "fieldname": "Supplier",
           "label": __("Supplier"),
           "fieldtype": "Link",
           "options": "Supplier"
       },
       {
           "fieldname": "Branch",
           "label": __("Branch"),
           "fieldtype": "Link",
            "options": "Branch",
           "reqd": 1
       },
       {
           "fieldname": "booking_percent",
           "label": __("Percent Overbooked/Underbooked"),
           "fieldtype": "Float",
           "reqd": 0
       }

	]
};

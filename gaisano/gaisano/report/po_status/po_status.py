# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import datetime, calendar

def execute(filters=None):
    report_type = filters.get("report_type")
    if report_type == "PO Status":
        columns, data = po_status(filters)
    elif report_type == "Overbooked POs":
        columns, data = overbooked_pos(filters)
    else:
        columns, data = underbooked_pos(filters)

    return columns, data

def po_status(filters):
    columns = [
        {"label": "PO #", 'width': 150, "fieldname": "po_name", "fieldtype": "Link", "options": "Purchase Order"},
        {"label": "Supplier", 'width': 150, "fieldname": "supplier"},
        {"label": "Supplier Discount", 'width': 200, "fieldname": "discount"},
        {"label": "Branch", 'width': 150, "fieldname": "branch"},
        {"label": "PO Date", 'width': 100, "fieldname": "po_date"},
        {"label": "Expected Delivery", 'width': 100, "fieldname": "expected_delivery"},
        {"label": "Status", 'width': 100, "fieldname": "status"},
        {"label": "Delivered?", 'width': 100, "fieldname": "stat"},
        {"label": "PO On Schedule?", 'width': 100, "fieldname": "on_schedule"},
        {"label": "RR On Schedule?", 'width': 100, "fieldname": "rr_on_schedule"}
    ]
    data = []
    doc = []
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    supplier = filters.get("Supplier")
    branch = filters.get("Branch")

    if (supplier != "") and (supplier is not None):
        doc = frappe.db.sql("""select po.name, po.supplier, po.branch, po.transaction_date, po.expected_delivery,
                                po.docstatus, rr.name, rr.posting_date, datediff(rr.posting_date, po.expected_delivery),
                                po.supplier_discount_name
                                from `tabPurchase Order` po left join `tabPurchase Receipt` rr
                                on po.name = rr.purchase_order where po.transaction_date >= %s and po.transaction_date <= %s
                                and po.supplier = %s and po.branch = %s""", (from_date, to_date, supplier, branch))
    else:
        doc = frappe.db.sql("""select po.name, po.supplier, po.branch, po.transaction_date, po.expected_delivery,
                                po.docstatus, rr.name, rr.posting_date, datediff(rr.posting_date, po.expected_delivery),
                                po.supplier_discount_name
                                from `tabPurchase Order` po left join `tabPurchase Receipt` rr
                                on po.name = rr.purchase_order where po.transaction_date >= %s and po.transaction_date <= %s
                                and po.branch = %s""", (from_date, to_date, branch))

    for d in doc:
        po_name = d[0]
        supplier = (d[1])
        branch = str(d[2])
        po_date = str(d[3])
        discount = d[9]
        expected_delivery = str(d[4])
        status = "Draft" if d[5] == 0 else ("Submitted" if d[5] == 1 else "Cancelled")
        rr = d[6]
        stat = "YES" if ((rr != "") and (rr is not None)) else "NO"

        # rr_date=d[7]
        date_diff = d[8]

        if (date_diff == "") or (date_diff is None):
            rr_status = "N/A"
        elif date_diff < 0:
            rr_status = "Early Delivery"
        elif date_diff == 0:
            rr_status = "On Schedule"
        else:
            rr_status = "Late Delivery"

        on_schedule = get_po_sched(po_date, d[0])
        data.append(
            {"po_name": po_name, "supplier": supplier, "branch": branch.center(90), "po_date": po_date.center(90),
             "expected_delivery": expected_delivery.center(90), "status": status.center(300),"discount":discount,
             "stat": stat.center(90), "on_schedule": on_schedule, "rr_on_schedule": rr_status})

    return columns, data

def get_po_sched(transaction_date, po):

    weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    po_doc = frappe.get_doc("Purchase Order",po)
    supplier = po_doc.supplier_discount
    po_date = datetime.datetime.strptime(transaction_date, "%Y-%m-%d")

    booking_status = ""

    po_details = frappe.db.sql("""select po_schedule from `tabSupplier Discounts` where name = %s""", supplier)#Change to Supplier Discount
    po_schedule = po_details[0][0]

    if calendar.day_name[po_date.weekday()] != po_schedule:
        return "No"
    else:
        return "Yes"


    return booking_status

def overbooked_pos(filters):
    columns = [
        {"label": "PO #", 'width': 150, "fieldname": "po_name", "fieldtype": "Link", "options": "Purchase Order"},
        {"label": "Supplier", 'width': 150, "fieldname": "supplier"},
        {"label": "Supplier Discount", 'width': 200, "fieldname": "discount"},
        {"label": "Branch", 'width': 150, "fieldname": "branch"},
        {"label": "PO Date", 'width': 100, "fieldname": "po_date"}
    ]

    data = []

    booking_percent = filters.get("booking_percent")
    if booking_percent is None:
        frappe.throw("Please Enter Percent Overbooked/Underbooked")

    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    supplier = filters.get("Supplier")
    branch = filters.get("Branch")

    if (supplier != "") and (supplier is not None):
        docs = frappe.db.sql("""select po.name, po.supplier, po.branch, po.transaction_date,po.supplier_discount_name
                              from `tabPurchase Order` po where po.transaction_date >= %s and po.transaction_date <= %s
                              and po.supplier = %s and po.branch = %s""", (from_date, to_date, supplier, branch))
    else:
        docs = frappe.db.sql("""select po.name, po.supplier, po.branch, po.transaction_date, po.supplier_discount_name
                              from `tabPurchase Order` po where po.transaction_date >= %s and po.transaction_date <= %s
                              and po.branch = %s""", (from_date, to_date, branch))

    for doc in docs:
        po= doc[0]
        supplier = doc[1]
        branch = doc[2]
        po_date = doc[3]
        discount = doc[4]

        items = frappe.db.sql("""select w_cisl, cisl, offtake_info, qty from `tabPurchase Order Item` where parent = %s""", po)

        for item in items:
            qty_orders = float(str(item[3]))
            base_cisl = float(str(item[0] if item[0] > item[1] else item[1]))
            offtake_info = str(item[2]).split(" ")
            inv_index = offtake_info.index("Inv:")
            temp = str(offtake_info[inv_index + 1])
            inv_temp = temp.split("|")
            inv = float(inv_temp[0])

            suggested_orders = 0 if base_cisl<inv else base_cisl - inv

            diff = 100 if suggested_orders ==0 else ((qty_orders - suggested_orders)/suggested_orders)*100
            if diff >= booking_percent:
            # print("wcisl:", item[0], "|poscisl:", item[1])
            # print("qty:",qty_orders,"|base CISL:",base_cisl)
            #print(offtake_info[inv_index])

                data.append({"po_name":po,"supplier":supplier,"branch":branch, "po_date":po_date, "discount":discount})
                break

    return columns, data


def underbooked_pos(filters):
    columns = [
        {"label": "PO #", 'width': 150, "fieldname": "po_name", "fieldtype": "Link", "options": "Purchase Order"},
        {"label": "Supplier", 'width': 150, "fieldname": "supplier"},
        {"label": "Supplier Discount", 'width': 200, "fieldname": "discount"},
        {"label": "Branch", 'width': 150, "fieldname": "branch"},
        {"label": "PO Date", 'width': 100, "fieldname": "po_date"}
    ]

    data = []

    booking_percent = filters.get("booking_percent")
    if booking_percent is None:
        frappe.throw("Please Enter Percent Overbooked/Underbooked")

    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    supplier = filters.get("Supplier")
    branch = filters.get("Branch")

    if (supplier != "") and (supplier is not None):
        docs = frappe.db.sql("""select po.name, po.supplier, po.branch, po.transaction_date, po.supplier_discount_name
                              from `tabPurchase Order` po where po.transaction_date >= %s and po.transaction_date <= %s
                              and po.supplier = %s and po.branch = %s""", (from_date, to_date, supplier, branch))
    else:
        docs = frappe.db.sql("""select po.name, po.supplier, po.branch, po.transaction_date, po.supplier_discount_name
                              from `tabPurchase Order` po where po.transaction_date >= %s and po.transaction_date <= %s
                              and po.branch = %s""", (from_date, to_date, branch))

    for doc in docs:
        po= doc[0]
        supplier = doc[1]
        branch = doc[2]
        po_date = doc[3]
        discount = doc[4]
        items = frappe.db.sql("""select w_cisl, cisl, offtake_info, qty from `tabPurchase Order Item` where parent = %s""", po)

        for item in items:
            qty_orders = float(str(item[3]))
            base_cisl = float(str(item[0] if item[0] > item[1] else item[1]))
            offtake_info = str(item[2]).split(" ")
            inv_index = offtake_info.index("Inv:")
            temp=str(offtake_info[inv_index+1])
            inv_temp = temp.split("|")
            inv = float(inv_temp[0])

            suggested_orders = 0 if base_cisl<inv else base_cisl - inv
            print("underbooked?",suggested_orders,qty_orders)

            diff = 100 if suggested_orders <=0 else ((qty_orders - suggested_orders)/suggested_orders)*100
            if diff <= (-booking_percent):
            # print("wcisl:", item[0], "|poscisl:", item[1])
            # print("qty:",qty_orders,"|base CISL:",base_cisl)
            #print(offtake_info[inv_index])

                data.append({"po_name":po,"supplier":supplier,"branch":branch, "po_date":po_date, "discount":discount})
                break

    return columns, data



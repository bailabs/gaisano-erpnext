// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Delivery Monitoring"] = {
    "filters": [
        {
            "fieldname": "from_date",
            "label": __("PO Start Date"),
            "fieldtype": "Date",
            "reqd": 1
        },
        {
            "fieldname": "to_date",
            "label": __("PO To Date"),
            "fieldtype": "Date",
            "reqd": 1
        },

        {
            "fieldname": "Branch",
            "label": __("Branch"),
            "fieldtype": "Link",
            "options": "Branch",
            "width": 150,
            "reqd": 1
        },

        {
            "fieldname": "Supplier",
            "label": __("Supplier"),
            "fieldtype": "Link",
            "options": "Supplier",
            "reqd": 1
        },
        {
            "fieldname": "supplier_discount",
            "label": __("Supplier Discounts"),
            "fieldtype": "Link",
            "options": "Supplier Discounts",
            "reqd": 0,
            "get_query": function () {
                var branch = frappe.query_report_filters_by_name.Branch.get_value();
                var supplier = frappe.query_report_filters_by_name.Supplier.get_value();
                console.log(branch, supplier);
                var filters = get_discount_filters(branch, supplier);
                return {
                    "filters": filters
                }
            }
        }
    ]
};


function get_discount_filters(branch, supplier) {
    if (branch == "CDO Main")
        var filters = {"disable": 0, "disabled_in_main": 0, "supplier_name": supplier};
    else
        var filters = {"disable": 0, "supplier_name": supplier};
    return filters
}

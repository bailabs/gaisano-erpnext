# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import datetime
from gaisano.poevents import get_balance

def execute(filters=None):
	columns = [
		{"label":"Item Code", "width":180, "fieldname":"item"},
		{"label": "Item Barcode", "width": 180, "fieldname": "barcode"},
		{"label": "Inventory", "width": 100, "fieldname": "inv"},
		{"label": "0-14 days", "width": 80, "fieldname": "i15"},
		{"label": "15-29 days", "width": 80, "fieldname": "i30"},
		{"label": "30-59 days", "width": 80, "fieldname": "i60"},
		{"label": "60-89 days", "width": 80, "fieldname": "i90"},
		{"label": "90 days or More", "width": 80, "fieldname": "i_exceeded"}
	]
	data = get_data(filters)
	return columns, data


def get_data(filters):
	warehouse = filters.get("warehouse")
	supplier = filters.get("supplier")
	supplier_discount = filters.get("supplier_discount")
	item_name = filters.get("item")
	date = filters.get("date")
	data = []
	ref_date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
	print "==============CHECK FILTERS!============"
	print ref_date

	if (supplier!=None) and (supplier!="") and (supplier_discount != None) and (supplier_discount!=""):
		print supplier, supplier_discount
		query = frappe.db.sql("""SELECT itm.name, itm.item_name_dummy, itm.barcode_retial from `tabItem` itm inner join
					`tabItem Supplier` sup on sup.parent = itm.name inner join `tabSupplier Discount Items` disc on disc.items = itm.name where itm.type != 'Disabled' and sup.supplier = %s and disc.parent = %s
					""",(supplier, supplier_discount))

	elif (supplier!=None) and (supplier!=""):
		query = frappe.db.sql("""SELECT itm.name, itm.item_name_dummy, itm.barcode_retial from `tabItem` itm inner join
					`tabItem Supplier` sup on sup.parent = itm.name where itm.type != 'Disabled' and sup.supplier = %s
					""",(supplier))
	else:
		query = frappe.db.sql("""SELECT name, item_name_dummy, barcode_retial from `tabItem` where type != 'Disabled'""",)
	#For Testing: inv = 20
	if (item_name != "") and (item_name !=None):
		print item_name
		item = frappe.get_doc("Item", item_name)
		inv = get_balance(warehouse, item_name, ref_date)
		i15, i30, i60, i90, i_exceeded = get_item_ageing(item_name, warehouse, ref_date, inv)
		temp = {"item":item.item_name_dummy, "barcode":item.barcode_retial, "inv":inv, "i15":i15, "i30":i30, "i60":i60, "i90": i90, "i_exceeded":i_exceeded}
		data.append(temp)

	else:
		print "================items!==============="
		for item in query:
			# print item[0], item[1], item[2]
			inv = get_balance(warehouse, item[0], ref_date)
			if inv <= 0:
				temp = {"item": item[1], "barcode": item[2], "inv":0, "i15":0, "i30":0, "i60":0, "i90": 0, "i_exceeded":0}
				continue
			else:
				i15, i30, i60, i90, i_exceeded = get_item_ageing(item[0],warehouse,ref_date, inv)
				temp = {"item": item[1],"barcode": item[2], "inv":inv, "i15":i15, "i30":i30, "i60":i60, "i90": i90, "i_exceeded":i_exceeded}
			data.append(temp)
	return data

def get_item_ageing(item, warehouse, ref_date, inv):
	print item, warehouse
	inv_list = []
	remainder = 0
	#current_inv = get_balance(warehouse, item, ref_date)
	date_list = [ref_date, (ref_date - datetime.timedelta(days=15)), (ref_date - datetime.timedelta(days=30)),
			 (ref_date - datetime.timedelta(days=60)), (ref_date - datetime.timedelta(days=90))]
	for i, date in enumerate(date_list):
		if i==(len(date_list)-1):
			continue
		from_date = (date_list[i+1])+datetime.timedelta(days=1)
		rrs = frappe.db.sql("""SELECT sum(actual_qty) from `tabStock Ledger Entry` where item_code =%s and posting_date<=%s
						AND posting_date >=%s and voucher_no like %s and warehouse = %s""",
							(item, date, from_date, "RR-%",warehouse))
		qty_received = (rrs[0][0] if rrs[0][0]!=None else 0)
		remainder = (inv - qty_received)
		print qty_received, inv, remainder, "FROM: ", from_date, "-", date
		if remainder>=0:

			inv = remainder
			inv_list.append(qty_received)
		else:
			inv_list.append(inv)
			inv = 0
	return inv_list[0], inv_list[1], inv_list[2], inv_list[3], inv

def get_balance2(warehouse, item_code, trans_date):
    to_date = str(trans_date)+ " 23:59:59"
    from_date = "2015-01-01 00:00:00"
    recon = 0


    sr = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty from `tabStock Reconciliation Item` itm
                            join `tabStock Reconciliation` sr on sr.name = itm.parent where itm.warehouse = %s and itm.item_code = %s
                            and sr.posting_date<= %s and sr.docstatus = 1 order by posting_date desc, posting_time desc, sr.name desc limit 1""",
                       (warehouse, item_code, trans_date))

    if len(sr)>0:
        if sr[0][0]:
            from_date = sr[0][0]
            recon = sr[0][1]

    print(str(from_date), str(to_date))

    rrs = frappe.db.sql("""select sum(itm.stock_qty) from `tabPurchase Receipt Item` itm join `tabPurchase Receipt` rr on
                        rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s and rr.docstatus = 1
                        and TIMESTAMP(rr.posting_date, rr.posting_time) between %s and %s""",
                        (warehouse, item_code, from_date, to_date))

    ses_minus = frappe.db.sql("""select sum(-1*itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
                              and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))

    ses_plus = frappe.db.sql("""select sum(itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
                              and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))

    total_rr = rrs[0][0] if rrs[0][0] is not None else 0
    total_se_minus = ses_minus[0][0] if ses_minus[0][0] is not None else 0
    total_se_plus = ses_plus[0][0] if ses_plus[0][0] is not None else 0

    inv = total_rr + total_se_minus + total_se_plus + recon

    return inv

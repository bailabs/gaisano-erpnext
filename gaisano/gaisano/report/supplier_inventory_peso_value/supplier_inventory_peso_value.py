# Copyright (c) 2017, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt
# Coded by: hvillanueva
from __future__ import unicode_literals
import frappe
import datetime


def execute(filters=None):
    columns = [
        {"label": "Item Code", 'width': 300, "fieldname": "item_code"},
        {"label": "Barcode", 'width': 150, "fieldname": "barcode"},
        {"label": "UOM", 'width': 80, "fieldname": "uom"},
        {"label": "Qty", 'width': 80, "fieldname": "qty"},
        {"label": "Item Cost", 'width': 100, "fieldname": "item_cost", "fieldtype":"Currency"},
        {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype":"Currency"}

    ]
    data = []
    warehouse = filters.get("warehouse")
    supplier = filters.get("supplier")
    date = filters.get("date")
    total_peso_value = 0.0
    show_promo_packs = filters.get("show_promo_packs")
    supplier_discount = filters.get("supplier_discount")

    if supplier == None:
        supplier = ""
    if supplier_discount == None:
        supplier_discount = ""
    if date == None:
        date = datetime.datetime.now()
    print "---------------DATA------------"
    print "supplier", supplier, "supplier_discount", supplier_discount
    if (supplier!= "") and (supplier_discount==""):
        print "filter by supplier"
        itm_det = frappe.db.sql("""Select itm.name, itm.barcode_retial, itm.packing, itm.stock_uom, itm.barcode_pack, itm.item_cost, itm.item_name_dummy, itm.type from `tabItem` itm
                    inner join `tabItem Supplier` sup on sup.parent = itm.name where sup.supplier = %s and itm.type != 'Disabled' ORDER BY itm.type DESC, itm.name ASC""", supplier)
    elif (supplier== "") and (supplier_discount!=""):
        print "filter by supplier discount"
        itm_det = frappe.db.sql("""Select itm.name, itm.barcode_retial, itm.packing, itm.stock_uom, itm.barcode_pack, itm.item_cost, itm.item_name_dummy,itm.type from `tabItem` itm
                            inner join `tabSupplier Discount Items` disc on disc.items = itm.name where disc.parent = %s and itm.type != 'Disabled' ORDER BY itm.type DESC, itm.name ASC""",supplier_discount)
    elif (supplier!="") and (supplier_discount!=""):
        itm_det = frappe.db.sql("""Select itm.name, itm.barcode_retial, itm.packing, itm.stock_uom, itm.barcode_pack, itm.item_cost, itm.item_name_dummy, itm.type from `tabItem` itm
                                    inner join `tabSupplier Discount Items` disc on disc.items = itm.name where disc.parent = %s and itm.type != 'Disabled' ORDER BY itm.type DESC, itm.name ASC""",supplier_discount)
    else: #all items
        itm_det = frappe.db.sql("""Select name, barcode_retial, packing, stock_uom, barcode_pack, item_cost, item_name_dummy, type from `tabItem` where type != 'Disabled' ORDER BY type DESC, name ASC""")

    for det in itm_det:
        item_code = det[0]
        item_desc = det[6]
        barcode = det[1]
        pack = det[2]
        uom = det[3]
        b_wholesale = det[4]
        item_cost = det[5]
        qty = get_balance(warehouse, item_code, date)
        item_type = det[7]

        if item_type == "Company Bundling" and show_promo_packs != 1:
            continue

        if qty is None:
            qty = 0
        if item_cost is None:
            item_cost = 0
        peso_value = float(item_cost)*float(qty)
        total_peso_value+= peso_value
        data.append({"item_code": item_desc, "packing_list": pack,
                 "barcode": barcode, "uom": uom, "b_wholesale": b_wholesale, "qty": qty, "item_cost":item_cost,
                     "peso_value":peso_value})
    data.append({"item_code": "TOTAL PESO VALUE", "peso_value": total_peso_value})
    return columns, data


def get_balance(warehouse, item_code, date):
    recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
                    where warehouse = %s and
                    item_code = %s and
                    posting_date <= %s
                    ORDER BY posting_date DESC, posting_time DESC limit 1""", (warehouse, item_code, date))
    try:
        print "============RECON==========="
        print recon[0][1]
    except:
        balance = 0
    else:
        balance = recon[0][1]

        return balance

# def get_balance2(warehouse, item_code, trans_date):
#     to_date = str(trans_date) + " 23:59:59"
#     from_date = "2015-01-01 00:00:00"
#     recon = 0
#
#
#     sr = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty from `tabStock Reconciliation Item` itm
#                             join `tabStock Reconciliation` sr on sr.name = itm.parent where itm.warehouse = %s and itm.item_code = %s
#                             and sr.posting_date<= %s and sr.docstatus = 1 order by posting_date desc, posting_time desc, sr.name desc limit 1""",
#                        (warehouse, item_code, trans_date))
#
#     if len(sr)>0:
#         if sr[0][0]:
#             from_date = sr[0][0]
#             recon = sr[0][1]
#
#     print(str(from_date), str(to_date))
#
#     rrs = frappe.db.sql("""select sum(itm.stock_qty) from `tabPurchase Receipt Item` itm join `tabPurchase Receipt` rr on
#                         rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s and rr.docstatus = 1
#                         and TIMESTAMP(rr.posting_date, rr.posting_time) between %s and %s""",
#                         (warehouse, item_code, from_date, to_date))
#
#     ses_minus = frappe.db.sql("""select sum(-1*itm.transfer_qty) from `tabStock Entry Detail` itm
#                               join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
#                               and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
#                               and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))
#
#     ses_plus = frappe.db.sql("""select sum(itm.transfer_qty) from `tabStock Entry Detail` itm
#                               join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
#                               and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
#                               and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))
#
#     total_rr = rrs[0][0] if rrs[0][0] is not None else 0
#     total_se_minus = ses_minus[0][0] if ses_minus[0][0] is not None else 0
#     total_se_plus = ses_plus[0][0] if ses_plus[0][0] is not None else 0
#
#     inv = total_rr + total_se_minus + total_se_plus + recon
#
#     return inv

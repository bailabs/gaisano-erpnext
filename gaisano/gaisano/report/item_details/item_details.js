// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Item Details"] = {
	"filters": [
        {
            "fieldname":"branch",
            "label":__("Branch"),
            "fieldtype":"Link",
            "options":"Branch",
            "reqd":1
        },
		{
            "fieldname": "report_type",
            "label": __("Report Type"),
            "fieldtype": "Select",
			"options": ["", "No SKUs in Category", "Cost, SRP, Margin, Price","Old System Cost vs New System Cost", "Old System SRP vs New System SRP", "Check if Old Barcodes in New System", "Item SRP vs Calculated SRP","Check Old System vs New System Department Code", "Check Items for Manual Update"],
            "reqd": 1,
            "on_change":function(){
                var supplier = frappe.query_report_filters_by_name.supplier;
                var discount = frappe.query_report_filters_by_name.supplier_discount;
                var type = frappe.query_report_filters_by_name.report_type.get_value();
                if((type == "No SKUs in Category")||(type == "")){
                    supplier.df.hidden = 1;
                    discount.df.hidden = 1;
                    supplier.refresh();
                    discount.refresh();
                    frappe.query_report.refresh();
                }
                else{
                    supplier.df.hidden = 0;
                    discount.df.hidden = 0;
                    supplier.refresh();
                    discount.refresh();
                    frappe.query_report.refresh();
                }
            }
        },
		{
            "fieldname": "supplier",
            "label": __("Supplier"),
            "fieldtype": "Link",
			"options": "Supplier",
            "reqd": 0
        },
		{
            "fieldname": "supplier_discount",
            "label": __("Supplier Discount"),
            "fieldtype": "Link",
			"options": "Supplier Discounts",
            "reqd": 0,
            "get_query": function () {
                var branch = frappe.query_report_filters_by_name.branch.get_value();
                var supplier = frappe.query_report_filters_by_name.supplier.get_value();
                console.log(branch, supplier);
                var filters = get_discount_filters(branch, supplier);
                return {
                    "filters": filters
                }
            }
        }
	]
};


function get_discount_filters(branch, supplier){
    if(branch == "CDO Main")
        var filters = {"disable":0,"disabled_in_main":0,"supplier_name":supplier};
    else
        var filters = {"disable": 0, "supplier_name":supplier};
    return filters
}

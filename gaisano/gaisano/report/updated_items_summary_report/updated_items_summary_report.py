# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime

def execute(filters=None):
	columns, data = [], []
	branch = filters.get('branch')
	supplier = filters.get("supplier")
	supplier_discount = filters.get("supplier_discount")
	date = filters.get("date")
	category = filters.get("category")
	print "----------GET QUERY---------"
	data = get_query(branch, date, category, supplier, supplier_discount)
	columns = get_columns()

	return columns, data

def get_query(branch, date, category,  supplier = None, supplier_discount= None):
	data = []
	category = category if category is not None else ""
	date_today = datetime.datetime.strptime(date,"%Y-%m-%d")
	date_tomorrow = date_today + datetime.timedelta(days=1)
	tomorrow = date_tomorrow.strftime("%Y-%m-%d")
	if (supplier == None) and (supplier_discount == None):
		items = frappe.db.sql("""select itm.item_name_dummy, itm.barcode_retial, itm.packing, itm.item_cost, item_price, itm.item_price_retail_with_margin, itm.modified, itm.modified_by, itm.margin_rate, itm.retail_margin_rate from `tabItem` itm inner join `tabItem Class Each` ice on itm.name = ice.parent where itm.modified >= %s and itm.modified < %s and type != 'Disabled' and ice.category like %s""",(date, tomorrow, '%'+category+'%'))
	elif (supplier != None) and (supplier_discount == None):
		items = frappe.db.sql("""select itm.item_name_dummy, itm.barcode_retial, itm.packing, itm.item_cost, item_price, itm.item_price_retail_with_margin, itm.modified, itm.modified_by, itm.margin_rate, itm.retail_margin_rate from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabItem Class Each` ice on itm.name = ice.parent where sup.supplier = %s and itm.modified >= %s and itm.modified < %s and itm.type != 'Disabled' and ice.category like %s""",(supplier, date, tomorrow, '%'+category+'%'))
	elif (supplier == None) and (supplier_discount != None):
		items = frappe.db.sql("""select itm.item_name_dummy, itm.barcode_retial, itm.packing, itm.item_cost, item_price, itm.item_price_retail_with_margin, itm.modified, itm.modified_by, itm.margin_rate, itm.retail_margin_rate from `tabItem` itm inner join `tabSupplier Discount Items` disc on disc.items = itm.name inner join `tabItem Class Each` ice on itm.name = ice.parent where disc.parent = %s and itm.modified >= %s and itm.modified < %s and itm.type != 'Disabled' and ice.category like %s""",(supplier_discount, date, tomorrow, '%'+category+'%'))
	elif (supplier != None ) and (supplier_discount != None):
		items = frappe.db.sql("""select itm.item_name_dummy, itm.barcode_retial, itm.packing, itm.item_cost, item_price, itm.item_price_retail_with_margin, itm.modified, itm.modified_by, itm.margin_rate, itm.retail_margin_rate from `tabItem` itm left join `tabItem Supplier` sup on sup.parent = itm.name inner join `tabItem Branch Discount` disc on disc.parent = itm.name inner join `tabItem Class Each` ice on itm.name = ice.parent where sup.supplier = %s and disc.supplier_discount = %s and disc.branch = %s and itm.modified >= %s and itm.modified < %s and itm.type != 'Disabled' and ice.category like %s""",(supplier, supplier_discount, branch, date, tomorrow, '%'+category+'%'))
	for item in items:
		item_name_dummy = item[0]
		barcode_retial = item[1]
		packing = item[2]
		item_cost = item[3]
		item_price = item[4]
		item_price_retail_with_margin = item[5]
		modified = item[6]
		modified_by = item[7]
		ws_margin_rate = item[8]
		ret_margin_rate = item[9]
		data.append({"item_name_dummy": item_name_dummy, "barcode_retial": barcode_retial, "packing": packing, "item_cost": item_cost, "item_price": item_price, "item_price_retail_with_margin": item_price_retail_with_margin,
					 "modified":modified, "modified_by":modified_by, "ws_rate":ws_margin_rate, "ret_rate":ret_margin_rate})

	return data



# def get_query(branch, date, supplier = None, supplier_discount=None):
# 	date_today = datetime.datetime.strptime(date,'%Y-%m-%d')
# 	date_tomorrow = date_today + datetime.timedelta(days=1)
# 	tomorrow = date_tomorrow.strftime('%Y-%m-%d')
#
# 	query ="SELECT itm.* from `tabItem` itm"
# 	inner_join =""
# 	join_conditions =""
# 	if supplier is not None:
# 		inner_join+= " inner join `tabItem Supplier` sup on sup.parent = itm.name"
# 		join_conditions+= " and sup.supplier = '"+supplier+"'"
# 	if supplier_discount is not None:
# 		inner_join += " inner join `tabItem Branch Discount` disc on disc.parent = itm.name"
# 		join_conditions += " and disc.supplier_discount = '" + supplier_discount + "' and disc.branch = '"+branch+"'"
# 	query+= inner_join
# 	conditions = " where itm.type!='Disabled' and itm.modified >='"+date+"' and itm.modified<'"+tomorrow+"'"+join_conditions
# 	query+=conditions
# 	return query

def get_columns():
	columns = [
		{"label": "Item Description","fieldname":"item_name_dummy", "fieldtype":"data","width":300},
		{"label": "Barcode","fieldname": "barcode_retial", "fieldtype": "data", "width": 120},
		{"label": "Packing", "fieldname": "packing", "fieldtype": "data", "width": 100},
		{"label": "Item Cost", "fieldname": "item_cost", "fieldtype" : "Currency", "width": 100},
		{"label": "Wholesale Margin", "fieldname": "ws_rate", "fieldtype": "Percent", "width": 100, "precision":2},
		{"label": "Wholesale SRP", "fieldname": "item_price", "fieldtype": "Currency", "width": 100},
		{"label": "Retail Margin", "fieldname": "ret_rate", "fieldtype": "Percent", "width": 100, "precision": 2},
		{"label": "Retail SRP", "fieldname":"item_price_retail_with_margin", "fieldtype": "Currency", "width": 100},
		{"label": "Modified On", "fieldname": "modified", "fieldtype": "datetime", "width": 180},
		{"label": "Modified By","fieldname": "modified_by", "fieldtype": "data", "width": 150}
	]
	return columns
// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Run ERP Record"] = {
	"filters": [
		{
			'fieldname':'report_type',
			'label':__('Report Type'),
			'fieldtype':'Select',
			'options':['New to old', 'Grocery to POS'],
			'reqd':1,
		}
	]
}

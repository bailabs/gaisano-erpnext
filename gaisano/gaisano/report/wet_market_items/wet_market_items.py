# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	data = get_ladlad_items()
	columns = get_columns()
	branch = filters.get("branch")
	return columns, data

def get_columns():
	columns = [
		{"label": "PLU Code", 'width': 50, "fieldname": "plu_code"},
		{"label": "Barcode", 'width': 100, "fieldname": "barcode"},
		{"label": "Item Name", 'width': 100, "fieldname": "item_name"},
		{"label": "SRP", 'width': 150, "fieldname": "srp"},
		{"label": "Item Code", 'width': 150, "fieldname": "item_code"},
		{"label": "Unit name", 'width': 80, "fieldname": "unit_name"}#,
		#{"label": "group", 'width': 80, "fieldname": "group"}
	]
	return columns

def get_ladlad_items():

	data = []
	eleven = slice(11)
	items = frappe.db.sql("""SELECT barcode_retial,item_short_name, plu_code,item_price_retail_with_margin,pos_uom, name from `tabItem` where ladlad = 1 and plu_code <> '' or plu_code <> Null""")
	for item in items:
		item_name = (item[1][eleven])

		item_code = item[0][1:6]
		uom = 0 if item[4] == "kilo" else 1
		#group = get_group(item[5])

		data.append({"plu_code": item[2], "item_name": item_name, "barcode": item[0], "srp": item[3], "item_code":item_code, "unit_name":uom}) #"group":group})

	return data

def get_group(item_code):
	group = 0

	item_doc = frappe.get_doc("Item", item_code)
	item_class = frappe.db.sql("""SELECT classification, sub_class from `tabItem Class Each` where parent = %s""",item_code)
	for row in item_class:
		subclass = row[1]
		if item_doc.my_suki == 1:
			group = 6
		else:
			if (subclass == "Pork") or (subclass=='Beef'):
				group = 1
			elif (subclass == "Fish") or (subclass=='Chicken'):
				group = 3
			elif (subclass == "Vegetables") or (subclass == "Native Preserves") or (subclass == "Flour"):
				group = 4
			elif (subclass == "Fruits"):
				group = 5
			else:
				group = 2

	return group
# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime


def execute(filters=None):
    columns = [
        {"label": "Supplier Discount", 'width': 210, "fieldname": "supplier_discount"},
        {"label": "Item Code", 'width': 300, "fieldname": "item_code"},
        {"label": "Barcode", 'width': 150, "fieldname": "barcode", "fieldtype": "Data"},
        {"label": "Packing", 'width': 150, "fieldname": "packing", "fieldtype": "Int"},
        {"label": "Inventory", 'width': 150, "fieldname": "inventory", "fieldtype": "Data"},
        {"label": "Offtake", 'width': 150, "fieldname": "cisl", "fieldtype": "Float", "precision": 2},#Nabali ang offtake ug CISL hahaha
        {"label": "Cycle Time", 'width': 150, "fieldname": "cycletime", "fieldtype": "Int"},
        {"label": "CISL", 'width': 150, "fieldname": "offtake", "fieldtype": "Float", "precision":2},
        {"label": "Days INV", 'width': 150, "fieldname": "days_inv", "fieldtype": "Float", "precision": 2}
    ]
    data = []
    supplier_discount = filters.get("supplier_discount") if filters.get("supplier_discount")!="" else ""
    to_date = filters.get("to_date")
    warehouse = filters.get("warehouse")
    branch = filters.get("branch")

    now = datetime.datetime.strptime(to_date, '%Y-%m-%d').date()
    from_date = now - datetime.timedelta(days=90)

    print(supplier_discount,"##############")

    if supplier_discount != "" and supplier_discount is not None:
        itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom, item.barcode_pack,disc.parent, item.item_name_dummy
        from `tabItem` item left join `tabSupplier Discount Items` disc on disc.items = item.item_code
        where disc.parent = %s ORDER BY item.type DESC, item.name ASC""", (supplier_discount))
    else:
        itm_det = frappe.db.sql("""Select item.name, item.barcode_retial, item.packing, item.stock_uom, item.barcode_pack, disc.parent, item.item_name_dummy
                                  from `tabItem` item join
                                    `tabSupplier Discount Items` disc on disc.items = item.item_code order by disc.parent ASC, item.name ASC""")

    for item in itm_det:
        item_code, barcode, packing = item[0], item[1], item[2]
        supplier_discount = item[5]
        discount_doc = frappe.get_doc("Supplier Discounts", supplier_discount)
        discount = discount_doc.supplier_discount_name
        get_qty_sold_temp = get_qty_sold(branch, item_code, supplier_discount, from_date, to_date)
        print "===========get qty sold temp value=========="
        print get_qty_sold_temp
        offtake = get_qty_sold_temp[4]
        cisl = get_qty_sold_temp[5]
        cycle_days = get_qty_sold_temp[6]
        print "for dict"
        print item_code, barcode, offtake, cisl, cycle_days
        inventory = get_balance(warehouse, item_code, to_date)
        days_inv = float(inventory)/float(cisl) if float(cisl)>0 else None
        temp = {"item_code": item[6], "barcode":barcode, "supplier_discount":discount, "packing": packing, "cisl": cisl,
                "cycletime": cycle_days, "offtake": offtake, "inventory":inventory, "days_inv": days_inv}
        data.append(temp)

    return columns, data
def get_qty_sold(branch, item_code, supplier_disc, date, now):
    cisl, cisl2 = 0, 0
    con_f = 1
    s1_sum, s1_qty = 0, 0
    s2_sum, s2_qty = 0, 0
    s3_sum, s3_qty = 0, 0
    new_avg = 0
    uom_conv = 0
    sum = 0
    supplier = ""

    frequency = frappe.db.sql("""Select offtake_calculation_frequency from `tabBranch` where branch = %s""", (branch))
    freq = str(frequency[0][0])
    fac_calc = frappe.db.sql("""Select (lead_time + cycle_time + buffer) from `tabSupplier Discounts` where name = %s""",(supplier_disc))  # factor calculation
    for f in fac_calc:

        factor = str(f[0])
        print "FACTOR = "+ factor
        con_factor = frappe.db.sql(
            """Select packing from `tabItem` where item_code = %s""", (item_code))

        for f in con_factor:
            con_f = str(f[0])
        if (str(con_f) == 'None') or (str(con_f) == ''):
            uom_conv = 1
        else:
            uom_conv = float(con_f)

        # ave qty: per piece/smallest UOM
        # count : # of records/days

    print "========================compute======================"

    s1 = frappe.db.sql("""Select count(qty), sum(qty), uom from `tabUpload POS`
                                                              where trans_date >= %s and trans_date <= %s and qty !=0 and
                                                              barcode = (Select barcode_retial from `tabItem` where item_code = %s)
                                                             and branch = %s""",
                           (date, now, item_code, branch))
    s2 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                 ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                 and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                 and `tabStock Entry Detail`.item_code = %s
                                 AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                           (date, now, item_code, branch))
    s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                            ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                            and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                            and `tabStock Entry Detail`.item_code = %s
                            AND `tabStock Entry`.branch = %s AND  `tabStock Entry`.type ='Stock Transfer' AND `tabStock Entry Detail`.s_warehouse = 'CDO Warehouse - GG' AND
                            `tabStock Entry Detail`.t_warehouse != 'CDO Main Display Area - GG' AND `tabStock Entry`.docstatus = 1""",
                            (date, now, item_code, branch))

    if str(s1[0][1]) == 'None':
        s1_sum, s1_qty = 0, 0
    else:
        s1_sum = s1[0][1]
        s1_qty = s1[0][0]

    if str(s2[0][1]) == 'None':
        s2_sum, s2_qty = 0, 0
    else:
        s2_sum = (s2[0][1] * uom_conv)
        s2_qty = s2[0][0]

    if str(s3[0][1]) == 'None':
        s3_sum, s3_qty = 0, 0
    else:
        s3_sum = (s3[0][1] * uom_conv)
        s3_qty = s3[0][0]

    divisor = s2_qty + s1_qty
    if divisor == 0:
        divisor = 1
            # if (s1_qty+s2_qty+s3_qty)!=0:
            # new_avg = (s1_sum+s2_sum+s3_sum)/(s1_qty+s2_qty+s3_qty)
    sum = s1_sum + s2_sum  # + s3_sum
    new_avg = sum / 90  # 90
    #new_avg = sum / 90
        #new_avg = (s1_sum + s2_sum + s3_sum) / 90
        # else:
        #    new_avg = 0
    if (str(factor) == 'None') or (str(factor)==''):
        factor = 1
    cisl2 = round((float(new_avg) / (float(con_f)) * float(factor)), 0)
    cisl_temp = round(new_avg/float(con_f),2)

    return sum, s1_sum, s2_sum, s3_sum, cisl2, cisl_temp, factor


def get_balance(warehouse, item_code, trans_date):
    balance = 0
    recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
                where warehouse = %s and
                item_code = %s and
                posting_date <= %s
                ORDER BY posting_date DESC, posting_time DESC, name DESC limit 1""",(warehouse, item_code, trans_date))
    try:
        print "============RECON==========="
        print recon[0][1]
    except:
        balance = 0
    else:
        balance = recon[0][1]

    return balance

# def get_balance2(warehouse, item_code, trans_date):
#     to_date = str(trans_date)+" 23:59:59"
#     from_date = "2015-01-01 00:00:00"
#     recon = 0
#
#
#     sr = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty from `tabStock Reconciliation Item` itm
#                             join `tabStock Reconciliation` sr on sr.name = itm.parent where itm.warehouse = %s and itm.item_code = %s
#                             and sr.posting_date<= %s and sr.docstatus = 1 order by posting_date desc, posting_time desc, sr.name desc limit 1""",
#                        (warehouse, item_code, trans_date))
#
#     if len(sr)>0:
#         if sr[0][0]:
#             from_date = sr[0][0]
#             recon = sr[0][1]
#
#     print(str(from_date), str(to_date))
#
#     rrs = frappe.db.sql("""select sum(itm.stock_qty) from `tabPurchase Receipt Item` itm join `tabPurchase Receipt` rr on
#                         rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s and rr.docstatus = 1
#                         and TIMESTAMP(rr.posting_date, rr.posting_time) between %s and %s""",
#                         (warehouse, item_code, from_date, to_date))
#
#     ses_minus = frappe.db.sql("""select sum(-1*itm.transfer_qty) from `tabStock Entry Detail` itm
#                               join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
#                               and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
#                               and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))
#
#     ses_plus = frappe.db.sql("""select sum(itm.transfer_qty) from `tabStock Entry Detail` itm
#                               join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
#                               and TIMESTAMP(se.posting_date, se.posting_time) between %s and %s
#                               and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))
#
#     total_rr = rrs[0][0] if rrs[0][0] is not None else 0
#     total_se_minus = ses_minus[0][0] if ses_minus[0][0] is not None else 0
#     total_se_plus = ses_plus[0][0] if ses_plus[0][0] is not None else 0
#
#     inv = total_rr + total_se_minus + total_se_plus + recon
#
#     return inv

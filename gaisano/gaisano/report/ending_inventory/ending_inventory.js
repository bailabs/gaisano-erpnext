// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Ending Inventory"] = {
	"filters": [
		{
		 	"fieldname":"branch",
		 	"label":__("Branch"),
		 	"fieldtype": "Link",
		 	"options": "Branch",
		 	"reqd": 1
		},
		{
			"fieldname": "warehouse",
			"label":__("Warehouse"),
			"fieldtype": "Link",
			"options": "Warehouse",
			"reqd": 1
		},
		{
			"fieldname": "reference_date",
			"label":__("Reference Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname": "per_supplier",
			"label":__("By Supplier"),
			"fieldtype": "Check",
			"reqd": 0
		}
	]
};

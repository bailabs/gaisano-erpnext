# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from gaisano.popupevents import get_report_password



def execute(filters=None):
    data = []
    columns = []
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    category = filters.get("subclass")
    per_company = filters.get("per_company")
    branch = filters.get("branch")

    if branch == None:
        branch = ""

    columns = [
            {"label": "Sub Class", 'width': 200, "fieldname": "sub_class"},
            {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
            {"label": "Item Code", 'width': 300, "fieldname": "item_code"},
            {"label": "Volume Percentage", 'width': 150, "fieldname": "percentage", "fieldtype": "Percent"},
            {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_percentage", "fieldtype": "Percent"},
            {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
            {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"},
            {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"}

        ]
    password = filters.get("report_password")
    show_peso_value = filters.get("show_peso_value")

    v_perc = float(filters.get("volume")) / 100
    p_perc = float(filters.get("peso_value")) / 100
    pr_perc = float(filters.get("profit")) / 100


    if show_peso_value == 1:
        if not get_report_password(password):
            report_msg = "Please Enter the Report Password" if password is None else "WRONG REPORT PASSWORD ENTERED."
            frappe.throw(report_msg)
        else:
            columns.append({"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype": "Currency"})


    itm_cat = frappe.db.sql("""Select parent, sub_class from `tabItem Class Each` where sub_class = %s""", (category))

    total_sales = frappe.db.sql("""select sum(qty), sum(amount), ((sum(amount))-(sum(qty*cost)))
                                              from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
                                              AND barcode IN (Select barcode_retial from `tabItem` where item_code in
                                              (Select parent from `tabItem Class Each` where sub_class=%s))""",
                                ('%'+branch+'%', str(from_date), str(to_date), category))

    #whole_sale = str(total_sales[0][0])
    #whole_sale_peso = str(total_sales[0][1])
    whole_sale, whole_sale_peso, whole_profit = calc_discount(from_date, to_date, category, branch)
    if (whole_sale==None) or (whole_sale == 0):
        frappe.msgprint("No transactions/sales were made OR subclass is not used anymore.")
    else:
        for itm in itm_cat:
            itm_code = str(itm[0])
            item_doc = frappe.get_doc("Item", itm_code)
            sub_class = str(itm[1])
            #idv_sales = frappe.db.sql("""select sum(qty), sum(amount), ((sum(amount))-(sum((qty*cost) * (1-(Select item_discount from `tabItem` where item_code = %s and item_discount is not null))))),
            #                                      (sum((qty*cost) * (Select item_discount from `tabItem` where item_code = %s and item_discount is not null))) as total_discount
            #                                      from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
            #                                      AND barcode = (Select barcode_retial from `tabItem` where item_code = %s)""",
            #
            #                           (itm_code, itm_code, '%'+branch+'%', str(from_date), str(to_date), itm_code))
            idv_sales = [""]
            for i in idv_sales:

                #itm_sales = str(i[0])
                #itm_sales_peso = str(i[1])
                #itm_profit = str(i[2])
                itm_sales, itm_sales_peso,itm_profit = get_idv_sales(branch, from_date, to_date, itm_code)
                if (itm_sales != 'None'):
                    # itm_profit_f = abs(float(itm_profit))
                    itm_profit_f = (float(itm_profit))
                    vol_perc = (float(itm_sales) / float(whole_sale)) * 100
                    peso_perc = (float(itm_sales_peso) / float(whole_sale_peso)) * 100
                    prof_perc = (float(itm_profit_f) / float(whole_profit)) * 100
                    end_vol_percent = ((float(vol_perc)) * v_perc)
                    end_peso_percent = ((float(peso_perc)) * p_perc)
                    end_prof_percent = ((float(prof_perc)) * pr_perc)
                    overall = end_vol_percent + end_peso_percent + end_prof_percent

                    supplier = frappe.db.sql("""Select supplier from `tabItem Supplier` where parent = %s""", (itm_code))
                    for s in supplier:
                        sup = s[0]
                        data.append({"sub_class": sub_class, "supplier": sup, "total_sales": whole_sale,
                                     "item_sales": itm_sales,
                                     "item_code": item_doc.item_name_dummy, "total_peso": whole_sale_peso,
                                     "peso_value": itm_sales_peso, "percentage": vol_perc,
                                     "peso_percentage": peso_perc, "end_vol_perc": end_vol_percent,
                                     "end_peso_perc": end_peso_percent, "whole_profit": whole_profit,
                                     "item_profit": itm_profit_f, "profit_perc": prof_perc,
                                     "end_profit_perc": end_prof_percent, "overall": overall
                                     })

    if per_company == 1:
        data = show_per_company(data)
        columns = [
                {"label": "Supplier", 'width': 200, "fieldname": "supplier"},
                {"label": "Volume Percentage", 'width': 150, "fieldname": "percentage", "fieldtype": "Percent"},
                {"label": "Peso Percentage", 'width': 150, "fieldname": "peso_percentage", "fieldtype": "Percent"},
                {"label": "Profit Percentage", 'width': 150, "fieldname": "profit_perc", "fieldtype": "Percent"},
                {"label": "Total Perc", 'width': 100, "fieldname": "overall", "fieldtype": "Percent"}
        ]
    return columns, data


def calc_discount(from_date, to_date, category, branch):
    itm_cat = frappe.db.sql("""Select parent from `tabItem Class Each` where sub_class = %s""", (category))

    volume, peso, profit = 0, 0, 0
    for itm in itm_cat:
        itm_code = str(itm[0])
        #idv_sales = frappe.db.sql("""select ((sum(amount))-(sum((qty*cost) * (1-(Select item_discount from `tabItem` where item_code = %s and item_discount is not null)))))
        #                                          from `tabUpload POS` where branch LIKE %s and trans_date >= %s and trans_date <= %s
        #                                          AND barcode = (Select barcode_retial from `tabItem` where item_code = %s)""",
        #                          (itm_code, '%'+branch+'%', str(from_date), str(to_date), itm_code))
        idv_sales = get_idv_sales(branch, from_date, to_date, itm_code)
        #for i in idv_sales:
        #itm_profit = str(i[0])
        #    if itm_profit != 'None':
        #        profit += float(itm_profit)
        #    else:
        #        itm_profit = 0
        #        profit += float(itm_profit)
        volume += float(idv_sales[0])
        peso += float(idv_sales[1])
        profit += float(idv_sales[2])
    return volume, peso, profit


def show_per_company(data):
    new_data = []

    temp_data = {}

    current_supplier = ""
    percentage = 0
    peso_percentage = 0
    profit_perc = 0
    overall = 0
    peso_value = 0

    total_perc = 0
    total_peso = 0
    total_profit = 0
    total_overall = 0
    total_peso_value = 0

    print "=================DATA====================="
    newlist = sorted(data, key=lambda k: k['supplier'])

    for i, item in enumerate(newlist):
        total_perc += item['percentage']
        total_peso += item['peso_percentage']
        total_profit += item['profit_perc']
        total_overall += item['overall']
        total_peso_value += item["peso_value"]

        if i == 0:
            current_supplier = item['supplier']

        if current_supplier != item['supplier']:
            temp_data = {"supplier": current_supplier, "percentage": percentage,
                         "peso_percentage": peso_percentage, "profit_perc": profit_perc,
                         "overall": overall, "peso_value": peso_value}
            new_data.append(temp_data)

            current_supplier = item['supplier']
            percentage = item['percentage']
            peso_percentage = item['peso_percentage']
            profit_perc = item['profit_perc']
            overall = item['overall']
            peso_value = item["peso_value"]

        else:
            percentage += item['percentage']
            peso_percentage += item['peso_percentage']
            profit_perc += item['profit_perc']
            overall += item['overall']
            peso_value += item["peso_value"]

        if (i + 1) == len(newlist):
            temp_data = {"supplier": current_supplier, "percentage": percentage,
                         "peso_percentage": peso_percentage, "profit_perc": profit_perc,
                         "overall": overall, "peso_value": peso_value}
            new_data.append(temp_data)

    new_data = check_totals(new_data)

    return new_data


def check_totals(new_data):
    temp1, temp2, temp3, temp4, = 0, 0, 0, 0
    for i, item in enumerate(new_data):
        temp1 += item['percentage']
        temp2 += item['peso_percentage']
        temp3 += item['profit_perc']
        temp4 += item['overall']
    new_data.append({"supplier": "<B>TOTAL based on Totals</B>", "percentage": temp1,
                     "peso_percentage": temp2, "profit_perc": temp3,
                     "overall": temp4
                     })
    return new_data


def get_idv_sales(branch, from_date, to_date, item_code):
    print "GET IDV SALES FUNCTION"
    volume , peso_value, profit = 0, 0, 0
    d1, d2, d3, d4 = get_supplier_discounts(item_code, branch)
    try:
        item_doc = frappe.get_doc("Item", item_code)
    except:
        print "ERROR"
    else:
        idv_sales = frappe.db.sql("""select sum(qty), sum(amount), sum(qty*cost) from `tabUpload POS` where branch LIKE %s and trans_date >= %s and
                              trans_date <= %s AND barcode = %s""",
                                  ('%' + branch + '%', str(from_date), str(to_date), str(item_doc.barcode_retial)))
        if len(idv_sales) > 0:
            for sales in idv_sales:
                volume = (sales[0] if sales[0] != None else 0)
                peso_value = (sales[1] if sales[1] != None else 0)
                total_cost = (sales[2] if sales[2] != None else 0)
                net_cost = ((((float(total_cost) * (1 - d1)) * (1 - d2)) * (1 - d3)) * (1 - d4))
                profit = peso_value - (net_cost)
    print volume , peso_value, profit
    return volume, peso_value, profit


def get_supplier_discounts(item_code, branch):
    discounts = frappe.db.sql("""Select supplier_discount from `tabItem Branch Discount` where parent = %s and branch = %s""", (item_code, branch))
    disc1, disc2, disc3, disc4 = 0, 0, 0, 0
    if len(discounts) > 0:
        for discount in discounts:
            supplier_discount = frappe.get_doc("Supplier Discounts", discount[0])
            disc1 = (supplier_discount.disc_1 if supplier_discount.disc_1 != None else 0)
            disc2 = (supplier_discount.disc_2 if supplier_discount.disc_2 != None else 0)
            disc3 = (supplier_discount.disc_3 if supplier_discount.disc_3 != None else 0)
            disc4 = (supplier_discount.disc_4 if supplier_discount.disc_4 != None else 0)
    return float(disc1/100), float(disc2/100), float(disc3/100), float(disc4/100)

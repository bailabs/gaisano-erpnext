# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	branch = filters.get("branch")
	margin_rate = filters.get("margin_rate")
	supplier = filters.get("supplier")

	columns = get_columns()
	data = get_items_below_margin(supplier,branch,margin_rate)

	return columns, data

def get_columns():
	columns = [
		{"label": "Item Name", 'width':250, "fieldname":"item_code"},
		{"label": "Barcode" , 'width':100, "fieldname":"barcode"},
		{"label": "Actual Wholesale Rate", 'width': 100, "fieldname":"actual_wholesale_rate","fieldtype": "Percent"},
		{"label": "Actual Retail Rate", 'width': 100, "fieldname":"actual_retail_rate", "fieldtype": "Percent"},
		{"label": "Supplier Discount", 'width':250, "fieldname":"supplier_discount_name","fieldtype": "Data"},
		{"label": "Discounts", 'width':100, "fieldname":"discounts", "fieldtype":"Data"}

	]
	return columns

def get_supplier_discounts(item_code, branch):
    discounts = frappe.db.sql("""Select supplier_discount from `tabItem Branch Discount` where parent = %s and branch = %s""", (item_code, branch))
    disc1, disc2, disc3, disc4 = 0, 0, 0, 0
    if len(discounts) > 0:
        for discount in discounts:
            supplier_discount = frappe.get_doc("Supplier Discounts", discount[0])
            disc1 = (supplier_discount.disc_1 if supplier_discount.disc_1 != None else 0)
            disc2 = (supplier_discount.disc_2 if supplier_discount.disc_2 != None else 0)
            disc3 = (supplier_discount.disc_3 if supplier_discount.disc_3 != None else 0)
            disc4 = (supplier_discount.disc_4 if supplier_discount.disc_4 != None else 0)
    return float(disc1/100), float(disc2/100), float(disc3/100), float(disc4/100)

def get_branch_discount(item_code, branch):
	discount = frappe.db.sql("""SELECT supplier_discount, supplier_discount_name from `tabItem Branch Discount` where parent = %s and branch = %s""",(item_code, branch))
	if discount:
		return discount[0][0], discount[0][1]
	else:
		return "None", " "

def get_items_below_margin(supplier=None, branch=None, margin_rate=None ):
	data = []

	if supplier is None:
		supplier = ""
	barcode_filter = "601"
	items = frappe.db.sql(
		"""SELECT itm.name, itm.item_cost_with_freight, itm.barcode_retial, itm.packing, itm.item_name_dummy,
			itm.item_price, itm.item_price_retail_with_margin from `tabItem` itm inner join
			`tabItem Supplier` sup on itm.name = sup.parent
			where itm.type != 'Disabled'  and sup.supplier like %s and itm.barcode_retial not like %s""",('%'+supplier+'%', barcode_filter+'%'))
	for item in items:
		disc1,disc2,disc3,disc4 = get_supplier_discounts(item[0], branch)
		discounts_string = disc1*100,disc2*100,disc3*100,disc4*100
		supplier_discount = get_branch_discount(item[0],branch)
		print disc1, disc2, disc3, disc4, supplier_discount[0]

		packing = item[3]
		retail_price = item[6]
		wholesale_price = item[5]

		net_cost = ((((float(item[1]) * (1 - disc1)) * (1 - disc2)) * (1 - disc3)) * (1 - disc4))
		retail_net_cost = net_cost/packing

		if net_cost != 0:
			calculated_actual_retail_margin_rate = (float(retail_price/retail_net_cost)-1) * 100
			calculated_wholesale_rate = (float((wholesale_price/net_cost)-1)) * 100
			print retail_net_cost, "/", retail_price, calculated_actual_retail_margin_rate
			print net_cost, "/", wholesale_price, calculated_wholesale_rate
			if margin_rate >= calculated_actual_retail_margin_rate or margin_rate >= calculated_wholesale_rate:
				data.append({"item_code":item[4], "barcode":item[2], "actual_wholesale_rate":calculated_wholesale_rate, "actual_retail_rate":calculated_actual_retail_margin_rate, "supplier_discount_name":supplier_discount[1],"discounts":discounts_string})
		else:
			calculated_actual_retail_margin_rate = 0
			calculated_wholesale_rate = 0
			if margin_rate >= calculated_actual_retail_margin_rate and margin_rate >= calculated_wholesale_rate:
				data.append({"item_code":item[4], "barcode":item[2], "actual_wholesale_rate":calculated_wholesale_rate, "actual_retail_rate":calculated_actual_retail_margin_rate, "supplier_discount_name":supplier_discount[1],"discounts":discounts_string})
	return data





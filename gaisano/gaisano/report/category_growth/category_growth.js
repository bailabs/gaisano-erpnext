// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Category Growth"] = {
	"filters": [
		{
            "fieldname": "from_year",
            "label": __("From Year"),
            "fieldtype": "Link",
            "options": "Fiscal Year",
            "reqd": 1
        },
		{
            "fieldname": "to_year",
            "label": __("To Year"),
            "fieldtype": "Link",
            "options": "Fiscal Year",
            "reqd": 1
        },
		{
            "fieldname": "branch",
            "label": __("Branch"),
            "fieldtype": "Link",
			"options": "Branch",
            "reqd": 1
        },
		{
            "fieldname": "category",
            "label": __("Category"),
            "fieldtype": "Link",
			"options": "Item Category",
            "reqd": 0
        },
        {
        "fieldname" : "report_password",
        "fieldtype" : "Password",
        "label" : "Password for Show Peso Value",
        "reqd" : 1
        }
	]
};

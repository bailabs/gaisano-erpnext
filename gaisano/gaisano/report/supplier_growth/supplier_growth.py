# Copyright (c) 2013, Bai Mobile and Web Labs and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from dateutil.relativedelta import relativedelta
import frappe, datetime

def execute(filters=None):
	columns = [
        {"label": "Supplier Name", 'width': 300, "fieldname": "supplier"},
		{"label": "Period Covered", 'width': 300, "fieldname": "period_covered"},
        {"label": "Peso Value", 'width': 150, "fieldname": "peso_value", "fieldtype":"Currency"},
		{"label": "Volume (in pcs)", 'width': 150, "fieldname": "volume", "fieldtype": "Float", "precision":2},
		{"label": "Percent Growth (Peso Value)", 'width': 100, "fieldname": "growth_peso", "fieldtype": "Percent", "precision":2},
		{"label": "Percent Growth (Volume)", 'width': 100, "fieldname": "growth_volume", "fieldtype": "Percent","precision": 2},
		{"label": "Daily Sellout", 'width': 150, "fieldname": "sellout", "fieldtype": "Currency"}
    ]


	data = []
	supplier = filters.get("supplier")
	from_date = filters.get("from_date")
	to_date = filters.get("to_date")
	branch = filters.get("branch")
	supplier_discount = filters.get("supplier_discount")
	breakdown_by_discount = filters.get("breakdown_by_discount")

	if breakdown_by_discount != 1:
		if supplier_discount is not None:
			data = get_discount_data_only(from_date, to_date, supplier_discount,branch,data)
		else:
			data = get_supplier_data_only(from_date, to_date, supplier, branch)
	else:
		if supplier_discount is not None:
			data = get_discount_data_only(from_date, to_date, supplier_discount,branch,data)
		else:
			for discount in frappe.db.sql("""SELECT name from `tabSupplier Discounts` where supplier_name = %s""", supplier):
				data = get_discount_data_only(from_date, to_date, discount[0], branch, data)

	return columns, data


def get_supplier_data_only(from_date, to_date, supplier, branch):

	data = []

	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)

	no_days = (prev_to_date - prev_from_date).days

	prev_from_date = datetime.datetime.strftime(prev_from_date,"%Y-%m-%d")
	prev_to_date = datetime.datetime.strftime(prev_to_date,"%Y-%m-%d")

	# get previous POS sales
	prev_pos_qty, prev_pos_amount, prev_period = get_supplier_sales_data( prev_from_date,prev_to_date, supplier, branch)
	data.append({"supplier":supplier+" (POS SALES)","period_covered":prev_period,"peso_value":prev_pos_amount,
				 "volume":prev_pos_qty,"growth_peso":"","growth_volume":"","sellout":""})

	# get current POS sales
	current_pos_qty, current_pos_amount, current_period = get_supplier_sales_data(from_date, to_date, supplier, branch)
	current_sellout = current_pos_amount/no_days
	data.append({"supplier": supplier + " (POS SALES)", "period_covered": current_period, "peso_value": current_pos_amount,
				 "volume": current_pos_qty, "growth_peso": "", "growth_volume": "", "sellout": current_sellout})

	# get POS SALES growth
	growth_peso = 100*((current_pos_amount-prev_pos_amount)/prev_pos_amount) if prev_pos_amount>0 else 100
	growth_qty = 100*((current_pos_qty-prev_pos_qty)/prev_pos_qty) if prev_pos_qty>0 else 100
	data.append({"supplier": "GROWTH", "period_covered": "", "peso_value": "",
		 		"volume": "", "growth_peso": growth_peso, "growth_volume": growth_qty, "sellout": ""})


	# get previous WHOLESALE sales
	prev_ws_qty, prev_ws_amount, prev_period = get_supplier_wholesale_data( prev_from_date,prev_to_date, supplier, branch)
	data.append({"supplier":supplier+" (WHOLESALE)","period_covered":prev_period,"peso_value":prev_ws_amount,
				 "volume":prev_ws_qty,"growth_peso":"","growth_volume":"","sellout":""})

	# get current WHOLESALE sales
	current_ws_qty, current_ws_amount, current_period = get_supplier_wholesale_data(from_date, to_date, supplier, branch)
	current_sellout = current_ws_amount/no_days
	data.append({"supplier": supplier + " (WHOLESALE)", "period_covered": current_period, "peso_value": current_ws_amount,
				 "volume": current_ws_qty, "growth_peso": "", "growth_volume": "", "sellout": current_sellout})

	# get WHOLESALE SALES growth
	growth_peso = 100*((current_ws_amount-prev_ws_amount)/prev_ws_amount) if prev_ws_amount>0 else 100
	growth_qty = 100*((current_ws_qty-prev_ws_qty)/prev_ws_qty) if prev_ws_qty>0 else 100
	data.append({"supplier": "GROWTH", "period_covered": "", "peso_value": "",
		 		"volume": "", "growth_peso": growth_peso, "growth_volume": growth_qty, "sellout": ""})

	# get POS + Wholsale total (Previous Year)
	prev_total_amount = prev_pos_amount + prev_ws_amount
	prev_total_qty = prev_pos_qty + prev_ws_qty
	data.append({"supplier": supplier + " (POS + WHOLESALE)", "period_covered": prev_period, "peso_value": prev_total_amount,
				 "volume": prev_total_qty, "growth_peso": "", "growth_volume": "", "sellout": ""})

	# get POS + Wholsale total (Current Year)
	current_total_amount = current_pos_amount + current_ws_amount
	current_total_qty = current_pos_qty + current_ws_qty
	sellout = current_total_amount/no_days
	data.append({"supplier": supplier + " (POS + WHOLESALE)", "period_covered": current_period, "peso_value": current_total_amount,
		 "volume": current_total_qty, "growth_peso": "", "growth_volume": "", "sellout": sellout})

	# get Total growth
	growth_peso = 100 * ((current_total_amount - prev_total_amount) / prev_total_amount) if prev_total_amount > 0 else 100
	growth_qty = 100 * ((current_total_qty - prev_total_qty) / prev_total_qty) if prev_total_qty > 0 else 100
	data.append({"supplier": "GROWTH", "period_covered": "", "peso_value": "",
				 "volume": "", "growth_peso": growth_peso, "growth_volume": growth_qty, "sellout": ""})
	return data

def get_discount_data_only(from_date, to_date, discount, branch, data):

	supplier_discount_name = frappe.db.get_value("Supplier Discounts",discount,"supplier_discount_name")

	prev_from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d") - relativedelta(years=1)
	prev_to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d") - relativedelta(years=1)

	no_days = (prev_to_date - prev_from_date).days

	prev_from_date = datetime.datetime.strftime(prev_from_date,"%Y-%m-%d")
	prev_to_date = datetime.datetime.strftime(prev_to_date,"%Y-%m-%d")

	# get previous POS sales
	prev_pos_qty, prev_pos_amount, prev_period = get_discount_sales_data( prev_from_date,prev_to_date, discount, branch)
	data.append({"supplier":supplier_discount_name+" (POS SALES)","period_covered":prev_period,"peso_value":prev_pos_amount,
				 "volume":prev_pos_qty,"growth_peso":"","growth_volume":"","sellout":""})

	# get current POS sales
	current_pos_qty, current_pos_amount, current_period = get_discount_sales_data(from_date, to_date, discount, branch)
	current_sellout = current_pos_amount/no_days
	data.append({"supplier": supplier_discount_name + " (POS SALES)", "period_covered": current_period, "peso_value": current_pos_amount,
				 "volume": current_pos_qty, "growth_peso": "", "growth_volume": "", "sellout": current_sellout})

	# get POS SALES growth
	growth_peso = 100*((current_pos_amount-prev_pos_amount)/prev_pos_amount) if prev_pos_amount>0 else 100
	growth_qty = 100*((current_pos_qty-prev_pos_qty)/prev_pos_qty) if prev_pos_qty>0 else 100
	data.append({"supplier": "GROWTH", "period_covered": "", "peso_value": "",
		 		"volume": "", "growth_peso": growth_peso, "growth_volume": growth_qty, "sellout": ""})


	# get previous WHOLESALE sales
	prev_ws_qty, prev_ws_amount, prev_period = get_discount_wholesale_data( prev_from_date,prev_to_date, discount, branch)
	data.append({"supplier":supplier_discount_name+" (WHOLESALE)","period_covered":prev_period,"peso_value":prev_ws_amount,
				 "volume":prev_ws_qty,"growth_peso":"","growth_volume":"","sellout":""})

	# get current WHOLESALE sales
	current_ws_qty, current_ws_amount, current_period = get_discount_wholesale_data(from_date, to_date, discount, branch)
	current_sellout = current_ws_amount/no_days
	data.append({"supplier": supplier_discount_name + " (WHOLESALE)", "period_covered": current_period, "peso_value": current_ws_amount,
				 "volume": current_ws_qty, "growth_peso": "", "growth_volume": "", "sellout": current_sellout})

	# get WHOLESALE SALES growth
	growth_peso = 100*((current_ws_amount-prev_ws_amount)/prev_ws_amount) if prev_ws_amount>0 else 100
	growth_qty = 100*((current_ws_qty-prev_ws_qty)/prev_ws_qty) if prev_ws_qty>0 else 100
	data.append({"supplier": "GROWTH", "period_covered": "", "peso_value": "",
		 		"volume": "", "growth_peso": growth_peso, "growth_volume": growth_qty, "sellout": ""})

	# get POS + Wholsale total (Previous Year)
	prev_total_amount = prev_pos_amount + prev_ws_amount
	prev_total_qty = prev_pos_qty + prev_ws_qty
	data.append({"supplier": discount + " (POS + WHOLESALE)", "period_covered": prev_period, "peso_value": prev_total_amount,
				 "volume": prev_total_qty, "growth_peso": "", "growth_volume": "", "sellout": ""})

	# get POS + Wholsale total (Current Year)
	current_total_amount = current_pos_amount + current_ws_amount
	current_total_qty = current_pos_qty + current_ws_qty
	sellout = current_total_amount/no_days
	data.append({"supplier": supplier_discount_name + " (POS + WHOLESALE)", "period_covered": current_period, "peso_value": current_total_amount,
		 "volume": current_total_qty, "growth_peso": "", "growth_volume": "", "sellout": sellout})

	# get Total growth
	growth_peso = 100 * ((current_total_amount - prev_total_amount) / prev_total_amount) if prev_total_amount > 0 else 100
	growth_qty = 100 * ((current_total_qty - prev_total_qty) / prev_total_qty) if prev_total_qty > 0 else 100
	data.append({"supplier": "GROWTH", "period_covered": "", "peso_value": "",
				 "volume": "", "growth_peso": growth_peso, "growth_volume": growth_qty, "sellout": ""})
	return data



############################# GET Sales data functions ###################################

def get_supplier_sales_data(from_date, to_date, supplier, branch):

	sales_data = frappe.db.sql("""SELECT sum(pos.qty), sum(pos.amount) from `tabUpload POS` pos 
									join `tabItem` itm on itm.barcode_retial = pos.barcode join `tabItem Supplier` sup
									on sup.parent = itm.name where sup.supplier = %s and pos.trans_date >=%s AND 
									pos.trans_date <=%s and pos.branch =%s""",
							   (supplier, from_date, to_date, branch))

	period = str(from_date)+ " TO " + str(to_date)
	print period

	if len(sales_data)>0:
		return sales_data[0][0] if sales_data[0][0] is not None else 0, sales_data[0][1] if sales_data[0][1] is not None else 0, period
	else:
		return 0, 0, period


def get_supplier_wholesale_data(from_date, to_date, supplier, branch):

	sales_data = frappe.db.sql("""select sum(sed.qty*sed.packing), sum(sed.amount) from `tabStock Entry` se inner join 
								  `tabStock Entry Detail` sed on se.name = sed.parent join `tabItem Supplier` sup
								  on sup.parent = sed.item_code where se.type = 'Wholesale' and 
								 se.posting_date >= %s and se.posting_date <= %s and se.branch = %s and 
								 sup.supplier = %s""",(from_date, to_date, branch, supplier))

	period = str(from_date)+ " TO " + str(to_date)
	print period

	if len(sales_data)>0:
		return sales_data[0][0] if sales_data[0][0] is not None else 0, sales_data[0][1] if sales_data[0][1] is not None else 0, period
	else:
		return 0, 0, period


def get_discount_sales_data(from_date, to_date, discount, branch):

	sales_data = frappe.db.sql("""SELECT sum(pos.qty) as volume, sum(pos.amount) as peso_value from `tabUpload POS` pos 
									join `tabItem` itm on itm.barcode_retial = pos.barcode join `tabItem Branch Discount` disc
									on disc.parent = itm.name where disc.supplier_discount = %s and pos.trans_date >=%s AND 
									pos.trans_date <=%s and pos.branch =%s and disc.branch = %s""",
							   (discount, from_date, to_date, branch, branch))

	period = str(from_date)+ " TO " + str(to_date)
	print period

	if len(sales_data)>0:
		return sales_data[0][0] if sales_data[0][0] is not None else 0, sales_data[0][1] if sales_data[0][1] is not None else 0, period
	else:
		return 0, 0, period


def get_discount_wholesale_data(from_date, to_date, discount, branch):

	sales_data = frappe.db.sql("""select sum(sed.qty*sed.packing), sum(sed.amount) from `tabStock Entry` se inner join 
								  `tabStock Entry Detail` sed on se.name = sed.parent join `tabItem Branch Discount` disc
								  on disc.parent = sed.item_code where se.type = 'Wholesale' and 
								 se.posting_date >= %s and se.posting_date <= %s and se.branch = %s and 
								 disc.supplier_discount = %s and disc.branch = %s""",
							   (from_date, to_date, branch, discount, branch))

	period = str(from_date)+ " TO " + str(to_date)
	print period

	if len(sales_data)>0:
		return sales_data[0][0] if sales_data[0][0] is not None else 0, sales_data[0][1] if sales_data[0][1] is not None else 0, period
	else:
		return 0, 0, period




// Copyright (c) 2016, Bai Mobile and Web Labs and contributors
// For license information, please see license.txt

frappe.query_reports["Supplier Growth"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
		{
			"fieldname":"branch",
			"label": __("Branch"),
			"fieldtype": "Link",
			"options":"Branch",
			"reqd": 1
		},
		{
			"fieldname":"supplier",
			"label": __("Supplier"),
			"fieldtype": "Link",
			"options": "Supplier",
			"reqd": 1
		},
		{
			"fieldname":"supplier_discount",
			"label": __("Supplier Discount"),
			"fieldtype": "Link",
			"options": "Supplier Discounts",
			"reqd": 0,
			"get_query": function(){
				var branch = frappe.query_report_filters_by_name.branch.get_value();
				var supplier = frappe.query_report_filters_by_name.supplier.get_value();
				console.log(branch,supplier);
				var filters = get_discount_filters(branch,supplier);
				return{
					"filters":filters
        		}
    		},
			"on_change": function(query_report){
                var discount = frappe.query_report_filters_by_name.supplier_discount.get_value();
                if (discount){
                    return frappe.call({
                        method : "frappe.client.get",
                        args :{
                            "doctype" : "Supplier Discounts",
                            "name" : discount
                        },
                        callback: function(r){
                            frappe.query_report_filters_by_name.supplier_discount_name.set_value(r.message['supplier_discount_name']);
                            frappe.query_report.refresh();
                        }
                    })
                }
                else{
                    frappe.query_report_filters_by_name.supplier_discount_name.set_value("");
                    frappe.query_report.refresh();
                }
            }
		},
		{
			"fieldname": "supplier_discount_name",
			"fieldtype": "Data",
			"reqd":0,
            "read_only":1
		},
		{
			"fieldname": "breakdown_by_discount",
			"label":"Breakdown by discount?",
			"fieldtype": "Check",
			"reqd":0
		}
	]
};

function get_discount_filters(branch, supplier){
    if(branch == "CDO Main")
        var filters = {"disable":0,"disabled_in_main":0,"supplier_name":supplier};
    else
        var filters = {"disable": 0, "supplier_name":supplier};
    return filters
}



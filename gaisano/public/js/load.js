$(document).on('app_ready', function(){
    if(frappe.user.has_role('Item Manager') || frappe.user.has_role('Gaisano PO Branch Head') || frappe.user.has_role('Support Team')) {
        frappe.call({
            method: "gaisano.popupevents.check_latest_pi_upload",
            callback: function (r, rt) {
                console.log(r.message);
                if (r.message) {
                    frappe.msgprint(r.message);
                }
            }
        })
    }
})

$(document).on('app_ready', function(){
    if(frappe.user.has_role('Item Manager') || frappe.user.has_role('Gaisano PO Branch Head') || frappe.user.has_role('Support Team')) {
        frappe.call({
            method: "gaisano.tasks.check_last_erp",
            callback: function (r, rt) {
                console.log(r.message);
                if (r.message) {
                    frappe.msgprint(r.message);
                }
            }
        })
    }
})

$(document).on('app_ready', function() {
    if(frappe.user.has_role('Gaisano PO Manager') || frappe.user.has_role('Gaisano PO Branch Head') || frappe.user.has_role('Support Team')) {
        frappe.call({
            method: "gaisano.events.get_email_failed",
            callback: function (r, rt) {
                if (r.message) {
                    frappe.msgprint(frappe.render_template("load", {message: r.message}));
                }
            }
        })
    }
});


$(document).on('app_ready',function(){
    if(frappe.user.has_role('Gaisano PO Manager') || frappe.user.has_role('Gaisano PO Branch Head') || frappe.user.has_role('Support Team')) {
        frappe.call({
            method: "gaisano.popupevents.po_schedule_pop_up",
            callback: function (r, rt) {
                console.log(r);
                if (r.message) {
                    // if ((frappe.datetime.now_time() > '13:00:00') && (frappe.datetime.now_time() < '18:00:00')){
                    frappe.msgprint(r.message, "Scheduled Suppliers with no PO");
                    //}

                }
            }
        })
    }

    });


$(document).on('app_ready',function(){
    if(frappe.user.has_role('Gaisano PO Manager') || frappe.user.has_role('Gaisano PO Branch Head') || frappe.user.has_role('Support Team')) {
        frappe.call({
            method: "gaisano.popupevents.auto_po",
            callback: function (r, rt) {
                console.log(r);
                if (r.message) {
                    frappe.msgprint(r.message, "Critical Stock Level");
                }
            }
        })
    }
});


$(document).on('app_ready',function(){
    if(frappe.user.has_role('Item Manager') || frappe.user.has_role('Gaisano PO Manager') || frappe.user.has_role('Gaisano PO Branch Head') || frappe.user.has_role('Support Team')) {
        frappe.call({
            method: "gaisano.popupevents.last_sync",
            callback: function (r, rt) {
                console.log(r);
                if (r.message) {
                    frappe.msgprint(r.message, "SERVER STATUS");
                }
            }
        })
    }

    });

/*
window.onhashchange = function() {
     $(document).on('page-change', function(){
    //console.log("ADD SIDE NAV BISH.");
    set_side_nav();
});
};*/

$(document).on('page-change', function(){
    //console.log("ADD SIDE NAV BISH.");
    set_side_nav();
});

function set_side_nav(){
    //console.log(((window.location.href.includes("/desk#List"))||(window.location.href.includes("/desk#Form"))));
    if ((window.location.href.includes("/desk#List")&&(document.querySelector(".layout-side-section")))) {
        //console.log("I'm inside the creation part");
        var newEl = document.createElement('div');
        newEl.className = "side-nav-drawer";
        newEl.innerHTML = '<button class="side-nav-drawer" id="nav_button" onclick="side_nav_control()">>></button>';

        var ref = document.querySelector('.layout-main-section-wrapper');

        ref.parentNode.insertBefore(newEl, ref);
    }
}


function side_nav_control() {
    var side_pane = document.querySelector(".layout-side-section");
    if (side_pane.style.width == "150px") {
        closeNav();
    }
    else {
        openNav();
    }
}


function openNav() {
    var side_pane = document.querySelector(".layout-side-section");
    var main = document.querySelector('.layout-main-section-wrapper');
    var button = document.getElementById('nav_button');
    side_pane.style.width = "150px";
    side_pane.style.display="block";
    button.innerHTML="<<";
    main.style.marginLeft = "150px";
}

function closeNav() {
    var side_pane = document.querySelector(".layout-side-section");
    var main = document.querySelector('.layout-main-section-wrapper');
    var button = document.getElementById('nav_button');
    side_pane.style.width = "0";
    side_pane.style.display="none";
    button.innerHTML=">>";
    main.style.marginLeft = "0";
}
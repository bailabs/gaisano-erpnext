# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "gaisano"
app_title = "Gaisano"
app_publisher = "Bai Mobile and Web Labs"
app_description = "Gaisano Mall"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "ccfiel@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "/assets/gaisano/css/gaisano.css"
app_include_js = "/assets/js/gaisano.js"

# include js, css files in header of web template
web_include_css = "/assets/gaisano/css/gaisano.css"
#web_include_js = "/assets/gaisano/js/gaisano.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "gaisano.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "gaisano.install.before_install"
# after_install = "gaisano.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "gaisano.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# on_session_creation = "gaisano.events.set_cart_count"

doc_events = {
    "Item": {
        "after_insert": "gaisano.events.create_price",
        "on_update": "gaisano.events.update_price",
        "validate": "gaisano.events.create_name"
        #"on_insert": "gaisano.itemevents.on_insert"
    },
    "Purchase Order": {
        "validate": "gaisano.poevents.po_validate",
        "on_submit": "gaisano.poevents.check_supplier_discount_for_auto_po"
    },
    "Purchase Receipt": {
        "validate": "gaisano.rrevents.calc_discounts",
        "on_submit": "gaisano.rrevents.update_po_status",
        "on_cancel": "gaisano.rrevents.delete_sle_log"
        #"on_cancel": "gaisano.rrevents.reverse_qty"
    },
    "Stock Entry": {
        "validate": "gaisano.seevents.set_transfer_qty",
        "on_submit": "gaisano.seevents.check_se_sle",
        "on_cancel": "gaisano.rrevents.delete_sle_log",
        "after_insert": "gaisano.seevents.set_bo_number"
        #"on_cancel": "gaisano.seevents.reverse_qty"
    },
    "Supplier Discounts": {
        #"validate": "gaisano.events.create_disc_name",
        #"on_insert": "gaisano.gaisano.supplier_discounts.supplier_discounts.update_discount_id"
    },
    "Stock Reconciliation":{
        "validate": "gaisano.srevents.check_repeat_item",
        "on_submit": "gaisano.srevents.create_sle_log",
        "on_cancel": "gaisano.rrevents.delete_sle_log"
    }
}

# Scheduled Tasks
# ---------------

scheduler_events = {
    "all": [
        "gaisano.tasks.process_task_run",
        "gaisano.apisync.minute_update_task",
        "gaisano.supplier_sync.minute_update_task"
    ],
    "daily": [
        "gaisano.autopoevents.get_discounts_for_po",
        "gaisano.apisync.check_items_not_syncing",
        "gaisano.apisync.first_update_task"
        #"gaisano.usevents.create_current_inv",
        #"gaisano.popupevents.auto_po",
        # "gaisano.popupevents.change_po_schedule",
        "gaisano.reorder_item",
        "gaisano.tasks.disable_discounts",
        #"gaisano.popupevents.send_sync_emails",
        "gaisano.gaisano.doctype.server_information.server_information.check_item_cost_sync",
        #"gaisano.popupevents.check_sles",
        #"gaisano.popupevents.check_sles_se",
        "gaisano.popupevents.check_and_delete_daily_rr",
        "gaisano.popupevents.check_and_delete_daily_se"
    ],
    #
     "hourly": [
        #
        "gaisano.apisync.merge_rename_hourly_task"#,
        #"gaisano.supplier_sync.daily_update_task",
        #"gaisano.popupevents.check_and_delete_hourly" #,
        #"gaisano.sletasks.delete_sle_hourly"

     ],
    "weekly": [
        "gaisano.apisync.first_update_task"
    ]#,
    # "monthly": [
    # 	"gaisano.tasks.monthly"
    # ]
}

# Testing
# -------

# before_tests = "gaisano.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "gaisano.event.get_events"
# }

website_context = {
    "favicon": "/assets/gaisano/images/favicon.png",
    "splash_image": "/assets/gaisano/images/splash.png"
}

fixtures = ["Custom Field", "Custom Script", "Property Setter", "Print Format", "Report"]

import frappe, decimal
from tasks import get_list
from events import get_rounded_price
from apisync import update_netcost_and_margins
import os, dbf, csv, datetime

@frappe.whitelist()
def get_branch():
    branch = frappe.db.get_value("Server Information", None, "Branch")
    return branch

def on_insert(doc):
    print "ON INSERT-------------->>>>>>>"
    from events import get_item_id
    branch = frappe.db.get_value("Server Information", None, "Branch")
    if doc.is_new():
        if branch == "CDO Main":
            new_id = get_item_id()
            print new_id, "NEW_ID"
            if doc.item_id != new_id:
                doc.item_id = get_item_id()
                doc.item_code = "ITEM-" + str(doc.item_id).zfill(6)
                doc.name = doc.item_code
        else:
            doc.item_code = "ITEM-" + str(doc.item_id).zfill(6)
            doc.name = doc.item_code

        for i, attribute in enumerate(doc.attributes):
            doc.attributes[i].parent = doc.item_code
        for i, uom in enumerate(doc.uoms):
            doc.uoms[i].parent = doc.item_code
        for i, item_branch_discount in enumerate(doc.item_branch_discounts):
            doc.item_branch_discounts[i].parent = doc.item_code
        for i, supplier_item in enumerate(doc.supplier_items):
            doc.supplier_items[i].parent = doc.item_code
        for i, display_palcement in enumerate(doc.display_palcement):
            doc.display_palcement[i].parent = doc.item_code
        for i, item_attribute in enumerate(doc.item_attributes):
            doc.item_attributes[i].parent = doc.item_code
        for i, item_class in enumerate(doc.item_classification_1):
            doc.item_classification_1[i].parent = doc.item_code
        for i, branch in enumerate(doc.branches):
            doc.branches[i].parent = doc.item_code
        for i, branch in enumerate(doc.branch_price_table):
            doc.branch_price_table[i].parent = doc.item_code
            doc.branch_price_table[i].price_id = doc.branch_price_table[i].branch_code + "-" +doc.item_code
            doc.branch_price_table[i].name = doc.branch_price_table[i].branch_code + "-" + doc.item_code
    return doc

#branch_price_table

def check_item_branches():
    for item in frappe.db.sql("""SELECT name, disabled from `tabItem`"""):
        if item[1]!=1:
           item_doc = frappe.get_doc("Item", item[0])
           if item_doc.all_branches ==1:
               continue
           else:
               print item[0], item_doc.item_name_dummy, item_doc.all_branches
               enable = in_branches(item_doc.branches)
               print "ENABLE?", enable
               if not enable:
                   frappe.db.sql("""UPDATE `tabItem` set disabled = 1, type = 'Disabled' where name = %s""", item[0])
               else:
                   frappe.db.sql("""UPDATE `tabItem` set disabled = 0 where name = %s""", item[0])
    frappe.db.commit()


def check_discount_branches():
    for discount in frappe.db.sql("""SELECT name, disable from `tabSupplier Discounts`"""):
        discount_name, disable = discount[0], discount[1]
        discount_doc = frappe.get_doc("Supplier Discounts", discount_name)
        print discount_name, discount_doc.supplier_discount_name
        disable_discount(discount_doc.all_branches, discount_doc.branch_table, discount_doc.name, disable)


def disable_discount(all_branches, branch_table, discount, disable):
    if disable != 1:
        if all_branches == 1:
            frappe.db.sql("""UPDATE `tabSupplier Discounts` set disable = 0 where name = %s""", discount)
        else:
            enable_discount = in_branches(branch_table)
            print "enable_discount?", enable_discount
            if enable_discount:
                frappe.db.sql("""UPDATE `tabSupplier Discounts` set disable = 0 where name = %s""", discount)
            else:
                frappe.db.sql("""UPDATE `tabSupplier Discounts` set disable = 1 where name = %s""", discount)
        frappe.db.commit()


def in_branches(branch_table):
    enable_discount = False
    server_branch = frappe.db.get_value("Server Information", None, "branch")
    print "This server is:", server_branch
    for branch in branch_table:
        print "Compare with ", branch.branch
        if branch.branch == server_branch:
            print "Branch in table. Return True."
            enable_discount = True
            break
    return enable_discount

######FOR ITEM BRANCH PRICES ########

# TODO: add scheduled tasks for sync.
def update_all_item_branch_prices():
	this_branch = frappe.db.get_value("Server Information", None, "branch")
	print this_branch
	items = frappe.db.sql("""SELECT name from `tabItem`""")
	for item in items:
		branch_price_dict = get_branch_price_dict(this_branch, item[0])
		update_item_branch_price(this_branch, item[0], branch_price_dict)

def get_branch_price_dict(branch, item_code):
	print branch, item_code
	branch_doc = frappe.get_doc("Branch",branch)
	item_doc = frappe.get_doc("Item", item_code)
	price_id = branch_doc.branch_code+"-"+item_code
	branch_price_dict = {
		"doctype": "Item Prices Per Branch",
		"parent":item_code,
		"parentfield": "branch_price_table",
		"parenttype": "Item",
		"branch": branch,
		"branch_code": branch_doc.branch_code,
		"freight_peso": item_doc.item_freight_peso,
		"freight_perc":item_doc.item_freight,
		"item_cost_with_freight":item_doc.item_cost_with_freight,
		"wholesale_margin_rate": item_doc.margin_rate,
		"wholesale_price": item_doc.item_price,
		"retail_cost": item_doc.item_price_retail_1,
		"retail_margin_rate": item_doc.retail_margin_rate,
		"retail_price":item_doc.item_price_retail_with_margin,
		"price_id":price_id
	}
	return branch_price_dict

def update_item_branch_price(branch, item_code, item_dict):
	print branch, item_code
	count_rows = frappe.db.sql("""SELECT count(*) from `tabItem Prices Per Branch` where branch = %s and parent = %s""",(branch, item_code))
	if count_rows[0][0] == 0:
		print "NEW"
		row_doc = frappe.get_doc(item_dict)
		row_doc.insert()
		frappe.db.commit()
	else:
		print "UPDATE"
		frappe.db.sql("""UPDATE `tabItem Prices Per Branch` set freight_peso=%s, freight_perc=%s,
					  item_cost_with_freight=%s, wholesale_margin_rate=%s, wholesale_price=%s,
					  retail_cost=%s, retail_margin_rate=%s, retail_price=%s
					  where name=%s""",
					  (item_dict['freight_peso'], item_dict['freight_perc'], item_dict['item_cost_with_freight'],
					   item_dict['wholesale_margin_rate'], item_dict['wholesale_price'], item_dict['retail_cost'],
					   item_dict['retail_margin_rate'], item_dict['retail_price'],
					   item_dict['price_id']))
		frappe.db.commit()

##ITEM DBF AND MYSQL METHODS:
def update_items_in_new():
    items_updated = []
    item_list_old = get_list()
    count_matches = 0
    for item in item_list_old:
        changes = 0
        item_new = get_item(item['barcode'])
        if item_new != None:
            item_doc = frappe.get_doc("Item", item_new)
            vat = 1 if item['vat'] == True else 0
            print item['barcode'], item_new, vat, item['vat']
            if vat == 0:
                print "ZERO----------------------------###"
            update_vat(item_new, vat)
            #if item_doc.item_freight_peso != item['freight']:
            #    changes+=1
            #    print "update item freight peso"
            #   frappe.db.sql("""UPDATE `tabItem` set item_freight_peso = %s where name = %s""",
            #                  (item['freight'],item_new))
            #if (item['cost_w']>0) and (item_doc.item_cost_with_freight != item['cost_w']):
            #    changes += 1
            #    print "update item_cost_with_freight"
            #    frappe.db.sql("""UPDATE `tabItem` set item_cost_with_freight = %s where name = %s""",
            #                  (item['cost_w'],item_new))
            #if item_doc.item_price_retail_1 != item['cost_r']:
            #    changes += 1
            #    print "update item_price_retail_1"
            #    if (item_doc.packing == 1) and (item['cost_r']==0):
            #        frappe.db.sql("""UPDATE `tabItem` set item_price_retail_1 = item_cost_with_freight where name = %s""",
            #                      (item_new))
            #    else:
            #        frappe.db.sql("""UPDATE `tabItem` set item_price_retail_1 = %s where name = %s""",
            #                      (item['cost_r'], item_new))
            if item_doc.item_price != item['price_w']:
                changes += 1
                print "update item_price"
                frappe.db.sql("""UPDATE `tabItem` set item_price = %s where name = %s""",
                              (item['price_w'], item_new))
                if (item['packing'] == 1) and (item['price_r']==0):
                    frappe.db.sql("""UPDATE `tabItem` set item_price_retail_with_margin = %s where name = %s""",
                                  (item['price_w'], item_new))
            if item_doc.item_price_retail_with_margin != item['price_r']:
                changes += 1
                print "update item_price_retail_with_margin"
                if (item['packing'] == 1) and (item['price_r']==0):
                    frappe.db.sql("""UPDATE `tabItem` set item_price_retail_with_margin = %s where name = %s""",
                                  (item['price_w'], item_new))
                else:
                    if item['price_r']>0:
                        frappe.db.sql("""UPDATE `tabItem` set item_price_retail_with_margin = %s where name = %s""",
                                      (item['price_r'], item_new))
                    else:
                        per_piece = (item_doc.item_cost_with_freight/item_doc.packing)*(1+(item_doc.retail_margin_rate/100))
                        per_piece_rounded = get_rounded_price(per_piece)
                        frappe.db.sql("""UPDATE `tabItem` set item_price_retail_with_margin = %s where name = %s""",
                                  (per_piece_rounded, item_new))
            #if (item_doc.item_cost != (item['cost_w']-item['freight'])) and ((item['cost_w']-item['freight'])>0):
            #    changes += 1
            #    print "update item_cost and cost without vat"
            #    cost_without_vat = (item['cost_w']-item['freight'])/1.12
            #    frappe.db.sql("""UPDATE `tabItem` set item_cost = %s, item_cost_without_vat = %s where name = %s""",
            #                  ((item['cost_w']-item['freight']), cost_without_vat, item_new))
            if changes > 0:
                item_doc_updated = frappe.get_doc("Item", item_new)
                items_updated.append(item_doc_updated.barcode_retial)
                update_margin_rates(item_new, item_doc_updated.item_price_retail_1, item_doc_updated.item_cost_with_freight,
                                    item_doc_updated.item_price_retail_with_margin, item_doc_updated.item_price)
                update_netcost_and_margins(item_doc_updated)
    frappe.db.commit()
    print len(item_list_old), "MATCHES: ", count_matches
    write_to_csv_updated_in_new(items_updated)

def get_item(barcode):
    items = frappe.db.sql("""SELECT name from `tabItem` where barcode_retial = %s""", barcode)
    if len(items) > 0:
        return items[0][0]
    else:
        return None

def get_item_barcode_list():
    item_list = []
    for item in frappe.db.sql("""SELECT barcode_retial from `tabItem`"""):
        item_list.append(item[0])
    return item_list


def update_margin_rates(item_name, cost_r, cost_w, price_r, price_w):
    margin_retail = ((price_r - cost_r)/cost_r)*100 if cost_r>0 else 0
    margin_wholesale = ((price_w - cost_w) / cost_w) * 100 if cost_w > 0 else 0
    print margin_retail, margin_wholesale
    frappe.db.sql("""UPDATE `tabItem` set margin_rate = %s, retail_margin_rate =%s where name = %s """,
                  (margin_wholesale, margin_retail, item_name))

def update_vat(item_name, vat):
    frappe.db.sql("""UPDATE `tabItem` set vat = %s where name = %s""",(vat, item_name))

##TODO: Try except filewriter lines
def write_to_csv_updated_in_new(items):
    all_items = get_item_barcode_list()
    oldsystem_items = [d['barcode'] for d in get_list()]
    branch = get_branch()
    with open('/home/frappe/frappe-v7/sites/site1.local/public/files/items_updated_in_new.csv', 'wb') as csvfile:
        #with open('/home/gaisano1/frappe-v7/sites/gaisano/public/files/items_updated_in_new.csv', 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for item in oldsystem_items:
            if item not in all_items:
                filewriter.writerow([item, "ITEM NOT IN NEW"])
        for item in items:
            filewriter.writerow([item, "UPDATED"])
            if item in all_items:
                all_items.remove(item)
        for item in all_items:
            filewriter.writerow([item, "NOT UPDATED"])

def write_to_csv_updated_in_old(items):
    #items_in_new = get_item_barcode_list()
    #oldsystem_items = [d['barcode'] for d in get_list()]
    branch = get_branch()
    with open('/home/frappe/frappe-v7/sites/site1.local/public/files/items_updated_in_old.csv', 'wb') as csvfile:
        # with open('/home/gaisano1/frappe-v7/sites/gaisano/public/files/items_updated_in_old.csv', 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # for item in items_in_new:
        #     try:
        #         if item not in oldsystem_items:
        #             # print item, "NOT IN OLD"
        #             filewriter.writerow([item, "NOT IN OLD"])
        #             # else:
        #             # print item, "IN NEW"
        #     except:
        #         continue
        #         # if item not in oldsystem_items:
        #         #    filewriter.writerow([item, "NOT IN OLD"])
        filewriter.writerow(["barcode", "item name", "old cost", "new cost", "old packing", "new packing",
                             "old wholesale", "new wholesale", "old retail", "new retail", "ws margin",
                             "ret margin"])

        for item in items:
            item_name = item["item_name"].replace(',','')
            filewriter.writerow([item["barcode"], item_name, item["old_cost"], item['new_cost'],
                                 item['old_packing'], item['new_packing'], item['old_ws'], item['new_ws'],
                                 item["old_ret"], item["new_ret"], item['ws_margin'], item['ret_margin']])


def copy_prices_to_dbf():
    item_list = []
    branch = get_branch()

    table = dbf.Table('/media/backup/pos_data/grocery_dbf/ITEM') #, codepage='cp437')
    #table = dbf.Table('/home/hera/dbf/ITEM')
    #dept_coding = get_department_coding()
    with table:
        i = 0
        print "Table exists."
        table_len = len(table)
        while i < table_len:
            try:
                print i, table[i].bar_code
            except:
                print "ERROR ON ROW,",i
            else:
                record = table[i]
                item = get_item(table[i].bar_code)
                old_cost, old_ws, old_ret, old_packing = "","","",""
                if item!=None:
                    record = table[i]
                    #dep_short = get_dep_short(table[i].dep_code)
                    with record:
                        changes = 0
                        item_doc = frappe.get_doc("Item", item)
                        vat = True if item_doc.vat == 1 else False
                        if item_doc.modified.date()!= record.updated:
                            changes += 1
                            print "Update 'UPDATED' field", record.updated, " | ", item_doc.modified.date()
                            record.updated = item_doc.modified.date()
                        if vat != table[i].vat:
                            changes +=1
                            record.vat=vat
                            #record.write_record(vat=vat)
                            print "Update VAT"
                        # if item_doc.item_name_dummy != table[i].item_name:
                        #     changes +=1
                        #     if len(item_doc.item_name_dummy)>36:
                        #         record.item_name=item_doc.item_name_dummy[:36]
                        #     else:
                        #         record.item_name = item_doc.item_name_dummy
                        #     #record.write_record(vat=vat)
                        #     print "Update item_name"
                        # if item_doc.item_short_name != table[i].item_short:
                        #     changes +=1
                        #     if (len(item_doc.item_short_name)>16):
                        #         record.item_short = item_doc.item_short_name[:16]
                        #     else:
                        #         record.item_short=item_doc.item_short_name
                        #     #record.write_record(vat=vat)
                        #     print "Update item_short"
                        if item_doc.packing != table[i].packing:
                            changes +=1
                            old_packing = table[i].packing
                            record.packing=item_doc.packing
                            #record.write_record(vat=vat)
                            print "Update packing"
                        if item_doc.item_freight_peso != table[i].freight:
                            changes += 1
                            record.freight = item_doc.item_freight_peso
                            #record.write_record(freight = item_doc.item_freight_peso)
                            print "Update Freight"
                        if (item_doc.item_cost != table[i].gross):
                            changes += 1
                            old_cost = record.gross
                            record.gross = item_doc.item_cost
                            print "update item_cost"
                        if (item_doc.item_freight!= table[i].chrg_pct):
                            changes +=1
                            record.chrg_pct = item_doc.item_freight
                            print "update freight_perc"
                        if (item_doc.item_freight_peso != table[i].freight):
                            changes +=1
                            record.freight = item_doc.item_freight_peso
                            print "update freight_peso"
                        if (record.discount != 0) or (record.disc_amt != 0):
                            record.discount = 0
                            record.disc_amt = 0
                                #record.freight = item_doc.item_freight_peso
                                #chrg_pct = item_freight
                                #freight = item_freight_peso
                                #print record.bar_code, record.gross, record.cost_w
                                #print record.discount, record.disc_amt, record.freight, record.chrg_pct
                                #print record.updated, record.dpct_first, record.cpct_first, record.disc_first
                                ######Pseudo code ni sir michael:
                                #temp:=gross
                                #if disc_first == " "
                                #   if dpct_first == " "
                                #       temp:=gross*(1-discount/100)-disc_amt
                                #   else
                                #       temp:=(gross-disc_amt)*(1-discount/100)
                                #if cpct_first == " "
                                #   temp:=temp*(1+chrg_pct/100) + freight
                                #else
                                #   temp:=(temp+freight)*(1+chrg_pct/100)
                                #if disc_first == "*"
                                #   if dpct_first = " "
                                #       temp:=temp*(1-discount/100)-disc_amt
                                #    else
                                #       temp:=(temp-disc_amt)*(1-discount/100)
                                # temp = item_cost_with_freight.
                        if (item_doc.item_cost_with_freight != table[i].cost_w):
                            changes += 1
                            record.cost_w = item_doc.item_cost_with_freight
                            #record.write_record(cost_w = item_doc.item_cost_with_freight)
                            print "update item_cost_with_freight"
                        if item_doc.item_price_retail_1 != table[i].cost_r:
                            changes += 1
                            if (table[i].packing >1):
                                print "update item_price_retail_1"
                                item_retail = decimal.Decimal(str(item_doc.item_price_retail_1)).quantize(decimal.Decimal('.01'),
                                                                                 rounding=decimal.ROUND_HALF_UP)
                                record.cost_r = item_retail
                            else:
                                record.cost_r = record.cost_w
                                #print "ROUNDED UP:", item_retail, " | ORIGINAL",item_doc.item_price_retail_1
                                #record.write_record(cost_r = item_doc.item_price_retail_1)
                        if item_doc.item_price != table[i].price_w:
                            changes += 1
                            print "update item_price"
                            old_ws = table[i].price_w
                            record.price_w = item_doc.item_price
                        if item_doc.item_price_retail_with_margin != table[i].price_r:
                            changes += 1
                            old_ret = table[i].price_r
                            if (table[i].packing >1):
                                record.price_r = item_doc.item_price_retail_with_margin
                            else:
                                record.price_r = record.price_w
                            print "update item_price_retail_with_margin"
                        #new fields as of 02-16-2022
                        if item_doc.barcode_pack != table[i].c_bar_code:
                            changes += 1
                            record.c_bar_code = item_doc.barcode_pack
                        if item_doc.unit_2:
                            print"check unit 2 fields"
                            if item_doc.unit_2 != table[i].unit_2:
                                changes += 1
                                record.unit_2 = item_doc.unit_2
                            if item_doc.bar_code_2 != table[i].bar_code_2:
                                changes += 1
                                record.bar_code_2 = item_doc.bar_code_2
                            if item_doc.packing_2 != table[i].packing_2:
                                changes +=1
                                record.packing_2 = item_doc.packing_2
                            if item_doc.item_price_retail_2 != table[i].price_2:
                                changes +=1
                                record.price_2 = item_doc.item_price_retail_2
                        # if str(item_doc.dep_code) != str(dep_short):
                        #     changes += 1
                        #     print "update DEPCODE"
                        #     temp_code = get_dep_code(dep_short,dep_coding)
                        #     #print item_doc.dep_code, int(table[i].dep_code), temp_code
                        #     record.dep_code = temp_code

                        if changes >0:
                            item_list.append({"barcode":item_doc.barcode_retial,
                                              "item_name":item_doc.item_name_dummy,
                                              "old_cost":old_cost,
                                              "new_cost":item_doc.item_cost,
                                              "old_packing": old_packing,
                                              "new_packing": item_doc.packing,
                                              "old_ws": old_ws,
                                              "new_ws": item_doc.item_price,
                                              "old_ret": old_ret,
                                              "new_ret": item_doc.item_price_retail_with_margin,
                                              "ws_margin": item_doc.margin_rate,
                                              "ret_margin": item_doc.retail_margin_rate
                                              })
                            print "SOMETHING CHANGED. APPEND TO ITEM LIST------------"
            i +=1
        print i, "i FINAL VALUE"
    write_to_csv_updated_in_old(item_list)
    update_updated_dbf()
    update_log_file()

def update_updated_dbf():

    now = datetime.datetime.now()
    date_now = now.date()
    time_now = now.time().isoformat()[0:8]

    print date_now, time_now
    sync_folder = frappe.get_value("Server Information",None, "sync_folder_dir")
    sync_folder = "/media/GROCERY_SERVER/"+sync_folder
    #table = dbf.Table('/home/gaisano1/gaisano_chaka/erp/UPDATED', codepage='cp437')
    table = dbf.Table(sync_folder+'/UPDATED') #, codepage='cp437')

    print sync_folder

    #reset_dbf(table)
    with table:
        for record in table:
            print "DATE TIME STORED:"
            print record
            with record:
                record.date = date_now
                record.time = time_now
            print "UPDATED DATE/TIME"
            print record



def reset_dbf(table):
    date_now = datetime.datetime.strptime('2018-01-01', "%Y-%m-%d")
    time_now = date_now.time().isoformat()[0:8]

    with table:
        for record in table:
            with record:
                record.date = date_now
                record.time = time_now

#TODO:remove later on
def check_table_fields():
    table = dbf.Table('/home/gaisano1/gaisano_chaka/grocery')#, codepage='cp437')
    print table.field_names

def check_gross_vs_cost_w():
    branch = get_branch()

    table = dbf.Table('/media/backup/pos_data/grocery_dbf/ITEM')#, codepage='cp437')
    #table = dbf.Table('/home/gaisano1/gaisano_chaka/grocery', codepage='cp437')
    #print table.field_names
    with table:
        i = 0
        count_barcodes = 0
        print "Table exists."
        table_len = len(table)
        print "item_barcode|discount|disc_amt|freight|chrg_pct|gross|cost_w"
        while i < table_len:
            try:
                #print i, table[i].bar_code
                record = table[i]
                with record:
                    record = table[i]
                    item = get_item(record.bar_code)
                    changes = 0
                    if record.cost_w != record.gross:
                        if (record.discount!=0) or (record.disc_amt!=0) or (record.freight != 0) or (record.chrg_pct!=0):
                            #print record.bar_code, record.gross, record.cost_w
                            #print record.discount, record.disc_amt, record.freight, record.chrg_pct
                            #print record.updated
                            if item:
                                count_barcodes += 1
                                print record.bar_code, "|", record.discount, "|", record.disc_amt, "|", record.freight, "|", record.chrg_pct,"|",record.gross,"|",record.cost_w
                            #print "item_barcode \t dpct_first \t cpct_first \t disc_first"
                            #print record.bar_code,"\t|", record.dpct_first,"\t|", record.cpct_first, "\t|",record.disc_first
            except:
                pass
            i+=1
        print "Number of barcodes", count_barcodes


def check_item_dep_codes():
    branch = get_branch()

    table = dbf.Table('/media/backup/pos_data/grocery_dbf/ITEM')#, codepage='cp437')
    # table = dbf.Table('/home/littlehera/dbf/ITEM2', codepage='cp437')
    items = []
    dept_coding = get_department_coding()
    with table:
        i = 1
        print "Table exists."
        table_len = len(table)
        while i < table_len:
            try:
                print i, table[i].bar_code
            except:
                pass
            else:
                record = table[i]
                item = get_item(table[i].bar_code)
                if item!=None:
                    record = table[i]
                    with record:
                        changes = 0
                        item_doc = frappe.get_doc("Item", item)
                        if item_doc.type != 'Disabled':
                            dep_short = get_dep_short(table[i].dep_code,dept_coding)
                            print str(item_doc.dep_code), str(dep_short), "<-------------"
                            if str(item_doc.dep_code) != str(dep_short):
                                status = "DEPCODE NOT MATCHING"
                                if (item_doc.dep_code is None) or (item_doc.dep_code ==""):
                                    status ="No New System Depcode"
                                items.append({'barcode':item_doc.barcode_retial,'item_desc':item_doc.item_name_dummy,
                                              'status':status,'depcode_in_new':str(item_doc.dep_code),
                                              'depcode_in_old':dep_short})

            i+=1

    return items

def copy_depcodes_from_old_to_new():
    branch = get_branch()

    table = dbf.Table('/media/backup/pos_data/grocery_dbf/ITEM')#, codepage='cp437')
    #table = dbf.Table('/home/littlehera/dbf/ITEM2', codepage='cp437')
    dept_coding = get_department_coding()

    with table:
        i = 1
        print "Table exists."
        table_len = len(table)
        while i < table_len:
            try:
                print i, table[i].bar_code
            except:
                pass
            else:
                record = table[i]
                item = get_item(table[i].bar_code)
                if item!=None:
                    record = table[i]
                    with record:
                        changes = 0
                        item_doc = frappe.get_doc("Item", item)
                        if item_doc.type != 'Disabled':
                            dep_short = get_dep_short(table[i].dep_code, dept_coding)
                            if str(item_doc.dep_code) != str(dep_short):
                                try:
                                    print str(item_doc.dep_code), str(dep_short), table[i].bar_code
                                    dep_code_doc = frappe.get_doc("Department Code",str(dep_short))
                                except:
                                    print "Depcode not found"
                                else:
                                    print "update dep short"
                                    department = dep_code_doc.department
                                    depcode_int = int(table[i].dep_code)
                                    frappe.db.sql(
                                        """UPDATE `tabItem` set dep_code = %s, item_category = %s where name = %s""",
                                        (depcode_int, department, item))
                                    frappe.db.commit()

            i+=1

def update_log_file():
    user = frappe.get_user()
    now = datetime.datetime.now()
    print user.name, now.date(), now.time(), now

    branch = get_branch()

    with open('/home/frappe/frappe-v7/sites/site1.local/public/files/copycost_log.csv', 'a') as csvfile:
        # with open('/home/littlehera/frappe-v7/sites/site1.local/public/files/copycost_log.csv', 'a') as csvfile:
        data = [user.name, now]
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(data)


def get_logfile_data():
    report_data = []
    branch = get_branch()

    with open('/home/frappe/frappe-v7/sites/site1.local/public/files/copycost_log.csv', 'rt') as csvfile:
    # with open('/home/baban/frappe-v7/copycost_log.csv', 'rt') as csvfile:
        data = csv.reader(csvfile)
        data = list(data)
        for row in reversed(data):
            try:
                report_data.append({"user": row[0], "updated": row[1]})
            except:
                continue
        return report_data


def get_department_coding():
    dbf_path = frappe.db.get_value("Server Information", None, "grocery_dbf_dir")
    branch = get_branch()
    cmd = "cp /media/GROCERY_SERVER/" + dbf_path + "/SP_DEP.DBF /media/backup/pos_data/grocery_dbf/SP_DEP.dbf"
    os.system(cmd)

    table = dbf.Table('/media/backup/pos_data/grocery_dbf/SP_DEP')#, codepage='cp437')
    #table = dbf.Table('/home/littlehera/dbf/SP_DEP', codepage='cp437')
    depcodes=[]
    with table:
        i = 1
        print "Table exists."
        table_len = len(table)
        while i < table_len:
            try:
                print i, table[i].dep_code, table[i].dep_short
            except:
                pass
            else:
                record = table[i]
                with record:
                    depcodes.append({"dep_code":table[i].dep_code,"dep_short": table[i].dep_short})
            i+=1
    return depcodes

def get_dep_short(depcode,dept_coding):
    #dept_coding = get_department_coding()
    dep_short = None
    for row in dept_coding:
        if row['dep_code']==depcode:
            dep_short = row['dep_short']
    return None if dep_short is None else int(dep_short)

def get_dep_code(depshort,dept_coding):
    #dept_coding = get_department_coding()
    dep_code = None
    print "**********************"
    for row in dept_coding:
        #print int(row['dep_short']), int(depshort)
        if int(row['dep_short'])==int(depshort):
            dep_code = row['dep_code']
    return None if dep_code is None else (dep_code)
import frappe
from frappe.utils import flt,cint, cstr
from poevents import get_supplier_discount
import json
from erpnext.controllers.buying_controller import BuyingController
from erpnext.accounts.utils import get_account_currency
import requests
from frappe.frappeclient import FrappeClient
import datetime
from apisync import connect_to_server
#TODO: Reflect if you still need to get the supplier discount from po.

@frappe.whitelist()
def withdraw_to_display(rr):
    rr_doc = frappe.get_doc("Purchase Receipt", rr)
    auto_withdraw(rr_doc,method="override")

@frappe.whitelist()
def auto_withdraw(doc, method):
    if doc.withdraw_to_display == "Yes":

        se = frappe.new_doc("Stock Entry")
        se.owner = doc.owner
        se.to_warehouse = doc.withdraw_to_warehouse
        se.from_warehouse = doc.delivered_to_warehouse
        se.naming_series = 'DIS-.branch_code.-.YY.MM.DD'
        se.posting_date = doc.posting_date
        se.branch = doc.branch
        se.type = "Display"
        se.branch_code = doc.branch_code
        se.rr_number = doc.name

        for item in doc.items:
            se.append('items', {
                'transfer_qty': item.qty,
                'image': '',
                'qty': item.qty,
                'cost_center': 'Main - GG',
                'transfer_qty': item.qty,
                'actual_qty': item.qty,
                'stock_uom': item.uom,
                'qty_dummy': item.qty,
                'item_id': item.item_id,
                'item_description': item.item_description,
                'item_code': item.item_code,
                'uom': item.uom,
                'conversion_factor': item.conversion_factor,
                'price': item.rate,
                'amount': item.amount,
                's_warehouse': doc.delivered_to_warehouse,
                't_warehouse': doc.withdraw_to_warehouse
            })
        se.save()
        se.submit()
        frappe.db.sql("""UPDATE `tabPurchase Receipt` set withdrawal_number = %s where name = %s""", (se.name, doc.name))
        frappe.db.commit()
        doc.withdrawal_number = se.name

def calc_discounts(doc, method):
    doc.calc_disc_5 = 0
    branch = frappe.get_doc("Branch",doc.branch)
    branch_code = branch.branch_code if (branch.branch_code !="")and(branch.branch_code !=None) else branch
    doc.branch_code = branch_code

    if (doc.purchase_order is not None) and (doc.purchase_order != ""):
        doc.discount_1, doc.discount_2, doc.discount_3, doc.discount_4, doc.additional_supplier_discount = get_po_discount(doc.purchase_order)

    if doc.supplier_discount:
        doc.title = doc.supplier_discount_name
        if doc.get_discounts_from_supplier_discount == 1:
            doc.discount_1, doc.discount_2, doc.discount_3, doc.discount_4 = get_supplier_discount_values(doc.supplier_discount)

    else:
        if doc.supplier:
            doc.title = doc.supplier
        else:
            doc.title = doc.stt_from_branches
    # calculate total less supplier discount
    disc1 = float(doc.discount_1) / 100.00
    disc2 = float(doc.discount_2) / 100.00
    disc3 = float(doc.discount_3) / 100.00
    disc4 = float(doc.discount_4) / 100.00
    disc5 = float(doc.additional_supplier_discount) / 100.00
    total_freight = 0
    total_case = 0
    total_case_order = 0
    total_trade_disc = 0
    doc.grand_total_less_discount = 0
    doc.trade_discount = 0
    grand_total_new = 0

    if (doc.purchase_order is None) or (doc.purchase_order == ""):
        if (doc.rr_type == "Get PO from Other Branch"):
            get_service_level_from_branch_po(doc.po_from_other_branch, doc)
        doc = compute_discounts_etc(doc)

    if (doc.purchase_order is not None) and (doc.purchase_order != ""):
        po = frappe.get_doc("Purchase Order",doc.purchase_order)

        if (po.total_case) <= 0:
            for po_item in po.items:
                print po_item.item_code, po_item.qty
                total_case_order += po_item.qty
                print "Cases PO" +str(total_case_order)
        else:
            total_case_order = po.total_case


        for row in doc.get("items"):
            item_discount = get_supplier_discount(row.item_code)
            if doc.supplier_discount not in item_discount:
                frappe.msgprint("Item " + row.item_description + "(" + item_discount[0] + ") does not belong in this Supplier Discount.",
                             "Warning")
            if doc.purchase_order:
                po_doc = frappe.get_doc("Purchase Order", str(doc.purchase_order))
                item_temp = frappe.db.sql("""SELECT name from `tabPurchase Order Item` where parent = %s and item_code = %s""", (doc.purchase_order, row.item_code))
                if len(item_temp) > 0:
                    po_item = frappe.get_doc("Purchase Order Item", item_temp[0][0])
                    print "PO ITEM =  " + po_item.name + "| qty: "+str(po_item.qty)
                    row.po_qty = po_item.qty
                else:
                    po_item = None
            #     row.freight_charge = po_doc.freight_charge

            if row.zero_cost == 1:
                total_trade_disc += (row.base_net_amount/row.conversion_factor)
                temp_rate = row.base_rate/row.conversion_factor
                temp_amount = (row.base_net_amount/row.conversion_factor)
                row.base_net_amount = temp_amount
                row.base_rate = temp_rate
                row.rate = 0
                row.amount = 0
                row.base_amount = 0
                row.net_amount = 0
            else:

                if (doc.double_served == 0):
                    if (po_item):
                        if (doc.cost_override_for!="WET MARKET/GAISANO VEGTABLES"):
                            #row.rate = po_item.rate
                            #row.amount = row.rate * row.qty
                            #row.base_amount = row.rate * row.qty
                            #row.base_net_amount = row.rate * row.qty
                            row.rate = po_item.rate
                            row.amount = row.rate * row.qty
                            row.qty_received = row.qty
                            row.base_amount = row.rate * row.qty
                            row.base_net_amount = row.rate * row.qty
                            row.conversion_factor = po_item.conversion_factor
                            row.uom = po_item.uom
                            print row.po_qty, row.qty_received, "TESTESTSETSETSET"
                            if (row.po_qty < row.qty_received):
                                frappe.throw("Item " + row.item_description + " is overserved!")

                    else:
                        frappe.throw("Item "+row.item_description+" is not in PO!")
                else:

                    po_detail = frappe.db.sql("""Select rate, qty from `tabPurchase Order Item`
                            where parent = %s and item_code = %s""", (doc.purchase_order, row.item_code))

                    for itm in po_detail:
                        rate = float(itm[0])
                        row.rate = rate
                        row.amount = rate * row.qty
                        row.base_amount = rate * row.qty
                        row.qty_received = row.qty
                        row.base_net_amount = rate * row.qty
                        row.po_qty = float(itm[1])

            if (row.conversion_factor != 0):
                row.stock_qty = row.qty / row.conversion_factor

            barcode = frappe.db.sql("""Select barcode_retial from tabItem where name = %s""", (row.item_code))
            row.warehouse = doc.delivered_to_warehouse
            row.freight_amount = row.freight_charge * row.qty
            total_freight += row.freight_amount
            total_case += row.qty
            #total_case_order += row.received_qty

            packing = frappe.db.sql("""Select packing from `tabItem`
                          where name = %s""", (row.item_code))
            for p in packing:
                pkg_list = int(p[0])
                row.packing = pkg_list/row.conversion_factor

            for ret_barcode in barcode:
                row.barcode_retial = str(ret_barcode[0])

                if disc1 != 0 and disc2 == 0 and disc3 == 0 and disc4 == 0:
                    discounted = float(row.rate) - float(row.rate * disc1)
                    row.net_cost = discounted
                    row.net_cost_amount = row.net_cost * row.qty
                elif disc1 != 0 and disc2 != 0 and disc3 == 0 and disc4 == 0:
                    discounted = float(row.rate) - float(row.rate * disc1)
                    second_discount = float(discounted) - float(discounted * disc2)
                    row.net_cost = second_discount
                    row.net_cost_amount = row.net_cost * row.qty
                elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 == 0:
                    discounted = float(row.rate) - float(row.rate * disc1)
                    second_discount = float(discounted) - float(discounted * disc2)
                    final_total = float(second_discount) - float(second_discount * disc3)
                    row.net_cost = final_total
                    row.net_cost_amount = row.net_cost * row.qty
                elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 != 0:
                    discounted = float(row.rate) - float(row.rate * disc1)
                    second_discount = float(discounted) - float(discounted * disc2)
                    final_total = float(second_discount) - float(second_discount * disc3)
                    super_total = float(final_total) - float(final_total * disc4)
                    row.net_cost = super_total
                    row.net_cost_amount = row.net_cost * row.qty
                else:
                    print "else : no discounts"
                    print disc5
                    row.net_cost = row.rate*float(1-disc5)
                    row.net_cost_amount = row.net_cost * row.qty
                    print row.net_cost
            grand_total_new +=row.amount
        doc.total_freight_charge = get_total_freight_cost(doc)
        doc.total_case = total_case
        doc.total_case_order = total_case_order
        doc.trade_discount = doc.trade_discount + total_trade_disc
        doc.grand_total = grand_total_new

        new_total = doc.grand_total #- doc.trade_discount
        doc.total_amount = new_total
        if disc1 != 0 and disc2 == 0 and disc3 == 0 and disc4 == 0:

            discounted = float(new_total) - float(new_total * disc1)
            disc_amt_1 = float(new_total* disc1)
            doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
            doc.calc_disc_2 = 0
            doc.calcl_disc_3 = 0
            doc.calc_disc_4 = 0
            less_disc = discounted
            trade_disc = float(doc.trade_discount)
            if (doc.additional_supplier_discount == 0):
                doc.grand_total_less_discount = less_disc
            else:
                add_disc = float(discounted) - float(discounted * disc5)
                disc_amt_5 = float(float(discounted * disc5))
                doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
                doc.grand_total_less_discount = add_disc
        elif disc1 != 0 and disc2 != 0 and disc3 == 0 and disc4 == 0:
            discounted = float(new_total) - float(new_total * disc1)
            disc_amt_1 = float(new_total * disc1)
            doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
            second_discount = float(discounted) - float(discounted * disc2)
            disc_amt_2 = float(float(discounted * disc2))
            doc.calc_disc_2 = '{:20,.2f}'.format(disc_amt_2)
            doc.calcl_disc_3 = 0
            doc.calc_disc_4 = 0
            less_disc = second_discount
            trade_disc = float(doc.trade_discount)

            if (doc.additional_supplier_discount == 0):
                doc.grand_total_less_discount = less_disc
            else:
                add_disc = float(second_discount) - float(second_discount * disc5)
                disc_amt_5 = float(float(second_discount * disc5))
                doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
                doc.grand_total_less_discount = add_disc
        elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 == 0:
            discounted = float(new_total) - float(new_total * disc1)
            disc_amt_1 = float(new_total * disc1)
            doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
            second_discount = float(discounted) - float(discounted * disc2)
            disc_amt_2 = float(float(discounted * disc2))
            doc.calc_disc_2 = '{:20,.2f}'.format(disc_amt_2)
            final_total = float(second_discount) - float(second_discount * disc3)
            disc_amt_3 = float(second_discount * disc3)
            doc.calcl_disc_3 = '{:20,.2f}'.format(disc_amt_3)
            doc.calc_disc_4 = 0
            less_disc = final_total
            trade_disc = float(doc.trade_discount)
            if (doc.additional_supplier_discount == 0):
                doc.grand_total_less_discount = less_disc
            else:
                add_disc = float(final_total) - float(final_total * disc5)
                disc_amt_5 = float(float(final_total * disc5))
                doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
                doc.grand_total_less_discount = add_disc
        elif disc1 != 0 and disc2 != 0 and disc3 != 0 and disc4 != 0:
            discounted = float(new_total) - float(new_total * disc1)
            disc_amt_1 = float(new_total * disc1)
            doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
            second_discount = float(discounted) - float(discounted * disc2)
            disc_amt_2 = float(float(discounted * disc2))
            doc.calc_disc_2 = '{:20,.2f}'.format(disc_amt_2)
            final_total = float(second_discount) - float(second_discount * disc3)
            disc_amt_3 = float(second_discount * disc3)
            doc.calcl_disc_3 = '{:20,.2f}'.format(disc_amt_3)
            final_total_4 = float(final_total) - float(final_total * disc4)
            disc_amt_4 = float(final_total * disc4)
            doc.calc_disc_4 = '{:20,.2f}'.format(disc_amt_4)
            less_disc = final_total_4
            trade_disc = float(doc.trade_discount)
            if (doc.additional_supplier_discount == 0):
                doc.grand_total_less_discount = less_disc
            else:
                add_disc = float(final_total_4) - float(final_total_4 * disc5)
                disc_amt_5 = float(float(final_total_4 * disc5))
                doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
                doc.grand_total_less_discount = add_disc
        else:
            doc.calc_disc_1 = 0
            doc.calc_disc_2 = 0
            doc.calcl_disc_3 = 0
            doc.calc_disc_4 = 0
            less_disc = doc.grand_total
            trade_disc = float(doc.trade_discount)

            if (doc.additional_supplier_discount == 0):
                doc.grand_total_less_discount = less_disc#- trade_disc
            else:
                print "============ Only Additional Discount ================="
                disc_amt = float(doc.grand_total) - float(doc.grand_total * disc5)
                disc_amt_5 = float(doc.grand_total * disc5)
                doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)
                doc.grand_total_less_discount = disc_amt# - trade_disc
                print "Grand total:" +str(doc.grand_total)
                print "Total with discount: "+ str(doc.grand_total_less_discount)
                frappe.db.commit()

        if (doc.calc_disc_1 == 0 and doc.calc_disc_2 == 0 and doc.calcl_disc_3 == 0 and doc.calc_disc_4 == 0 and doc.additional_supplier_discount == 0):
            print "=======no discounts========"
            doc.grand_total_less_discount = doc.grand_total_less_discount

        compute_service_level(doc)
    else:
        #print "PO NOT FROM MAIN?"
        total_case= 0
        grand_total = 0
        for row in doc.items:
            total_case += row.qty
            grand_total += row.amount
        doc.total_case = total_case
        #doc.grand_total = grand_total
        #doc.grand_total_less_discount = grand_total

    for i,item in enumerate(doc.items):
        if item.name != None:
            item_id = branch_code + '-' + item.name
            doc.items[i].item_id = item_id
            print item_id

    for x, item in enumerate(doc.items):
        current_item = doc.items[x].item_code
        current_qty = doc.items[x].qty
        is_free = doc.items[x].zero_cost

        if is_free == 0 and doc.rr_type == "Receive PO from this Branch":
            result = frappe.db.sql(
            """select qty from `tabPurchase Order Item` where parent = %s and docstatus = 1 and item_code = %s """,
            (doc.purchase_order, current_item))
            qty_po = result[0][0]
            rr = frappe.db.sql(
            """select sum(qty) from `tabPurchase Receipt Item` where purchase_order = %s and docstatus = 1 and item_code = %s""",
            (doc.purchase_order, current_item))
            total_rr_qty = rr[0][0]
            if total_rr_qty != None:
                remaining = (qty_po - total_rr_qty) - current_qty
                print remaining, qty_po, total_rr_qty, current_qty, "cacl"
                if (remaining < 0 and doc.items[x].zero_cost != 1 and doc.cost_override_for != 'WET MARKET/GAISANO VEGTABLES'):
                    frappe.throw(doc.items[x].item_description + " has exceeded PO quantity.")
            else:
                print "PROCEED"

    for x, item in enumerate(doc.items):
        current_item = doc.items[x].item_code
        for y, item in enumerate(doc.items):
            if x != y and current_item == doc.items[y].item_code and (
                    doc.items[y].zero_cost != 1 and doc.items[x].zero_cost != 1):
                frappe.throw(
                    "Duplicate Item: " + current_item + ". Please check row " + str(x + 1) + " and " + str(y + 1) + ".")


@frappe.whitelist()
def create_item_id(from_date, to_date):
    po_name = frappe.db.sql("""SELECT name,branch_code from `tabPurchase Receipt` where creation BETWEEN %s and %s""",(from_date, to_date))
    for po_names in po_name:
        naming = frappe.db.sql("""SELECT name from `tabPurchase Receipt Item` WHERE parent = %s AND item_id IS NULL""",(po_names[0]))
        print po_names[0]
        for namings in naming:
            print namings[0]
            item_id = po_names[1] + "-" + namings[0]
            frappe.db.sql("""UPDATE `tabPurchase Receipt Item` set item_id = %s where parent = %s""",(item_id, po_names[0]))
            frappe.db.commit()

def compute_discounts_etc(doc):
    if (doc.supplier_discount is None) or (doc.supplier_discount == ""):
        doc.discount_1, doc.discount_2, doc.discount_3, doc.discount_4 = 0, 0, 0, 0

    disc1 = float(doc.discount_1) / 100.00
    disc2 = float(doc.discount_2) / 100.00
    disc3 = float(doc.discount_3) / 100.00
    disc4 = float(doc.discount_4) / 100.00
    disc5 = float(doc.additional_supplier_discount) / 100.00
    total_discount = 0
    total_trade_disc = 0
    total_freight = 0
    total_case = 0
    grand_total_new = 0
    for row in doc.get("items"):
        print "CHECK IF ITEM IN DISCOUNT"
        item_discount = get_supplier_discount(row.item_code)
        # print (doc.supplier_discount), "This discount?"
        if (doc.supplier_discount not in item_discount) and ((doc.supplier_discount is not None) and (doc.supplier_discount != "")):
            frappe.msgprint("Item " + row.item_description + "(" + item_discount[
                    0] + ") does not belong in this Supplier Discount.",
                             "Warning")
        if row.zero_cost == 1:
            print "ZERO COST == 1"
            total_trade_disc += (row.base_net_amount / row.conversion_factor)
            temp_rate = row.base_rate / row.conversion_factor
            temp_amount = (row.base_net_amount / row.conversion_factor)
            row.base_net_amount = temp_amount
            row.base_rate = temp_rate
            row.rate = 0
            row.amount = 0
            row.base_amount = 0
            row.net_amount = 0

        if (row.conversion_factor != 0):
            row.stock_qty = row.qty / row.conversion_factor

        barcode = frappe.db.sql("""Select barcode_retial from tabItem where name = %s""", (row.item_code))
        row.warehouse = doc.delivered_to_warehouse
        row.freight_amount = row.freight_charge * row.qty
        total_freight += row.freight_amount
        total_case += row.qty

        packing = frappe.db.sql("""Select packing from `tabItem`
                      where name = %s""", (row.item_code))
        for p in packing:
            pkg_list = int(p[0])
            row.packing = pkg_list / row.conversion_factor

        for ret_barcode in barcode:
            row.barcode_retial = str(ret_barcode[0])
            discounted = float(row.rate)*(1-disc1)*(1-disc2)*(1-disc3)*(1-disc4)*(1-disc5)
            row.net_cost = discounted
            row.net_cost_amount = row.net_cost * row.qty
            print row.net_cost
            total_discount += discounted
        grand_total_new += row.amount

    doc.total_freight_charge = get_total_freight_cost(doc)
    doc.total_case = total_case
    doc.trade_discount = doc.trade_discount + total_trade_disc
    doc.grand_total = grand_total_new
    new_total = doc.grand_total  # - doc.trade_discount
    doc.total_amount = new_total
    disc_amt_1 = float(new_total * disc1)
    disc_amt_2 = float(new_total * (1-disc1) * (disc2))
    disc_amt_3 = float(new_total * (1-disc1) * (1-disc2) * disc3)
    disc_amt_4 = float(new_total * (1-disc1) * (1-disc2) * (1-disc3) * disc4)
    disc_amt_5 = float(new_total * (1-disc1) * (1-disc2) * (1-disc3) * (1-disc4) * disc5)

    doc.calc_disc_1 = '{:20,.2f}'.format(disc_amt_1)
    doc.calc_disc_2 = '{:20,.2f}'.format(disc_amt_2)
    doc.calc_disc_3 = '{:20,.2f}'.format(disc_amt_3)
    doc.calc_disc_4 = '{:20,.2f}'.format(disc_amt_4)
    doc.calc_disc_5 = '{:20,.2f}'.format(disc_amt_5)

    conv1 = float("".join(doc.calc_disc_1.split(",")))
    conv2 = float("".join(doc.calc_disc_2.split(",")))
    conv3 = float("".join(doc.calc_disc_3.split(",")))
    conv4 = float("".join(doc.calc_disc_4.split(",")))
    conv5 = float("".join(doc.calc_disc_5.split(",")))

    doc.grand_total_less_discount = doc.grand_total - (conv1 + conv2 + conv3 + conv4 + conv5)
    return doc

def update_po_status(doc, method):

    #count_sle = frappe.db.sql("""SELECT COUNT(*) from `tabStock Ledger Entry` where voucher_no = %s""",doc.name)
    #if len(doc.items) != int(count_sle[0][0]):
    #    frappe.throw("Inventory generated is wrong. Please submit document again.")

    from gaisano.report.service_level_report.service_level_report import get_po_totals
    if (doc.purchase_order):
        total_percent, total_qty, rr_links, rr_names, total_peso_value, rr_qty, rr_peso_value = get_po_totals(doc.purchase_order)
        print "QTY SERVICE LEVEL = ", total_qty
        po = frappe.get_doc("Purchase Order", doc.purchase_order)
        print "+++++++++++++++++++++++++++++++++++"
        if (po.is_delivered != 'Yes'):
            po.status = 'Yes'
            frappe.db.sql("""Update `tabPurchase Order` set is_delivered = 'Yes', status = 'To Bill', per_received = %s, 
            receive_unserved_items = 'No' where name = %s""", (total_qty, doc.purchase_order))
            print po.name, po, "Receive unserved items"
            print "Updated +++++++++++++++"
        else:
            frappe.db.sql("""Update `tabPurchase Order` set per_received = %s
                            where name = %s""", (total_qty, doc.purchase_order))
            print "Already Delivered"
        frappe.db.commit()

        for item in doc.items:
            po_item = frappe.db.sql("""SELECT name from `tabPurchase Order Item` where parent = %s and item_code = %s""",(po.name, item.item_code))
            if len(po_item)>0:
                print po_item[0][0], item.qty, item.item_code, "Update qty received."
                frappe.db.sql("""UPDATE `tabPurchase Order Item` set received_qty = %s where name = %s""",(item.qty, po_item[0][0]))
        frappe.db.commit()

        auto_withdraw(doc,method)
        #update_item_inv(doc)
        create_sle_log(doc) #create log entry

@frappe.whitelist()
def get_po_details(name):
    doc = frappe.get_doc("Purchase Order", name)
    return {"discount_1": str(doc.discount_1),
            "discount_2": str(doc.discount_2),
            "discount_3": str(doc.discount_3),
            "discount_4": str(doc.discount_4),
            "supplier_discount": str(doc.supplier_discount),
            "supplier_discount_name": str(doc.supplier_discount_name),
            "supplier": str(doc.supplier),
            "branch": str(doc.branch),
            "delivered_to_warehouse": str(doc.delivered_to_warehouse),
            "trade_discount":str(doc.trade_discount),
            "memo": str(doc.memo),
            "additional_supplier_discount":str(doc.additional_supplier_discount)}

@frappe.whitelist()
def get_items_from_double_served_po(po_name):

    doc = frappe.db.sql("""Select item_name, item_code, rate, qty, uom from `tabPurchase Order Item`
          where parent = %s""", (po_name))

    #print "Im Here in PO"
    #print doc

    items_array_code = []
    items_array_name = []
    items_array_cost = []
    items_array_uom = []
    items_array_qty = []

    for po_num in doc:
        print "Im Here inside PO Items"
        item_name = str(po_num[0])
        item_cost = float(po_num[2])
        item_uom = str(po_num[4])
        item_code = str(po_num[1])
        item_qty = float(po_num[3])
        items_dict_code = {"item": item_code}
        items_dict_name = {"item": item_name}
        items_dict_cost = {"item": item_cost}
        items_dict_uom = {"item": item_uom}
        items_dict_qty = {"item": item_qty}

        items_array_code.append(items_dict_code)
        items_array_name.append(items_dict_name)
        items_array_cost.append(items_dict_cost)
        items_array_uom.append(items_dict_uom)
        items_array_qty.append(items_dict_qty)

        print "returning arrays"
        print items_array_code
        print items_array_name
        print items_array_cost
        print items_array_uom
        print items_array_qty

    return {"itm_code": items_array_code, "itm_name": items_array_name,
            "itm_cost": items_array_cost, "itm_uom": items_array_uom,
            "itm_qty": items_array_qty}


@frappe.whitelist()
def get_total_freight_cost(doc):
    print "=============++GET TOTAL FREIGHT++============"
    print doc
    total_freight_cost = 0
    items = doc.items
    #items = json.loads(doc)["items"]
    #items = [0,1,2]
    if len(items)>0:
        for item in items:
            current_item = frappe.get_doc("Item", item.item_code)
            print current_item.item_cost_with_freight
            print item.qty
            difference = abs(current_item.item_cost_with_freight - current_item.item_cost)
            if difference != current_item.item_cost:
                total_freight_cost += float(difference)*float(item.qty)
            print "Total Freight cost is now=" +str(total_freight_cost)

    return float(total_freight_cost)


def compute_service_level(doc):
    print "=============SERVICE LEVEL==========="
    peso_value = 0.00
    qty_based = 0.00

    temp_html = '<h6 style = "{margin-left:100px;}">'
    linked_po = frappe.get_doc("Purchase Order", str(doc.purchase_order))
    po_grand_total = linked_po.grand_total
    pr_grand_total = doc.grand_total


    peso_value = pr_grand_total/po_grand_total*100 if po_grand_total>0 else None

    po_items = get_items_total_qty(linked_po.items)
    pr_items = get_items_total_qty(doc.items)

    qty_based = pr_items/po_items*100

    print peso_value
    print qty_based
    #temp_html += "PESO VALUE: {:20,.2f} %".format(peso_value)
    #temp_html += "<br><span>QTY: {:20,.2f} %</h6>".format(qty_based)
    doc.service_level = peso_value
    doc.service_level_qty = qty_based

def get_items_total_qty(items):
    total_qty = 0
    print "=============items=============="
    for item in items:
        print item.item_code
        print item.qty
        total_qty += item.qty
    return total_qty

"""
Force Submit RR: Used to force submit an RR via custom button in script.
"""
@frappe.whitelist()
def force_submit_rr(rr_no):
    this_rr = frappe.get_doc("Purchase Receipt", rr_no)
    force_submit_rr_items(rr_no)
    update_po_status(this_rr, None)
    #update_item_inv(this_rr, None)
    try:
        update_stock_ledger(this_rr)
        create_sle_log(this_rr)
    except:
        return "Something went wrong with updating the SLE's"

def force_submit_rr_items(rr_no):
    frappe.db.sql("""Update `tabPurchase Receipt Item` set docstatus = 1, modified = NOW() where parent = %s;""",rr_no)
    frappe.db.sql("""Update `tabPurchase Receipt` set docstatus = 1, status = "To Bill", modified = NOW() where name = %s""",rr_no)
    frappe.db.commit()

def create_sle(rr_no):
    this_rr = frappe.get_doc("Purchase Receipt", rr_no)
    print this_rr.docstatus
    if this_rr.docstatus == 1:
        rows = frappe.db.sql("""SELECT name from `tabStock Ledger Entry` where voucher_no = %s""", rr_no)
        if len(rows) ==0:
            try:
                update_stock_ledger(this_rr)
            except:
                return "Something went wrong with updating the SLE's"
            else:
                print "SLE's CREATED SUCCESSFULLY FOR "+rr_no
        else:
            print "SLE ALREADY EXISTS"

def update_stock_ledger(self, allow_negative_stock=False, via_landed_cost_voucher=False):
    self.update_ordered_qty()

    sl_entries = []
    stock_items = self.get_stock_items()

    for d in self.get('items'):
        if d.item_code in stock_items and d.warehouse:
            pr_qty = flt(d.qty) / flt(d.conversion_factor)

            if pr_qty:
                sle = self.get_sl_entries(d, {
                    "actual_qty": flt(pr_qty),
                    "serial_no": cstr(d.serial_no).strip()
                })
                if self.is_return:
                    original_incoming_rate = frappe.db.get_value("Stock Ledger Entry",
                                                                 {"voucher_type": "Purchase Receipt",
                                                                  "voucher_no": self.return_against,
                                                                  "item_code": d.item_code}, "incoming_rate")

                    sle.update({
                        "outgoing_rate": original_incoming_rate
                    })
                else:
                    val_rate_db_precision = 6 if cint(self.precision("valuation_rate", d)) <= 6 else 9
                    incoming_rate = flt(d.valuation_rate, val_rate_db_precision)
                    sle.update({
                        "incoming_rate": incoming_rate
                    })
                sl_entries.append(sle)

            if flt(d.rejected_qty) != 0:
                sl_entries.append(self.get_sl_entries(d, {
                    "warehouse": d.rejected_warehouse,
                    "actual_qty": flt(d.rejected_qty) * flt(d.conversion_factor),
                    "serial_no": cstr(d.rejected_serial_no).strip(),
                    "incoming_rate": 0.0
                }))

    self.make_sl_entries(sl_entries, allow_negative_stock=allow_negative_stock,
                         via_landed_cost_voucher=via_landed_cost_voucher)

@frappe.whitelist()
def test_api(po_number):
    # tailerp_api = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
    tailerp_api = connect_to_server()
    try:
        po_doc = tailerp_api.get_doc("Purchase Order", po_number)
    except:
        # tailerp_api = FrappeClient("http://202.137.117.24:8080", "tailerp_api@gaisano.com", 't@1l3rpAPI')
        tailerp_api = connect_to_server()
        po_doc = tailerp_api.get_doc("Purchase Order", po_number)
    po_doc['docstatus'] = 0
    po_doc['status'] = 'Draft'
    po_doc['amended_from'] = None
    # print po_doc['docstatus'], po_doc['status']
    for item in po_doc['items']:
        item['docstatus'] = 0
    # print po_doc
    try:
        po_doc['memo'] += " [from " + po_number + "]"
    except:
        po_doc['memo'] = " [from " + po_number + "]"
    po_doc = frappe.get_doc(po_doc)
    po_doc.insert()
    po_doc.submit()

    po_name_new = str(po_number) + '-MANUAL_CP'
    frappe.db.sql("""UPDATE `tabPurchase Order Item` set parent = %s where parent = %s""", (po_name_new, po_doc.name))
    frappe.db.sql("""UPDATE `tabPurchase Order` set name = %s where name = %s""", (po_name_new, po_doc.name))

    frappe.db.commit()
    return po_name_new

@frappe.whitelist()
def get_po_from_replication(po_number):
    # tailerp_api = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
    tailerp_api =connect_to_server()
    print "========get from replication========="
    # TODO: reincode to accomodate OLD naming series.
    try:
        try:
            po_doc = tailerp_api.get_doc("Purchase Order", po_number)
        except:# get from replication/ TODO: deprecate/remove
            # tailerp_api = FrappeClient("http://202.137.117.24:8080", "tailerp_api@gaisano.com", 't@1l3rpAPI')
            tailerp_api = connect_to_server()
            po_doc = tailerp_api.get_doc("Purchase Order", po_number)

        po_in_local = frappe.db.sql("""Select name from `tabPurchase Order` where name = %s""", (po_number))

        if len(po_in_local) > 0:
            print "IN LOCAL"
            is_local = verify_if_local(po_number, po_doc['supplier'],po_doc['supplier_discount'],po_doc['transaction_date'])
            print "IS LOCAL?",is_local
            if is_local == True:
                return po_number
            else:
                po_name_new = str(po_number) + "-copy"
        else:
            po_name_new = str(po_number)

        count_copy = frappe.db.sql("""Select name from `tabPurchase Order` where name = %s""", (po_name_new))
        print "Check if local exists"
        if len(count_copy) > 0:
            print "-copy exists. Please return number. No need to insert doc."
            return po_name_new

        po_doc['docstatus'] = 0
        po_doc['status'] = 'Draft'
        po_doc['amended_from'] = None
        # print po_doc['docstatus'], po_doc['status']
        for item in po_doc['items']:
            item['docstatus'] = 0
            # print po_doc
        try:
            po_doc['memo'] += " [from " + po_number + "]"
        except:
            po_doc['memo'] = " [from " + po_number + "]"

        po_doc = frappe.get_doc(po_doc)
        po_doc.insert()
        po_doc.submit()

        frappe.db.sql("""UPDATE `tabPurchase Order Item` set parent = %s where parent = %s""", (po_name_new, po_doc.name))
        frappe.db.sql("""UPDATE `tabPurchase Order` set name = %s where name = %s""", (po_name_new, po_doc.name))

        frappe.db.commit()
        print po_name_new
        return po_name_new

    except:
        frappe.msgprint(
            "PO Not found. Please try entering the COMPLETE PO number including the branch code (eg. PO-MALL-1802010000")  # print po_doc['items']
        # json_format = json.loads(po_doc)
        # return po_doc

def verify_if_local(po_number, supplier, supplier_discount, transaction_date):
    local_doc = frappe.get_doc("Purchase Order", po_number)
    print "SUPPLIER", local_doc.supplier, supplier, (local_doc.supplier==supplier)
    print "SUPPLIER DISCOUNT", local_doc.supplier_discount, supplier_discount, (local_doc.supplier_discount==supplier_discount)
    print "TRANSACTION DATE", local_doc.transaction_date, transaction_date, (local_doc.transaction_date==transaction_date)
    if (str(local_doc.supplier) == str(supplier)) and (str(local_doc.supplier_discount) == str(supplier_discount)) and (str(local_doc.transaction_date) == str(transaction_date)):
        return True
    else:
        return False


@frappe.whitelist()
def get_usr_branch():
    try:
        frappe_user = frappe.get_user()
        print "==============312u4129380471298374=============="
        print frappe_user.name
        user = frappe.get_doc("User", frappe_user.name)
        print user.branch

    except:
        frappe.throw("Please Add your branch your account settings.")
    else:
        if (user.branch !=None) and (user.branch != ""):
            return user.branch
        else:
            frappe.throw("Please Add your branch your account settings.")

def get_supplier_discount_values(supplier_discount):
    disc = frappe.get_doc("Supplier Discounts",supplier_discount)
    return disc.disc_1, disc.disc_2, disc.disc_3, disc.disc_4

def get_po_discount(po):
    po = frappe.get_doc("Purchase Order",po)
    return po.discount_1, po.discount_2, po.discount_3, po.discount_4, po.additional_supplier_discount

@frappe.whitelist()
def get_stt_from_main(stt):
    branch = frappe.db.get_value("Server Information", None, 'branch')
    items = {}
    try:
        # tailerp_api = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
        tailerp_api = connect_to_server()
        se_doc = tailerp_api.get_doc("Stock Entry", stt)
    except:
        frappe.throw("""STT DOES NOT EXIST.""")
    else:
        total_case = 0
        se_document = frappe.get_doc(se_doc)
        to_warehouse = frappe.get_doc("Warehouse", se_document.to_warehouse)
        print se_document.to_warehouse, to_warehouse.branch
        if branch != to_warehouse.branch:
            frappe.throw("DO NOT RECEIVE STT. STT is for " + se_document.to_warehouse + " .")
        else:
            item_list = []
            memo = (se_document.memo + " [from " + se_document.name + "]") if se_document.memo is not None else (
            " [from " + se_document.name + "]")
            for item in se_document.items:
                total_case+= item.qty
                item_list.append({"item_code": item.item_code, "actual_qty": item.qty, "uom": item.uom,
                                  "conv_factor": item.conversion_factor, "qty_dummy": item.qty_dummy,
                                  "item_description": item.item_description, "cost": item.price, "amount": item.amount,
                                  "item_name": item.item_name, "packing": item.packing, "barcode":item.barcode_retial
                                  })
                # print item.item_code, item.qty, item.uom, item.conversion_factor, item.qty_dummy, item.amount
            return {"items": item_list, "memo": memo, "warehouse": se_document.to_warehouse,
                    "supplier":se_document.for_supplier, "source_branch":se_document.from_warehouse, "total_case":total_case}




@frappe.whitelist()
def get_po_items_from_main(po):
    branch = frappe.db.get_value("Server Information", None, 'branch')
    items = {}
    try:
        # tailerp_api = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
        tailerp_api = connect_to_server()
        po_doc = tailerp_api.get_doc("Purchase Order", po)
    except:
        frappe.throw("""PO DOES NOT EXIST.""")
    else:
        total_case = 0
        po_document = frappe.get_doc(po_doc)
        to_warehouse = frappe.get_doc("Warehouse", po_document.delivered_to_warehouse)
        print po_document.delivered_to_warehouse, to_warehouse.branch
        if branch != to_warehouse.branch:
            frappe.msgprint("PO is WRONG SERVED and should be sent to: " + po_document.delivered_to_warehouse + " .")
        item_list = []
        memo = (po_document.memo + " [from " + po_document.name + "]") if po_document.memo is not None else (
        " [from " + po_document.name + "]")
        for item in po_document.items:
            total_case += item.qty
            item_list.append({"item_code": item.item_code, "qty": item.qty, "uom": item.uom,
                              "conv_factor": item.conversion_factor, "item_description": item.item_description,
                              "cost": item.rate, "amount": item.amount, "non_vat": item.non_vat, "net_cost":item.net_cost,
                              "item_name": item.item_name, "packing": item.packing, "barcode": item.barcode_retial,
                              "net_cost_amount":item.net_cost_amount
                              })
            # print item.item_code, item.qty, item.uom, item.conversion_factor, item.qty_dummy, item.amount
        return {"items": item_list, "memo": memo, "warehouse": po_document.delivered_to_warehouse,
                "supplier_discount":po_document.supplier_discount,"supplier_discount_name":po_document.supplier_discount_name,
                "discount_1":po_document.discount_1,"discount_2":po_document.discount_2,
                "discount_3":po_document.discount_3,"discount_4":po_document.discount_4,
                "additional_supplier_discount":po_document.additional_supplier_discount,
                "supplier": po_document.supplier, "source_branch": po_document.branch, "total_case": total_case}


def get_service_level_from_branch_po(branch_po, rr_doc):
    try:
        # tailerp_api = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
        tailerp_api = connect_to_server()
        po_doc = tailerp_api.get_doc("Purchase Order", branch_po)
    except:
        frappe.throw("""Please check your internet connection and if PO number is correct.""")
    else:
        po_document = frappe.get_doc(po_doc)

        peso_value = 0.00
        qty_based = 0.00

        temp_html = '<h6 style = "{margin-left:100px;}">'
        #linked_po = frappe.get_doc("Purchase Order", str(doc.purchase_order))
        po_grand_total = po_document.grand_total
        pr_grand_total = rr_doc.grand_total

        peso_value = pr_grand_total / po_grand_total * 100 if po_grand_total > 0 else None

        po_items = get_items_total_qty(po_document.items)
        pr_items = get_items_total_qty(rr_doc.items)

        qty_based = pr_items / po_items * 100

        print peso_value
        print qty_based
        # temp_html += "PESO VALUE: {:20,.2f} %".format(peso_value)
        # temp_html += "<br><span>QTY: {:20,.2f} %</h6>".format(qty_based)
        rr_doc.service_level = peso_value
        rr_doc.service_level_qty = qty_based

#SLE LOGGING FEATURE: insert and delete on submit/cancel

def create_sle_log(doc):
    from erpnext.stock.stock_ledger import get_previous_sle
    voucher_no = doc.name
    posting_date = doc.posting_date
    posting_time = doc.posting_time
    warehouse = doc.delivered_to_warehouse
    is_match = 0

    print("#################CREATE SLE LOG##################")


    for item in doc.items:
        actual_qty = item.stock_qty
        last_entry = get_previous_sle({
            "item_code": item.item_code,
            "warehouse": warehouse,
            "posting_date": posting_date,
            "posting_time": posting_time - datetime.timedelta(seconds=1)})
        try:
            qty_after_transaction = float(last_entry.qty_after_transaction) + float(actual_qty)
        except:
            qty_after_transaction = actual_qty
        else:
            qty_after_transaction = float(last_entry.qty_after_transaction) + float(actual_qty)

        #check_match
        match = find_sle(voucher_no, item.item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time)
        if match >0:
            is_match=1
        print(voucher_no, item.item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match)
        frappe.db.sql("""insert into slelogging(voucher_no, item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match)
                        values(%s,%s,%s,%s,%s,%s,%s,%s)""",(voucher_no, item.item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match))
        frappe.db.commit()

def find_sle(voucher_no, item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time):
    count_sle = frappe.db.sql("""select count(*) from `tabStock Ledger Entry` where item_code = %s and warehouse = %s
                                and actual_qty = %s and qty_after_transaction = %s and voucher_no =%s
                                and posting_date =%s and posting_time = %s""",(item_code, warehouse, actual_qty, qty_after_transaction,voucher_no,posting_date, posting_time))
    print count_sle[0][0]
    return count_sle[0][0] if count_sle[0][0] is not None else 0

def delete_sle_log(doc,method):
    frappe.db.sql("""DELETE from slelogging where voucher_no = %s""",doc.name)
    frappe.db.commit()
import frappe
import datetime

def purge_pi(year):
    month = 1

    print get_total_pi(year), "===Before Purge"

    while month <=11:
        from_date = str(datetime.date(year, month, 1))
        to_date = str(datetime.date(year, month+1, 1))
        #break
        #print(from_date," To ", to_date)
        total_per_barcode_per_month(from_date, to_date)
        delete_pi_records(from_date, to_date)
        month+=1

    from_date = str(datetime.date(year, month, 1))
    to_date = str(datetime.date(year+1, 1, 1))

    #print(from_date, " To ", to_date)
    total_per_barcode_per_month(from_date, to_date)
    delete_pi_records(from_date, to_date)

    print get_total_pi(year), "===After Purge"


def get_total_pi(year):
    from_date = str(datetime.date(year, 1, 1))
    to_date = str(datetime.date(year+1, 1, 1))
    total = frappe.db.sql("""SELECT sum(amount) from `tabUpload POS` where trans_date >=%s and trans_date <%s""",(
        from_date, to_date
    ))
    return total[0][0]

def total_per_barcode_per_month(from_date,to_date):

    upload_date = str(datetime.date.today())
    branch = frappe.db.get_value("Server Information", None, 'branch')
    date_split = from_date.split('-')
    filename = str(date_split[0])+"-"+str(date_split[1])+"_TOTAL"
    #print filename
    #print(branch)
    totals = frappe.db.sql("""SELECT barcode, SUM(qty) as qty, SUM(amount) as amount, AVG(cost) as cost, AVG(price) as price
                            from `tabUpload POS` where trans_date >=%s and trans_date <%s and branch = %s group by barcode""",
                            (from_date,to_date, branch), as_dict = True)

    create_pi_table()

    for total in totals:

        #new_doc = frappe.get_doc(total)
        #new_doc.insert(ignore_permissions = True)
        #print total['barcode'], total['qty'], total['amount']
        frappe.db.sql("""UPDATE temp_pi set trans_date = %s, my_suki = 0, filename = %s, branch = %s, upload_date = %s,
                        owner = %s, modified_by = %s, creation = %s, modified = %s, name = %s, naming_series=%s,
                        barcode = %s, qty = %s, amount = %s, cost = %s, price = %s""",(
            from_date, filename, branch, upload_date, "Administrator", "Administrator", str(datetime.datetime.now()),
            str(datetime.datetime.now()), filename+'-'+branch+'-'+str(total['barcode']), filename+'-'+branch+'-'+str(total['barcode']),
            total['barcode'], total['qty'], total['amount'], total['cost'], total['price']
        ))
        frappe.db.sql("""INSERT INTO `tabUpload POS` (SELECT * from temp_pi)""")

def create_pi_table():
    try:
        frappe.db.sql("""CREATE TABLE temp_pi (SELECT * from `tabUpload POS` limit 1)""")
    except:
        pass

def delete_pi_records(from_date, to_date):

    frappe.db.sql("""delete from `tabUpload POS` where trans_date >=%s and trans_date < %s and name not like %s """,(from_date, to_date,'%TOTAL%'))
    frappe.db.commit()
import frappe, datetime
from dateutil.relativedelta import relativedelta

#calculate offtake per item. if date is not specified, get today's date
def calculate_offtake(today=None):
    items = frappe.db.sql("""SELECT name, barcode_retial, packing from `tabItem` where type!='Disabled' and type!='Phased Out'""")

    if today is None:
        today = datetime.datetime.now()

    else:
        temp_date = datetime.datetime.strptime(today,"%Y-%m-%d")
        today = temp_date

    year = today.year
    month = today.month

    today = datetime.datetime(year, month, 1)
    from_date= today - relativedelta(months=3)
    to_date = today - relativedelta(days=1)

    branch = frappe.db.get_value("Server Information", None, "branch")

    print from_date, to_date

    for item in items:
        item_code = item[0]
        barcode = item[1]
        packing = item[2]
        pos_offtake = get_pos_offtake(barcode,packing,from_date, to_date,branch)
        ws_offtake = get_wholesale_offtake(item_code,from_date, to_date, branch)
        stt_offtake = get_stt_offtake(item_code, from_date, to_date, branch)
        case_offtake = get_case_offtake(item_code, from_date, to_date, branch)
        inner_offtake = get_inner_box_offtake(item_code, from_date, to_date, branch)

        sales_offtake = pos_offtake+ws_offtake+stt_offtake+case_offtake+inner_offtake

        wd_offtake = get_withdrawals_offtake(item_code, from_date, to_date, branch)

        #if pos_offtake>0:
        #print item_code, sales_offtake, wd_offtake
        if check_bin_exists(item_code):
            frappe.db.sql("""UPDATE `tabOfftake Bin` set pos_offtake = %s, withdrawals_offtake = %s where item_code = %s""",(
                sales_offtake, wd_offtake, item_code
            ))
        else:
            #print "CREATE BIN"
            create_offtake_bin(item_code, sales_offtake, wd_offtake)

#GET OFFTAKE from POS (in Pcs) and convert to CASE
def get_pos_offtake(barcode, packing, from_date, to_date, branch):
    s1 = frappe.db.sql("""Select sum(qty), uom from `tabUpload POS` where trans_date >= %s and 
                          trans_date <= %s and qty !=0 and barcode = %s and branch = %s""",
                       (from_date, to_date, barcode, branch))

    return (float(s1[0][0])/float(packing)) if s1[0][0] is not None else 0

# GET OFFTAKE from WHOLESALE (in UOM) and convert to CASE
def get_wholesale_offtake(item, from_date, to_date, branch):
    s2 = frappe.db.sql("""SELECT sum(`tabStock Entry Detail`.transfer_qty) from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                and `tabStock Entry Detail`.item_code = %s
                                AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Wholesale' AND `tabStock Entry`.docstatus = 1""",
                       (from_date, to_date, item, branch))

    return float(s2[0][0]) if s2[0][0] is not None else 0


# GET OFFTAKE from STTs (in UOM) and convert to CASE
def get_stt_offtake(item, from_date, to_date, branch):
    s3 = frappe.db.sql("""SELECT count(`tabStock Entry Detail`.qty), sum(`tabStock Entry Detail`.qty), `tabStock Entry Detail`.stock_uom, `tabStock Entry Detail`.uom   from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                and `tabStock Entry Detail`.item_code = %s
                                AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type ='Stock Transfer' AND
                                `tabStock Entry`.docstatus = 1 AND `tabStock Entry`.change_cost_for_wet_market = %s""",
                       (from_date, to_date, item, branch, "STOCK TRANSFER TO OTHER DEPARTMENTS (USE SRP)"))

    return s3[0][0] if s3[0][0] is not None else 0


#GET CASE BARCODE OFFTAKE from POS
def get_case_offtake(item, from_date, to_date, branch):
    item_doc = frappe.get_doc("Item", item)
    if item_doc.barcode_retial == item_doc.barcode_pack:
        return 0
    else:
        offtake = frappe.db.sql("""Select sum(qty) from `tabUpload POS` where barcode = %s and
    								trans_date >= %s and trans_date <= %s and branch = %s""",
                          (item_doc.barcode_pack, from_date, to_date, branch))

        qty_sold = offtake[0][0] if offtake[0][0]!= None else 0
        return qty_sold


#GET INNER BOX BARCODE OFFTAKE from POS and Convert to CASE; Use Packing 2
def get_inner_box_offtake(item, from_date, to_date, branch):
    offtake_in_cases = 0

    rows = frappe.db.sql("""select bar_code_2, packing_2 from `tabItem` where name = %s""", item)

    if len(rows)>0:
        barcode_2 = rows[0][0]
        packing_2 = rows[0][1]
        sales = get_pos_offtake(barcode_2, packing_2, from_date, to_date, branch)

    return sales

#GET OFFTAKE from WITHDRAWALS. Convert if needed
def get_withdrawals_offtake(item, from_date,to_date, branch):
    withdrawals_temp = frappe.db.sql("""SELECT sum(`tabStock Entry Detail`.transfer_qty) from `tabStock Entry Detail` LEFT JOIN `tabStock Entry`
                                    ON `tabStock Entry Detail`.parent = `tabStock Entry`.name WHERE `tabStock Entry`.posting_date >= %s
                                    and `tabStock Entry`.posting_date <= %s and `tabStock Entry Detail`.qty >0
                                    and `tabStock Entry Detail`.item_code = %s
                                    AND `tabStock Entry`.branch = %s AND `tabStock Entry`.type = 'Withdrawals' AND `tabStock Entry`.docstatus = 1""",
                                     (from_date, to_date, item, branch))

    return withdrawals_temp[0][0] if withdrawals_temp[0][0] is not None else 0



####################
##TEST ITEM FOR INNER BOX: ITEM-102025
####################

def check_bin_exists(item):
    is_in_db = frappe.db.sql("""SELECT count(*) from `tabOfftake Bin` where item_code = %s""", item)

    if len(is_in_db)>0:
        return True if is_in_db[0][0]>0 else False
    else:
        return False

def create_offtake_bin(item, pos_offtake, wd_offtake):
    offtake_bin = {
        "doctype" : "Offtake Bin",
        "item_code": item,
        "pos_offtake": pos_offtake,
        "withdrawals_offtake": wd_offtake,
        "name": "OFFTAKE-"+item
    }
    bin_doc = frappe.get_doc(offtake_bin)
    bin_doc.db_insert()
    frappe.db.commit()

def get_offtake_from_bin(item_code):
    data = frappe.db.sql("""SELECT pos_offtake, withdrawals_offtake from `tabOfftake Bin` where item_code = %s""", item_code)
    if len(data)>0:
        return data[0][0], data[0][1]
    else:
        return 0,0
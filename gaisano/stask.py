import csv
import os
import datetime
import dateutil.parser
import frappe
import thread
import threading


def get_sales():
    path = frappe.db.sql("""Select path, branch, my_suki from `tabBranch Folder Path Item`""")
    print "+++++ Getting Branch Path +++++++++++"
    for pt in path:
        branch_path = str(pt[0])
        br_name = pt[1]
        my_suki = pt[2]
        print branch_path, br_name, my_suki

        allfiles = os.listdir(branch_path)
        for files in allfiles:
            print files
            os.chdir(branch_path)
            to_upload_file = os.path.basename(files)
            if files.endswith(".txt"):
                if os.path.isfile(to_upload_file):
                    file_name = os.path.splitext(to_upload_file)[0]
                    os.rename(to_upload_file, (file_name + ".Process")) #uncomment after testing
                    if my_suki == 1:
                        # if mysuki, use a different method.
                        print file_name, "My Suki"
                        read_mysuki_sales(file_name, br_name)
                        os.rename(file_name + ".Process", (file_name + ".End")) #uncomment after testing

            if files.endswith(".TXT"):
                if os.path.isfile(to_upload_file):
                    file_name = os.path.splitext(to_upload_file)[0]
                    print file_name
                    # rename file name to .Process
                    os.rename(to_upload_file, (file_name + ".Process"))
                    # open file and insert
                    open_file(file_name, br_name)
                    # rename to .End
                    os.rename(file_name + ".Process", (file_name + ".End"))

def read_mysuki_sales(file_name, br_name):
    now = datetime.datetime.now()
    #with open(file_name + ".txt", 'rb') as csvfile:
    with open(file_name + ".Process", 'rb') as csvfile:
        sales_reader = csv.reader(csvfile, delimiter='\t', quotechar='"')
        current = 0
        for i,row in enumerate(sales_reader):
            #print current
            if i == 0:
                continue
            current+=1
            #['Gaisano City Mall', '02/05/2021', '9556111408088', '1', '496.85', '496.85', 'A']
            trans_date = datetime.datetime.strptime(row[1],"%m/%d/%Y")
            barcode = row[2]
            if len(str(barcode)) < 13 and (barcode!="delivery"):
                print barcode, len(barcode), barcode[0]
                if (len(str(barcode)) == 5) and ((barcode[0]=='2') or barcode[0]==2):
                    new_barcode = "0"+str(barcode)+"0000000"
                    if check_item_ladlad(new_barcode):
                        print "ladlad"
                        barcode = new_barcode
                    else:
                        print "not ladlad"
                        leading_zeros = str(barcode).zfill(13)
                        barcode = leading_zeros
                else:
                    print "else not ladlad"
                    leading_zeros = str(barcode).zfill(13)
                    barcode = leading_zeros

                #trailing_zeros = str(barcode).ljust(13,'0')
                #print leading_zeros, trailing_zeros
            qty = row[3]
            srp = row[4]
            amount = row[5]
            item_cost = get_item_cost(barcode)
            #print item_cost
            current_str = str(current).zfill(7)
            temp_name = file_name + "-" + br_name + "-" + current_str
            pos_data = {
                'doctype': 'Upload POS',
                'docstatus': 0,
                'idx': 0,
                'branch': br_name,
                'upload_date': now.strftime("%Y-%m-%d %H:%M"),
                'trans_date': trans_date,
                'filename': file_name,
                'naming_series': temp_name,
                'name': temp_name,
                '`_assign`': '',
                '`_liked_by`': '',
                '`_comments`': '',
                '`_user_tags`': '',
                'barcode': barcode,
                'qty': float(qty),
                'price': float(srp),
                'cost': float(item_cost),
                'amount': amount,
                'my_suki': 1
            }
            try:
                from_pos_data = frappe.get_doc(pos_data)
                from_pos_data.insert()
            except:
                continue
def check_item_ladlad(barcode):

    is_ladlad = frappe.db.sql("""select ladlad from `tabItem` where barcode_retial = %s""",barcode)
    for row in is_ladlad:
        if row[0] == 1:
            return True
        else:
            return False

def get_item_cost(barcode):
    item_cost = frappe.db.sql("""SELECT item_price_retail_1 from `tabItem` where barcode_retial = %s""",barcode)
    itm_cost_final = 0
    for row in item_cost:
        itm_cost_final = row[0]
    return itm_cost_final

def open_file(file_name, br_name):
    now = datetime.datetime.now()
    trans_date = dateutil.parser.parse(file_name[2:], yearfirst=True)

    with open(file_name + ".Process", 'rU') as csvfile:
        sales_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        current = 0
        for row in sales_reader:
            try:
                print current
                current += 1
                pr = str(row[2])
                qty = str(row[1])
                cost = str(row[3])
                current_str = str(current).zfill(7)
                temp_name = file_name +"-"+br_name+"-"+current_str
                print temp_name
            except:
                continue
            else:
                pos_data = {
                    'doctype': 'Upload POS',
                    'docstatus': 0,
                    'idx': 0,
                    'branch': br_name,
                    'upload_date': now.strftime("%Y-%m-%d %H:%M"),
                    'trans_date': trans_date,
                    'filename': file_name,
                    'naming_series':temp_name,
                    'name':temp_name,
                    '`_assign`': '',
                    '`_liked_by`': '',
                    '`_comments`': '',
                    '`_user_tags`': '',
                    'barcode': row[0],
                    'qty': float(qty),
                    'price': float(pr),
                    'cost': float(cost),
                    'amount': (float(qty) * float(pr))
                }
                try:
                    from_pos_data = frappe.get_doc(pos_data)
                    from_pos_data.insert()
                except:
                    continue

def create_sales_from_pos():
    # find "Requesting" status to create SO
    create_so_task = frappe.db.sql("""Select name from `tabCreate SO` where status='Requesting' limit 1""")

    if create_so_task:
        print "*** Start ***"
        data = create_so_task[0]
        doc_name = str(data[0])
        doc = frappe.get_doc("Create SO", doc_name)

        frappe.db.sql("""Update `tabCreate SO` set status = 'Processing' where name = %s""", (doc.name))
        frappe.db.commit()
        # doc.db_update()
        date = str(doc.date)
        print "Date: " + date

        print "Update POS Date"
        update_pos_data(date)

        print "Get Branches"

        branch = frappe.db.sql("""Select name from `tabBranch`""")

        for br in branch:

            branch_name = br[0]
            print "Get Suppliers"
            sup_array = []
            sup_array = get_supplier(date, branch_name)
            print sup_array

            for sup in sup_array:
                print "Create SO"
                so = create_so_from_pos(date, sup, branch_name)
                print "Create DR"
                create_dr_from_pos(so)

                # sup = 'Phil. Spring Water'
                # print "Create SO"
                # so = create_so_from_pos(date, sup)
                # print "Create DR"
                # create_dr_from_pos(so)

        doc.status = "Finish"
        doc.db_update()

        print "*** Finish ***"


def update_pos_data(date):
    pos_data = frappe.db.sql("""Select trans_date, barcode, qty, price, amount, barcode_memo, uom_memo
                      from `tabUpload POS` where trans_date = %s""", (date))

    # set barcode_memo to 'Item Found' and 'Item Not Found'
    for pos_itm in pos_data:
        barcode = str(pos_itm[1])
        data_date = str(pos_itm[0])
        itm_exist = frappe.db.sql("""Select item_code from tabItem where barcode_retial = %s""", (barcode))
        if not itm_exist:
            frappe.db.sql("""Update `tabUpload POS` set
                        barcode_memo = 'Item Not Found' where barcode = %s and trans_date = %s""",
                          (barcode, data_date))
            frappe.db.commit()
        else:
            frappe.db.sql("""Update `tabUpload POS` set
                                    barcode_memo = 'Item Found' where barcode = %s and trans_date = %s""",
                          (barcode, data_date))
            frappe.db.commit()
            for itm_code in itm_exist:
                code = str(itm_code[0])

                supplier = frappe.db.sql("""Select supplier from `tabItem Supplier`
                              where parent = %s""", (code))

                factor = frappe.db.sql("""select max(conversion_factor) as conversion from `tabUOM Conversion Detail`
                              where parent = %s group by parent""", (code))

                # set Item Supplier
                for sup in supplier:
                    # print str(sup[0])
                    itm_supplier = str(sup[0])

                    # set UOM and Conversion Factor for Smallest Conversion
                    for conversion in factor:
                        conv = str(conversion[0])

                        uom = frappe.db.sql("""select uom from `tabUOM Conversion Detail`
                                                      where parent = %s and conversion_factor = %s""",
                                            (code, conv))

                        for unit in uom:
                            itm_uom = str(unit[0])

                            if not itm_code:
                                frappe.db.sql("""Update `tabUpload POS` set
                                            supplier = 'Not Found' where barcode = %s and trans_date = %s""",
                                              (barcode, data_date))
                                frappe.db.commit()
                            else:
                                frappe.db.sql("""Update `tabUpload POS` set
                                            supplier = %s where barcode = %s and trans_date = %s""",
                                              (itm_supplier, barcode, data_date))
                                frappe.db.commit()

                                frappe.db.sql("""Update `tabUpload POS` set
                                            factor = %s where barcode = %s and trans_date = %s""",
                                              (conv, barcode, data_date))
                                frappe.db.commit()

                                frappe.db.sql("""Update `tabUpload POS` set
                                            uom = %s where barcode = %s and trans_date = %s""",
                                              (itm_uom, barcode, data_date))
                                frappe.db.commit()


def get_supplier(date, branch_name):
    supplier = frappe.db.sql("""Select DISTINCT supplier from `tabUpload POS` where trans_date = %s and branch = %s""",
                             (date, branch_name))

    sup_array = []

    for s in supplier:
        sup = str(s[0])

        sup_array.append(sup)
        print sup
    return sup_array


def create_so_from_pos(date, sup, branch_name):
    # Select Specific Supplier
    # 'Nestle Phils., Inc' 'Phil. Spring Water'
    # Create SO for Status "Processing"
    now = datetime.datetime.now()

    print sup

    pos_new_data = frappe.db.sql("""Select trans_date, barcode, qty, price, amount, uom, factor, supplier, barcode_memo, cost
                                                                      from `tabUpload POS` where trans_date = %s and
                                                                      barcode_memo = 'Item Found' and so_number is null
                                                                      and supplier = %s and branch = %s
                                                                      """, (date, sup, branch_name))

    warehouse = frappe.db.sql("""Select name from `tabWarehouse` where is_display = 1 and branch = %s limit 1""",
                              branch_name)
    disp_warehouse = warehouse[0][0]
    so_header = {
        'doctype': 'Sales Order',
        'docstatus': 0,
        'parent': '',
        'parentfield': '',
        'parenttype': '',
        'idx': 0,
        'branch': branch_name,
        'selling_price_list': 'Standard Selling',
        'customer_name': 'Walk-in Customer',
        'customer': 'Walk-in Customer',
        'order_type': 'Sales',
        'price_list_currency': 'PHP',
        'company': 'Gaisano Grocery',
        'customer_group': 'Individual',
        'delivery_date': date,
        'transaction_date': date
    }
    to_so_header = frappe.get_doc(so_header)

    for pos in pos_new_data:
        pos_date = str(pos[0])
        pos_barcode = str(pos[1])
        pos_qty = str(pos[2])
        pos_price = str(pos[3])
        pos_amt = str(pos[4])
        pos_uom = str(pos[5])
        pos_factor = str(pos[6])
        pos_supplier = str(pos[7])
        pos_cost = str(pos[9])
        try:
            so_qty = float(pos_qty) / float(pos_factor)
        except:
            None

        frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s and retail_rate = %s where price_list = 'Standard Selling' AND branch = 'CDO Main'
                                          AND item_code = (Select item_code from `tabItem` where barcode_retial = %s)""",
                      (pos_amt, pos_price, pos_barcode))
        frappe.db.commit()
        print "++++++++++++++++Update Prices!++++++++++++"

        item_det = frappe.db.sql(
            """select item_name, item_code, item_group, item_cost, item_price from tabItem where barcode_retial = %s"""
            , (pos_barcode))

        for n in item_det:
            det_name = str(n[0])
            det_code = str(n[1])
            det_grp = str(n[2])
            det_cost = str(n[3])
            det_price = str(n[4])
            gross = float(det_cost) * -1
            try:
                factor = float(det_cost) / float(pos_cost)
                qty = float(pos_qty) / float(factor)
            except:
                qty = 0

            so_detail = {
                'doctype': 'Sales Order Item',
                'docstatus': 0,
                'parentfield': 'items',
                'parenttype': 'Sales Order',
                'parent': to_so_header.name,
                'selling_price_list': 'Standard Selling',
                'transaction_date': to_so_header.transaction_date,
                'price_list_currency': 'PHP',
                'base_net_total': round(float(pos_amt), 2),
                'total': round(float(pos_amt), 2),
                'base_total': round(float(pos_amt), 2),
                'company': 'Gaisano Grocery',
                'customer_group': 'Individual',
                'item_name': det_name,
                # 'target_warehouse': 'CDO Main Display Area - GG',
                'warehouse': disp_warehouse,
                'rate': round(float(pos_price), 2),
                'base_amount': round(float(pos_amt), 2),
                'base_net_rate': round(float(pos_price), 2),
                'net_rate': round(float(pos_price), 2),
                'qty': round(float(qty), 2),
                'item_code': det_code,
                'amount': round(float(pos_amt), 2),
                'description': det_code,
                'net_amount': round(float(pos_amt), 2),
                'base_net_amount': round(float(pos_amt), 2),
                'base_rate': round(float(pos_price), 2),
                'price_list_rate': round(float(pos_price), 2),
                'item_group': det_grp,
                'gross_profit': round(float(gross), 2),
                'valuation_rate': round(float(det_cost), 2)
            }
            to_so_header.append("items", so_detail)
    to_so_header.insert()
    print to_so_header.name
    # to_so_header.submit()
    frappe.db.commit()
    print "Created SO"
    # Set So Number to upload POS to tagged as already created So from.
    frappe.db.sql("""Update `tabUpload POS` set so_number = %s where
                              supplier = %s and trans_date = %s and branch = %s""",
                  (to_so_header.name, sup, date, branch_name))
    frappe.db.commit()
    return to_so_header


def create_dr_from_pos(to_so_header):
    # Create DN

    if (to_so_header.get("items")):
        dn_header = {
            'doctype': 'Delivery Note',
            'docstatus': 0,
            'parent': '',
            'parentfield': '',
            'parenttype': '',
            'idx': 0,
            'branch': to_so_header.branch,
            'selling_price_list': 'Standard Selling',
            'customer_name': 'Walk-in Customer',
            'customer': 'Walk-in Customer',
            'order_type': 'Sales',
            'price_list_currency': 'PHP',
            'company': 'Gaisano Grocery',
            'customer_group': 'Individual',
            'posting_date': to_so_header.delivery_date,
            'territory': 'Philippines'
        }
        to_dn_header = frappe.get_doc(dn_header)

        for itm in to_so_header.get("items"):
            if itm.item_code and itm.warehouse:
                actual_qty = frappe.db.sql("""select actual_qty from `tabBin`
            			  where item_code = %s and warehouse = %s""", (itm.item_code, itm.warehouse))
                itm_actual_qty = actual_qty and float(actual_qty[0][0]) or 0
            dn_detail = {
                'doctype': 'Delivery Note Item',
                'docstatus': 0,
                'parentfield': 'items',
                'parenttype': 'Delivery Note',
                'selling_price_list': 'Standard Selling',
                'price_list_currency': 'PHP',
                'company': 'Gaisano Grocery',
                'customer_group': 'Individual',
                'item_name': itm.item_name,
                'stock_uom': itm.stock_uom,
                'warehouse': itm.warehouse,
                'rate': itm.rate,
                'base_amount': itm.base_amount,
                'base_net_rate': itm.base_net_rate,
                'net_rate': itm.net_rate,
                'actual_qty': itm_actual_qty,
                'qty': itm.qty,
                'item_code': itm.item_code,
                'amount': itm.amount,
                'description': itm.description,
                'net_amount': itm.net_amount,
                'base_net_amount': itm.base_net_amount,
                'base_rate': itm.base_rate,
                'price_list_rate': itm.price_list_rate,
                'item_group': itm.item_group,
                'expense_account': 'Cost of Goods Sold - GG',
                'cost_center': 'Main - GG',
                'so_detail': itm.name,
                'against_sales_order': to_so_header.name
            }
            to_dn_header.append("items", dn_detail)
        to_dn_header.insert()
        # to_dn_header.submit()
        frappe.db.commit()
        print to_dn_header.name
        print "created DR"


def submit_so_dn_final():
    import time
    threads = []
    print "Sulod ko sa Task"
    so_drafts = frappe.db.sql(
        """select name from `tabSales Order` where status = 'Draft' and grand_total != 0 ORDER BY NAME ASC limit 5""")
    print "humana ko query sa draft"
    for so_draft in so_drafts:
        print so_draft
        name = str(so_draft[0])
        sup_count = frappe.db.sql("""Select count(supplier) from `tabItem Supplier` where parent =
                              (Select item_code from `tabSales Order Item` where idx = 1 and parent = %s)""", (name))

        print "Queryy kog supplier"
        print sup_count
        if (sup_count[0][0] != 0):

            supplier = frappe.db.sql("""Select supplier from `tabItem Supplier` where parent =
                                          (Select item_code from `tabSales Order Item` where idx = 1 and parent = %s)""",
                                     (name))
            print " IM HEEERRRREEEEE sulod supplier"
            print " Sulod Supplier"
            sup = str(supplier[0][0])
            print sup
            cust = frappe.db.sql("""Select count(*) from `tabCustomer` where name = %s""", (sup))
            count = str(cust[0][0])
            print count
            if (sup):
                if (count == '0'):
                    print " Create Customer "
                    # doc = frappe.db.sql("""Select supplier_name from `tabSupplier` where name = %s""", sup)
                    # sup_name = str(doc[0])
                    new_cust = {
                        'doctype': 'Customer',
                        'name': sup,
                        'customer_name': sup,
                        'territory': 'All Territories',
                        'customer_group': 'Individual',
                        'customer_type': 'Individual'
                    }
                    to_new_cust = frappe.get_doc(new_cust)
                    to_new_cust.insert()
                    frappe.db.commit()

                    t = threading.Thread(target=process_submit_so_dn_final, args=(so_draft[0], to_new_cust.name))
                    threads.append(t)
                else:
                    print " Naa customer "
                    t = threading.Thread(target=process_submit_so_dn_final, args=(so_draft[0], sup))
                    threads.append(t)
            else:
                sup = 'Walk-in Customer'
                t = threading.Thread(target=process_submit_so_dn_final, args=(so_draft[0], sup))
                threads.append(t)
        else:
            sup = 'Walk-in Customer'
            t = threading.Thread(target=process_submit_so_dn_final, args=(so_draft[0], sup))
            threads.append(t)

    for x in threads:
        x.start()
        time.sleep(5)

    for x in threads:
        x.join()


def process_submit_so_dn_final(so_name, sup):
    print "staring thread " + so_name
    frappe.connect('gaisano.tailerp.com', 'e691e8b2d5')
    # frappe.connect('development', '05134426bc')
    # frappe.connect('develop', '418a6bc4deccf0f7')
    print "+++++++++++++++++++Starting submitting Recon+++++++++++++"

    item = frappe.db.sql("""Select item_code, valuation_rate from `tabSales Order Item` where parent = %s""", so_name)

    for itm in item:
        itm_code = str(itm[0][0])

        recon = frappe.db.sql("""Select parent from `tabStock Reconciliation Item` where item_code = %s""", itm_code)

        for r in recon:
            rec_name = str(r[0][0])

            recon_final = frappe.db.sql("""Select name from `tabStock Reconciliation`
                            where docstatus = 0 and posting_date >= '2016-08-01' and posting_date <= '2016-08-02' and name = %s""",
                                        rec_name)

            for rec in recon_final:
                name = str(rec[0][0])
                doc = frappe.get_doc("Stock Reconciliation", name)
                doc.submit()
                frappe.db.commit()

            print "++++++++Finifh Stock Recon Submit" + name


    so = frappe.get_doc("Sales Order", so_name)
    print "++++++++++++++++++++++SUPPLIER" + sup
    print "+++++++++++++SO Customer" + so.customer + so.customer_name
    # frappe.db.sql("""Update `tabSales Order` set customer = %s and customer_name = %s where name = %s""",(sup,sup, so_name))
    so.customer = sup
    so.db_update()
    frappe.db.commit()
    so.customer_name = sup
    so.db_update()
    frappe.db.commit()
    print "+++++++ New SO Customer" + so.customer + so.customer_name
    so.submit()
    frappe.db.commit()
    print "++++++ SO Submitted+++++"
    print so.status

    dn_draft = frappe.db.sql("""Select parent from `tabDelivery Note Item` where against_sales_order = %s limit 1""",
                             (so_name))
    if (dn_draft):
        dn_name = str(dn_draft[0][0])
        dn = frappe.get_doc("Delivery Note", dn_name)
        dn.customer = sup
        dn.db_update()
        frappe.db.commit()
        dn.customer_name = sup
        dn.db_update()
        frappe.db.commit()
        dn.submit()
        frappe.db.commit()
        print "++++++ DN Submitted+++++"
        print dn.status
    print "ending thread " + so_name


# def submit_stock_recon(so_name):
#     print "+++++++++++++++++++Starting submitting Recon+++++++++++++"
#
#     item = frappe.db.sql("""Select item_code, valuation_rate from `tabSales Order Item` where parent = %s""", so_name)
#
#     for itm in item:
#         itm_code = str(itm[0][0])
#
#         recon = frappe.db.sql("""Select parent from `tabStock Reconciliation Item` where item_code = %s""", itm_code)
#
#         for r in recon:
#             rec_name = str(r[0][0])
#
#             recon_final = frappe.db.sql("""Select name from `tabStock Reconciliation`
#                         where docstatus = 0 and posting_date >= '2016-08-01' and posting_date <= '2016-08-02' and name = %s""",
#                                         rec_name)
#
#             for rec in recon_final:
#                 name = str(rec[0][0])
#                 doc = frappe.get_doc("Stock Reconciliation", name)
#                 doc.submit()
#                 frappe.db.commit()
#
#             print "++++++++Finifh Stock Recon Submit" + name


def create_stock_recon():
    print "+++++++++++++++++++Starting creating Recon"
    item = frappe.db.sql("""Select name, item_cost from `tabItem` where disabled = 0 and item_code
            NOT IN (Select distinct(item_code) from `tabStock Reconciliation Item`)""")
    for itm in item:
        itm_code = str(itm[0])
        itm_cost = str(itm[1])

        recon_header = {
            'doctype': 'Stock Reconciliation',
            'posting_date': '2016-08-01',
            'expense_account': 'Temporary Opening - GG',
            'cost_center': 'Main - GG',
            'company': 'Gaisano Grocery'
        }
        to_recon_header = frappe.get_doc(recon_header)
        warehouse = frappe.db.sql("""Select name from `tabWarehouse` where is_group = 0""")
        for wr in warehouse:
            wr_name = wr[0]
            recon_detail = {
                'doctype': 'Stock Reconciliation Item',
                'parenttype': 'Stock Reconciliation',
                'parentfield': 'items',
                'warehouse': wr_name,
                'item_code': itm_code,
                'valuation_rate': itm_cost,
                'qty': 1,
                'amount': itm_cost,
                # 'parent': to_recon_header.name
            }
            to_recon_header.append("items", recon_detail)
        to_recon_header.insert()
        frappe.db.commit()
        # to_recon_header.submit()
        # frappe.db.commit()

        recon_header_rev = {
            'doctype': 'Stock Reconciliation',
            'posting_date': '2016-08-02',
            'expense_account': 'Temporary Opening - GG',
            'cost_center': 'Main - GG',
            'company': 'Gaisano Grocery'
        }
        to_recon_header_rev = frappe.get_doc(recon_header_rev)
        ware = frappe.db.sql("""Select name from `tabWarehouse` where is_group = 0""")
        for w in ware:
            w_name = w[0]
            recon_detail_rev = {
                'doctype': 'Stock Reconciliation Item',
                'parenttype': 'Stock Reconciliation',
                'parentfield': 'items',
                'warehouse': w_name,
                'item_code': itm_code,
                'valuation_rate': itm_cost,
                'qty': 0,
                'amount': 0,
                # 'parent': to_recon_header.name
            }
            to_recon_header_rev.append("items", recon_detail_rev)
        to_recon_header_rev.insert()
        frappe.db.commit()
        # to_recon_header_rev.submit()
        # frappe.db.commit()

        print "+++++++++++++++++++Finished Item:" + itm_code
#     so_draft = frappe.db.sql("""select name from `tabSales Order` where status = 'Draft' ORDER BY NAME ASC limit 1 """)
#
#     if (so_draft):
#         so_name = str(so_draft[0][0])
#         so = frappe.get_doc("Sales Order", so_name)
#         so.submit()
#         frappe.db.commit()
#         print "++++++++++++++++++++" + so_name + "+++++++++++++"
#
#     dn_draft = frappe.db.sql("""Select parent from `tabDelivery Note Item` where against_sales_order = %s limit 1""",
#                              (so_name))
#
#     if (dn_draft):
#         dn_name = str(dn_draft[0][0])
#         dn = frappe.get_doc("Delivery Note", dn_name)
#         dn.submit()
#         frappe.db.commit()
#         print "++++++++++++++++++++" + dn_name + "+++++++++++++"
#
#
# def submit_so_dn2():
#     so_draft = frappe.db.sql("""select name from `tabSales Order` where status = 'Draft' ORDER BY NAME DESC limit 1 """)
#
#     if (so_draft):
#         so_name = str(so_draft[0][0])
#         so = frappe.get_doc("Sales Order", so_name)
#         so.submit()
#         frappe.db.commit()
#         print "++++++++++++++++++++" + so_name + "+++++++++++++"
#
#     dn_draft = frappe.db.sq l("""Select parent from `tabDelivery Note Item` where against_sales_order = %s limit 1""",
#                              (so_name))
#
#     if (dn_draft):
#         dn_name = str(dn_draft[0][0])
#         dn = frappe.get_doc("Delivery Note", dn_name)
#         dn.submit()
#         frappe.db.commit()
#         print "++++++++++++++++++++" + dn_name + "+++++++++++++"

# def get_balance():
#     warehouse = 'CDO Main Display Area - GG'
#     item_code = 'NATURES SPRING MINERAL WATER 500ML'
#     from_date = '2016-08-01'
#     to_date = '2017-01-12'
#
#     recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
#             where warehouse = %s and
#             item_code = %s and
#             posting_date >= %s and
#             posting_date <= %s and
#             voucher_type = 'Stock Reconciliation' ORDER BY posting_date DESC limit 1""",
#                           (warehouse, item_code, from_date, to_date))
#
#     for r in recon:
#         date = str(r[0])
#         qty = str(r[1])
#
#         rem_total = frappe.db.sql("""Select sum(actual_qty) from `tabStock Ledger Entry`
#             where warehouse = %s and
#             item_code = %s and
#             posting_date >= %s and
#             posting_date <= %s and
#             voucher_type != 'Stock Reconciliation'""", (warehouse, item_code, date, to_date))
#
#         rem_sum = str(rem_total[0][0])
#
#         pos_total = frappe.db.sql("""Select sum(qty * cost) from `tabUpload POS`
#             where
#             branch = (Select branch from `tabWarehouse` where name = %s) and
#             barcode = (Select barcode_retial from `tabItem` where item_code = %s) and
#             trans_date >= %s and
#             trans_date <= %s""", (warehouse, item_code, date, to_date))
#
#         pos_sum = str(pos_total[0][0])
#
#         itm_factor = frappe.db.sql("""Select item_cost from `tabItem` where item_code = %s""", (item_code))
#
#         factor = str(itm_factor[0][0])
#
#
#         balance = float(qty) + float(rem_sum) + ((float(pos_sum)/float(factor)) * -1)
#
#         return balance

def cancel_dn():

    import time
    threads = []
    print "Sulod ko sa cancel_dn"
    dn = frappe.db.sql("""Select name from `tabDelivery Note` where docstatus = 1 order by name DESC limit 5""")
    for d in dn:
        dn_name = str(d[0])
        print "loop DN"
        t = threading.Thread(target=cancel_process, args=dn_name)
        threads.append(t)
        print t

    for x in threads:
        x.start()
        time.sleep(5)

    for x in threads:
        x.join()

def cancel_process(dn_name):
    # frappe.connect('gaisano.tailerp.com', 'e691e8b2d5')
    frappe.connect('development', '05134426bc')
    # frappe.connect('develop', '418a6bc4deccf0f7')
    doc = frappe.get_doc("Delivery Note", dn_name)
    try:
        doc.cancel()
        frappe.db.commit()
    except:
        print doc.name + "skipped"
    print "canceled DN: " + doc.name


def cancel_delivery():
    dn = frappe.db.sql("""Select name from `tabDelivery Note` where docstatus = 1 order by name DESC""")
    for d in dn:
        dn_name = str(d[0])
        doc = frappe.get_doc("Delivery Note", dn_name)
        try:
            doc.cancel()
            frappe.db.commit()
        except:
            print doc.name + "skipped"
        print "canceled DN: " + doc.name


def submit_stock_recon():

    recon = frappe.db.sql("""Select name from `tabStock Reconciliation` where status = 'Draft' Order By name ASC""")

    for r in recon:
        r_name = str(r[0])
        doc = frappe.get_doc("Stock Reconciliation", r_name)
        doc.submit()

@frappe.whitelist()
def change_item_uom(item_code, to_uom, from_uom):

    frappe.db.sql("""Update `tabItem` set uom = %s, stock_uom = %s, modified = NOW()
        where item_code = %s""", (to_uom, to_uom, item_code))
    frappe.db.commit()
    frappe.db.sql("""Update `tabPurchase Order Item` set uom = %s, stock_uom = %s
        where uom = stock_uom and uom = %s and item_code = %s""", (to_uom, to_uom, from_uom,item_code))
    frappe.db.sql("""Update `tabPurchase Receipt Item` set uom = %s, stock_uom = %s
            where uom = stock_uom and uom = %s and item_code = %s""", (to_uom, to_uom, from_uom, item_code))
    frappe.db.commit()

def update_item_price():
    price = frappe.db.sql("""Select item_code, item_price from `tabItem`""")

    for p in price:
        itm_code = str(p[0])
        itm_pr = str(p[1])

        frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s
          where item_code = %s and price_list = 'Standard Selling' where branch""", (itm_pr, itm_code))

        print itm_code + "   Sucessfully update to : " + itm_pr

        frappe.db.commit()


def submit_stock_entry():

    now = datetime.datetime.now()
    st_name = frappe.db.sql("""Select name from `tabStock Entry`
        where docstatus = 0 and posting_date >= '2017-01-01' and posting_date <= %s order by name ASC""", (now))

    for s in st_name:
        s_name = frappe.get_doc("Stock Entry", s[0])
        try :
            s_name.submit()
            frappe.db.commit()
        except:
            print "Cannot submit"
            print s_name
        print s_name.name
        print s_name.docstatus


def set_packing():

    item = frappe.db.sql("""Select name from `tabItem`""")

    for i in item:
        item_code = str(i[0])

        con_factor = frappe.db.sql(
            """Select max(conversion_factor) from `tabUOM Conversion Detail` where parent = %s""", (item_code))

        packing = str(con_factor[0][0])
        frappe.db.sql("""Update `tabItem` set packing = %s where item_code = %s""", ((round(float(packing),0)), item_code))
        frappe.db.commit()
        print item_code
        print packing


def update_all_price():
    item = frappe.db.sql("""Select barcode_retial from `tabItem` where item_price_retail_1 = 0""")

    for i in item:
        barcode = str(i[0])

        price_in_pos = frappe.db.sql("""Select price from `tabUpload POS` where barcode = %s order by trans_date desc limit 1""", (barcode))

        for p in price_in_pos:
            price = str(p[0])

            frappe.db.sql("""Update `tabItem` set item_price_retail_1 = %s where barcode_retial = %s""", (price, barcode))
            print barcode
            print price


def update_price():
    item = frappe.db.sql("""Select barcode, price, trans_date from `tabUpload POS`
        where trans_date = (select max(trans_date) from `tabUpload POS`)""")

    for i in item:
        barcode = str(i[0])
        price = str(i[1])

        frappe.db.sql("""Update `tabItem` set item_price_retail_1 = %s where barcode_retial = %s""", (price, barcode))
        print barcode
        print price






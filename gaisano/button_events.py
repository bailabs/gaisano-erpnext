import frappe


@frappe.whitelist()
def process_force_backup():
    frappe.db.set_value("System Settings", None, "run_task", "backup")
    return {"message": "Please wait for a minute to process the backup"}


@frappe.whitelist()
def process_reset_permission():
    frappe.db.set_value("System Settings", None, "run_task", "permission")
    return {"message": "Please wait for a minute to process reset permission"}


@frappe.whitelist()
def set_qty_to_zero():
    pass


@frappe.whitelist()
def delete_zero_items():
    pass




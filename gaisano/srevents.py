import frappe
from array import array
import datetime
from erpnext.controllers.stock_controller import StockController
from popupevents import test_duplicate_rr

#TODO: on save, check if there is an existing stock recon for the item with the same date, time and docstatus = 1; if yes: throw

@frappe.whitelist()
def get_items_recon(sup_disc):
    items = []

    doc = frappe.db.sql("""Select supplier.items from `tabSupplier Discount Items` supplier LEFT join `tabItem` item on supplier.items = item.name where supplier.parent = %s order by item.type DESC, supplier.items asc""",
                        (sup_disc))

    for item in doc:
        item_code = str(item[0])
        db_item = frappe.db.sql("""Select name, item_cost, uom, item_name_dummy from tabItem where item_code = %s""", (item_code))
        for d in db_item:
            item_name = str(d[0])
            item_cost = str(d[1])
            item_uom = str(d[2])
            item_description= str(d[3])
            items.append({"item_code":item_code, "item_name":item_name, "uom":item_uom,
                          "item_description":item_description, "item_cost":item_cost})
            #items_dict_code = {"item": item_code}
            #items_dict_name = {"item": item_name}
            #items_dict_cost = {"item": item_cost}
            #items_dict_uom = {"item": item_uom}

            #items_array_code.append(items_dict_code)
            #items_array_name.append(items_dict_name)
            #items_array_cost.append(items_dict_cost)
            #items_array_uom.append(items_dict_uom)

    return items


"""
Force submit: Used to submit SR
"""
@frappe.whitelist()
def force_submit(sr):
    sr_doc = frappe.get_doc("Stock Reconciliation", sr)
    frappe.db.sql("""Update `tabStock Reconciliation Item` set docstatus = 1, modified = NOW() where parent = %s;""", sr)
    frappe.db.sql("""Update `tabStock Reconciliation` set docstatus = 1, modified = NOW() where name = %s""", sr)
    frappe.db.commit()
    try:
        update_stock_ledger(sr_doc)
        create_sle_log(sr_doc, None)
    except:
        return "Error on update stock ledger"

def update_stock_ledger(self):
    """	find difference between current and expected entries
        and create stock ledger entries based on the difference"""
    from erpnext.stock.stock_ledger import get_previous_sle

    for row in self.items:
        previous_sle = get_previous_sle({
            "item_code": row.item_code,
            "warehouse": row.warehouse,
            "posting_date": self.posting_date,
            "posting_time": self.posting_time
        })
        if previous_sle:
            if row.qty in ("", None):
                row.qty = previous_sle.get("qty_after_transaction", 0)

            if row.valuation_rate in ("", None):
                row.valuation_rate = previous_sle.get("valuation_rate", 0)

        if row.qty and not row.valuation_rate:
            frappe.throw("Valuation Rate required for Item in row {0}".format(row.idx))

        if ((previous_sle and row.qty == previous_sle.get("qty_after_transaction")
             and row.valuation_rate == previous_sle.get("valuation_rate"))
            or (not previous_sle and not row.qty)):
            continue

        self.insert_entries(row)


def insert_entries(self, row):
    """Insert Stock Ledger Entries"""
    args = frappe._dict({
        "doctype": "Stock Ledger Entry",
        "item_code": row.item_code,
        "warehouse": row.warehouse,
        "posting_date": self.posting_date,
        "posting_time": self.posting_time,
        "voucher_type": self.doctype,
        "voucher_no": self.name,
        "company": self.company,
        "stock_uom": frappe.db.get_value("Item", row.item_code, "stock_uom"),
        "is_cancelled": "No",
        "qty_after_transaction": row.qty,
        "valuation_rate": row.valuation_rate
    })
    self.make_sl_entries([args])


def update_sle_srs():
    today = datetime.date.today()
    december = (today - datetime.timedelta(days=180))
    items = []
    #get latest SR's and their items + details. if Item has been checked, add item to the list. Item will no be checked again.
    latest_sr = frappe.db.sql("""Select sr.name, sr.actual_qty, sr.qty_after_transaction, sr.posting_date, sr.posting_time, sr.warehouse, sr.item_code
                    from `tabStock Ledger Entry` as sr inner join `tabWarehouse` wh on sr.warehouse = wh.name where
                    sr.voucher_no like %s and sr.warehouse = %s and sr.posting_date >=%s ORDER BY name desc """,
                              ('SR-' + '%', "CDO Warehouse - GG", str(december) ))
    for sr in latest_sr:
        sr_name = sr[0]
        sr_actual_qty = sr[1]
        sr_qty_after_transaction = sr[2]
        sr_posting_date = sr[3]
        sr_posting_time = sr[4]
        sr_warehouse = sr[5]
        item = sr[6]

        #print sr_name, sr_actual_qty, sr_qty_after_transaction, sr_posting_date, sr_posting_time, item
        if item not in items:
            items.append(check_sles_after_sr(sr_qty_after_transaction, sr_posting_date, sr_posting_time, sr_warehouse, item))


def check_sles_after_sr(sr_qty_after_transaction, sr_posting_date, sr_posting_time, sr_warehouse, item):
    sles = frappe.db.sql("""SELECT name, posting_date, posting_time, actual_qty, qty_after_transaction, voucher_no from `tabStock Ledger Entry` where
                        item_code = %s and posting_date >= %s and warehouse = %s order by posting_date ASC, posting_time ASC"""
                         , (item, sr_posting_date, sr_warehouse))
    current_qty = int(sr_qty_after_transaction)
    #print "Current QTY AFter RECON:", current_qty
    for sle in sles:
        sle_name, sle_posting_date, sle_posting_time = sle[0], sle[1], sle[2]
        sle_actual_qty, sle_qty_after_transaction = int(sle[3]), int(sle[4])
        voucher_no = sle[5]
        if sle_posting_date == sr_posting_date:
            if sle_posting_time > sr_posting_time:
                #print "QTY after transaction Should be", (current_qty + sle_actual_qty)
                current_qty = update_sle(sle_name, (current_qty + sle_actual_qty), sle_qty_after_transaction, voucher_no)
        elif sle_posting_date > sr_posting_date:
            #print "Actual QTY ", sle_actual_qty, "QTY after Transaction", sle_qty_after_transaction, item
            #print "QTY after transaction Should be", (current_qty + sle_actual_qty)
            current_qty = update_sle(sle_name, (current_qty + sle_actual_qty), sle_qty_after_transaction, voucher_no)
    return item

def update_sle(name, qty_after_transaction, sle_qty_after_transaction, voucher_no):
    if qty_after_transaction != sle_qty_after_transaction:
        if "RR" in voucher_no:
            print "Update this RR: ", voucher_no #if RR, correct.
            test_duplicate_rr(voucher_no)
        #frappe.db.sql("""UPDATE `tabStock Ledger Entry` set qty_after_transaction = %s where name = %s""",(qty_after_transaction, name))
        #frappe.db.commit()
    else:
        print "OKAY~No need to worry"
    return qty_after_transaction

def check_repeat_item(doc, method):
    items = doc.items
    for item in items:
        print item
        duplicates = frappe.db.sql("""SELECT count(*) from `tabStock Reconciliation Item` itm join `tabStock Reconciliation` sr
                                on sr.name = itm.parent where itm.item_code = %s and sr.posting_time = %s and sr.posting_date = %s
                                and sr.name != %s""",(item.item_code, doc.posting_time, doc.posting_date, doc.name))
        if len(duplicates)>0:
            if duplicates[0][0]>0:
                str = "Stock Recon for "+doc.posting_date+" "+doc.posting_time+"already exists for item "+item.item_description
                frappe.throw(str)


#SLE LOGGING
def create_sle_log(doc,method):
    from rrevents import find_sle
    from erpnext.stock.stock_ledger import get_previous_sle
    voucher_no = doc.name
    posting_date = doc.posting_date
    posting_time = doc.posting_time
    warehouse = doc.warehouse
    is_match = 0

    print("#################CREATE SLE LOG##################")


    for item in doc.items:
        actual_qty = 0
        qty_after_transaction = item.qty
        #check_match
        match = find_sle(voucher_no, item.item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time)
        if match >0:
            is_match=1
        print(voucher_no, item.item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match)
        frappe.db.sql("""insert into slelogging(voucher_no, item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match)
                        values(%s,%s,%s,%s,%s,%s,%s,%s)""",(voucher_no, item.item_code, actual_qty, qty_after_transaction, warehouse, posting_date, posting_time, is_match))
        frappe.db.commit()


def create_beginning_sle(branch_warehouse=None, date_=None, time_=None):

    counter = 0

    warehouse = branch_warehouse if branch_warehouse is not None else frappe.db.get_value("Auto PO", None, "auto_po_warehouse")
    #print "DATE NOW:", str(date_now), str(time_now), warehouse
    items = frappe.db.sql("""select distinct(name) from `tabItem` where type!='Disabled'""")

    try:
        frappe.db.sql("""CREATE TABLE tempsle1 (select * from `tabStock Ledger Entry` limit 1)""")
    except:
        pass

    for item in items:
        date_now = datetime.datetime.now().date() if date_ is None else date_
        time_now = datetime.datetime.now().time() if time_ is None else time_

        balance = get_balance3(warehouse,item[0],date_now, time_now)
        sle_balance = get_balance(warehouse,item[0],date_now)
        sle_name = "SR-MANUAL-2023-"+str(item[0])+"/"+warehouse

        #print balance, sle_balance, sle_balance[0], sle_balance[1]
        #print item[0]
        if (sle_balance[1] != balance) or (sle_balance[0] < datetime.datetime.strptime('2023-02-19','%Y-%m-%d').date()) :
            counter += 1
            print sle_name#, item[0], balance
            try:

                frappe.db.sql("""UPDATE tempsle1 set name=%s, item_code = %s, qty_after_transaction=%s, actual_qty=%s,
                                  posting_date =%s, posting_time = %s, modified_by='Administrator', owner='Administrator',
                                  fiscal_year='2023', voucher_type='Manual BegBal', voucher_no = 'SR-MANUAL-2023',
                                  modified = NOW(), creation = NOW(), warehouse = %s """,
                                (sle_name, item[0], balance, 0, date_now, time_now, warehouse))
                frappe.db.commit()
                frappe.db.sql("""INSERT INTO `tabStock Ledger Entry` (select * from tempsle1)""")
                frappe.db.commit()
            except:
                frappe.db.sql("""UPDATE `tabStock Ledger Entry` set item_code = %s, qty_after_transaction=%s, actual_qty=%s,
                                  posting_date =%s, posting_time = %s, modified_by='Administrator', owner='Administrator',
                                  fiscal_year='2023', voucher_type='Manual BegBal', voucher_no = 'SR-MANUAL-2023',
                                  modified = NOW(), creation = NOW(), warehouse = %s where name =%s""",
                                (item[0], balance, 0, date_now, time_now, warehouse, sle_name))
                frappe.db.commit()
    print counter, " # of SLE Records Created"


def get_balance(warehouse, item_code, trans_date):
    balance = 0
    recon = frappe.db.sql("""select posting_date, qty_after_transaction from `tabStock Ledger Entry`
                where warehouse = %s and
                item_code = %s and
                posting_date <= %s
                ORDER BY posting_date DESC, posting_time DESC, name DESC limit 1""",(warehouse, item_code, trans_date))
    balance = (recon[0][1] if len(recon)>0 else 0)
    posting_date = (recon[0][0] if len(recon) > 0 else datetime.datetime.strptime('2015-01-01',"%Y-%m-%d").date())

    return posting_date, balance


def sle_recalculate(branch_warehouse=None, date_=None, time_=None):

    counter = 0

    warehouse = branch_warehouse if branch_warehouse is not None else frappe.db.get_value("Auto PO", None, "auto_po_warehouse")
    #print "DATE NOW:", str(date_now), str(time_now), warehouse
    items = frappe.db.sql("""select distinct(name) from `tabItem` where type!='Disabled'""")

    for item in items:
        date_now = datetime.datetime.now().date() if date_ is None else date_
        time_now = datetime.datetime.now().time() if time_ is None else time_

        balance = get_balance3(warehouse,item[0],date_now, time_now)
        sle_balance = get_balance(warehouse,item[0],date_now)
        sle_name = "SR-MANUAL-2023-"+str(item[0])+"/"+warehouse

        #print balance, sle_balance, sle_balance[0], sle_balance[1]
        #print item[0]
        if (sle_balance[1] != balance):
            from erpnext.stock.stock_ledger import update_entries_after
            try:
                sle_doc_test = frappe.get_doc("Stock Ledger Entry",sle_name)
            except:
                frappe.db.sql("""UPDATE tempsle1 set name=%s, item_code = %s, qty_after_transaction=%s, actual_qty=%s,
                                                  posting_date =%s, posting_time = %s, modified_by='Administrator', owner='Administrator',
                                                  fiscal_year='2023', voucher_type='Manual BegBal', voucher_no = 'SR-MANUAL-2023',
                                                  modified = NOW(), creation = NOW(), warehouse = %s """,
                              (sle_name, item[0], balance, 0, date_now, time_now, warehouse))
                frappe.db.commit()
                frappe.db.sql("""INSERT INTO `tabStock Ledger Entry` (select * from tempsle1)""")
                frappe.db.commit()
                sle_doc_test = frappe.get_doc("Stock Ledger Entry", sle_name)
                print "Recalculate:", sle_name, item[0], balance, sle_balance[1]
                update_voucher_entries_after(sle_doc_test)
                counter+=1
            else:
                sle_doc_test = frappe.get_doc("Stock Ledger Entry", sle_name)
                print "Recalculate:", sle_name, item[0], balance, sle_balance[1]
                update_voucher_entries_after(sle_doc_test)
                counter += 1
    print counter, " # of SLE Records Adjusted"


def update_voucher_entries_after(previous_sle):
    # """get stock ledger entries filtered by specific posting datetime conditions"""

    from popupevents import delete_duplicate_rr, delete_duplicate_se

    sle_posting_date = previous_sle.posting_date
    sle_posting_time = previous_sle.posting_time

    vouchers = frappe.db.sql("""select distinct voucher_no, voucher_type from `tabStock Ledger Entry` where 
                                TIMESTAMP(posting_date, posting_time) > TIMESTAMP(%s,%s) and item_code = %s
                                order by TIMESTAMP(posting_date, posting_time) asc """,(
                                sle_posting_date, sle_posting_time, previous_sle.item_code
                            ), as_dict = True)
    for voucher in vouchers:
        print voucher.voucher_no, voucher.voucher_type
        if voucher.voucher_type == "Purchase Receipt":
            delete_duplicate_rr(voucher.voucher_no)
            break
        elif voucher.voucher_type == "Stock Entry":
            delete_duplicate_se(voucher.voucher_no)
            break
        else:
            continue


def get_balance3(warehouse, item_code, trans_date, trans_time):
    time_now = trans_time
    to_date = str(trans_date)+ " " + str(time_now)
    from_date = "2015-01-01 00:00:00"
    recon = 0


    sr = frappe.db.sql("""select timestamp(sr.posting_date, sr.posting_time), itm.qty from `tabStock Reconciliation Item` itm
                            join `tabStock Reconciliation` sr on sr.name = itm.parent where itm.warehouse = %s and itm.item_code = %s
                            and sr.posting_date< %s and sr.docstatus = 1 order by posting_date desc, posting_time desc, sr.name desc limit 1""",
                       (warehouse, item_code, trans_date))

    if len(sr)>0:
        if sr[0][0]:
            from_date = sr[0][0]
            recon = sr[0][1]

    print(str(from_date), str(to_date))

    rrs = frappe.db.sql("""select sum(itm.stock_qty) from `tabPurchase Receipt Item` itm join `tabPurchase Receipt` rr on
                        rr.name = itm.parent where itm.warehouse = %s and itm.item_code =%s and rr.docstatus = 1
                        and TIMESTAMP(rr.posting_date, rr.posting_time)>=%s and TIMESTAMP(rr.posting_date, rr.posting_time)< %s""",
                        (warehouse, item_code, from_date, to_date))

    ses_minus = frappe.db.sql("""select sum(-1*itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.s_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time)>= %s and TIMESTAMP(se.posting_date, se.posting_time)<%s
                              and se.docstatus = 1""",(warehouse, item_code, from_date, to_date))

    ses_plus = frappe.db.sql("""select sum(itm.transfer_qty) from `tabStock Entry Detail` itm
                              join `tabStock Entry` se on se.name = itm.parent where itm.t_warehouse = %s and itm.item_code =%s
                              and TIMESTAMP(se.posting_date, se.posting_time)>= %s and TIMESTAMP(se.posting_date, se.posting_time)<%s
                              and se.docstatus = 1""", (warehouse, item_code, from_date, to_date))

    total_rr = rrs[0][0] if rrs[0][0] is not None else 0
    total_se_minus = ses_minus[0][0] if ses_minus[0][0] is not None else 0
    total_se_plus = ses_plus[0][0] if ses_plus[0][0] is not None else 0

    inv = total_rr + total_se_minus + total_se_plus + recon

    return inv
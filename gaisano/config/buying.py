from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        {
            "label": _("Setup"),
            "icon": "octicon octicon-cloud-upload",
            "items": [
                {
                    "type": "doctype",
                    "name": "Payment Terms",
                    "label": _("Payment Terms"),
                    "description": _("Payment Term"),
                    "hide_count": True
                }
            ]
        },
        {
            "label": _("Setup"),
            "icon": "octicon octicon-cloud-upload",
            "items": [
                {
                    "type": "doctype",
                    "name": "Supplier Discounts",
                    "label": _("Supplier Discounts"),
                    "description": _("Supplier Discounts"),
                    "hide_count": True
                }
            ]
        },
    ]
import frappe
import datetime
from autopoevents import update_item_stock_level

@frappe.whitelist()
def get_email_failed():
    emails = frappe.db.sql('''select DATE_FORMAT(q.creation, "%M %d %Y %h:%i %p"), q.name, q.reference_name, q.sender,
                              q.status, c.status, c.recipient FROM `tabEmail Queue` q
                              LEFT JOIN `tabEmail Queue Recipient` c on q.name = c.parent
                              where c.status!='Sent' and q.reference_doctype = 'Purchase Order'
                              and q.creation > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY q.creation DESC;''')

    new_arr = []
    for email in emails:
        temp = []
        email_ref = "<a class = 'btn btn-primary' href=%s target='_blank'>" % ("/desk#Form/Email%20Queue/" + (email[1])) + ("SEND" + "</a>")
        # po_ref = "<a href=%s target='_blank'>" % ("/desk#Form/Purchase%20Order/" + (email[2])) + (email[2] + "</a>")
        temp.append(email[0])
        temp.append(email[2])
        temp.append(email[1])
        #temp.append(po_ref)
        temp.append(email[3])
        temp.append(email[6])
        temp.append(email_ref)
        new_arr.append(temp)
    return new_arr
    #return emails

@frappe.whitelist()
def po_schedule():
    text_for_popup = "<table class = 'table table-bordered table-hover'>"
    count = 0
    discounts = frappe.db.sql("""SELECT name, supplier_name from `tabSupplier Discounts` where po_date = curdate()""")
    now = datetime.date.today()

    print now
    for discount in discounts:
        pos = frappe.db.sql("""SELECT name, workflow_state from `tabPurchase Order` where supplier_discount= %s and transaction_date = %s and workflow_state <> 'Submitted'""",(discount[0], now))
        if len(pos) != 0:
            print discount[0], discount[1], pos
            text_for_popup += "<tr><td>"+discount[1] + "</td></tr>"
            count +=1
        else:
            print discount[0], discount[1], pos
            continue
    text_for_popup += "</table>"
    if count > 0:
        return text_for_popup

#########################################
######## ITEM METHODS FOR HOOKS #########
#########################################

def create_price(doc, method):
    insert_item_to_supplier_discount(doc) #insert item to supplier discount
    save_classification_packing(doc)
    add_item_price_history(doc)
    update_srp_margins(doc)  # update item margin rate based on SRP
    #update_branch_item_price(doc)

    branch = frappe.db.get_value("Server Information", None, "Branch")
    if branch == "CDO Main":
        if (doc.item_id == 0) or (doc.item_id == None):
            doc.item_id = get_item_id()
        else:
            if get_item_from_id(doc.item_id)!=doc.name:
                doc.item_id = get_item_id()

    # on item insert, create Item price from item_cost, item_price field info
    item = frappe.db.sql("""select * from `tabItem Price` where item_code = %s""", doc.name)

    if len(item) == 0:
        now = datetime.datetime.now()
        new_price_rec_dict_buy = {
            'doctype': 'Item Price',
            # 'name': 'item-price' + str(doc.name) + str(doc.item_cost),
            'creation': now.strftime("%Y-%m-%d %H:%M"),
            'modified': now.strftime("%Y-%m-%d %H:%M"),
            'modified_by': doc.modified_by,
            'owner': doc.owner,
            'docstatus': 0,
            'parent': '',
            'parentfield': '',
            'parenttype': '',
            'idx': 0,
            'price_list_rate': str(doc.item_cost_with_freight),
            'selling': 0,
            'item_name': doc.name,
            '`_assign`': '',
            'price_list': 'Standard Buying',
            'item_code': doc.item_code,
            'currency': 'PHP',
            '`_liked_by`': '',
            '`_comments`': '',
            'item_description': doc.name,
            '`_user_tags`': '',
            'buying': 1,
            'margin': 0.0
        }
        new_price_rec_dict_sell = {
            'doctype': 'Item Price',
            # 'name': 'item-price' + str(doc.name) + str(doc.item_price),
            'creation': now.strftime("%Y-%m-%d %H:%M"),
            'modified': now.strftime("%Y-%m-%d %H:%M"),
            'modified_by': doc.modified_by,
            'owner': doc.owner,
            'docstatus': 0,
            'parent': '',
            'parentfield': '',
            'parenttype': '',
            'idx': 0,
            'price_list_rate': str(doc.item_price),
            'selling': 0,
            'item_name': doc.name,
            '`_assign`': '',
            'price_list': 'Standard Selling',
            'item_code': doc.item_code,
            'currency': 'PHP',
            '`_liked_by`': '',
            '`_comments`': '',
            'item_description': doc.name,
            '`_user_tags`': '',
            'buying': 1,
            'margin': doc.margin_rate
        }
        try:
            newrecsell = frappe.get_doc(new_price_rec_dict_sell)
            newrecbuy = frappe.get_doc(new_price_rec_dict_buy)
            newrecsell.insert()
            frappe.db.commit()
            newrecbuy.insert()
            frappe.db.commit()
        except:
            print "ERROR."
    else:
        frappe.msgprint("Item with item_code "+doc.name+"already has an item price.")


def update_price(doc, method):

    insert_item_to_supplier_discount(doc)  # insert item to supplier discount
    save_classification_packing(doc)
    add_item_price_history(doc)
    update_srp_margins(doc)  # update item margin rate based on SRP
    #update_branch_item_price(doc)

    # on item SAVE, update Item price from item_cost, item_price field infos
    item = frappe.db.sql("""select * from `tabItem Price` where item_code = %s""", doc.name)
    if len(item) != 0:
        frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where selling = 0 and item_code = %s""",
                      (str(doc.item_cost_with_freight), doc.name))
        frappe.db.commit()
        frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where selling = 1 and item_code = %s""",
                      (str(doc.item_price), doc.name))
        frappe.db.commit()
    else:
        now = datetime.datetime.now()
        new_price_rec_dict_buy = {
            'doctype': 'Item Price',
            # 'name': 'item-price' + str(doc.name) + str(doc.item_cost),
            'creation': now.strftime("%Y-%m-%d %H:%M"),
            'modified': now.strftime("%Y-%m-%d %H:%M"),
            'modified_by': doc.modified_by,
            'owner': doc.owner,
            'docstatus': 0,
            'parent': '',
            'parentfield': '',
            'parenttype': '',
            'idx': 0,
            'price_list_rate': str(doc.item_cost_with_freight),
            'selling': 0,
            'item_name': doc.name,
            '`_assign`': '',
            'price_list': 'Standard Buying',
            'item_code': doc.item_code,
            'currency': 'PHP',
            '`_liked_by`': '',
            '`_comments`': '',
            'item_description': doc.name,
            '`_user_tags`': '',
            'buying': 1,
            'margin': 0.0
        }
        new_price_rec_dict_sell = {
            'doctype': 'Item Price',
            # 'name': 'item-price' + str(doc.name) + str(doc.item_price),
            'creation': now.strftime("%Y-%m-%d %H:%M"),
            'modified': now.strftime("%Y-%m-%d %H:%M"),
            'modified_by': doc.modified_by,
            'owner': doc.owner,
            'docstatus': 0,
            'parent': '',
            'parentfield': '',
            'parenttype': '',
            'idx': 0,
            'price_list_rate': str(doc.item_price),
            'selling': 0,
            'item_name': doc.name,
            '`_assign`': '',
            'price_list': 'Standard Selling',
            'item_code': doc.item_code,
            'currency': 'PHP',
            '`_liked_by`': '',
            '`_comments`': '',
            'item_description': doc.name,
            '`_user_tags`': '',
            'buying': 1,
            'margin': doc.margin_rate
        }
        try:
            newrecsell = frappe.get_doc(new_price_rec_dict_sell)
            newrecbuy = frappe.get_doc(new_price_rec_dict_buy)
            newrecsell.insert()
            frappe.db.commit()
            newrecbuy.insert()
            frappe.db.commit()
        except:
            frappe.msgprint("Error in creating item price.")

def create_name(doc, method):
    from itemevents import on_insert
    doc = on_insert(doc)
    print "--------UPDATE NAME-------"
    doc.valuation_rate = doc.item_cost
    doc.item_name = str(doc.item_name_dummy) + " (" + str(doc.type) + " )" + "/ " + str(doc.barcode_retial) + " / " + str(doc.barcode_pack) + " / " + str(doc.bar_code_2)
    for i, branch in enumerate(doc.branch_price_table):
        if doc.branch_price_table[i].branch == frappe.db.get_value("Server Information", None, "branch"):
            doc.branch_price_table[i].price_id = doc.branch_price_table[i].branch_code + "-" + doc.item_code
    update_item_stock_level(doc.item_code)
    check_plu_code(doc)
    update_branch_item_price(doc)

def insert_item_to_supplier_discount(this_item):
    print this_item.item_code, "---ADD TO DISCOUNTS--**"
    item_code = this_item.name
    update_item_supplier_discount(this_item.name)
    discounts = frappe.db.sql("""SELECT branch, supplier_discount from `tabItem Branch Discount` where parent = %s""", item_code)
    print "------ADD TO THE FOLLOWING DISCOUNTS------"
    for discount in discounts:
        print discount
        add_item_to_supplier_discount(item_code, discount[1])
        update_discount_branches(discount[1], discount[0])
    update_item_stock_level(item_code)


#call when adding item to supplier discounts
def add_item_to_supplier_discount(item_code, supplier_disc):
    count_item = frappe.db.sql("""SELECT count(*) from `tabSupplier Discount Items` where parent = %s and items = %s""",(supplier_disc, item_code))
    if count_item[0][0]==0:
        item_doc = frappe.get_doc("Item", item_code)
        discount_item = {
            "doctype": "Supplier Discount Items",
            "items":item_code,
            "item_description":item_doc.item_name_dummy,
            "parent": supplier_disc,
            "parentfield":"items",
            "parenttype": "Supplier Discounts"
        }

        item = frappe.get_doc(discount_item)
        item.insert()
        user = frappe.get_user()
        print user.name, "------------------------>>> USER<<-----------"
        frappe.db.sql("""UPDATE `tabSupplier Discounts` set modified_by = %s, modified = NOW() where name = %s""",
                      (user.name, supplier_disc))
        frappe.db.commit()

def update_item_supplier_discount(item_code):
    #delete old supplier_discounts
    items = frappe.db.sql("""SELECT name, parent from `tabSupplier Discount Items` where items = %s""", item_code)
    for item in items:
            frappe.get_doc("Supplier Discount Items", item[0]).delete()
            frappe.db.commit()

def update_discount_branches(discount, branch):
    discount_doc = frappe.get_doc("Supplier Discounts", discount)
    if discount_doc.all_branches !=1:
        branches = frappe.db.sql("""SELECT count(*) from `tabItem Branch` where parent = %s and branch = %s""", (discount, branch))
        if branches[0][0] == 0:
            print "INSERT Supplier Discount Branches for", discount, branch
            discount_branch = {
                "doctype": "Item Branch",
                "branch": branch,
                "parent": discount,
                "parentfield": "branch_table",
                "parenttype": "Supplier Discounts"
            }

            branch_row = frappe.get_doc(discount_branch)
            branch_row.insert()
            frappe.db.commit()


def price_with_freight():
    # todo: if branches : check if branch freight is available and use branch freight. Else, use freight cost.
    item = frappe.db.sql("""Select name from `tabItem` where item_freight != 0 or item_freight_peso !=0""")

    for i in item:
        name = i[0]
        doc = frappe.get_doc('Item', name)
        if (doc.item_freight_peso !=0) :
            new_cost = doc.item_cost + doc.item_freight_peso
            frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where item_code = %s
            and price_list = 'Standard Buying'""", (new_cost, name))
            frappe.db.commit()
        elif (doc.item_freight !=0) :
            new_cost = doc.item_cost * (1 + (doc.item_freight/100))
            frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where item_code = %s
                        and price_list = 'Standard Buying'""", (new_cost, name))
            frappe.db.commit()
        else:
            new_cost = doc.item_cost
            frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where item_code = %s
                        and price_list = 'Standard Buying'""", (new_cost, name))
            frappe.db.commit()

        print "Finish ++++++++"
        print name
        print new_cost

def save_classification_packing(item):
    item_doc = item
    if len(item_doc.item_classification_1) > 0:
        print item_doc.item_classification_1[0].classification
        temp = str(item_doc.item_classification_1[0].classification) + " | pck: " + str(item_doc.packing) + " | " + str(
            item_doc.barcode_retial)
        frappe.db.sql("UPDATE `tabItem` set classification_packing_barcode = %s where name = %s", (temp, item_doc.name))
        frappe.db.commit()

def add_item_price_history(item):
    print "==================ITEM PRICE HISTORY================"
    latest_prices = frappe.db.sql("""SELECT new_price,new_wholesale, new_retail, name from `tabItem Price History` where item = %s ORDER BY date_modified desc, time_modified desc""", item.name)
    print latest_prices
    prev_cost = item.previous_price
    if len(latest_prices)>0:
        last_cost, last_wholesale, last_retail = latest_prices[0][0], latest_prices[0][1], latest_prices[0][2]
        if (last_cost == item.item_cost) and (last_wholesale == item.item_price) and (last_retail == item.item_price_retail_with_margin):
            print "DO NOT ADD PRICE!"
        else:
            item.previous_price = last_cost
            price_history = {
                "doctype": "Item Price History",
                "modified_by":item.modified_by,
                "time_modified":str(datetime.datetime.now().time()),
                "date_modified":str(datetime.datetime.now().date()),
                "item":item.name,
                "item_code": item.item_code,
                "item_name": item.item_name,
                "barcode":item.barcode_retial,
                "new_price":item.item_cost,
                "prev_price":last_cost,
                "new_wholesale":item.item_price,
                "prev_wholesale":last_wholesale,
                "new_retail":item.item_price_retail_with_margin,
                "prev_retail":last_retail,
                "naming_series": "barcode.-.YYYY.MM.DD.-"
            }
            print "Not the same! Create new price entry"
            #print price_history
            new_history = frappe.get_doc(price_history)
            new_history.insert()
    else:
        price_history = {
            "doctype": "Item Price History",
            "modified_by": item.modified_by,
            "time_modified": str(datetime.datetime.now().time()),
            "date_modified": str(datetime.datetime.now().date()),
            "item": item.name,
            "item_name": item.item_name,
            "item_code": item.item_code,
            "barcode": item.barcode_retial,
            "new_price": item.item_cost,
            "prev_price": prev_cost,
            "new_wholesale": item.item_price,
            "prev_wholesale": 0,
            "new_retail": item.item_price_retail_with_margin,
            "prev_retail": 0,
            "naming_series": "barcode.-.YYYY.MM.DD.-"
        }
        new_history = frappe.get_doc(price_history)
        new_history.insert()

def update_buying_price():
    #todo: if branches : check if branch freight is available and use branch freight. Else, use freight cost.
    items = frappe.db.sql("""select item_code from `tabItem Price` where selling = 0""")
    if len(items) != 0:
        for item in items:
            try:
                item_doctype = frappe.get_doc("Item", item[0])
                if (item_doctype.item_cost_with_freight == "") or (item_doctype.item_cost_with_freight == None) or (item_doctype.item_cost_with_freight == 0):
                    frappe.db.sql(
                        """Update `tabItem Price` set price_list_rate = %s where selling = 0 and item_code = %s""",
                        (str(item_doctype.item_cost), item_doctype.name))
                else:
                    frappe.db.sql("""Update `tabItem Price` set price_list_rate = %s where selling = 0 and item_code = %s""",
                          (str(item_doctype.item_cost_with_freight), item_doctype.name))
            except:
                print "Item code not found for "+str(item[0])
                continue
            else:
                print item[0]+" Price updated"
                frappe.db.commit()


def update_srp_margins(item):
    print "===========update srp margins=========="
    print item.name, item.item_cost_with_freight, item.item_price, item.margin_rate, item.item_price_retail_1, item.item_price_retail_with_margin, item.retail_margin_rate

    if item.item_cost_with_freight > 0:
        srp_w = (((float(item.item_price)-float(item.item_cost_with_freight))/float(item.item_cost_with_freight))*100)
    else:
        srp_w = 100 if (float(item.item_price)-float(item.item_cost_with_freight))>0 else 0

    if item.item_price_retail_1 >0:
        srp_r = (((float(item.item_price_retail_with_margin) - float(item.item_price_retail_1)) / float(
            item.item_price_retail_1)) * 100)
    else:
        srp_r = 100 if (float(item.item_price_retail_with_margin) - float(item.item_price_retail_1)) > 0 else 0

    print "Actual srp w: ", srp_w
    print "Actual srp r: ", srp_r
    if srp_w!=item.margin_rate:
        #print "UPDATE MARGIN_W"
        item.margin_rate = srp_w
        frappe.db.sql("""UPDATE `tabItem` set margin_rate = %s where name = %s""",(srp_w, item.name))
    if srp_r!= item.retail_margin_rate:
        #print "UPDATE MARGIN_R"
        item.retail_margin_rate = srp_r
        frappe.db.sql("""UPDATE `tabItem` set retail_margin_rate = %s where name = %s""", (srp_r, item.name))

    if item.unit_2:
        print("UNIT2 "+item.unit_2)
        print("#################################")
        uom2_cost = float(item.item_cost_with_freight)/float(item.packing_2)
        uom2_margin = ((float(item.item_price_retail_2)-uom2_cost)/uom2_cost)*100
        print item.uom2_margin, uom2_margin
        if uom2_margin!=item.uom2_margin:
            item.uom2_margin = uom2_margin
            frappe.db.sql("""UPDATE `tabItem` set uom2_margin = %s where name = %s""",(uom2_margin,item.name))

    frappe.db.commit()

    disc_name = get_branch_discount(item.name)
    #print "ITEM BRANCH DISCOUNT:", disc_name

    if disc_name is not None:
        disc = frappe.get_doc("Supplier Discounts",get_branch_discount(item.name))
        # print disc.supplier_discount_name, disc.name, item.name 
        net_cost = item.item_cost_with_freight * (1 - (float(disc.disc_1)/100)) * (1 - (float(disc.disc_2)/100)) * (1 - (float(disc.disc_3)/100)) * (1 - (float(disc.disc_4)/100))
        net_cost_retail = net_cost / item.packing
        actual_ws_margin = 0 if net_cost == 0 else ((item.item_price - net_cost) / net_cost) * 100
        actual_ret_margin = 0 if net_cost_retail == 0 else ((
                                                                   float(item.item_price_retail_with_margin) - net_cost_retail) / net_cost_retail) * 100
        print net_cost, net_cost_retail, actual_ws_margin, actual_ret_margin
        item.actual_retail_rate = actual_ret_margin
        item.actual_wholesale_rate = actual_ws_margin
        item.net_cost = net_cost

        frappe.db.sql("""UPDATE `tabItem` set actual_retail_rate=%s, actual_wholesale_rate=%s, net_cost=%s where name = %s""",
                      (actual_ret_margin,actual_ws_margin, net_cost, item.name))
    else:
        item.actual_retail_rate = item.retail_margin_rate
        item.actual_wholesale_rate = item.margin_rate
        item.net_cost = item.item_cost_with_freight
        frappe.db.sql(
            """UPDATE `tabItem` set actual_retail_rate=%s, actual_wholesale_rate=%s, net_cost=%s where name = %s""",
            (item.retail_margin_rate, item.margin_rate, item.item_cost_with_freight, item.name))

    frappe.db.commit()


def update_item_stock_level(item):
    discount = get_branch_discount(item)
    if discount:
        print "*************UPDATE STOCK LEVEL FOR AUTO PO******************"
        print discount
        discount_doc = frappe.get_doc("Supplier Discounts", discount)
        print "LEAD TIME", discount_doc.lead_time


@frappe.whitelist()
def get_branch_discount(item_name, branch = None):
    branch = frappe.db.get_value("Server Information", None, "branch") if branch is None else branch
    branch_discount = None
    print "update_item_actual_margins for ", item_name, "FOR ", branch
    discount = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where parent = %s and branch =%s""", (item_name, branch))
    if len(discount) > 0:
        branch_discount = discount[0][0]
    return branch_discount

@frappe.whitelist()
def get_branch_discount_values(item_name, branch = None):
    branch = frappe.db.get_value("Server Information", None, "branch") if branch is None else branch
    disc1, disc2, disc3, disc4 = 0,0,0,0
    print "update_item_actual_margins for ", item_name, "FOR ", branch
    discount = frappe.db.sql("""SELECT supplier_discount from `tabItem Branch Discount` where parent = %s and branch =%s""", (item_name, branch))
    if len(discount) > 0:
        branch_disc = frappe.get_doc("Supplier Discounts",discount[0][0])
        disc1, disc2, disc3, disc4 = branch_disc.disc_1, branch_disc.disc_2, branch_disc.disc_3, branch_disc.disc_4
    return 1-disc1/100, 1-disc2/100, 1-disc3/100, 1-disc4/100

@frappe.whitelist()
def create_barcode():
    #print "------------GENERATE BARCODE------------"
    last_barcode_promo = int(get_last_barcode_promo())+1
    is_taken = True
    while is_taken:
        barcode_temp = str(last_barcode_promo).zfill(9)
        barcode_temp = "601" + barcode_temp
        print barcode_temp, "TEMP"
        is_taken = check_barcode(barcode_temp)
        print "TAKEN?", is_taken
        if is_taken == False:
            break
        last_barcode_promo += 1

    even_sum = int(barcode_temp[1]) + int(barcode_temp[3]) + int(barcode_temp[5]) + int(barcode_temp[7]) + int(
        barcode_temp[9]) + int(barcode_temp[11])
    even_sum_prod = even_sum * 3
    odd_sum = int(barcode_temp[0]) + int(barcode_temp[2]) + int(barcode_temp[4]) + int(barcode_temp[6]) + int(
        barcode_temp[8]) + int(barcode_temp[10])
    total = even_sum_prod + odd_sum
    product = (int(total / 10) * 10)
    check_digit = (product-total) if (product>=total) else ((product+10)-total)
    print "checksum is ", total, "| Check digit is ", check_digit
    barcode_temp +=str(check_digit)
    return barcode_temp

@frappe.whitelist()
def create_barcode_regular():
    #print "------------GENERATE BARCODE------------"
    last_barcode_promo = int(get_last_barcode_regular())+1
    is_taken = True
    while is_taken:
        barcode_temp = str(last_barcode_promo).zfill(9)
        barcode_temp = "120" + barcode_temp
        print barcode_temp, "TEMP"
        is_taken = check_barcode(barcode_temp)
        print "TAKEN?", is_taken
        if is_taken == False:
            break
        last_barcode_promo += 1

    even_sum = int(barcode_temp[1]) + int(barcode_temp[3]) + int(barcode_temp[5]) + int(barcode_temp[7]) + int(
        barcode_temp[9]) + int(barcode_temp[11])
    even_sum_prod = even_sum * 3
    odd_sum = int(barcode_temp[0]) + int(barcode_temp[2]) + int(barcode_temp[4]) + int(barcode_temp[6]) + int(
        barcode_temp[8]) + int(barcode_temp[10])
    total = even_sum_prod + odd_sum
    product = (int(total / 10) * 10)
    check_digit = (product-total) if (product>=total) else ((product+10)-total)
    print "checksum is ", total, "| Check digit is ", check_digit
    barcode_temp +=str(check_digit)
    return barcode_temp


def get_last_barcode_promo():
    last_barcode_promo = frappe.db.get_value("Server Information", None, "last_barcode_promo")
    if last_barcode_promo:
        return last_barcode_promo
    else:
        print "no existing barcode data"
        return 0

def get_last_barcode_regular():
    last_barcode_promo = frappe.db.get_value("Server Information", None, "last_barcode_regular")
    if last_barcode_promo:
        return last_barcode_promo
    else:
        print "no existing barcode data"
        return 0


def check_barcode(barcode):
    items = frappe.db.sql("""select name from `tabItem` where barcode_retial = %s""", barcode)
    if len(items) > 0:
        return True
    else:
        return False

def update_last_barcode_promo(barcode):
    print barcode
    last_barcode_promo = int(str(barcode)[4:12])
    print last_barcode_promo
    frappe.db.set_value("Server Information", None, "last_barcode_promo", last_barcode_promo)
    frappe.db.commit()

def update_last_barcode_regular(barcode):
    print barcode
    last_barcode_regular = int(str(barcode)[4:12])
    print last_barcode_regular
    frappe.db.set_value("Server Information", None, "last_barcode_regular", last_barcode_regular)
    frappe.db.commit()

@frappe.whitelist()
def check_item_barcode(barcode):
    #print create_barcode()
    if barcode == create_barcode():
        update_last_barcode_promo(barcode)
        print "save to db."
    elif barcode == create_barcode_regular():
        update_last_barcode_regular(barcode)
    else:
        print "ignore."

@frappe.whitelist()
def check_items_has_manual_updates(barcode):
    frappe.db.sql("""delete from `tabItems Not Syncing` where barcode = %s and sync_error = 'Price Decrease/For Manual Updating/Rebates' """, barcode)
    frappe.db.commit()


def get_rounded_price(price):
    final = 0.00
    price = float(price)
    rounded = round(price, 1)
    truncated = round(price, 2)
    diff = round(float(rounded - truncated), 2)
    print "R1:", rounded, "|R2:", truncated, "|DIFF=", diff
    if ((diff == 0) or (diff == 0.05) or (diff == -0.05)):
        final = truncated
    elif ((diff >= 0.01) and (diff <= .02)):
        final = rounded
    elif (diff >= 0.03) and (diff <= 0.04):
        final = (rounded - 0.05)
    elif (diff >= -0.02) and (diff <= -0.01):
        final = rounded
    else:
        final = (rounded + 0.05)  # print "FINAL : %.2f"%final
    return final

@frappe.whitelist()
def get_item_id():
    last_id = frappe.db.sql("""SELECT MAX(item_id) from `tabItem`""")
    print last_id[0][0]
    new_id = last_id[0][0] + 1
    print "New Item ID: ", new_id
    return new_id

def get_item_from_id(item_id):
    item = frappe.db.sql("""SELECT name from `tabItem` where item_id = %s""",item_id)
    return (item[0][0] if (len(item)>0) else None)

@frappe.whitelist()
def get_all_branches():
    branches = []
    for row in frappe.db.sql("""select name from `tabBranch`"""):
        if (row[0] != "Replication") and (row[0] != "Temporary"):
            branches.append(row[0])
    return branches

@frappe.whitelist()
def item_name_duplicates(item_name, item):
    dups = frappe.db.sql("""SELECT COUNT(*) from `tabItem` where item_name_dummy = %s and name != %s""", (item_name, item))
    return dups[0][0]

def check_plu_code(this_item):
    print this_item.item_code,this_item.plu_code, "TESTING"
    items = frappe.db.sql("""select name, plu_code, description, item_name_dummy from `tabItem` where plu_code != Null or plu_code != ' ' and name != %s and ladlad = 1 """,this_item.item_code)
    for item in items:
        if this_item.plu_code == item[1]:
            frappe.throw("Same PLU code with item code "+item[3]+".")
        else:
            continue


#######################################################
########UPDATE CDO MAIN BRANCH ITEM PRICE TABLE########
#######################################################

def update_branch_item_price(item):
    from itemevents import get_branch
    item_branch_price = {}

    branch = get_branch()
    branch_code = frappe.get_doc("Branch",branch).branch_code

    if branch != "CDO Main":

        print "CREATE LOCAL ITEM BRANCH PRICES"

        frappe.db.sql("delete from `tabItem Prices Per Branch` where branch = %s and parent = %s", (branch, item.name))
        frappe.db.commit()

        cost_w = item.item_cost_with_freight
        cost_r = item.item_price_retail_1
        srp_w = item.item_price
        srp_r = item.item_price_retail_with_margin
        margin_w = item.margin_rate
        margin_r = item.retail_margin_rate
        freight = item.item_freight_peso

        # UPDATE LOCAL SERVER
        branch_price_json = {
            "branch": branch,
            "branch_code": branch_code,
            "doctype": "Item Prices Per Branch",
            "price_id": branch_code + "-" + item.name,
            "parent": item.name,
            "parentfield": "branch_price_table",
            "parenttype": "Item",
            "freight_peso": freight,
            "item_cost_with_freight": cost_w,
            "retail_cost": cost_r,
            "wholesale_margin_rate": margin_w,
            "retail_margin_rate": margin_r,
            "wholesale_price": srp_w,
            "retail_price": srp_r
        }
        branch_price_doc = frappe.get_doc(branch_price_json)
        branch_price_doc.insert()
        frappe.db.commit()

def create_branch_item_prices():
    counter = 0
    items = frappe.db.sql("""SELECT name from `tabItem` where type!='Disabled'""")
    for item in items:
        print item[0]
        item_doc = frappe.get_doc("Item",item[0])
        print item_doc.name, counter + 1
        update_branch_item_price(item_doc)

        counter += 1
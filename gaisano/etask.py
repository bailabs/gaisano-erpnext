import frappe, os, datetime
from tasks import generate_depcode_dbf, generate_item_dbf
from itemevents import update_items_in_new, copy_prices_to_dbf

@frappe.whitelist()
def get_branch():
    branch = frappe.db.get_value("Server Information", None, "Branch")
    return branch

@frappe.whitelist()
def mount_drive(type=None,url=None,credentials_file=None):
    branch = get_branch()
    print "##########MOUNT DRIVE###########"
    mount_action = ""
    mypass = 'qwerty'
    if type=="POS":
        path = "/media/POS_SERVER"
    elif type == "GROCERY":
        path = "/media/GROCERY_SERVER"
    mount_status = os.path.ismount(path)

    print url, credentials_file, path

    if mount_status != True:
        mount_action = "mount"
        if branch == "CDO Main":
            command = "mount -t cifs -o credentials=" + credentials_file + ",vers=1.0,iocharset=utf8,file_mode=0777,dir_mode=0777 " + url + " " + path
        else:
            command = "mount -t cifs -o credentials=" + credentials_file + ",vers=1.0,iocharset=utf8,file_mode=0777,dir_mode=0777 " + url + " " + path
    else:
        mount_action = "unmount"
        command = "umount "+path

    print "MOUNT COMMAND", command
    p = os.system('echo %s|sudo -S %s' % (mypass, command))

    mount_status = os.path.ismount(path)

    if mount_status is False and mount_action=="mount":
        command2 = "mount -t cifs -o credentials=" + credentials_file + ",iocharset=utf8,file_mode=0777,dir_mode=0777 " + url + " " + path
        p = os.system('echo %s|sudo -S %s' % (mypass, command2))
        mount_status = os.path.ismount(path)

    return "CONNECTED" if mount_status is True else "DISCONNECTED"


@frappe.whitelist()
def get_mount_status():
    pos_status = os.path.ismount("/media/POS_SERVER")
    grocery_staus = os.path.ismount("/media/GROCERY_SERVER")
    return {"grocery":grocery_staus, "pos":pos_status}

@frappe.whitelist()
def cp_files():
    dbf_path = frappe.db.get_value("Server Information", None, "pos_dbf_dir")
    print dbf_path, "POS_DBF PATH"
    generate_depcode_dbf()
    generate_item_dbf()
    try:
        #cmd = "cp /home/gaisano1/test_copy.txt /media/POS_SERVER/"+dbf_path+"/test_copy.txt"
        #os.system(cmd)
        backup = "cp /media/POS_SERVER/"+dbf_path+"/ITEM.DBF /media/POS_SERVER/"+dbf_path+"/ITEM_BACKUP.DBF"
        os.system(backup)
        backup = "cp /media/POS_SERVER/"+dbf_path+"/DEP.DBF /media/POS_SERVER/"+dbf_path+"/DEP_BACKUP.DBF"
        os.system(backup)

        branch = get_branch()

        cp_cmd = "cp /media/backup/pos_data/pos_dbf/ITEM.DBF /media/POS_SERVER/"+dbf_path+"/ITEM.DBF"
        os.system(cp_cmd)
        cp_cmd = "cp /media/backup/pos_data/pos_dbf/DEP.DBF /media/POS_SERVER/"+dbf_path+"/DEP.DBF"
        os.system(cp_cmd)
    except:
        print "An Error Occured"
        frappe.msgprint("ERROR ON ITEM DBF COPY")
    else:
        print "CP SUCCESSFUL"
        #rm_item_dbf()

@frappe.whitelist()
def rm_item_dbf():
    dbf_path = frappe.db.get_value("Server Information", None, "pos_ntx_dir")
    print "---------DELETE ITEM DBF --------------"
    cmd = "rm /media/POS_SERVER/"+dbf_path+"/*.NTX"
    #TEST#cmd = "rm -f /media/POS_SERVER/" + dbf_path + "/test_copy.txt"
    os.system(cmd)

@frappe.whitelist()
def copy_grocery_dbf_to_media_backup():
    status = get_mount_status()
    branch = get_branch()
    print status
    if status['grocery'] == True:
        print "grocery_server is mounted"
        dbf_path = frappe.db.get_value("Server Information", None, "grocery_dbf_dir")
        #test
        #cmd = "cp /media/GROCERY_SERVER/" + dbf_path + "/ITEM.DBF /home/gaisano1/gaisano_chaka/hera_copy.dbf"

        cmd = "cp /media/GROCERY_SERVER/" + dbf_path + "/ITEM.DBF /media/backup/pos_data/grocery_dbf/ITEM.dbf"
        #print cmd
        msg = os.system(cmd)
        print "<<<<<<<<<<<DONE COPYING THE THING>>>>>>>>>>>>"
        return msg if msg!=0 else "Item.dbf copied to /media/backup/grocery_dbf!"
    else:
        return "Old System Server not connected!"

@frappe.whitelist()
def copy_prices_to_new_system():
    copy_grocery_dbf_to_media_backup()
    update_items_in_new()
    frappe.msgprint("Items have been updated. To check the items updated, open <a class= 'btn btn-primary' href='/files/items_updated_in_new.csv'>items_updated_in_new.csv</a>")
    return "DONE"

@frappe.whitelist()
def copy_prices_to_old_system():
    copy_start = datetime.datetime.now()
    copy_new_to_old_start = str(copy_start)
    copy_grocery_dbf_to_media_backup()
    copy_prices_to_dbf()
    status = get_mount_status()
    print status
    if status['grocery'] == True:
        branch = get_branch()
        dbf_path = frappe.db.get_value("Server Information", None, "sync_folder_dir")
        #test
        #cmd = "cp /media/GROCERY_SERVER/" + dbf_path + "/ITEM.DBF /home/gaisano1/gaisano_chaka/hera_copy.dbf"

        #cmd1 = "cp /media/GROCERY_SERVER/ITEM.DBF /media/GROCERY_SERVER/ITEM_BACKUP.DBF"

        cmd2 = "cp /media/backup/pos_data/grocery_dbf/ITEM.dbf /media/GROCERY_SERVER/" + dbf_path + "/NEW_ITEM.DBF"
        print cmd2
        #os.system(cmd1) #create item backup
        os.system(cmd2) #copy ITEM.dbf to SYNC folder
        frappe.msgprint("NEW_ITEM DBF has been copied to "+dbf_path+" Folder. To check the items updated, open <a class= 'btn btn-primary' href='/files/items_updated_in_old.csv'>items_updated_in_old.csv</a>")
        copy_finish = datetime.datetime.now()
        copy_new_to_old_finish = str(copy_finish)
        message = "Start " +copy_new_to_old_start + " - " + "Finish " + copy_new_to_old_finish
        frappe.db.set_value("Server Information", None, "time_started_to_time_finished", message)
    else:
        frappe.throw("CAN'T CONNECT TO GROCERY OLD SYSTEM. PLEASE TRY AGAIN.")
    return "DONE"

@frappe.whitelist()
def check_upload_pi(from_date, to_date, branch):
    items = frappe.db.sql("""select count(*), trans_date from `tabUpload POS` where trans_date >= %s and trans_date <= %s  and branch = %s  group by trans_date;""",(from_date, to_date, branch))
    print "SALES|" , "DATE"
    for item in items:
        print item[0], "|",  item[1]

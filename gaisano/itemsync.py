import frappe
from frappe.frappeclient import FrappeClient
from apisync import connect_to_server

####################################
######## SERVER API CALLS ##########
####################################

@frappe.whitelist()
def get_items_and_ids():
    return frappe.db.sql("""SELECT name, barcode_retial, item_id from `tabItem`""")

#get newly updated items:
@frappe.whitelist()
def get_updated_items(date_modified):
    items = frappe.db.sql("""select name, item_cost, item_id, item_freight_peso, item_cost_with_freight,item_cost_without_vat,modified, barcode_retial from `tabItem` where modified >= %s ORDER by modified """, date_modified)
    if len(items) >0:
        return items

@frappe.whitelist()
def get_item_doc(item_id):
    item_name = frappe.db.sql("""SELECT name from `tabItem` where item_id = %s""", item_id)
    if item_name:
        return frappe.get_doc("Item", item_name[0][0])
    else:
        return "ERROR"

######################################
###  GET CLASSIFICATIONS/CATEGORY  ###
######################################
def test_get():
    doctype = "Item Classification"
    main_server = FrappeClient("http://192.168.5.200", "tailerp_api@gaisano.com", 't@1l3rpAPI')
    list = main_server.get_list(doctype)
    print len(list)
    for item in list:
        name = item["name"]
        #print item
        print name
        temp = main_server.get_doc(doctype, name)
        print temp
        this_doc = frappe.get_doc(temp)
        this_doc.insert()
    main_server.logout()

###################################
###### FOR BRANCH EXECUTION #######
###################################

def set_item_ids():
    # main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
    main_server = connect_to_server()
    items = main_server.get_api('gaisano.itemsync.get_items_and_ids')
    frappe.db.sql("""UPDATE `tabItem` set item_id = NULL """)
    frappe.db.commit()
    for item in items:
        print item[0], item[1], item[2]
        frappe.db.sql("""UPDATE `tabItem` set item_id = %s where barcode_retial = %s""",(item[2], item[1]))
    frappe.db.commit()

def test_get_item(item_id):
    # main_server = FrappeClient("http://202.137.117.24", "tailerp_api@gaisano.com", 't@1l3rpAPI')
    main_server = connect_to_server()
    item = main_server.get_api('gaisano.itemsync.get_item_doc', params={"item_id":item_id})
    if item != None:
        item_doc = frappe.get_doc(item)
        print item_doc.name, item_doc.barcode_retial, item_doc.item_id


def test_duplicates():
    items = frappe.db.sql("""SELECT name, item_id from `tabItem`""")
    for item in items:
        print item[0], item[1]
        duplicates = frappe.db.sql("""select count(item_id) from `tabItem` where item_id = %s and name!= %s""", (item[1], item[0]))
        print "Duplicates", duplicates[0][0]
        if duplicates[0][0]>0:
            print duplicates[0][0], "DUPLICATES FOR ", item[0], "| id: ", item[1], "--------------------**"
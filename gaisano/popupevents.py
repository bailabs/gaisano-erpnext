import frappe, json
from frappe.frappeclient import FrappeClient
from gaisano.doctype.advanced_settings.advanced_settings import delete_and_rename
from usevents import check_and_delete_rr, check_and_delete_se, check_delete_rr_hourly, check_delete_se_hourly
import time
from datetime import datetime, date, time, timedelta
from frappe.utils.password import get_decrypted_password, check_password, encrypt, decrypt

@frappe.whitelist()
def remove_po_schedule(supplier_discount):
    schedule = frappe.db.sql("""select supplier_discount from `tabPO Schedule Details` where supplier_discount = %s""", supplier_discount)
    if schedule:
        frappe.db.sql("""delete from `tabPO Schedule Details` where supplier_discount = %s""", supplier_discount)
        frappe.db.commit()

@frappe.whitelist()
def po_schedule_pop_up():
    po_schedule = frappe.db.sql("""select discount_name from `tabPO Schedule Details`""")
    text_popup = "<table class = 'table table-bordered table-hover'>"
    rows = 0
    total_row = 0
    text_popup += "<tr><th>" + "Scheduled PO Today" + "</th></tr>"
    for po_schedules in po_schedule:
        rows += 1
        total_row += 1
        if po_schedule == None:
            break
        if rows != 4:
            text_popup += "<tr><td>" + po_schedules[0] + "</td></tr>"
        else:
            text_popup += "<tr><td>" + " <button><a href = /desk#Form/PO%20Schedule>" + str(len(po_schedule)) + " Suppliers Discount. Click here to see all" + "</a>" "</button> " + "</td></tr> "
            break
    return text_popup


@frappe.whitelist()
def po_scheduling():
    po_schedule = frappe.db.sql("""select name, supplier_discount_name from `tabSupplier Discounts` where po_date = CURDATE()""")
    supplier_disc_list = []
    supplier_name_list = []
    now = datetime.now()
    today = now.strftime('%A')
    x = str(today.lower())

    for po_schedules in po_schedule:
        last_po = frappe.db.sql("""select max(name), max(expected_delivery) from `tabPurchase Order` where supplier_discount = %s""", (po_schedules[0]))
        if (last_po[0][0]):
            expected_delivery = last_po[0][1].strftime('%Y-%m-%d')
            expected_delivery = datetime.strptime(expected_delivery, '%Y-%m-%d')

            today = now.strftime('%Y-%m-%d')
            today = datetime.strptime(today, '%Y-%m-%d')

            if (expected_delivery < today):
                supplier_disc_list.append(po_schedules[0])
                supplier_name_list.append(po_schedules[1])

        po_schedule = frappe.db.sql("""select name, supplier_discount_name from `tabSupplier Discounts` where `%s` = 1""" % x)

    for po_schedules in po_schedule:
        supplier_disc_list.append(po_schedules[0])
        supplier_name_list.append(po_schedules[1])

    for x in  range(len(supplier_disc_list)):
        print supplier_disc_list[x]
        print supplier_name_list[x]
        item_dict = {
            "doctype": "PO Schedule Details",
            "parent": "PO Schedule",
            "parenttype": "PO Schedule",
            "parentfield": "schedule",
            "supplier_discount": supplier_disc_list[x],
            "discount_name": supplier_name_list[x]
        }
        table_row = frappe.get_doc(item_dict)
        table_row.insert()

@frappe.whitelist()
def check_latest_pi_upload():
    today = date.today()
    yesterday = today - timedelta(days=1)
    print today, yesterday
    branch = frappe.db.get_value("Server Information", None, "branch")
    text_popup = "<table class = 'table table-bordered table-hover'>"
    pi_files = frappe.db.sql("""select trans_date from `tabUpload POS` where branch = %s order by trans_date desc limit 1""", branch)
    for pi_file in pi_files:
        trans_date = pi_file[0]
        if trans_date == yesterday:
            print "Updated Pi File"
            msg = ""
        else:
            # text_popup += " <tr><td><div class='alert alert-danger' role='alert'><p>" + str(trans_date) + "</p> </div></td><td> "+ "PI File is outdated." + "</td></tr>"
            msg = "<div class='alert alert-danger' role ='alert'><center> PI-" + str(trans_date) + ". File is outdated. Please contact your POS personnel</center></div>"
    return msg

@frappe.whitelist()
def next_po_date(po_date, frequency):
    date = po_date
    converted_date = datetime.strptime(date, '%Y-%m-%d').date()
    schedule = converted_date.strftime("%A")
    left = ""

    if frequency == "Monthly":
        next_po_date = converted_date + timedelta(days=30)
        if next_po_date.strftime('%A') == 'Sunday':
            next_po_date = next_po_date + timedelta(1)
            print next_po_date.strftime("%A")

    else:
        if frequency == "Weekly":
            days = 7
        elif frequency == "Bi-monthly":
            days = 14

        next_po_date = converted_date + timedelta(days=days)

    return {"next_po_date": next_po_date, "po_schedule": schedule}

#change date
def change_po_schedule():
    next_po = frappe.db.sql("""SELECT name, po_date, frequency, lead_time from `tabSupplier Discounts` where po_date = CURDATE()""")
    for next_pos in next_po:
        po_date = next_pos[1]
        frequency = next_pos[2]
        lead_time = next_pos[3]

        if frequency == "Monthly":
            number_of_days = 30
            po_date = po_date + timedelta(number_of_days)
            if po_date.strftime('%A') == 'Sunday':
                po_date = next_po_date + timedelta(1)
            rr_date = po_date + timedelta(lead_time)
            next_po_date = po_date + timedelta(number_of_days)
            next_rr_date = next_po_date + timedelta(lead_time)
            frappe.db.sql("""UPDATE `tabSupplier Discounts` set
po_date = %s, rr_date = %s, next_po_date = %s, next_rr_date = %s, po_schedule = %s where name = %s""",
                          (po_date, rr_date, next_po_date, next_rr_date, po_date.strftime('%A'),next_pos[0] ))
            frappe.db.commit()

        else:

            if frequency == "Weekly":
                number_of_days = 7

            elif frequency == "Bi-monthly":
                number_of_days = 14

            po_date = po_date + timedelta(number_of_days)
            rr_date = po_date + timedelta(lead_time)
            next_po_date = po_date + timedelta(number_of_days)
            next_rr_date = next_po_date + timedelta(lead_time)

            frappe.db.sql("""UPDATE `tabSupplier Discounts` set po_date = %s, rr_date = %s, next_po_date = %s, next_rr_date = %s where name = %s""", (po_date, rr_date, next_po_date ,next_rr_date,next_pos[0]))
            frappe.db.commit()

@frappe.whitelist()
def auto_po():
    text_popup = "<table class = 'table table-bordered table-hover'>" #<button><a href = /desk#Form/Auto%20PO> Auto Po </a></button>"
    suppliers = frappe.db.sql("""select distinct disc.supplier_name  from `tabDiscounts for Auto Po` auto_po inner join `tabSupplier Discounts` disc on disc.supplier_discount_name = auto_po.supplier_discount_name""",)
    rows = 0
    total_row = 0
    # print supplier
    text_popup += "<tr><th>" + "Supplier below Safety Stock" + "</th></tr>"
    for sup in suppliers:
        rows += 1
        total_row += 1
        if suppliers == None:
            break
        if rows != 4:
            text_popup += "<tr><td>" + sup[0] + "</td></tr>"
        else:
            text_popup += "<tr><td>" + " <button><a href = /desk#Form/Auto%20PO> " +str(len(suppliers)) +" Suppliers. See Supplier Discounts."+" </a> ""</button> " + "</td></tr> "
            break
    return text_popup

@frappe.whitelist()
def check_sles():
    msg = "<h3>RR's With duplicate SLE's</h3><small>Please double check if problem persists by running gaisano.popupevents.check_and_delete_sles()</small>"
    data = []
    today = date.today()
    yesterday = today - timedelta(days=2)
    sles = frappe.db.sql("""SELECT DISTINCT item_code, voucher_no, warehouse, actual_qty from `tabStock Ledger Entry` where voucher_type ='Purchase Receipt' and posting_date >= %s""", yesterday)
    for sle in sles:
        count_sles = frappe.db.sql("""SELECT count(name) from `tabStock Ledger Entry` where item_code = %s and voucher_no = %s and warehouse = %s and actual_qty = %s """, (sle[0], sle[1], sle[2], sle[3]))
        if count_sles[0][0] > 1:
            print "-----------Number of Excess:----------"
            print (count_sles[0][0] - 1), sle[1]
            data.append(sle[1])
    if len(data)>0:
        data=list(set(data))
        for rr in data:
            msg += "<li>" + rr + "</li>"
        send_emails(msg, "RR's With duplicate SLE's")

@frappe.whitelist()
def check_sles_se(days = None):
    days_diff = days if days!=None else 2
    msg = "<h3>SE's With duplicate SLE's</h3>"
    data = []
    today = date.today()
    yesterday = today - timedelta(days=days_diff)
    sles = frappe.db.sql("""SELECT DISTINCT item_code, voucher_no, warehouse, actual_qty from `tabStock Ledger Entry` where voucher_type ='Stock Entry' and posting_date >= %s""", yesterday)
    for sle in sles:
        count_sles = frappe.db.sql("""SELECT count(name) from `tabStock Ledger Entry` where item_code = %s and voucher_no = %s and warehouse = %s and actual_qty = %s """, (sle[0], sle[1], sle[2], sle[3]))
        if count_sles[0][0] > 1:
            print "-----------Number of Excess:----------"
            print (count_sles[0][0] - 1), sle[1]
            data.append(sle[1])
    if len(data)>0:
        data=list(set(data))
        for rr in data:
            msg += "<li>" + rr + "</li>"
        send_emails(msg, "SE's With duplicate SLE's")

@frappe.whitelist()
def send_emails(msg, subject):
    emails = ['hvillanueva.gaisano@gmail.com', 'rdvdeleonio@gmail.com']
    from frappe.desk.page.chat.chat import post
    for email in emails:
        print email
        post(**{"txt": msg, "contact": email, "subject": subject, "notify": 1})

@frappe.whitelist()
def check_and_delete_daily_rr():
    today = date.today()
    yesterday = today - timedelta(days=1)
    today = date.today() + timedelta(days=1)
    check_and_delete_rr(yesterday, today)

@frappe.whitelist()
def check_and_delete_daily_se():
    today = date.today()
    yesterday = today - timedelta(days=1)
    today = date.today() + timedelta(days=1)
    check_and_delete_se(yesterday, today)

@frappe.whitelist()
def check_and_delete_hourly():
    time_start = datetime.now()
    branch = frappe.db.get_value("Server Information", None, "branch")
    now = datetime.now()#.strftime("%H:%M:%S")
    before = datetime.now() - timedelta(hours=1)
    #before = before.strftime("%H:%M:%S")
    print "CHECK AND DELETE RR/SE from ", before, "to", now, branch
    check_delete_rr_hourly(branch,before, now)
    check_delete_se_hourly(branch,before, now)
    time_end = datetime.now()
    time_diff = time_end-time_start

    print "time Start", time_start.strftime("%H:%M:%S")
    print "time End", time_end.strftime("%H:%M:%S")
    print "time Spent", time_diff



@frappe.whitelist()
def test_duplicate_rr(rr_no):
    rr = frappe.get_doc("Purchase Receipt", rr_no)
    duplicate = frappe.copy_doc(rr, False)
    duplicate.branch_code = rr.branch_code
    duplicate.posting_date = rr.posting_date
    duplicate.posting_time = rr.posting_time
    duplicate.name = "TEMPCOPY-"+rr.name
    duplicate.save()
    print duplicate.as_json()
    delete_and_rename("Purchase Receipt", rr.name, duplicate.name)

@frappe.whitelist()
def delete_duplicate_se(se_no):
    from seevents import force_submit
    frappe.db.sql("""delete from `tabStock Ledger Entry` where voucher_no= %s """, se_no)
    frappe.db.commit()
    force_submit(se_no)

@frappe.whitelist()
def delete_duplicate_rr(rr_no):
    from rrevents import update_stock_ledger
    rr = frappe.get_doc("Purchase Receipt", rr_no)
    frappe.db.sql("""DELETE FROM `tabStock Ledger Entry` where voucher_no = %s""", rr_no)
    frappe.db.commit()
    update_stock_ledger(rr)

@frappe.whitelist()
def minute_update_task():
    print "----------***CHECK SLES PER MINUTE TASK***-------------"
    end_time = datetime.datetime.now()
    start_time = end_time - datetime.timedelta(minutes=5)
    print "CHECK SLES from RR/SE from ", str(start_time), " to ", str(end_time)
    check_and_delete_se(start_time, end_time)
    check_and_delete_rr(start_time, end_time)

@frappe.whitelist()
def get_last_item():
    items = frappe.db.sql("""select name, date_modified, modified, item from `tabItem Price History` ORDER by modified desc limit 1""")
    if len(items) >0:
        return items[0]

@frappe.whitelist()
def get_item(date_modified):
    items = frappe.db.sql("""select name, item_cost from `tabItem` where modified <= %s ORDER by modified desc limit 1""", date_modified)
    if len(items) >0:
        return items[0]

@frappe.whitelist()
def get_all_items(date_modified):
    items = frappe.db.sql("""select name, item_cost, barcode_retial, item_freight_peso, item_cost_with_freight,item_cost_without_vat from `tabItem` where modified <= %s ORDER by modified """, date_modified)
    if len(items) >0:
        return items

@frappe.whitelist()
def last_sync():
    sync_status = frappe.db.get_value("Server Information", None, "sync_status")
    last_sync = frappe.db.get_value("Server Information", None, "last_sync")
    print last_sync
    msg = ""
    branch = frappe.db.get_value("Server Information", None, "branch") if frappe.db.get_value("Server Information", None, "branch") else ""
    error_message = "<br><br><a class = \"btn btn-danger\" href =\"{0}\">{1} </a> </center>".format(("http://"+my_ip()+"/desk#Form/Server Information"),"Go To Server Information Page")
    if branch != "CDO Main":
        if sync_status== "Syncing":
            msg = "<center>Sync is working. :)<br>LAST SYNC:"+last_sync+"</center>"
        elif sync_status== "Sync Not Working" or "Sync Not Started" or "Synced but with Errors":
            msg = "<center>Branch " + branch + " Sync is not working. <br><br>LAST SYNC ATTEMPTED ON:"+last_sync+"<br>Please Check the Server Information Page for more info, or call the IT team @09171309981. " + error_message
        elif sync_status== "CDO Main Sever Cannot be Reached.":
            msg = "<center> CDO Main Server cannot be reached. Either branch "+branch+" internet is gone or there is no internet Connection @ Gaisano CDO. " + error_message
        else:
            msg = "<center>Branch "+branch+" Sync is behaving weirdly... Please Check the Server Information Page for more info, or call the IT team @09171309981."+error_message
    return msg

def send_sync_emails():
    msg = last_sync()
    if (msg != "") and (msg!= "<center>Sync is working. :)</center>"):
        send_emails(msg, "BRANCHES SYNC")

def check_if_main():
    if my_ip()!= "192.168.5.200":
        print "Branches"
        return True
    else:
        return False

def my_ip():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    this_ip = (s.getsockname()[0])
    s.close()
    return this_ip

@frappe.whitelist()
def get_report_password(entered_password):
    password = get_decrypted_password("Advanced Settings", "Advanced Settings", 'report_password')
    if entered_password == password:
        return True
    else:
        return False

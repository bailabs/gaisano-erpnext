#!/usr/bin/env bash
cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/buying_controller.py /home/frappe/frappe-v7/apps/erpnext/erpnext/controllers/buying_controller.py

cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/purchase_receipt.py /home/frappe/frappe-v7/apps/erpnext/erpnext/stock/doctype/purchase_receipt/purchase_receipt.py

cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/item_dashboard.py /home/frappe/frappe-v7/apps/erpnext/erpnext/stock/doctype/item/item_dashboard.py

cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/stock_item_dashboard.py /home/frappe/frappe-v7/apps/erpnext/erpnext/stock/dashboard/item_dashboard.py

cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/item.json /home/frappe/frappe-v7/apps/erpnext/erpnext/stock/doctype/item/item.json

cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/stock_ledger_entry.json /home/frappe/frappe-v7/apps/erpnext/erpnext/stock/doctype/stock_ledger_entry/stock_ledger_entry.json

cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/stock_entry.py /home/frappe/frappe-v7/apps/erpnext/erpnext/stock/doctype/stock_entry/stock_entry.py

cp /home/frappe/frappe-v7/apps/gaisano/gaisano/core_changes/stock_ledger.py /home/frappe/frappe-v7/apps/erpnext/erpnext/stock/stock_ledger.py